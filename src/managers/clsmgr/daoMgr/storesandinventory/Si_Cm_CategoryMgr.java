package managers.clsmgr.daoMgr.storesandinventory;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.storesandinventory.Si_Dm_Category;

public class Si_Cm_CategoryMgr {
    
    //  method to insert StockCategory in database
    public boolean insSi_Category(Si_Dm_Category si_category) {
        try {          
            Config.sql = "insert into config_si_category ("
                    + "categoryname) "                               
                    + "values (?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, si_category.getCategoryname());                     
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_si_categorymgr.load_Si_Category();
                Config.si_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    //method to Update Stockcategory in database 
    public boolean updSi_Category (Si_Dm_Category  si_category) {
        try {
            Config.sql = "update config_si_category set "
                   + "categoryname = ? "                   
                    + " where category_id = '"+si_category.getCategory_id()+"'";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
              Config.pstmt.setString(1, si_category.getCategoryname()); 
              int x = Config.pstmt.executeUpdate();
            if (x>0){
                  Config.xml_si_categorymgr.load_Si_Category();
                Config.si_home.onloadReset();
            return true;
            }else{
            return false;   
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
    
    //method to delete Stockcategory in database
    public boolean delStockCategory(String category_id) {
        try {          
            Config.sql = "delete from config_si_Category where category_id = '"+category_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                      Config.xml_si_categorymgr.load_Si_Category();
                Config.si_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
