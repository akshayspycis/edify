package managers.clsmgr.daoMgr.storesandinventory;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.storesandinventory.Si_Dm_SupplierProfile;

public class Si_Cm_SupplierProfileMgr {
    
    //method to insert SupplierProfile in database
    public boolean insSi_SupplierProfile(Si_Dm_SupplierProfile si_supplierprofile) {
        try {          
            Config.sql = "insert into config_si_supplierprofile ("
                     + "orgname, "
                     + "tinno,"
                     + "name, "
                     + "contactperson, "
                     + "emailid, "
                     + "contactno, "
                     + "country, "
                     + "state, "
                     + "city, "
                     + "locality,"                   
                     + "zippostal, "            
                     + "website, "
                     + "faxno, "
                     + "orgcontact) "                 
              
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
                    Config.pstmt = Config.conn.prepareStatement(Config.sql);
                    Config.pstmt.setString(1, si_supplierprofile.getOrgname());
                    Config.pstmt.setString(2, si_supplierprofile.getTinno()); 
                    Config.pstmt.setString(3, si_supplierprofile.getName()); 
                    Config.pstmt.setString(4, si_supplierprofile.getContactperson()); 
                    Config.pstmt.setString(5, si_supplierprofile.getEmailid());
                    Config.pstmt.setString(6, si_supplierprofile.getContactno()); 
                    Config.pstmt.setString(7, si_supplierprofile.getCountry());
                    Config.pstmt.setString(8, si_supplierprofile.getState());                          
                    Config.pstmt.setString(9, si_supplierprofile.getCity());
                    Config.pstmt.setString(10, si_supplierprofile.getLocality());
                    Config.pstmt.setString(11, si_supplierprofile.getZippostal());               
                    Config.pstmt.setString(12, si_supplierprofile.getWebsite());
                    Config.pstmt.setString(13, si_supplierprofile.getFaxno());
                    Config.pstmt.setString(14, si_supplierprofile.getOrgcontact());
            int x = Config.pstmt.executeUpdate();            
            if(x>0){
                Config.xml_supplierprofilemgr.load_SupplierProfile();
                Config.si_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    //method to supplierprofile in database
    public boolean updSi_SupplierProfile(Si_Dm_SupplierProfile si_supplierprofile) {
        try {
            Config.sql = "update config_si_supplierprofile set "
                     + "orgname = ?, "
                     + "tinno = ?, "
                     + "name = ?, "
                     + "contactperson = ?, "
                     + "emailid = ?, "
                     + "contactno = ?, "
                     + "country = ?, "
                     + "state = ?, "
                     + "city = ?, "
                     + "locality = ?, "                   
                     + "zippostal = ?, "            
                     + "website = ?, "
                     + "faxno = ?, "
                     + "orgcontact = ? "      
                     + "where supplier_id = '"+si_supplierprofile.getSupplier_id()+"'";            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, si_supplierprofile.getOrgname());
                        Config.pstmt.setString(2, si_supplierprofile.getTinno()); 
                        Config.pstmt.setString(3, si_supplierprofile.getName()); 
                        Config.pstmt.setString(4, si_supplierprofile.getContactperson()); 
                        Config.pstmt.setString(5, si_supplierprofile.getEmailid());
                        Config.pstmt.setString(6, si_supplierprofile.getContactno());
                        Config.pstmt.setString(7, si_supplierprofile.getCountry());
                        Config.pstmt.setString(8, si_supplierprofile.getState());                        
                        Config.pstmt.setString(9, si_supplierprofile.getCity());
                        Config.pstmt.setString(10, si_supplierprofile.getLocality());
                        Config.pstmt.setString(11, si_supplierprofile.getZippostal());          
                        Config.pstmt.setString(12, si_supplierprofile.getWebsite());
                        Config.pstmt.setString(13, si_supplierprofile.getFaxno());
                        Config.pstmt.setString(14, si_supplierprofile.getOrgcontact());                        
            int x = Config.pstmt.executeUpdate();
            if (x>0) { 
                Config.xml_supplierprofilemgr.load_SupplierProfile();
                Config.si_home.onloadReset();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
   
    //method to delete agreementid in database
    public boolean delSi_SupplierProfile(String supplier_id) {
       try {
            Config.sql = "delete from config_si_supplierprofile where supplier_id = '"+supplier_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_supplierprofilemgr.load_SupplierProfile();
//                Config.configmgr.loadSi_SupplierProfile();
                Config.si_home.onloadReset();
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
