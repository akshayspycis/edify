package managers.clsmgr.daoMgr.storesandinventory;

import java.util.ArrayList;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.storesandinventory.Si_Dm_SubCategory;

public class Si_Cm_SubCategoryMgr {

    //  method to insert StockSubcategory in database
    public boolean insStockSubCategory(ArrayList<Si_Dm_SubCategory> si_subcategory) {
        try {          
            Config.sql = "insert into config_si_subcategory ("
                    + "category_id, "
                    + "subcategoryname) "               
                    + "values (?,?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int i =0;
            for (i = 0; i < si_subcategory.size(); i++) {
            Config.pstmt.setString(1, si_subcategory.get(i).getCategory_id());           
            Config.pstmt.setString(2, si_subcategory.get(i).getSubcategoryname() );
            
            Config.pstmt.addBatch();
            }
            int[] result_array = Config.pstmt.executeBatch();
            
            if( result_array.length==i){
//                  Config.configmgr.loadSi_SubCategogy();
                  Config.si_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    //method to StockSubcategory in database 
    public boolean updsSiSubCategory (Si_Dm_SubCategory  si_subcategory) {
        try {
            Config.sql = "update config_si_subcategory set "                   
                    + " category_id = ?," 
                    + "subcategoryname = ? "
                    + " where subcategory_id ='"+si_subcategory.getSubcategory_id()+"'";
            System.out.println(Config.sql);
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.setString(1, si_subcategory.getCategory_id());   
                Config.pstmt.setString(2, si_subcategory.getSubcategoryname());
                           
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                  Config.si_home.onloadReset();
            return true;
            }
            else
            {
            return false;   
            }
        } 
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    } 
    
    //method to delete StockSubcategory in database
    public boolean delStockSubCategory(String subcategory_id) {
        try {          
            Config.sql = "delete from config_si_SubCategory where subcategory_id = '"+subcategory_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                  Config.si_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
