package managers.clsmgr.daoMgr.transport;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.transport.Config_Tr_Dm_VehicleDetail;

public class Config_Tr_Cm_VehicleDetailMgr {
    
    public boolean insTr_Vehicledetail(Config_Tr_Dm_VehicleDetail tr_dm_vehicledetail){
        try {
            Config.sql = "insert into config_tr_vehicledetail( "
                    + "vehiclename, "
                    + "vehicleno, "
                    + "vehicleseatcapacity ) "
                    + "values(?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, tr_dm_vehicledetail.getVehiclename());
            Config.pstmt.setString(2, tr_dm_vehicledetail.getVehicleno());
            Config.pstmt.setString(3, tr_dm_vehicledetail.getVehicleseatcapacity());
            
            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_config_tr_vehicledetailmgr.load_VehicleDetail();
                Config.tr_home.onloadReset();
                return true;
            } else {
                return false;   
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        
        
    } 
     public boolean upd_Vehicledetail(Config_Tr_Dm_VehicleDetail tr_dm_vedetail){
         try {
             Config.sql = "update config_exam_grade set "
                     + "vehiclename = ?, "
                     + "vehicleno = ?, "
                     + "vehicleseatcapacity = ? "
                     + "where vehicle_id = '"+tr_dm_vedetail.getVehicle_id()+"'";
             Config.pstmt = Config.conn.prepareStatement(Config.sql);            
             Config.pstmt.setString(1, tr_dm_vedetail.getVehicleno());
             Config.pstmt.setString(2, tr_dm_vedetail.getVehiclename());
             Config.pstmt.setString(3, tr_dm_vedetail.getVehicleseatcapacity());
             
             int x = Config.pstmt.executeUpdate();
             if(x>0) {
                Config.xml_config_tr_vehicledetailmgr.load_VehicleDetail();
                Config.tr_home.onloadReset();
                return true;
            } else{
                return false;   
            }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
         
     }
     public boolean del_Grade(String gradeid){
         try {
             Config.sql = "delete from config_exam_grade where grade_id = '"+gradeid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
             if (true) {
                  Config.xml_config_examgrademgr.load_ExamGrade();
                   Config.exam_home.onloadReset();
                 return true;
             }else{
                 return false;
             }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
     }
    
    

}
