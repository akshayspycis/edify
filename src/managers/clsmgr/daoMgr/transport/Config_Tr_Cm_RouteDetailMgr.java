package managers.clsmgr.daoMgr.transport;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.transport.Config_Tr_Dm_RouteDetail;


public class Config_Tr_Cm_RouteDetailMgr {
    
    public boolean insTr_RouteDetail(Config_Tr_Dm_RouteDetail tr_dm_routedetail){
        try {
            Config.sql = "insert into config_tr_routedetail("
                    + "vehicle_id, "
                    + "areaname) "
                    + "values(?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, tr_dm_routedetail.getVehicle_id());
            Config.pstmt.setString(2, tr_dm_routedetail.getAreaname());
            
            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_config_tr_vehicledetailmgr.load_VehicleDetail();
                Config.tr_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            
            return false;
        }
     }
       
}
