package managers.clsmgr.daoMgr.transport;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.transport.Config_Tr_Dm_StopDetail;


public class Config_Tr_Cm_StopDetailMgr {
    
    public boolean insTr_StopDetail(Config_Tr_Dm_StopDetail tr_dm_stopdetail){
        
        try {
            Config.sql = "insert into config_tr_stopdetail ("
                    + "route_id, "
                    + "stopname, "
                    + "pickuptime, "
                    + "droptime ) "
                    + "values(?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, tr_dm_stopdetail.getRoute_id());
            Config.pstmt.setString(2, tr_dm_stopdetail.getStopname());
            Config.pstmt.setString(3, tr_dm_stopdetail.getPickuptime());
            Config.pstmt.setString(4, tr_dm_stopdetail.getDroptime());
            
            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_config_tr_stopdetailmgr.load_StopDetail();
                Config.tr_home.onloadReset();
                return true;
            } else {
                return false;   
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
     
        
    }
    
}
