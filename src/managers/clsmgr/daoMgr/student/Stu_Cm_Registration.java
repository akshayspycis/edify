/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managers.clsmgr.daoMgr.student;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.student.Stu_Dm_Registration;

/**
 *
 * @author Ashok
 */
public class Stu_Cm_Registration {
    
     public boolean insStu_Registration(Stu_Dm_Registration stu_dm_registration){
        try {
            Config.sql = "insert into stu_registration( "
                    + "form_no, "
                    + "date, "
                    + "firstname, "
                    + "middlename, "
                    + "lastname, "
                    + "fathername, "
                    + "mothername, "
                    + "dob, "
                    + "gender, "
                    + "class_id, "
                    + "category, "
                    + "religion, "
                    + "nationality, "
                    + "fatherqualification, "
                    + "fatheroccupation, "
                    + "fatherincome, "
                    + "f_emailid, "
                    + "motherqualification, "
                    + "motheroccupation, "
                    + "motherincome, "
                    + "m_emailid, "
                    + "r_address, "
                    + "r_city, "
                    + "r_state, "
                    + "r_country, "
                    + "r_pincode, "
                    + "r_phoneno, "
                    + "father_mobno, "
                    + "mother_mobno, "
                    + "p_address, "
                    + "p_city, "
                    + "p_state, "
                    + "p_country, "
                    + "p_pincode, "
                    + "p_phoneno, "
                    + "pre_class, "
                    + "pre_year, "
                    + "pre_result, "
                    + "pre_percentage, "
                    + "pre_schoolname, "
                    + "pre_boardname, "
                    + "bloodgroup, "
                    + "height, "
                    + "weight, "
                    + "handicapped_status, "
                    + "handicapped_description, "
                    + "session_id) "
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, stu_dm_registration.getForm_no());
            Config.pstmt.setString(2, stu_dm_registration.getDate());
            Config.pstmt.setString(3, stu_dm_registration.getFirstname());
            Config.pstmt.setString(4, stu_dm_registration.getMiddlename());
            Config.pstmt.setString(5, stu_dm_registration.getLastname());
            Config.pstmt.setString(6, stu_dm_registration.getFathername());
            Config.pstmt.setString(7, stu_dm_registration.getMothername());
            Config.pstmt.setString(8, stu_dm_registration.getDob());
            Config.pstmt.setString(9, stu_dm_registration.getGender());
            Config.pstmt.setString(10, stu_dm_registration.getClass_id());
            Config.pstmt.setString(11, stu_dm_registration.getCategory());
            Config.pstmt.setString(12, stu_dm_registration.getReligion());
            Config.pstmt.setString(13, stu_dm_registration.getNationality());
            Config.pstmt.setString(14, stu_dm_registration.getFatherqualification());
            Config.pstmt.setString(15, stu_dm_registration.getFatheroccupation());
            Config.pstmt.setString(16, stu_dm_registration.getFatherincome());
            Config.pstmt.setString(17, stu_dm_registration.getF_emailid());
            Config.pstmt.setString(18, stu_dm_registration.getMotherqualification());
            Config.pstmt.setString(19, stu_dm_registration.getMotheroccupation());
            Config.pstmt.setString(20, stu_dm_registration.getMotherincome());
            Config.pstmt.setString(21, stu_dm_registration.getM_emailid());
            Config.pstmt.setString(22, stu_dm_registration.getR_address());
            Config.pstmt.setString(23, stu_dm_registration.getR_city());
            Config.pstmt.setString(24, stu_dm_registration.getR_state());
            Config.pstmt.setString(25, stu_dm_registration.getR_country());
            Config.pstmt.setString(26, stu_dm_registration.getR_pincode());
            Config.pstmt.setString(27, stu_dm_registration.getR_phoneno());
            Config.pstmt.setString(28, stu_dm_registration.getFather_mobno());
            Config.pstmt.setString(29, stu_dm_registration.getMother_mobno());
            Config.pstmt.setString(30, stu_dm_registration.getP_address());
            Config.pstmt.setString(31, stu_dm_registration.getP_city());
            Config.pstmt.setString(32, stu_dm_registration.getP_state());
            Config.pstmt.setString(33, stu_dm_registration.getP_country());
            Config.pstmt.setString(34, stu_dm_registration.getP_pincode());
            Config.pstmt.setString(35, stu_dm_registration.getP_phoneno());
            Config.pstmt.setString(36, stu_dm_registration.getPre_class());
            Config.pstmt.setString(37, stu_dm_registration.getPre_year());
            Config.pstmt.setString(38, stu_dm_registration.getPre_result());
            Config.pstmt.setString(39, stu_dm_registration.getPre_percentage());
            Config.pstmt.setString(40, stu_dm_registration.getPre_schoolname());
            Config.pstmt.setString(41, stu_dm_registration.getPre_boardname());
            Config.pstmt.setString(42, stu_dm_registration.getBloodgroup());
            Config.pstmt.setString(43, stu_dm_registration.getHeight());
            Config.pstmt.setString(44, stu_dm_registration.getWeight());
            Config.pstmt.setString(45, stu_dm_registration.getHandicapped_status());
            Config.pstmt.setString(46, stu_dm_registration.getHandicapped_description());
            Config.pstmt.setString(47, stu_dm_registration.getSession_id());
            
            
            
            
            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_stu_registrationmgr.load_StuRegistration();
                Config.student_registration.onloadReset();
                return true;
            } else {
                return false;   
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        
        
    } 
     public boolean upd_StudentRegistration(Stu_Dm_Registration stu_dm_registration){
         try {
             Config.sql = "update stu_registration set "
                     + "form_no = ? ,"
                    + "date = ? ,"
                    + "firstname = ? ,"
                    + "middlename = ? ,"
                    + "lastname = ? ,"
                    + "fathername = ? ,"
                    + "mothername = ?, "
                    + "dob = ? ,"
                    + "gender = ?,"
                    + "class_id = ? ,"
                    + "category = ? ,"
                    + "religion = ? ,"
                    + "nationality = ? ,"
                    + "fatherqualification = ?,"
                    + "fatheroccupation = ? ,"
                    + "fatherincome = ? ,"
                    + "f_emailid = ? ,"
                    + "motherqualification = ? ,"
                    + "motheroccupation = ? ,"
                    + "motherincome = ?,"
                    + "m_emailid = ? ,"
                    + "r_address = ? ,"
                    + "r_city = ? ,"
                    + "r_state = ?, "
                    + "r_country = ? ,"
                    + "r_pincode = ? ,"
                    + "r_phoneno = ? ,"
                    + "father_mobno = ?  ,"
                    + "mother_mobno = ? ,"
                    + "p_address = ? ,"
                    + "p_city = ? ,"
                    + "p_state = ? ,"
                    + "p_country = ? ,"
                    + "p_pincode = ? ,"
                    + "p_phoneno = ?, "
                    + "pre_class = ? ,"
                    + "pre_year = ?,"
                    + "pre_result = ?,"
                    + "pre_percentage = ? ,"
                    + "pre_schoolname = ? ,"
                    + "pre_boardname = ? ,"
                    + "bloodgroup = ? ,"
                    + "height = ? ,"
                    + "weight = ? ,"
                    + "handicapped_status = ? ,"
                    + "handicapped_description = ? "
                    + "session_id = ? "                     
                    + "where registration_id = '"+stu_dm_registration.getRegistration_id()+"'";
             
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
             
            Config.pstmt.setString(1, stu_dm_registration.getForm_no());
            Config.pstmt.setString(2, stu_dm_registration.getDate());
            Config.pstmt.setString(3, stu_dm_registration.getFirstname());
            Config.pstmt.setString(4, stu_dm_registration.getMiddlename());
            Config.pstmt.setString(5, stu_dm_registration.getLastname());
            Config.pstmt.setString(6, stu_dm_registration.getFathername());
            Config.pstmt.setString(7, stu_dm_registration.getMothername());
            Config.pstmt.setString(8, stu_dm_registration.getDob());
            Config.pstmt.setString(9, stu_dm_registration.getGender());
            Config.pstmt.setString(10, stu_dm_registration.getClass_id());
            Config.pstmt.setString(11, stu_dm_registration.getCategory());
            Config.pstmt.setString(12, stu_dm_registration.getReligion());
            Config.pstmt.setString(13, stu_dm_registration.getNationality());
            Config.pstmt.setString(14, stu_dm_registration.getFatherqualification());
            Config.pstmt.setString(15, stu_dm_registration.getFatheroccupation());
            Config.pstmt.setString(16, stu_dm_registration.getFatherincome());
            Config.pstmt.setString(17, stu_dm_registration.getF_emailid());
            Config.pstmt.setString(18, stu_dm_registration.getMotherqualification());
            Config.pstmt.setString(19, stu_dm_registration.getMotheroccupation());
            Config.pstmt.setString(20, stu_dm_registration.getMotherincome());
            Config.pstmt.setString(21, stu_dm_registration.getM_emailid());
            Config.pstmt.setString(22, stu_dm_registration.getR_address());
            Config.pstmt.setString(23, stu_dm_registration.getR_city());
            Config.pstmt.setString(24, stu_dm_registration.getR_state());
            Config.pstmt.setString(25, stu_dm_registration.getR_country());
            Config.pstmt.setString(26, stu_dm_registration.getR_pincode());
            Config.pstmt.setString(27, stu_dm_registration.getR_phoneno());
            Config.pstmt.setString(28, stu_dm_registration.getFather_mobno());
            Config.pstmt.setString(29, stu_dm_registration.getMother_mobno());
            Config.pstmt.setString(30, stu_dm_registration.getP_address());
            Config.pstmt.setString(31, stu_dm_registration.getP_city());
            Config.pstmt.setString(32, stu_dm_registration.getP_state());
            Config.pstmt.setString(33, stu_dm_registration.getP_country());
            Config.pstmt.setString(34, stu_dm_registration.getP_pincode());
            Config.pstmt.setString(35, stu_dm_registration.getP_phoneno());
            Config.pstmt.setString(36, stu_dm_registration.getPre_class());
            Config.pstmt.setString(37, stu_dm_registration.getPre_year());
            Config.pstmt.setString(38, stu_dm_registration.getPre_result());
            Config.pstmt.setString(39, stu_dm_registration.getPre_percentage());
            Config.pstmt.setString(40, stu_dm_registration.getPre_schoolname());
            Config.pstmt.setString(41, stu_dm_registration.getPre_boardname());
            Config.pstmt.setString(42, stu_dm_registration.getBloodgroup());
            Config.pstmt.setString(43, stu_dm_registration.getHeight());
            Config.pstmt.setString(44, stu_dm_registration.getWeight());
            Config.pstmt.setString(45, stu_dm_registration.getHandicapped_status());
            Config.pstmt.setString(46, stu_dm_registration.getHandicapped_description());
            Config.pstmt.setString(47, stu_dm_registration.getSession_id());
             
             int x = Config.pstmt.executeUpdate();
             if(x>0) {
                Config.xml_stu_registrationmgr.load_StuRegistration();
                Config.stumgmt_home.onloadReset();
                return true;
            } else{
                return false;   
            }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
         
     }
     public boolean del_StudentRegistration(String registrationid){
         try {
             Config.sql = "delete from stu_registration where registration_id = '"+registrationid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
             if (true) {
                  Config.xml_stu_registrationmgr.load_StuRegistration();
                   Config.stumgmt_home.onloadReset();
                 return true;
             }else{
                 return false;
             }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
     }
    
}
