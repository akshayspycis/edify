package managers.clsmgr.daoMgr.student;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.student.Stu_Dm_Admission;

/**
 *
 * @author Ashok
 */
public class Stu_Cm_Admission {
    
     public boolean insStu_Addmission(Stu_Dm_Admission stu_dm_admission){
        try {
            Config.sql = "insert into stu_admission("
                    + "registration_id, "
                    + "addmission_date, "
                    + "ssm_id, "
                    + "fm_id, "
                    + "aadharcard_no, "
                    + "subject_id, "
                    + "bus, "
                    + "hostal, "
                    + "tc, "
                    + "birth, "
                    + "domicile, "
                    + "cast, "
                    + "income, "
                    + "guar_name, "
                    + "relation, "
                    + "address, "
                    + "mob_no, "
                    + "phone_no, "
                    + "email_id, "
                    + "session, "
                    + "tcno, "
                    + "ccno, "
                    + "birthno, "
                    + "domicileno, "
                    + "casteno, "
                    + "incomeno, "
                    + "section_id, "                    
                    + "route_id )"
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, stu_dm_admission.getRegistration_id());
            Config.pstmt.setString(2, stu_dm_admission.getAddmission_date());
            Config.pstmt.setString(3, stu_dm_admission.getSsm_id());
            Config.pstmt.setString(4, stu_dm_admission.getFm_id());
            Config.pstmt.setString(5, stu_dm_admission.getAadharcard_no());
            Config.pstmt.setString(6, stu_dm_admission.getSubject_id());
            Config.pstmt.setString(7, stu_dm_admission.getBus());
            Config.pstmt.setString(8, stu_dm_admission.getHostal());
            Config.pstmt.setString(9, stu_dm_admission.getTc());
            Config.pstmt.setString(10, stu_dm_admission.getBirth());
            Config.pstmt.setString(11, stu_dm_admission.getDomicile());
            Config.pstmt.setString(12, stu_dm_admission.getCast());
            Config.pstmt.setString(13, stu_dm_admission.getIncome());
            Config.pstmt.setString(14, stu_dm_admission.getGuar_name());
            Config.pstmt.setString(15, stu_dm_admission.getRelation());
            Config.pstmt.setString(16, stu_dm_admission.getAddress());
            Config.pstmt.setString(17, stu_dm_admission.getMob_no());
            Config.pstmt.setString(18, stu_dm_admission.getPhone_no());
            Config.pstmt.setString(19, stu_dm_admission.getEmail_id());
            Config.pstmt.setString(20, stu_dm_admission.getSession());
            Config.pstmt.setString(21, stu_dm_admission.getTcno());
            Config.pstmt.setString(22, stu_dm_admission.getCcno());
            Config.pstmt.setString(23, stu_dm_admission.getBirthno());
            Config.pstmt.setString(24, stu_dm_admission.getDomicileno());
            Config.pstmt.setString(25, stu_dm_admission.getCasteno());
            Config.pstmt.setString(26, stu_dm_admission.getIncomeno());
            Config.pstmt.setString(27, stu_dm_admission.getSection_id());
            Config.pstmt.setString(28, stu_dm_admission.getRoute_id());
            
            
            
            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_stu_addmissionmgr.load_StuAddmission();
                Config.stumgmt_home.onloadReset();
                return true;
            } else {
                return false;   
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
    } 
     
     public boolean updStu_Addmission(Stu_Dm_Admission stu_dm_admission){
         try {
             Config.sql = "update stu_admission set "
                     
                     + "registration_id = ? , "
                    + "addmission_date = ? ,"
                    + "ssm_id = ? ,"
                    + "fm_id = ? ,"
                    + "aadharcard_no = ? ,"
                    + "subject_id = ? ,"
                    + "bus = ? ,"
                    + "hostal = ? ,"
                    + "tc = ? ,"
                    + "birth =? ,"
                    + "domicile = ? ,"
                    + "cast = ? ,"
                    + "income = ? ,"
                    + "guar_name = ? ,"
                    + "relation = ? ,"
                    + "address = ? ,"
                    + "mob_no = ? , "
                    + "phone_no = ? ,"
                    + "email_id = ? ,"
                    + "session = ? ,"
                    + "tcno = ? ,"
                    + "ccno = ? ,"
                    + "birthno = ? ,"
                    + "domicileno = ? ,"
                    + "casteno = ? ,"
                    + "incomeno = ? ,"
                    + "section_id = ? ,"                    
                    + "route_id = ? " 
                    + "where admission_id = '"+stu_dm_admission.getAdmission_id()+"'";
             Config.pstmt = Config.conn.prepareStatement(Config.sql);            
             Config.pstmt.setString(1, stu_dm_admission.getRegistration_id());
            Config.pstmt.setString(2, stu_dm_admission.getAddmission_date());
            Config.pstmt.setString(3, stu_dm_admission.getSsm_id());
            Config.pstmt.setString(4, stu_dm_admission.getFm_id());
            Config.pstmt.setString(5, stu_dm_admission.getAadharcard_no());
            Config.pstmt.setString(6, stu_dm_admission.getSubject_id());
            Config.pstmt.setString(7, stu_dm_admission.getBus());
            Config.pstmt.setString(8, stu_dm_admission.getHostal());
            Config.pstmt.setString(9, stu_dm_admission.getTc());
            Config.pstmt.setString(10, stu_dm_admission.getBirth());
            Config.pstmt.setString(11, stu_dm_admission.getDomicile());
            Config.pstmt.setString(12, stu_dm_admission.getCast());
            Config.pstmt.setString(13, stu_dm_admission.getIncome());
            Config.pstmt.setString(14, stu_dm_admission.getGuar_name());
            Config.pstmt.setString(15, stu_dm_admission.getRelation());
            Config.pstmt.setString(16, stu_dm_admission.getAddress());
            Config.pstmt.setString(17, stu_dm_admission.getMob_no());
            Config.pstmt.setString(18, stu_dm_admission.getPhone_no());
            Config.pstmt.setString(19, stu_dm_admission.getEmail_id());
            Config.pstmt.setString(20, stu_dm_admission.getSession());
            Config.pstmt.setString(21, stu_dm_admission.getTcno());
            Config.pstmt.setString(22, stu_dm_admission.getCcno());
            Config.pstmt.setString(23, stu_dm_admission.getBirthno());
            Config.pstmt.setString(24, stu_dm_admission.getDomicileno());
            Config.pstmt.setString(25, stu_dm_admission.getCasteno());
            Config.pstmt.setString(26, stu_dm_admission.getIncomeno());
            Config.pstmt.setString(27, stu_dm_admission.getSection_id());
            Config.pstmt.setString(28, stu_dm_admission.getRoute_id());
             
             
             int x = Config.pstmt.executeUpdate();
             if(x>0) {
                Config.xml_stu_addmissionmgr.load_StuAddmission();
                Config.stumgmt_home.onloadReset();
                return true;
            } else{
                return false;   
            }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
         
     }
     public boolean delStu_Addmission(String admisonid){
         try {
             Config.sql = "delete from stu_admission where admission_id = '"+admisonid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
             if (true) {
                  Config.xml_stu_addmissionmgr.load_StuAddmission();
                   Config.stumgmt_home.onloadReset();
                 return true;
             }else{
                 return false;
             }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
     }
    
}
