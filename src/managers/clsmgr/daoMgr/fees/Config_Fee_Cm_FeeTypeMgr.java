package managers.clsmgr.daoMgr.fees;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.fees.Config_Fee_Dm_FeeType;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Cm_FeeTypeMgr {
    public boolean ins_FeeType(Config_Fee_Dm_FeeType fee_type){
        try {
            Config.sql = "insert into config_fee_feetype("
                    + "session_id,"
                    + "typename)"
                    + "values(?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, fee_type.getSession_id());
            Config.pstmt.setString(2, fee_type.getTypename());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                
                 Config.xml_config_fee_feetypemgr.load_FeeType();
                 Config.fee_home1.onloadReset();
                 
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        
        
//------------------------------------------------------------------------------
        
    }
    public boolean upd_Type(Config_Fee_Dm_FeeType fee_dm_type) {
        try {
            Config.sql = "update config_fee_feetype set "
                    + "session_id = ?, "
                    + "typename = ? "
                    + "where feetype_id = '"+fee_dm_type.getFeetype_id()+"'";
            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);            
                        Config.pstmt.setString(1, fee_dm_type.getSession_id());
                        Config.pstmt.setString(2, fee_dm_type.getTypename());
            
                        int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                  Config.xml_config_fee_feetypemgr.load_FeeType();
                  Config.fee_home1.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean del_Type(String feeid) {
        try {          
            Config.sql = "delete from config_fee_feetype where feetype_id = '"+feeid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                 Config.xml_config_fee_feetypemgr.load_FeeType();
                    Config.fee_home1.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
