package managers.clsmgr.daoMgr.fees;

import java.util.ArrayList;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.fees.Fee_Dm_FeeCollection;

/**
 *
 * @author Ashok
 */
public class Fee_Cm_FeeCollection {
    
     public boolean ins_collection(ArrayList<Fee_Dm_FeeCollection> feecollection){
        try {
            Config.sql = "insert into fee_collection("
                    + "registration_id, "
                    + "session_id, "
                    + "class_id, "
                    + "feetype_id, "
                    + "feeamount_id, "
                    + "date, "
                    + "modepay, "
                    + "bankname, "
                    + "cheqdate, "
                    + "cheqno, "
                    + "totalamount, "
                    + "discount, "
                    + "netamount, "
                    + "payable, "
                    + "balance, "
                    + "recepitno)"
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int i=0;
            for (i = 0; i < feecollection.size(); i++) {
               
            Config.pstmt.setString(1, feecollection.get(i).getRegistration_id());
            Config.pstmt.setString(2, feecollection.get(i).getSession_id());
            Config.pstmt.setString(3, feecollection.get(i).getClass_id());
            Config.pstmt.setString(4, feecollection.get(i).getFeetype_id());
            Config.pstmt.setString(5, feecollection.get(i).getFeeamount_id());
            Config.pstmt.setString(6, feecollection.get(i).getDate());
            Config.pstmt.setString(7, feecollection.get(i).getModepay());
            Config.pstmt.setString(8, feecollection.get(i).getBankname());
            Config.pstmt.setString(9, feecollection.get(i).getCheqdate());
            Config.pstmt.setString(10, feecollection.get(i).getCheqno());
            Config.pstmt.setString(11, feecollection.get(i).getTotalamount());
            Config.pstmt.setString(12, feecollection.get(i).getDiscount());
            Config.pstmt.setString(13, feecollection.get(i).getNetamount());
            Config.pstmt.setString(14, feecollection.get(i).getPayable());
            Config.pstmt.setString(15, feecollection.get(i).getBalance());
            Config.pstmt.setString(16, feecollection.get(i).getRecepitno());
//            int x = Config.pstmt.executeUpdate();
              Config.pstmt.addBatch();
                
            }
            int[] result_array = Config.pstmt.executeBatch();
            if( result_array.length== i){
               Config.fee_managment.onloadReset();
                
                return true;
            } else {
                return false;   
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
   }
 }
}
     
     //------------------------------------------------------------------------------
     
     
//    public boolean upd_collection(Fee_Dm_FeeCollection feecollection){
//        try {
//            Config.sql = "update fee_collection set"
//                    + "session_id = ?, "
//                    + "registration_id = ?, "
//                    + "fee_description_id = ?, "
//                    + "payment_detail_id = ?, "
//                    + "date = ? "
//                    + " where feecollection_id = '"+feecollection.getFeecollection_id()+"'";
//            
//                        Config.pstmt = Config.conn.prepareStatement(Config.sql);            
//                        Config.pstmt.setString(1, feecollection.getSession_id());
//                        Config.pstmt.setString(2, feecollection.getRegistration_id());
//                        
//            
//                        int x = Config.pstmt.executeUpdate();
//            
//            if(x>0) {
//                  
////                  Config.fee_home1.onloadReset();
//                return true;
//            } else{
//                return false;   
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//    }
//    //----------------------------------------------------------------------------------------------
//    
//    
//    public boolean del_collection(String feecollection){
//        try {          
//            Config.sql = "delete from fee_collection where feecollection_id = '"+feecollection+"'";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);
//            
//            int x = Config.pstmt.executeUpdate();
//                if (x>0) {
//                 
////                    Config.fee_home1.onloadReset();
//                return true;
//                }else{
//                    return false;
//                }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//   }
//    
    

