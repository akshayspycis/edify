package managers.clsmgr.daoMgr.fees;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.fees.Fee_Dm_FeePaymentDetail;

/**
 *
 * @author Ashok
 */
public class Fee_Cm_FeePaymentDetail {
     public boolean ins_payment(Fee_Dm_FeePaymentDetail paymentdetail){
        try {
            Config.sql = "insert into fee_payment_detail(paymentmode, bankname, paydate, paynumber) values(?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, paymentdetail.getPaymentmode());
            Config.pstmt.setString(2, paymentdetail.getBankname());
            Config.pstmt.setString(3, paymentdetail.getPaydate());
            Config.pstmt.setString(4, paymentdetail.getPaynumber());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                
                 
//                 Config.fee_home1.onloadReset();
                 
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        
        
//------------------------------------------------------------------------------
        
    }
    public boolean upd_payment(Fee_Dm_FeePaymentDetail paymentdetail){
        try {
            Config.sql = "update fee_payment_detail set"
                    + "paymentmode = ?, "
                    + "bankname = ?, "
                    + "paydate = ?, "
                    + "paynumber = ? " 
                    + " where payment_detail_id = '"+paymentdetail.getPayment_detail_id()+"'";
            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);            
                        Config.pstmt.setString(1, paymentdetail.getPaymentmode());
                        Config.pstmt.setString(2, paymentdetail.getBankname());
                        Config.pstmt.setString(3, paymentdetail.getPaydate());
                        Config.pstmt.setString(4, paymentdetail.getPaynumber());
            
                        int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                  
//                  Config.fee_home1.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean del_payment(String paymentdetailid) {
        try {          
            Config.sql = "delete from fee_payment_detail where payment_detail_id = '"+paymentdetailid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                 
//                    Config.fee_home1.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
