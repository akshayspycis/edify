package managers.clsmgr.daoMgr.fees;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.fees.Config_Fee_Dm_FeeNam;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Cm_FeeNamMgr {
    public boolean ins_FeeNam(Config_Fee_Dm_FeeNam fee_feename){
        try {
            Config.sql = "insert into config_fee_feenam("
                    + "session_id, "
                    + "feetype_id, "
                    + "feenmae)"
                    + "values(?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, fee_feename.getSession_id());
            Config.pstmt.setString(2, fee_feename.getFeetype_id());
            Config.pstmt.setString(3, fee_feename.getFeenmae());
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                
                 Config.xml_config_fee_feenammgr.load_FeeNam();
                 Config.fee_home1.onloadReset();
                 
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        
        
//------------------------------------------------------------------------------
        
    }
    public boolean upd_FeeNam(Config_Fee_Dm_FeeNam fee_dm_feenam) {
        try {
            Config.sql = "update config_fee_feenam set "
                     + "session_id = ?, "
                     + "feetype_id = ?, "
                     + "feenmae = ? "
                     + "where feename_id = '"+fee_dm_feenam.getFeename_id()+"'";
             Config.pstmt = Config.conn.prepareStatement(Config.sql);            
             Config.pstmt.setString(1, fee_dm_feenam.getSession_id());
             Config.pstmt.setString(2, fee_dm_feenam.getFeetype_id());
             Config.pstmt.setString(3, fee_dm_feenam.getFeenmae());
             
             int x = Config.pstmt.executeUpdate();
             if(x>0) {
                Config.xml_config_fee_feenammgr.load_FeeNam();
                Config.fee_home1.onloadReset();
                return true;
            } else{
                return false;   
            }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean del_FeeNam(String feenamid) {
        try {          
            Config.sql = "delete from config_fee_feenam where feename_id = '"+feenamid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_fee_feenammgr.load_FeeNam();
                   Config.fee_home1.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
