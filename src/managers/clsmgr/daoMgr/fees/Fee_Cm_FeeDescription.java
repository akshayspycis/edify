package managers.clsmgr.daoMgr.fees;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.fees.Fee_Dm_FeeDescription;

/**
 *
 * @author Ashok
 */
public class Fee_Cm_FeeDescription {
    public boolean ins_description(Fee_Dm_FeeDescription fedescrip){
        try {
            Config.sql = "insert into fee_description("
                    + "feeamount_id, "
                    + "totalamount, "
                    + "discount, "
                    + "netamount, "
                    + "payableamount, "
                    + "balanceamount) "
                    + "values(?,?,?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, fedescrip.getFeeamount_id());
            Config.pstmt.setString(2, fedescrip.getTotalamount());
            Config.pstmt.setString(3, fedescrip.getDiscount());
            Config.pstmt.setString(4, fedescrip.getNetamount());
            Config.pstmt.setString(5, fedescrip.getPayableamount());
            Config.pstmt.setString(6, fedescrip.getBalanceamount());
            
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                
                 
//                 Config.fee_home1.onloadReset();
                 
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        
        
//------------------------------------------------------------------------------
        
    }
    public boolean upd_description(Fee_Dm_FeeDescription fedescrip){
        try {
            Config.sql = "update fee_description set"
                    + "feeamount_id = ?, "
                    + "totalamount = ?, "
                    + "discount = ?, "
                    + "netamount = ?, "
                    + "payableamount = ?, "
                    + "balanceamount = ? "
                    + " where fee_description_id = '"+fedescrip.getFee_description_id()+"'";
            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);            
                        Config.pstmt.setString(1, fedescrip.getFeeamount_id());
                        Config.pstmt.setString(2, fedescrip.getTotalamount());
                        Config.pstmt.setString(3, fedescrip.getDiscount());
                        Config.pstmt.setString(4, fedescrip.getNetamount());
                        Config.pstmt.setString(5, fedescrip.getPayableamount());
                        Config.pstmt.setString(6, fedescrip.getBalanceamount());
            
                        int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                  
//                  Config.fee_home1.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean del_description(String feedescriptionid) {
        try {          
            Config.sql = "delete from fee_description where fee_description_id = '"+feedescriptionid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                 
//                    Config.fee_home1.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
    
}
