package managers.clsmgr.daoMgr.fees;

import java.util.ArrayList;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.fees.Config_Fee_Dm_FeeAmount;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Cm_FeeAmountMgr {
    
    public boolean ins_FeeAmount(ArrayList<Config_Fee_Dm_FeeAmount> fee_feeamount){
        try {
            Config.sql = "insert into config_fee_feeamount("
                    + "session_id, "
                    + "class_id, "
                    + "feename_id, "
                    + "amount, "
                    + "feetype_id)"
                    + "values(?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int i=0;
            for (i = 0; i < fee_feeamount.size(); i++) {
                
            Config.pstmt.setString(1, fee_feeamount.get(i).getSession_id());
            Config.pstmt.setString(2, fee_feeamount.get(i).getClass_id());
            Config.pstmt.setString(3, fee_feeamount.get(i).getFeename_id());
            Config.pstmt.setString(4, fee_feeamount.get(i).getAmount());
            Config.pstmt.setString(5, fee_feeamount.get(i).getFeetype_id());
            
            Config.pstmt.addBatch();
                
            }
            int[] result_array = Config.pstmt.executeBatch();
            
            
            if( result_array.length== i){
//              
                 Config.xml_config_fee_feeamountmgr.load_FeeAmunt();
//                 Config.fee_home1.onloadReset();
                
                return true;
            } else {
                return false;   
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    //------------------------------------------------------------------------------
    
    public boolean upd_FeeAmount(Config_Fee_Dm_FeeAmount fee_dm_feeamount) {
        try {
            Config.sql = "update config_fee_feeamount set "
                    + "session_id = ?, "
                    + "class_id, "
                    + "feename_id, "
                    + "amount, "
                    + "feetype_id"
                    + "where feeamount_id = '"+fee_dm_feeamount.getFeeamount_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, fee_dm_feeamount.getSession_id());
            Config.pstmt.setString(2, fee_dm_feeamount.getClass_id());
            Config.pstmt.setString(3, fee_dm_feeamount.getFeename_id());
            Config.pstmt.setString(4, fee_dm_feeamount.getAmount());
            Config.pstmt.setString(5, fee_dm_feeamount.getFeetype_id());
            int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
               Config.xml_config_fee_feeamountmgr.load_FeeAmunt();
                Config.fee_home1.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean del_FeeAmount(String feeid) {
        try {          
            Config.sql = "delete from config_fee_feeamount where feeamount_id = '"+feeid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_fee_feeamountmgr.load_FeeAmunt();
                   Config.fee_home1.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
