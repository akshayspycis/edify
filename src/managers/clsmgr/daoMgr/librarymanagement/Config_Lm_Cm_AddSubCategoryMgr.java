package managers.clsmgr.daoMgr.librarymanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddSubCategory;


public class Config_Lm_Cm_AddSubCategoryMgr {

    public boolean insSubCategory(Config_Lm_Dm_AddSubCategory lm_dm_subcategory) {
        try {          
            Config.sql = "insert into config_lm_subcategory ("                   
                        + "subcategoryname,"                    
                        + "category_id )"            
                        + "values (?,?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);       
            Config.pstmt.setString(1, lm_dm_subcategory.getSubcategoryname());
            Config.pstmt.setString(2, lm_dm_subcategory.getCategory_id());
            int x = Config.pstmt.executeUpdate();
            if(x>0){
                Config.xml_config_lm_subcategorymgr.load_Subcategory();
                Config.lm_repository_home.onloadReset();                
                return true;
            }else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
        
    public boolean updLm_SubCategory(Config_Lm_Dm_AddSubCategory lm_dm_subcategory) {
        try {
            Config.sql = "update config_lm_subcategory set"
                           
                         + " subcategoryname = ? ,"       
                         + " category_id = ? "
                         + " where subcategory_id = '"+lm_dm_subcategory.getSubcategory_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, lm_dm_subcategory.getSubcategoryname());
            Config.pstmt.setString(2, lm_dm_subcategory.getCategory_id());
            
            

            int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                Config.xml_config_lm_subcategorymgr.load_Subcategory();
                Config.lm_repository_home.onloadReset();
                return true;
            } else{
                System.out.println("hhdkhsw");
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
        
    public boolean delLm_Subcategory(String subcategoryid) {
        try {          
            Config.sql = "delete from config_lm_subcategory where subcategory_id = '"+subcategoryid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_lm_subcategorymgr.load_Subcategory();
                   Config.lm_repository_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
