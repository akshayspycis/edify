package managers.clsmgr.daoMgr.librarymanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddBookDuration;


public class Config_Lm_Cm_AddBookDurationMgr {
    
    public boolean insAddBookDuration(Config_Lm_Dm_AddBookDuration lm_dm_addbookduration) {
        try {          
            Config.sql = "insert into config_lm_addduration ("                   
                    + "category,"
                    + "subcategory,"
                    + "forstudent,"
                    + "forstaff) "
                    
                   
                    
                    + "values (?,?,?,?)";
            
             Config.pstmt = Config.conn.prepareStatement(Config.sql);       
             Config.pstmt.setString(1, lm_dm_addbookduration.getCategory());
             Config.pstmt.setString(2, lm_dm_addbookduration.getSubcategory());
             Config.pstmt.setString(3, lm_dm_addbookduration.getForstudent());             
             Config.pstmt.setString(4, lm_dm_addbookduration.getForstaff());
            
       
                  

            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                Config.xml_config_lm_bookdurationmgr.load_BookDuration();
                Config.lm_repository_home.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean upd_BookDuration(Config_Lm_Dm_AddBookDuration lm_addbookduration){
        try {
            Config.sql = "update config_lm_addduration set"
                    + " category = ? ,"                    
                    + " subcategory = ? ,"
                    + " forstudent = ? ,"
                    + " forstaff = ? "
                    + " where duration_id = '"+lm_addbookduration.getDuration_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, lm_addbookduration.getCategory());
            Config.pstmt.setString(2, lm_addbookduration.getSubcategory());
            Config.pstmt.setString(3, lm_addbookduration.getForstudent());
            Config.pstmt.setString(4, lm_addbookduration.getForstaff());
            
            int x = Config.pstmt.executeUpdate();
            if(x>0) {
                Config.xml_config_lm_bookdurationmgr.load_BookDuration();
                Config.lm_repository_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;            
        }
        
    
    }
    
    public boolean del_BookDuration(String bookid) {
        try {          
            Config.sql = "delete from config_lm_addduration where duration_id = '"+bookid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_lm_bookdurationmgr.load_BookDuration();
                   Config.lm_repository_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
