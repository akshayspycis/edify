package managers.clsmgr.daoMgr.librarymanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Lm_Dm_BookDetails;


public class Lm_Cm_BookDetailsMgr {
    
    //method to insert bookdetail in database
    public boolean insBookDetails(Lm_Dm_BookDetails lm_dm_bookdetail) {
        try {          
            Config.sql = "insert into lm_bookdetails ("
                    + "category,"
                    + "subcategory,"
                    + "booktitle,"                    
                    + "publisher,"                    
                    + "authorname,"
                    + "edition,"
                    + "purchasedate,"
                    + "price,"
                    + "rackno,"
                    + "quantity,"                                                                                                    
                    + "year ,"
                    + "subject,"
                    + "standard ) "
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);       
            
            Config.pstmt.setString(1, lm_dm_bookdetail.getCategory());
            Config.pstmt.setString(2, lm_dm_bookdetail.getSubcategory());
            Config.pstmt.setString(3, lm_dm_bookdetail.getBooktitle());
            Config.pstmt.setString(4, lm_dm_bookdetail.getPublisher());            
            Config.pstmt.setString(5, lm_dm_bookdetail.getAuthorname());
            Config.pstmt.setString(6, lm_dm_bookdetail.getEdition());
            Config.pstmt.setString(7, lm_dm_bookdetail.getPurchasedate());
            Config.pstmt.setString(8, lm_dm_bookdetail.getPrice());
            Config.pstmt.setString(9, lm_dm_bookdetail.getRackno());
            Config.pstmt.setString(10, lm_dm_bookdetail.getQuantity());                        
            Config.pstmt.setString(11, lm_dm_bookdetail.getYear());
            Config.pstmt.setString(12, lm_dm_bookdetail.getSubject());
            Config.pstmt.setString(13, lm_dm_bookdetail.getStandard());
            

            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_lm_bookdetailsmgr.load_BookDetails();
                Config.lm_repository_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
//   method to update bookdetail in database 
   public boolean updBookDetails(Lm_Dm_BookDetails lm_dm_bookdetail) {
        try {
            Config.sql = "update lm_bookdetails set"                    
                    + " category = ?, "
                    + " subcategory = ?, "
                    + "booktitle = ?,"                    
                    + "publisher = ?,"                    
                    + "authorname = ?,"
                    + "edition = ?,"
                    + "purchasedate = ?,"
                    + "price = ?,"
                    + "rackno = ?,"
                    + "quantity = ?,"                                                                                                    
                    + "year = ?,"
                    + "subject = ?,"
                    + "standard = ?"
                    + " where book_id = '"+ lm_dm_bookdetail.getBook_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, lm_dm_bookdetail.getCategory());
            Config.pstmt.setString(2, lm_dm_bookdetail.getSubcategory());
            Config.pstmt.setString(3, lm_dm_bookdetail.getBooktitle());
            Config.pstmt.setString(4, lm_dm_bookdetail.getPublisher());
            Config.pstmt.setString(5, lm_dm_bookdetail.getAuthorname());
            Config.pstmt.setString(6, lm_dm_bookdetail.getEdition());
            Config.pstmt.setString(7, lm_dm_bookdetail.getPurchasedate());
            Config.pstmt.setString(8, lm_dm_bookdetail.getPrice());
            Config.pstmt.setString(9, lm_dm_bookdetail.getRackno());
            Config.pstmt.setString(10, lm_dm_bookdetail.getQuantity());
            Config.pstmt.setString(11, lm_dm_bookdetail.getYear());
            Config.pstmt.setString(12, lm_dm_bookdetail.getSubject());
            Config.pstmt.setString(13, lm_dm_bookdetail.getStandard());
                   
          

                      
            int x = Config.pstmt.executeUpdate();
            if(x>0){
                Config.xml_lm_bookdetailsmgr.load_BookDetails();
                Config.lm_repository_home.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete bookdetailid in database
   public boolean delBookDetails(String bookdetailid)
   {
        try {          
            Config.sql = "delete from lm_bookdetails where book_id = '"+bookdetailid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

                if (x>0) {
                    Config.xml_lm_bookdetailsmgr.load_BookDetails();
                    Config.lm_repository_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }

    
}
