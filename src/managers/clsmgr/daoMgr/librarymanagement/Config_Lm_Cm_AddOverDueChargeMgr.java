package managers.clsmgr.daoMgr.librarymanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddBookDuration;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddOverDueCharge;

public class Config_Lm_Cm_AddOverDueChargeMgr {

    public boolean insOverDueCharge(Config_Lm_Dm_AddOverDueCharge lm_dm_addoverduecharge) {
        try {          
            Config.sql = "insert into config_lm_addoverduecharge ("                   
                    + "category,"
                    + "subcategory,"
                    + "forstudent,"
                    + "forstaff) "
                    
                   
                    
                    + "values (?,?,?,?)";
            
             Config.pstmt = Config.conn.prepareStatement(Config.sql);       
             Config.pstmt.setString(1, lm_dm_addoverduecharge.getCategory());
             Config.pstmt.setString(2, lm_dm_addoverduecharge.getSubcategory());
             Config.pstmt.setString(3, lm_dm_addoverduecharge.getForstudent());
             Config.pstmt.setString(4, lm_dm_addoverduecharge.getForstaff());
            
       
                  

            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                Config.xml_config_lm_overduechargemgr.load_overdueCharge();
                Config.lm_repository_home.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean upd_Overduecharge(Config_Lm_Dm_AddOverDueCharge lm_addoverduecharge){
        try {
            Config.sql = "update config_lm_addoverduecharge set"
                    + " category = ? ,"                    
                    + " subcategory = ? ,"
                    + " forstudent = ? ,"
                    + " forstaff = ? "
                    + " where overduecharge_id = '"+lm_addoverduecharge.getOverduecharge_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, lm_addoverduecharge.getCategory());
            Config.pstmt.setString(2, lm_addoverduecharge.getSubcategory());
            Config.pstmt.setString(3, lm_addoverduecharge.getForstudent());
            Config.pstmt.setString(4, lm_addoverduecharge.getForstaff());
            
            int x = Config.pstmt.executeUpdate();
            if(x>0) {
                Config.xml_config_lm_overduechargemgr.load_overdueCharge();
                Config.lm_repository_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;            
        }
        
    
    }
    
    public boolean del_OverdueCharge(String bookid) {
        try {          
            Config.sql = "delete from config_lm_addoverduecharge where overduecharge_id = '"+bookid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_lm_overduechargemgr.load_overdueCharge();
                   Config.lm_repository_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
