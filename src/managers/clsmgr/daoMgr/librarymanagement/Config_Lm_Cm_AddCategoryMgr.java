package managers.clsmgr.daoMgr.librarymanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddCategory;


public class Config_Lm_Cm_AddCategoryMgr {
   
    //method to insert category in database 
    public boolean insLm_Category(Config_Lm_Dm_AddCategory lm_dm_category) {
        try {          
            Config.sql = "insert into config_lm_category ("
                    + "categoryname ) "
                    + "values (?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);       
            Config.pstmt.setString(1, lm_dm_category.getCategoryname());
            

            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_config_lm_categorymgr.load_Category();
                Config.lm_repository_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    //method to update category in database 
    public boolean updLm_Category(Config_Lm_Dm_AddCategory lm_dm_category) {
        try {
            Config.sql = "update config_lm_category set"
                    + " categoryname = ? "                    
                    + " where category_id = '"+lm_dm_category.getCategory_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, lm_dm_category.getCategoryname());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                Config.xml_config_lm_categorymgr.load_Category();
                Config.lm_repository_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean delLm_category(String categoryid) {
        try {          
            Config.sql = "delete from config_lm_category where category_id = '"+categoryid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_lm_categorymgr.load_Category();
                   Config.lm_repository_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
