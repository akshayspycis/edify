package managers.clsmgr.daoMgr.librarymanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Lm_Dm_IssueBook;

public class Lm_Cm_IssueBookMgr {    

    public boolean insIssueBook(Lm_Dm_IssueBook lm_dm_issuebook) {
        try {          
            Config.sql = "insert into config_lm_issuebook ("                   
                    + "sno,"
                    + "bookid ,"
                    + "bookname,"
                    + "duration,"
                    + "issuedate,"
                    + "duedate,"
                    + "returndate,"
                    + "fine "          
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?)";
            
             Config.pstmt = Config.conn.prepareStatement(Config.sql);       
             Config.pstmt.setString(1, lm_dm_issuebook.getSno());
             Config.pstmt.setString(2, lm_dm_issuebook.getBookid());
             Config.pstmt.setString(3, lm_dm_issuebook.getBookname());
             Config.pstmt.setString(3, lm_dm_issuebook.getDuration());
             Config.pstmt.setString(3, lm_dm_issuebook.getIssuedate());
             Config.pstmt.setString(3, lm_dm_issuebook.getDuedate());
             Config.pstmt.setString(3, lm_dm_issuebook.getReturndate());
             Config.pstmt.setString(3, lm_dm_issuebook.getFine());
       
                  

            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
            return true;
            }else{
            return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
//----------------------------------------------------------------------------------------------
    
//   method to issuebook in database 
//   public boolean updBookDetails(BookDetails issuebook) {
//        try {
//            Config.sql = "update issuebook set"
//                    + " issuebookno = ?, "
//                    + " issuebookdate = ?, "
//                    + " workorderno = ?, "
//                    + " workordernodate = ?, "
//                    + " sdrs = ?, "
//                    + " shapeof = ?, "
//                    + " shapeofdate = ?, "
//                    + " nameofbank = ?, "
//                    + " period = ?, "
//                    + " formoftender = ?, "
//                    + " negotiation = ?, "
//                    + " acceptance = ?, "
//                    + " letterno = ?, "
//                    + " dt = ?, "
//                    + " actualdateofworkstarted = ?, "
//                    + " completionofworkdate = ?, "
//                    + " stipulateddate = ?, "
//                    + " timeextention1 = ?, "
//                    + " timeextention2 = ? "
//                    + " where issuebookid = '"+lm_dm_issuebook.getAgreementid()+"'";
//            
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);
//            
//              Config.pstmt.setString(1, lm_dm_issuebook.getAgreementno());
//            Config.pstmt.setString(2, lm_dm_issuebook.getAgreementdate());
//            Config.pstmt.setString(3, lm_dm_issuebook.getWorkorderno());
//            Config.pstmt.setString(4, lm_dm_issuebook.getWorkordernodate());
//            Config.pstmt.setString(5, lm_dm_issuebook.getSdrs());
//            Config.pstmt.setString(6, lm_dm_issuebook.getShapeof());
//            Config.pstmt.setString(7, lm_dm_issuebook.getShapeofdate());
//            Config.pstmt.setString(8, lm_dm_issuebook.getNameofbank());
//            Config.pstmt.setString(9, lm_dm_issuebook.getPeriod());
//            Config.pstmt.setString(10, lm_dm_issuebook.getFormoftender());
//            Config.pstmt.setString(11, lm_dm_issuebook.getNegotiation());
//            Config.pstmt.setString(12, lm_dm_issuebook.getAcceptance());
//            Config.pstmt.setString(13, lm_dm_issuebook.getLetterno());            
//            Config.pstmt.setString(14, lm_dm_issuebook.getDt());  
//            Config.pstmt.setString(15, lm_dm_issuebook.getActualdateofworkstarted());            
//            Config.pstmt.setString(16, lm_dm_issuebook.getCompletionofworkdate());            
//            Config.pstmt.setString(17, lm_dm_issuebook.getStipulateddate());            
//            Config.pstmt.setString(18, lm_dm_issuebook.getTimeextention1());            
//            Config.pstmt.setString(19, lm_dm_issuebook.getTimeextention2());            
//          
//
//                      
//            int x = Config.pstmt.executeUpdate();
//            if(x>0){
//            return true;
//            }else{
//            return false;   
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete issuebookid in database
//   public boolean delAgreement(String issuebookid)
//   {
//        try {          
//            Config.sql = "delete from issuebook where issuebookid = '"+issuebookid+"'";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);
//            
//            int x = Config.pstmt.executeUpdate();
//
//                if (x>0) {
//                return true;
//                }else{
//                    return false;
//                }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//   }


}
