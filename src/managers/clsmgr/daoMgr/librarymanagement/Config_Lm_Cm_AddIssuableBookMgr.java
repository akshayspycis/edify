package managers.clsmgr.daoMgr.librarymanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddCategory;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddIssuableBook;

public class Config_Lm_Cm_AddIssuableBookMgr {
     
    public boolean insAddIssuableBook(Config_Lm_Dm_AddIssuableBook lm_dm_addissuablebook) {
        try {          
            Config.sql = "insert into config_lm_addissuablebook ("                   
                    + "categoryname,"
                    + "subcategoryname,"
                    + "forstudent,"
                    + "forstaff)"                   
                    + "values (?,?,?,?)";            
             Config.pstmt = Config.conn.prepareStatement(Config.sql);       
             Config.pstmt.setString(1, lm_dm_addissuablebook.getCategoryname());
             Config.pstmt.setString(2, lm_dm_addissuablebook.getSubcategoryname());
             Config.pstmt.setString(3, lm_dm_addissuablebook.getForstudent());
             Config.pstmt.setString(4, lm_dm_addissuablebook.getForstaff());
             int x = Config.pstmt.executeUpdate();
             if(x>0){
                 Config.xml_config_lm_issuablebookmgr.load_IssuableBook();
                Config.lm_repository_home.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
     public boolean updIssuablebook(Config_Lm_Dm_AddIssuableBook lm_dm_addissuablebook) {
        try {
            Config.sql = "update config_lm_addissuablebook set"
                    + " categoryname = ? ,"                    
                    + " subcategoryname = ? ,"
                    + " forstudent = ? ,"
                    + " forstaff = ? "
                    + " where issuablebook_id = '"+lm_dm_addissuablebook.getIssuablebook_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, lm_dm_addissuablebook.getCategoryname());
            Config.pstmt.setString(2, lm_dm_addissuablebook.getSubcategoryname());
            Config.pstmt.setString(3, lm_dm_addissuablebook.getForstudent());
            Config.pstmt.setString(4, lm_dm_addissuablebook.getForstaff());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                Config.xml_config_lm_issuablebookmgr.load_IssuableBook();
                Config.lm_repository_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean delIssuablebook(String bookid) {
        try {          
            Config.sql = "delete from config_lm_addissuablebook where issuablebook_id = '"+bookid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_lm_issuablebookmgr.load_IssuableBook();
                   Config.lm_repository_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
