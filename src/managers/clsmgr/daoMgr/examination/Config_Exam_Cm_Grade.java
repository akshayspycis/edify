package managers.clsmgr.daoMgr.examination;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.examination.Config_Exam_Dm_Grade;


/**
 *
 * @author Ashok
 */
public class Config_Exam_Cm_Grade {
    
     public boolean ins_Grade(Config_Exam_Dm_Grade exam_grade){
        try {
            Config.sql = "insert into config_exam_grade("
                    + "grade_name,"
                    + "min_marks,"
                    + "max_marks,"
                    + "grade_point,"
                    + "remarks)"
                    + "values(?,?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, exam_grade.getGrade_name());
            Config.pstmt.setString(2, exam_grade.getMin_marks());
            Config.pstmt.setString(3, exam_grade.getMax_marks());
            Config.pstmt.setString(4, exam_grade.getGrade_point());
            Config.pstmt.setString(5, exam_grade.getRemarks());
            
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_config_examgrademgr.load_ExamGrade();
                Config.exam_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        
        
        
    }
     
     public boolean upd_Grade(Config_Exam_Dm_Grade exam_dm_grade){
         try {
             Config.sql = "update config_exam_grade set "
                     + "grade_name = ?, "
                     + "min_marks = ?, "
                     + "max_marks = ?, "
                     + "grade_point = ?, "
                     + "remarks = ?"
                     + "where grade_id = '"+exam_dm_grade.getGrade_id()+"'";
             Config.pstmt = Config.conn.prepareStatement(Config.sql);            
             Config.pstmt.setString(1, exam_dm_grade.getGrade_name());
             Config.pstmt.setString(2, exam_dm_grade.getMin_marks());
             Config.pstmt.setString(3, exam_dm_grade.getMax_marks());
             Config.pstmt.setString(4, exam_dm_grade.getGrade_point());
             Config.pstmt.setString(5, exam_dm_grade.getRemarks());
             
             int x = Config.pstmt.executeUpdate();
             if(x>0) {
                Config.xml_config_examgrademgr.load_ExamGrade();
                Config.exam_home.onloadReset();
                return true;
            } else{
                return false;   
            }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
         
     }
     public boolean del_Grade(String gradeid){
         try {
             Config.sql = "delete from config_exam_grade where grade_id = '"+gradeid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
             if (true) {
                  Config.xml_config_examgrademgr.load_ExamGrade();
                   Config.exam_home.onloadReset();
                 return true;
             }else{
                 return false;
             }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
         
     }
     
     
    
}
