package managers.clsmgr.daoMgr.examination;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Class;
import managers.datamgr.dao.examination.Config_Exam_Dm_Type;


public class Config_Exam_Cm_Type {
    
    
    public boolean ins_Exam(Config_Exam_Dm_Type exam_type){
        try {
            Config.sql = "insert into config_exam_type("
                    + "examtype_name)"
                    + "values(?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, exam_type.getExamtype_name());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                
                 Config.xml_config_examtypemgr.load_ExamType();
                 Config.exam_home.onloadReset();
                 
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        
        
//------------------------------------------------------------------------------
        
    }
    public boolean upd_Type(Config_Exam_Dm_Type exam_dm_type) {
        try {
            Config.sql = "update config_exam_type set"
                    + " examtype_name = ? "                    
                    + " where exam_id = '"+exam_dm_type.getExam_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, exam_dm_type.getExamtype_name());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                Config.xml_config_examtypemgr.load_ExamType();
                Config.exam_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean del_Type(String examid) {
        try {          
            Config.sql = "delete from config_exam_type where exam_id = '"+examid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_examtypemgr.load_ExamType();
                   Config.exam_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
    
    
}
