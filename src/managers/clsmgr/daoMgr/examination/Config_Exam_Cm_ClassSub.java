package managers.clsmgr.daoMgr.examination;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.examination.Config_Exam_Dm_ClassSub;

/**
 *
 * @author Ashok
 */
public class Config_Exam_Cm_ClassSub {
    
    
    public boolean ins_ClasSub(Config_Exam_Dm_ClassSub exam_classub){
        try {
            Config.sql = "insert into config_exam_class_sub("
                    + "class_id,"
                    + "subject_id,"
                    + "total_theorymarks,"
                    + "theory_passingmarks,"
                    + "tototal_practicalmarks,"
                    + "practical_passingmarks)"
                    + "values(?,?,?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, exam_classub.getClass_id());
            Config.pstmt.setString(2, exam_classub.getClasssubject_id());
            Config.pstmt.setString(3, exam_classub.getTotal_theorymarks());
            Config.pstmt.setString(4, exam_classub.getTheory_passingmarks());
            Config.pstmt.setString(5, exam_classub.getTototal_practicalmarks());
            Config.pstmt.setString(6, exam_classub.getPractical_passingmarks());
//            
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_config_examclasssubmgr.load_ExamClassSub();
                Config.exam_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        
        
        
    }
    public boolean upd_ClasSub(Config_Exam_Dm_ClassSub exam_classsub){
       try {
             Config.sql = "update config_exam_class_sub set "
                     + "class_id = ?, "
                     + "subject_id = ?, "
                     + "total_theorymarks = ?, "
                     + "theory_passingmarks = ?, "
                     + "tototal_practicalmarks = ?,"
                     + "practical_passingmarks = ?"
                     + "where classsubject_id = '"+exam_classsub.getClasssubject_id()+"'";
             Config.pstmt = Config.conn.prepareStatement(Config.sql);            
             Config.pstmt.setString(1, exam_classsub.getClass_id());
             Config.pstmt.setString(2, exam_classsub.getSubject_id());
             Config.pstmt.setString(3, exam_classsub.getTotal_theorymarks());
             Config.pstmt.setString(4, exam_classsub.getTheory_passingmarks());
             Config.pstmt.setString(5, exam_classsub.getTototal_practicalmarks());
             Config.pstmt.setString(6, exam_classsub.getPractical_passingmarks());
             
             int x = Config.pstmt.executeUpdate();
             if(x>0) {
                Config.xml_config_examclasssubmgr.load_ExamClassSub();
                Config.exam_home.onloadReset();
                return true;
            } else{
                return false;   
            }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
    }
    
    public boolean del_ClasSub(String classubid){
        
        try {
             Config.sql = "delete from config_exam_class_sub where classsubject_id = '"+classubid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
             if (true) {
                  Config.xml_config_examclasssubmgr.load_ExamClassSub();
                   Config.exam_home.onloadReset();
                 return true;
             }else{
                 return false;
             }
         } catch (Exception e) {
             e.printStackTrace();
             return false;
         }
        
    }
    
}
