package managers.clsmgr.daoMgr.admin;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Class_Section;


public class Config_Ad_Cm_Class_SectionMgr {
 
    public boolean insAd_Class_Section(Config_Ad_Dm_Class_Section ad_classsection) {
        try {          
            Config.sql = "insert into config_ad_class_section ("
                    + "section_name,"
                    + "class_id, "
                    + "session_id)"
                                        
                    + "values (? , ? , ?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, ad_classsection.getSection_name());
            Config.pstmt.setString(2, ad_classsection.getClass_id());
            Config.pstmt.setString(3, ad_classsection.getSession_id());
               
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_config_ad_classsectionmgr.load_Ad_ClassSection();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
     //method to supplierprofile in database
    public boolean updAd_Class_Section(Config_Ad_Dm_Class_Section ad_classsection) {
        try {
            Config.sql = "update config_ad_class_section set "
                     + "section_name = ?, "
                     + "class_id = ?, "
                    + "session_id = ?"
                     
                         
                     + "where section_id = '"+ad_classsection.getSection_id()+"'";            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, ad_classsection.getSection_name());
                        Config.pstmt.setString(2, ad_classsection.getClass_id()); 
                        Config.pstmt.setString(3, ad_classsection.getSession_id());
                        
                        int x = Config.pstmt.executeUpdate();
                        if (x>0) { 
                            Config.xml_config_ad_classsectionmgr.load_Ad_ClassSection();
                            Config.ad_home.onloadReset();
                            return true;
                        } else {
                            return false;
                        }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
   
//method to delete agreementid in database
   public boolean delAd_Class_Section(String section_id) {
       try {
            Config.sql = "delete from config_ad_class_section where section_id = '"+section_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_config_ad_classsectionmgr.load_Ad_ClassSection();
//                Config.configmgr.loadSi_SupplierProfile();
                Config.ad_home.onloadReset();
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
