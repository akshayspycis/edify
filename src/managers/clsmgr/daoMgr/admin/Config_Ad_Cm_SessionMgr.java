package managers.clsmgr.daoMgr.admin;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Session;


public class Config_Ad_Cm_SessionMgr {
      public boolean insAd_session(Config_Ad_Dm_Session ad_session) {
        try {          
            Config.sql = "insert into config_ad_session ("
                    + "session_name)"
                    + "values (?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, ad_session.getSession_name());
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_config_ad_sessionmgr.load_Ad_Session();
                 Config.ad_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
     //method to supplierprofile in database
    public boolean updAd_session(Config_Ad_Dm_Session ad_session) {
        try {
            Config.sql = "update config_ad_session set "
                     + "session_name = ?"
                     + "where session_id = '"+ad_session.getSession_id()+"'";            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, ad_session.getSession_name());
                        
            int x = Config.pstmt.executeUpdate();
            if (x>0) { 
                Config.xml_config_ad_sessionmgr.load_Ad_Session();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
   
//method to delete agreementid in database
   public boolean delAd_session(String session_id) {
       try {
            Config.sql = "delete from config_ad_session where session_id = '"+session_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_config_ad_sessionmgr.load_Ad_Session();
                Config.ad_home.onloadReset();

                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
