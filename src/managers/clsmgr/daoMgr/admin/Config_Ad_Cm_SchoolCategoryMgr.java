
package managers.clsmgr.daoMgr.admin;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_SchoolCategory;



public class Config_Ad_Cm_SchoolCategoryMgr {
     public boolean insAd_SchoolCategory(Config_Ad_Dm_SchoolCategory ad_schoolcategory) {
        try {          
            Config.sql = "insert into config_ad_schoolcategory ("
                    + "category_name,"
                    + "session_id) "                               
                    + "values (?,?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, ad_schoolcategory.getCategory_name());
            Config.pstmt.setString(2, ad_schoolcategory.getSession_id());
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_config_ad_schoolcategorymgr.load_Ad_SchoolCategory();
                Config.ad_home.onloadReset();
                Config.ad_scnewentry.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
     //method to supplierprofile in database
    public boolean updAd_SchoolCategory(Config_Ad_Dm_SchoolCategory ad_schoolcategory) {
        try {
            Config.sql = "update config_ad_schoolcategory set "
                     + "category_name = ?, "
                     + "session_id = ? "
                         
                     + "where category_id = '"+ad_schoolcategory.getCategory_id()+"'";            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, ad_schoolcategory.getCategory_name());
                        Config.pstmt.setString(2, ad_schoolcategory.getSession_id());
                                               
            int x = Config.pstmt.executeUpdate();
            if (x>0) { 
                Config.xml_config_ad_schoolcategorymgr.load_Ad_SchoolCategory();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
   
//method to delete agreementid in database
   public boolean delAd_SchoolCategory(String category_id) {
       try {
            Config.sql = "delete from config_ad_schoolcategory where category_id = '"+category_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_config_ad_schoolcategorymgr.load_Ad_SchoolCategory();

                Config.ad_home.onloadReset();
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
