
package managers.clsmgr.daoMgr.admin;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Subject;


public class Config_Ad_Cm_SubjectMgr {
    public boolean insAd_Subject(Config_Ad_Dm_Subject ad_subject) {
        try {          
            Config.sql = "insert into config_ad_subject ("
                    + "subject_name, "
                    + "session_id)"                               
                    + "values (? , ?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, ad_subject.getSubject_name());
            Config.pstmt.setString(2, ad_subject.getSession_id());
            int x = Config.pstmt.executeUpdate(); 
            if(x>0){
                Config.xml_config_ad_subjectmgr.load_Ad_Subject();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
     //method to supplierprofile in database
    public boolean updAd_Subject(Config_Ad_Dm_Subject ad_subject) {
        try {
            Config.sql = "update config_ad_subject set "
                     + "subject_name = ?, "
                    + "session_id = ? "
                      
                     + "where subject_id = '"+ad_subject.getSubject_id()+"'";            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, ad_subject.getSubject_name());
                        Config.pstmt.setString(2, ad_subject.getSession_id());
                        
                                                                     
            int x = Config.pstmt.executeUpdate();
            if (x>0) { 
                Config.xml_config_ad_subjectmgr.load_Ad_Subject();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
   
//method to delete agreementid in database
   public boolean delAd_Subject(String subject_id) {
       try {
            Config.sql = "delete from config_ad_subject where subject_id = '"+subject_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_config_ad_subjectmgr.load_Ad_Subject();
                Config.ad_home.onloadReset();
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
