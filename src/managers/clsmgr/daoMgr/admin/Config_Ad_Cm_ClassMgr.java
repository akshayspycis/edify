

package managers.clsmgr.daoMgr.admin;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Class;


public class Config_Ad_Cm_ClassMgr {
    
//    ####################      insert Code    ###################################
    
    public boolean insAd_Class(Config_Ad_Dm_Class ad_class) {
        try {          
            Config.sql = "insert into config_ad_class ("
                    + "class_name,"
                    + "category_id, "
                    + "session_id )"
                                                  
                    + "values (? , ? , ?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, ad_class.getClass_name());
            Config.pstmt.setString(2, ad_class.getCategory_id());
            Config.pstmt.setString(3, ad_class.getSession_id());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_config_ad_classmgr.load_Ad_Class();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//     update 
    public boolean updAd_Class(Config_Ad_Dm_Class ad_class) {
        try {
            Config.sql = "update config_ad_class set "
                     + "class_name = ?, "
                     + "category_id = ?, "
                     + "session_id = ? "
                                          
                     + "where class_id = '"+ad_class.getClass_id()+"'"; 
            
            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, ad_class.getClass_name());
                        Config.pstmt.setString(2, ad_class.getCategory_id()); 
                        Config.pstmt.setString(3, ad_class.getSession_id());
                                                                    
            int x = Config.pstmt.executeUpdate();
            if (x>0) { 
                Config.xml_config_ad_classmgr.load_Ad_Class();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
   
//method to delete agreementid in database
   public boolean delAd_Class(String class_id) {
       try {
            Config.sql = "delete from config_ad_class where class_id = '"+class_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_config_ad_classmgr.load_Ad_Class();

                Config.ad_home.onloadReset();
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
