
package managers.clsmgr.daoMgr.admin;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_ElectiveGroup;


public class Config_Ad_Cm_ElectiveGroupMgr {
    public boolean insAd_electivegroup(Config_Ad_Dm_ElectiveGroup ad_electivegroup) {
        try {          
            Config.sql = "insert into config_ad_electivegroup ("
                    + "electivegroupname, "
                    + "session_id)"
                                                                    
                    + "values (? , ?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, ad_electivegroup.getElectivegroupname());
            Config.pstmt.setString(2, ad_electivegroup.getSession_id());
            
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0){
                 Config.xml_config_ad_electivegroupmgr.load_Ad_ElectiveGroup();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
     //method to supplierprofile in database
    public boolean updAd_electivegroup(Config_Ad_Dm_ElectiveGroup ad_electivegroup) {
        try {
            Config.sql = "update config_ad_electivegroup set "
                     + "electivegroupname = ?, "
                    + "session_id = ?"
                    
                                          
                     + "where electivegroup_id = '"+ad_electivegroup.getElectivegroup_id()+"'";            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, ad_electivegroup.getElectivegroupname());
                        Config.pstmt.setString(2, ad_electivegroup.getSession_id());
            int x = Config.pstmt.executeUpdate();
            if (x>0) { 
                Config.xml_config_ad_electivegroupmgr.load_Ad_ElectiveGroup();
                Config.ad_home.onloadReset();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
   
//method to delete agreementid in database
   public boolean delAd_electivegroup(String electivegroup_id) {
       try {
            Config.sql = "delete from config_ad_electivegroup where electivegroup_id = '"+electivegroup_id+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_config_ad_electivegroupmgr.load_Ad_ElectiveGroup();
//                Config.configmgr.loadSi_SupplierProfile();
                Config.ad_home.onloadReset();
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
