

package managers.clsmgr.daoMgr.admin;

import java.util.ArrayList;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_ElectiveGroupSubject;

public class Config_Ad_Cm_ElectiveGroupSubjectMgr {
     public boolean insAd_electivesubject(ArrayList<Config_Ad_Dm_ElectiveGroupSubject> ad_electivesubject) {
        try {          
            Config.sql = "insert into config_ad_electivegroupsubject ("
                    + "subject_id,"
                    + "electivegroup_id, "
                    + "session_id)"                                        
                    + "values (? , ? , ?)";            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int i=0;
            for(i=0;i<ad_electivesubject.size();i++){
            Config.pstmt.setString(1, ad_electivesubject.get(i).getSubject_id());
            Config.pstmt.setString(2, ad_electivesubject.get(i).getElectivegroup_id());
            Config.pstmt.setString(3, ad_electivesubject.get(i).getSession_id());
              Config.pstmt.addBatch();
            }
            int[] result_array = Config.pstmt.executeBatch();
            
            if( result_array.length==i){
                  Config.xml_config_ad_electivegroupsubjectmgr.load_Ad_ElectiveGroupSubject();
                  Config.ad_home.onloadReset();
                return true;
            } else {
                return false;   
            }                  
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
     //method to supplierprofile in database
    public boolean updAd_electivesubject(Config_Ad_Dm_ElectiveGroupSubject ad_electivesubject) {
        try {
            Config.sql = "update config_ad_electivegroupsubject set "
                     + "subject_id = ?, "
                     + "electivegroup_id = ?, "
                    + "session_id = ? "                       
                     + "where electivegroupsubject_id = '"+ad_electivesubject.getElectivegroupsubject_id()+"'";            
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);                        
                        Config.pstmt.setString(1, ad_electivesubject.getSubject_id());
                        Config.pstmt.setString(2, ad_electivesubject.getElectivegroup_id()); 
                        Config.pstmt.setString(3, ad_electivesubject.getSession_id());
                        int x = Config.pstmt.executeUpdate();
                        if (x>0) { 
                            Config.xml_config_ad_electivegroupsubjectmgr.load_Ad_ElectiveGroupSubject();
                Config.ad_home.onloadReset();
                            return true;
                        } else {
                            return false;
                        }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
    
//method to delete agreementid in database
   public boolean delAd_electivesubject(String electivegroupsubectid) {
       try {
            Config.sql = "delete from config_ad_electivegroupsubject where electivegroupsubject_id = '"+electivegroupsubectid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql); 
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.xml_config_ad_electivegroupsubjectmgr.load_Ad_ElectiveGroupSubject();
                Config.ad_home.onloadReset();
                return true;
            }else{
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
    
}
