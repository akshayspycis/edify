

package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Hr_Dm_EmpRecord;
import managers.datamgr.dao.hrmanagement.Hr_Dm_GenerateSalary;

public class Hr_Cm_GenerateSalaryMgr {
    public boolean insSalary(Hr_Dm_GenerateSalary hr_dm_salary){
        try {
          Config.sql = "insert into hr_generatesalary ("                                       
                    + "emp_id,"                    
                    + "checkno,"
                    + "paydate,"
                    + "incometax,"
                    + "hra,"
                    + "da,"
                    + "medical,"
                    + "conveyance,"
                    + "travel,"
                    + "special , "    
                    + "pf , " 
                    + "netsal , " 
                    + "advance_id , " 
                    + "attendance_id ) " 
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";  
          
          Config.pstmt = Config.conn.prepareStatement(Config.sql);       
          Config.pstmt.setString(1, hr_dm_salary.getEmp_id());  
          Config.pstmt.setString(2, hr_dm_salary.getCheckno());
          Config.pstmt.setString(3, hr_dm_salary.getPaydate());
          Config.pstmt.setString(4, hr_dm_salary.getIncometax());          
          Config.pstmt.setString(5, hr_dm_salary.getHra());
          Config.pstmt.setString(6, hr_dm_salary.getDa());
          Config.pstmt.setString(7, hr_dm_salary.getMedical());
          Config.pstmt.setString(8, hr_dm_salary.getConveyance());
          Config.pstmt.setString(9, hr_dm_salary.getTravel());
          Config.pstmt.setString(10, hr_dm_salary.getSpecial());
          Config.pstmt.setString(11, hr_dm_salary.getPf());
          Config.pstmt.setString(12, hr_dm_salary.getNetsal());
          Config.pstmt.setString(13, hr_dm_salary.getAdvance_id());
          Config.pstmt.setString(14, hr_dm_salary.getAttendance_id());
          
          
          
          
          int x = Config.pstmt.executeUpdate();
           
            if(x>0){
                Config.xml_hr_generatesalarymgr.load_Salary();
                Config.hr_home.onloadReset();
                Config.hr_empmgmt.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean upd_Salary(Hr_Dm_GenerateSalary hr_dm_salary){
        try {
            Config.sql = "update hr_generatesalary set"
                    + " emp_id = ? ,"                    
                    + " checkno = ? ,"
                    + " paydate = ? ,"
                    + " incometax = ? "
                    + " hra = ? "
                    + " da = ? "
                    + " medical = ? "
                    + " conveyance = ? "
                    + " travel = ? "
                    + " special = ? "
                    + " pf = ? "
                    + " netsal = ? "
                    + " advance_id = ? "
                    + " attendance_id = ? "                  
                    + " where salary_id = '"+hr_dm_salary.getSalary_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);  
            
            Config.pstmt.setString(1, hr_dm_salary.getEmp_id());  
            Config.pstmt.setString(2, hr_dm_salary.getCheckno());
            Config.pstmt.setString(3, hr_dm_salary.getPaydate());
            Config.pstmt.setString(4, hr_dm_salary.getIncometax());          
            Config.pstmt.setString(5, hr_dm_salary.getHra());
            Config.pstmt.setString(6, hr_dm_salary.getDa());
            Config.pstmt.setString(7, hr_dm_salary.getMedical());
            Config.pstmt.setString(8, hr_dm_salary.getConveyance());
            Config.pstmt.setString(9, hr_dm_salary.getTravel());
            Config.pstmt.setString(10, hr_dm_salary.getSpecial());
            Config.pstmt.setString(11, hr_dm_salary.getPf());
            Config.pstmt.setString(12, hr_dm_salary.getNetsal());
            Config.pstmt.setString(13, hr_dm_salary.getAdvance_id());
            Config.pstmt.setString(14, hr_dm_salary.getAttendance_id());
            
            
            int x = Config.pstmt.executeUpdate();
            if(x>0) {
                Config.xml_hr_generatesalarymgr.load_Salary();
                Config.hr_home.onloadReset();
                Config.hr_empmgmt.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;            
        }
        
    
    }
    
    public boolean del_Salary(String salaryid) {
        try {          
            Config.sql = "delete from hr_generatesalary where salary_id = '"+salaryid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_hr_generatesalarymgr.load_Salary();
                   Config.hr_home.onloadReset();
                   Config.hr_empmgmt.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
