package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Hr_Dm_Deduction;

public class Hr_Cm_DeductionMgr {
    public boolean insDeduction(Hr_Dm_Deduction hr_dm_deduction){
        try {
          Config.sql = "insert into hr_deduction ("                                          
                    + "emp_id,"
                    + "advance_id,"
                    + "amount,"
                    + "deduction_date,"                                                       
                    + "balance )"
                    + "values (?,?,?,?,?)";  
          
          Config.pstmt = Config.conn.prepareStatement(Config.sql);                  
          Config.pstmt.setString(1, hr_dm_deduction.getEmp_id());
          Config.pstmt.setString(2, hr_dm_deduction.getAdvance_id());
          Config.pstmt.setString(3, hr_dm_deduction.getAmount());                
          Config.pstmt.setString(4, hr_dm_deduction.getDeduction_date());                
          Config.pstmt.setString(5, hr_dm_deduction.getBalance());                
          
          
          int x = Config.pstmt.executeUpdate();
           
            if(x>0){
                Config.xml_hr_deductionmgr.load_Deduction();
                Config.hr_empmgmt.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
