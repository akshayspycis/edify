

package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Hr_Dm_EmpAttendance;


public class Hr_Cm_EmpAttendanceMgr {
    public boolean insAttendance(Hr_Dm_EmpAttendance hr_dm_attendance){
        try {
          Config.sql = "insert into hr_empattendance ("                                          
                    + "emp_id,"
                    + "status,"
                    + "timein,"
                    + "timeout,"
                    + "date )"                    
                    + "values (?,?,?,?,?)";  
          
          Config.pstmt = Config.conn.prepareStatement(Config.sql);                  
          Config.pstmt.setString(1, hr_dm_attendance.getEmp_id());
          Config.pstmt.setString(2, hr_dm_attendance.getStatus());
          Config.pstmt.setString(3, hr_dm_attendance.getTimein());          
          Config.pstmt.setString(4, hr_dm_attendance.getTimeout());
          Config.pstmt.setString(5, hr_dm_attendance.getDate());
          
          
          int x = Config.pstmt.executeUpdate();
           
            if(x>0){
                Config.xml_hr_empattendancemgr.load_EmpAttendance();
                Config.hr_empmgmt.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    
    public boolean upd_Attendance(Hr_Dm_EmpAttendance hr_dm_attendance){
        try {
            Config.sql = "update hr_empattendance set"
                    + " emp_id = ? ,"                    
                    + " status = ? ,"
                    + " timein = ? ,"
                    + " timeout = ? "
                    + " date = ? "                               
                    + " where emp_attendance_id = '"+hr_dm_attendance.getEmp_attendance_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);                  
            Config.pstmt.setString(1, hr_dm_attendance.getEmp_id());
            Config.pstmt.setString(2, hr_dm_attendance.getStatus());
            Config.pstmt.setString(3, hr_dm_attendance.getTimein());          
            Config.pstmt.setString(4, hr_dm_attendance.getTimeout());
            Config.pstmt.setString(5, hr_dm_attendance.getDate());
            
            
            
            int x = Config.pstmt.executeUpdate();
            if(x>0) {
                Config.xml_hr_empattendancemgr.load_EmpAttendance();
                Config.hr_empmgmt.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;            
        }
        
    
    }
    
    public boolean del_Attendance(String attendanceid) {
        try {          
            Config.sql = "delete from hr_empattendance where emp_attendance_id = '"+attendanceid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_hr_empattendancemgr.load_EmpAttendance();
                   Config.hr_empmgmt.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
