

package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Hr_Dm_Advance;

public class Hr_Cm_AdvanceMgr {
    public boolean insAdvance(Hr_Dm_Advance hr_dm_advance){
        try {
          Config.sql = "insert into hr_advance ("                                          
                    + "emp_id,"
                    + "advance_amt,"
                    + "advancedate)"                                                       
                    + "values (?,?,?)";  
          
          Config.pstmt = Config.conn.prepareStatement(Config.sql);                  
          Config.pstmt.setString(1, hr_dm_advance.getEmp_id());
          Config.pstmt.setString(2, hr_dm_advance.getAdvance_amt());
          Config.pstmt.setString(3, hr_dm_advance.getAdvancedate());                
          
          
          int x = Config.pstmt.executeUpdate();
           
            if(x>0){
                Config.xml_hr_advancemgr.load_Advance();
                Config.hr_empmgmt.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
//    public boolean upd_Advance(Hr_Dm_Advance hr_dm_advance){
//        try {
//            Config.sql = "update hr_advance set"
//                    + " emp_id = ? ,"                    
//                    + " advance_amt = ? ,"
//                    + " advancedate = ? ,"
//                    + " payslip_id = ? "                                                 
//                    + " netsalary = ? "
//                    + " where advance_id = '"+hr_dm_advance.getAdvance_id()+"'";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
//            Config.pstmt.setString(1, hr_dm_advance.getEmp_id());
//            Config.pstmt.setString(2, hr_dm_advance.getAdvance_amt());
//            Config.pstmt.setString(3, hr_dm_advance.getAdvancedate());
//            Config.pstmt.setString(4, hr_dm_advance.getPayslip_id());
//            Config.pstmt.setString(5, hr_dm_advance.getNetsalary());
//                            
//                        
//            int x = Config.pstmt.executeUpdate();
//            if(x>0) {
//                Config.xml_hr_advancemgr.load_Advance();
//                Config.hr_empmgmt.onloadReset();
//                return true;
//            } else{
//                return false;   
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;            
//        }
//        
//    
//    }
//    
//    public boolean del_Salary(String advanceid) {
//        try {          
//            Config.sql = "delete from hr_advance where advance_id = '"+advanceid+"'";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);
//            
//            int x = Config.pstmt.executeUpdate();
//                if (x>0) {
//                   Config.xml_hr_advancemgr.load_Advance();
//                   Config.hr_empmgmt.onloadReset();
//                return true;
//                }else{
//                    return false;
//                }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//   }
}
