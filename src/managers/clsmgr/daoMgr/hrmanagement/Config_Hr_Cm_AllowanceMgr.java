
package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Config_Hr_Dm_Allowances;

public class Config_Hr_Cm_AllowanceMgr {
    
    public boolean insAllowance(Config_Hr_Dm_Allowances config_hr_dm_allowances){
        try {
          Config.sql = "insert into config_hr_allowance ("                                          
                    + "hra,"
                    + "da,"
                    + "medical,"
                    + "conveyance,"
                    + "travel,"
                    + "special,"
                    + "pf,"                       
                    + "designation_id )"
                    + "values (?,?,?,?,?,?,?,?)";  
          
          Config.pstmt = Config.conn.prepareStatement(Config.sql);                  
          Config.pstmt.setString(1, config_hr_dm_allowances.getHra());
          Config.pstmt.setString(2, config_hr_dm_allowances.getDa());
          Config.pstmt.setString(3, config_hr_dm_allowances.getMedical());          
          Config.pstmt.setString(4, config_hr_dm_allowances.getConveyance());
          Config.pstmt.setString(5, config_hr_dm_allowances.getTravel());
          Config.pstmt.setString(6, config_hr_dm_allowances.getSpecial());
          Config.pstmt.setString(7, config_hr_dm_allowances.getPf());          
          Config.pstmt.setString(8, config_hr_dm_allowances.getDesignation_id());
          
          int x = Config.pstmt.executeUpdate();
           
            if(x>0){
                Config.xml_config_hr_allowancemgr.load_AllowanceDistribution();
                Config.hr_home.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    
    public boolean upd_Allowance(Config_Hr_Dm_Allowances config_hr_dm_allowances){
        try {
            Config.sql = "update config_hr_allowance set"
                    + " hra = ? ,"                    
                    + " da = ? ,"
                    + " medical = ? ,"
                    + " travel = ? , "
                    + " special = ? ,"
                    + " pf = ? ,"                    
                    + " designation_id = ? "           
                    + " where allowance_id = '"+config_hr_dm_allowances.getAllowance_id()+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, config_hr_dm_allowances.getHra());
          Config.pstmt.setString(2, config_hr_dm_allowances.getDa());
          Config.pstmt.setString(3, config_hr_dm_allowances.getMedical());          
          Config.pstmt.setString(4, config_hr_dm_allowances.getConveyance());
          Config.pstmt.setString(5, config_hr_dm_allowances.getTravel());
          Config.pstmt.setString(6, config_hr_dm_allowances.getSpecial());
          Config.pstmt.setString(7, config_hr_dm_allowances.getPf());          
          Config.pstmt.setString(8, config_hr_dm_allowances.getDesignation_id());
            
            
            
            int x = Config.pstmt.executeUpdate();
            if(x>0) {
                Config.xml_config_hr_allowancemgr.load_AllowanceDistribution();
                Config.hr_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;            
        }
        
    
    }
    
    public boolean del_Allowance(String allowanceid) {
        try {          
            Config.sql = "delete from config_hr_allowance where allowance_id = '"+allowanceid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_hr_allowancemgr.load_AllowanceDistribution();
                   Config.hr_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
