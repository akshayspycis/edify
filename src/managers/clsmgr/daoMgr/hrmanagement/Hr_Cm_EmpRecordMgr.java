
package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Hr_Dm_EmpRecord;


public class Hr_Cm_EmpRecordMgr {
    public boolean insEmpRecord(Hr_Dm_EmpRecord hr_dm_emprecord){
        try {
          Config.sql = "insert into hr_emprecord ("                                       
                    + "fname,"                    
                    + "midname,"
                    + "lname,"
                    + "dob,"
                    + "fathername,"
                    + "gender,"
                    + "category,"
                    + "religion,"
                    + "qualification,"
                    + "otherQualification , "    
                    + "Raddress , " 
                    + "Rcity , " 
                    + "Rstate , " 
                    + "Rcountry , " 
                    + "Rpincode , " 
                    + "Paddress , " 
                    + "Pcity , " 
                    + "Pstate , " 
                    + "Pcountry , " 
                    + "Ppincode , " 
                    + "mobno , " 
                    + "contactno , " 
                    + "emailid , " 
                    + "doj , " 
                    + "department_id , " 
                    + "designation_id , "                  
                    + "basicpay ,"
                    + "identity_type ,"
                    + "identityno ,"
                    + "bank_accountno ,"
                    + "pf_accountno )"                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";  
          
          Config.pstmt = Config.conn.prepareStatement(Config.sql);       
          Config.pstmt.setString(1, hr_dm_emprecord.getFname());  
          Config.pstmt.setString(2, hr_dm_emprecord.getMidname());
          Config.pstmt.setString(3, hr_dm_emprecord.getLname());
          Config.pstmt.setString(4, hr_dm_emprecord.getDob());          
          Config.pstmt.setString(5, hr_dm_emprecord.getFathername());
          Config.pstmt.setString(6, hr_dm_emprecord.getGender());
          Config.pstmt.setString(7, hr_dm_emprecord.getCategory());
          Config.pstmt.setString(8, hr_dm_emprecord.getReligion());
          Config.pstmt.setString(9, hr_dm_emprecord.getQualification());
          Config.pstmt.setString(10, hr_dm_emprecord.getOtherQualification());
          Config.pstmt.setString(11, hr_dm_emprecord.getRaddress());
          Config.pstmt.setString(12, hr_dm_emprecord.getRcity());
          Config.pstmt.setString(13, hr_dm_emprecord.getRstate());
          Config.pstmt.setString(14, hr_dm_emprecord.getRcountry());
          Config.pstmt.setString(15, hr_dm_emprecord.getRpincode());
          Config.pstmt.setString(16, hr_dm_emprecord.getPaddress());
          Config.pstmt.setString(17, hr_dm_emprecord.getPcity());
          Config.pstmt.setString(18, hr_dm_emprecord.getPstate());
          Config.pstmt.setString(19, hr_dm_emprecord.getPcountry());
          Config.pstmt.setString(20, hr_dm_emprecord.getPpincode());
          Config.pstmt.setString(21, hr_dm_emprecord.getMobno());
          Config.pstmt.setString(22, hr_dm_emprecord.getContactno());
          Config.pstmt.setString(23, hr_dm_emprecord.getEmailid());
          Config.pstmt.setString(24, hr_dm_emprecord.getDoj());
          Config.pstmt.setString(25, hr_dm_emprecord.getDepartment_id());
          Config.pstmt.setString(26, hr_dm_emprecord.getDesignation_id());
          Config.pstmt.setString(27, hr_dm_emprecord.getBasicpay());
          Config.pstmt.setString(28, hr_dm_emprecord.getIdentity_type());
          Config.pstmt.setString(29, hr_dm_emprecord.getIdentityno());
          Config.pstmt.setString(30, hr_dm_emprecord.getBank_accountno());
          Config.pstmt.setString(31, hr_dm_emprecord.getPf_accountno());
          
          
          
          
          int x = Config.pstmt.executeUpdate();
           
            if(x>0){
                Config.xml_hr_emprecordmgr.load_EmpRecord();
                Config.hr_home.onloadReset();
                Config.hr_empmgmt.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    
    public boolean upd_EmpRecord(Hr_Dm_EmpRecord hr_dm_emprecord){
        try {
            Config.sql = "update hr_emprecord set "
                    + "fname = ? ,"
                    + " midname = ? ,"
                    + " lname = ? ,"
                    + " dob = ? ,"
                    + " fathername = ? ,"
                    + " gender = ? ,"
                    + " category = ? ,"
                    + " religion = ? ,"                              
                    + " qualification = ? ,"
                    + " otherQualification = ? ,"
                    + " Raddress = ? ,"
                    + " Rcity = ? ,"
                    + " Rstate = ? ,"
                    + " Rcountry = ? ,"
                    + " Rpincode = ? ,"
                    + " Paddress = ? ,"
                    + " Pcity = ? ,"
                    + " Pstate = ? ,"
                    + " Pcountry = ? ,"
                    + " Ppincode = ? ,"
                    + " mobno = ? ,"
                    + " contactno = ? ,"
                    + " emailid = ? ,"
                    + " doj = ? ,"
                    + " department_id = ? ,"
                    + " designation_id = ? ,"
                    + " basicpay = ? ,"
                    + " identity_type ?,"
                    + " identityno ? ,"
                    + " bank_accountno ? ,"
                    + " pf_accountno ? "
                    + " where emp_id = '"+hr_dm_emprecord.getEmp_id()+"'";
            
          Config.pstmt = Config.conn.prepareStatement(Config.sql);       
          Config.pstmt.setString(1, hr_dm_emprecord.getFname());  
          Config.pstmt.setString(2, hr_dm_emprecord.getMidname());
          Config.pstmt.setString(3, hr_dm_emprecord.getLname());
          Config.pstmt.setString(4, hr_dm_emprecord.getDob());          
          Config.pstmt.setString(5, hr_dm_emprecord.getFathername());
          Config.pstmt.setString(6, hr_dm_emprecord.getGender());
          Config.pstmt.setString(7, hr_dm_emprecord.getCategory());
          Config.pstmt.setString(8, hr_dm_emprecord.getReligion());
          Config.pstmt.setString(9, hr_dm_emprecord.getQualification());
          Config.pstmt.setString(10, hr_dm_emprecord.getOtherQualification());
          Config.pstmt.setString(11, hr_dm_emprecord.getRaddress());
          Config.pstmt.setString(12, hr_dm_emprecord.getRcity());
          Config.pstmt.setString(13, hr_dm_emprecord.getRstate());
          Config.pstmt.setString(14, hr_dm_emprecord.getRcountry());
          Config.pstmt.setString(15, hr_dm_emprecord.getRpincode());
          Config.pstmt.setString(16, hr_dm_emprecord.getPaddress());
          Config.pstmt.setString(17, hr_dm_emprecord.getPcity());
          Config.pstmt.setString(18, hr_dm_emprecord.getPstate());
          Config.pstmt.setString(19, hr_dm_emprecord.getPcountry());
          Config.pstmt.setString(20, hr_dm_emprecord.getPpincode());
          Config.pstmt.setString(21, hr_dm_emprecord.getMobno());
          Config.pstmt.setString(22, hr_dm_emprecord.getContactno());
          Config.pstmt.setString(23, hr_dm_emprecord.getEmailid());
          Config.pstmt.setString(24, hr_dm_emprecord.getDoj());
          Config.pstmt.setString(25, hr_dm_emprecord.getDepartment_id());
          Config.pstmt.setString(26, hr_dm_emprecord.getDesignation_id());
          Config.pstmt.setString(27, hr_dm_emprecord.getBasicpay());
          Config.pstmt.setString(28, hr_dm_emprecord.getIdentity_type());
          Config.pstmt.setString(29, hr_dm_emprecord.getIdentityno());
          Config.pstmt.setString(30, hr_dm_emprecord.getBank_accountno());
          Config.pstmt.setString(31, hr_dm_emprecord.getPf_accountno());
          
            
            
            int x = Config.pstmt.executeUpdate();
            if(x>0) {
                Config.xml_hr_emprecordmgr.load_EmpRecord();
                Config.hr_home.onloadReset();
                Config.hr_empmgmt.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;            
        }
        
    
    }
    
    public boolean del_EmpRecord(String empid) {
        try {          
            Config.sql = "delete from hr_emprecord where emp_id = '"+empid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_hr_emprecordmgr.load_EmpRecord();
                   Config.hr_home.onloadReset();
                   Config.hr_empmgmt.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
