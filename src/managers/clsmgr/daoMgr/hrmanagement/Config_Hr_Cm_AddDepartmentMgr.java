

package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Config_Hr_Dm_AddDepartment;


public class Config_Hr_Cm_AddDepartmentMgr {
    //method to insert category in database 
    public boolean insHr_Department(Config_Hr_Dm_AddDepartment hr_dm_department) {
        try {          
            Config.sql = "insert into config_hr_department ("
                    + "departmentname ) "
                    + "values (?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);       
            Config.pstmt.setString(1, hr_dm_department.getDepartmentname());
            

            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_config_hr_departmentmgr.load_Department();
                Config.hr_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {    
            ex.printStackTrace();
            return false;
        }
    }
    
    //method to update category in database 
    public boolean updHr_Department(Config_Hr_Dm_AddDepartment hr_dm_department) {
        try {
            Config.sql = "update config_hr_department set"
                    + " departmentname = ? "                    
                    + " where department_id = '"+hr_dm_department.getDepartment_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, hr_dm_department.getDepartmentname());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                Config.xml_config_hr_departmentmgr.load_Department();
                Config.hr_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean delHr_Department(String departmentid) {
        try {          
            Config.sql = "delete from config_hr_department where department_id = '"+departmentid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_hr_departmentmgr.load_Department();
                   Config.hr_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
