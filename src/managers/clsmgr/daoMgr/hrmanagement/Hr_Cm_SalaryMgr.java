

package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Hr_Dm_Salary;

public class Hr_Cm_SalaryMgr {
    public boolean insSalary(Hr_Dm_Salary hr_dm_salary){
        try {
          Config.sql = "insert into hr_salary ("                                          
                    + "emp_id,"                    
                    + "advance_id,"
                    + "emp_attendance_id,"
                    + "salarydate )"                    
                    + "values (?,?,?,?)";  
          
          Config.pstmt = Config.conn.prepareStatement(Config.sql);                  
          Config.pstmt.setString(1, hr_dm_salary.getEmp_id());          
          Config.pstmt.setString(2, hr_dm_salary.getAdvance_id());          
          Config.pstmt.setString(3, hr_dm_salary.getEmp_attendance_id());
          Config.pstmt.setString(4, hr_dm_salary.getSalarydate());
          
          
          int x = Config.pstmt.executeUpdate();
           
            if(x>0){
                Config.xml_hr_salarymgr.load_Salary();
                Config.hr_empmgmt.onloadReset();
            return true;
            }else{
            return false;   
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    
//    public boolean upd_Salary(Hr_Dm_Salary hr_dm_salary){
//        try {
//            Config.sql = "update hr_salary set"
//                    + " emp_id = ? ,"                                        
//                    + " advance_id = ? ,"
//                    + " emp_attendance_id = ? "
//                    + " salarydate = ? "                              
//                    + " where salary_id = '"+hr_dm_salary.getSalary_id()+"'";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);                                       
//            Config.pstmt.setString(1, hr_dm_salary.getEmp_id());          
//            Config.pstmt.setString(2, hr_dm_salary.getAdvance_id());          
//            Config.pstmt.setString(3, hr_dm_salary.getEmp_attendance_id());
//            Config.pstmt.setString(4, hr_dm_salary.getSalarydate());                    
//                        
//            int x = Config.pstmt.executeUpdate();
//            if(x>0) {
//                Config.xml_hr_salarymgr.load_Salary();
//                Config.hr_empmgmt.onloadReset();
//                return true;
//            } else{
//                return false;   
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;            
//        }
//        
//    
//    }
    
//    public boolean del_Salary(String salaryid) {
//        try {          
//            Config.sql = "delete from hr_salary where salary_id = '"+salaryid+"'";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);
//            
//            int x = Config.pstmt.executeUpdate();
//                if (x>0) {
//                   Config.xml_hr_salarymgr.load_Salary();
//                   Config.hr_empmgmt.onloadReset();
//                return true;
//                }else{
//                    return false;
//                }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//   }
}
