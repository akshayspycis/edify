

package managers.clsmgr.daoMgr.hrmanagement;

import managers.datamgr.configuration.Config;
import managers.datamgr.dao.hrmanagement.Config_Hr_Dm_AddDesignation;


public class Config_Hr_Cm_AddDesignationMgr {
    //method to insert category in database 
    public boolean insHr_Designation(Config_Hr_Dm_AddDesignation hr_dm_designation) {
        try {          
            Config.sql = "insert into config_hr_designation ("
                    + "designationname ) "
                    + "values (?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);       
            Config.pstmt.setString(1, hr_dm_designation.getDesignationname());
            

            int x = Config.pstmt.executeUpdate();            
            if(x>0) {
                Config.xml_config_hr_designationmgr.load_Designation();
                Config.hr_home.onloadReset();
                return true;
            } else {
                return false;   
            }
        } catch (Exception ex) {
            
            
            
            ex.printStackTrace();
            return false;
        }
    }
    
    //method to update category in database 
    public boolean updHr_Designation(Config_Hr_Dm_AddDesignation hr_dm_designation) {
        try {
            Config.sql = "update config_hr_designation set"
                    + " designationname = ? "                    
                    + " where designation_id = '"+hr_dm_designation.getDesignation_id()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);            
            Config.pstmt.setString(1, hr_dm_designation.getDesignationname());
            
            int x = Config.pstmt.executeUpdate();
            
            if(x>0) {
                Config.xml_config_hr_designationmgr.load_Designation();
                Config.hr_home.onloadReset();
                return true;
            } else{
                return false;   
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    
    public boolean delHr_Designation(String designationid) {
        try {          
            Config.sql = "delete from config_hr_designation where designation_id = '"+designationid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();
                if (x>0) {
                   Config.xml_config_hr_designationmgr.load_Designation();
                   Config.hr_home.onloadReset();
                return true;
                }else{
                    return false;
                }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
