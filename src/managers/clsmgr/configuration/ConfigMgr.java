package managers.clsmgr.configuration;

import java.sql.DriverManager;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_ClassMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_Class_SectionMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_ElectiveGroupMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_ElectiveGroupSubjectMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_SchoolCategoryMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_SessionMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_SubjectMgr;

import managers.clsmgr.daoMgr.examination.Config_Exam_Cm_ClassSub;
import managers.clsmgr.daoMgr.examination.Config_Exam_Cm_Grade;
import managers.clsmgr.daoMgr.examination.Config_Exam_Cm_Type;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_AmountMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_DamageTypeMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FeeAmountMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FeeNamMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FeeTypeMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FineAmountMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FineTypeMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_TypeMgr;
import managers.clsmgr.daoMgr.fees.Fee_Cm_FeeCollection;
import managers.clsmgr.daoMgr.hrmanagement.Config_Hr_Cm_AddDepartmentMgr;
import managers.clsmgr.daoMgr.hrmanagement.Config_Hr_Cm_AddDesignationMgr;
import managers.clsmgr.daoMgr.hrmanagement.Config_Hr_Cm_AllowanceMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_AdvanceMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_DeductionMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_EmpAttendanceMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_EmpRecordMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_GenerateSalaryMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_SalaryMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddBookDurationMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddCategoryMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddIssuableBookMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddOverDueChargeMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddSubCategoryMgr;
import managers.clsmgr.daoMgr.librarymanagement.Lm_Cm_BookDetailsMgr;
import managers.clsmgr.daoMgr.storesandinventory.Si_Cm_CategoryMgr;
import managers.clsmgr.daoMgr.storesandinventory.Si_Cm_SubCategoryMgr;
import managers.clsmgr.daoMgr.storesandinventory.Si_Cm_SupplierProfileMgr;
import managers.clsmgr.daoMgr.student.Stu_Cm_Registration;
import managers.clsmgr.daoMgr.transport.Config_Tr_Cm_RouteDetailMgr;
import managers.clsmgr.daoMgr.transport.Config_Tr_Cm_StopDetailMgr;
import managers.clsmgr.daoMgr.transport.Config_Tr_Cm_VehicleDetailMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ClassMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ClassSectionMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ElectiveGroupMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ElectiveGroupSubjectMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ResultTypeMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_SchoolCategoryMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_SessionMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_SubjectMgr;
import managers.clsmgr.xmlMgr.examination.Xml_Config_ExamClassSubMgr;
import managers.clsmgr.xmlMgr.examination.Xml_Config_ExamGradeMgr;
import managers.clsmgr.xmlMgr.examination.Xml_Config_ExamTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_AmountMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_DamageTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FeeAmountMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FeeNamMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FeeTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FineAmountMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FineTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_TypeMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Config_Hr_AllowanceMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Config_Hr_DepartmentMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Config_Hr_DesignationMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_AdvanceMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_DeductionMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_EmpAttendanceMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_EmpRecordMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_GenerateSalaryMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_SalaryMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_BookDurationMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_CategoryMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_IssuableBookMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_OverdueChargeMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_SubcategoryMgr;
import managers.clsmgr.xmlMgr.library.Xml_Lm_BookDetailsMgr;
import managers.clsmgr.xmlMgr.storeandinventory.Xml_Si_CategoryMgr;
import managers.clsmgr.xmlMgr.storeandinventory.Xml_Si_SubCategoryMgr;
import managers.clsmgr.xmlMgr.storeandinventory.Xml_SupplierProfileMgr;
import managers.clsmgr.xmlMgr.student.Xml_Stu_AddmissionMgr;
import managers.clsmgr.xmlMgr.student.Xml_Stu_RegistrationMgr;
import managers.clsmgr.xmlMgr.transport.Xml_Config_Tr_RouteDetailMgr;
import managers.clsmgr.xmlMgr.transport.Xml_Config_Tr_StopDetailMgr;
import managers.clsmgr.xmlMgr.transport.Xml_Config_Tr_VehicleDetailMgr;
import managers.datamgr.configuration.Config;
import modules.admin.repository.Admin_ClassNewEntry;
import modules.admin.repository.Admin_ElectiveGroSub;
import modules.admin.repository.Admin_ElectiveGroup;
import modules.admin.repository.Admin_Home;
import modules.admin.repository.Admin_SchoolCategoryNewEntry;
import modules.admin.repository.Admin_SectionNewEntry;
import modules.admin.repository.Admin_SessionNewEntry;
import modules.admin.repository.Admin_SubjectNewEntry;
import modules.admin.repository.Admin_ViewClass;
import modules.admin.repository.Admin_ViewElectiveGroup;
import modules.admin.repository.Admin_ViewElectiveGroupSubject;
import modules.admin.repository.Admin_ViewSchoolCategory;
import modules.admin.repository.Admin_ViewSection;
import modules.admin.repository.Admin_ViewSession;
import modules.admin.repository.Admin_ViewSubject;
import modules.dashboard.Dashlets;
import modules.dashboard.Home;
import modules.dashboard.panel_home;
import modules.dashboard.panel_login;
import modules.exam.Ex_Repository.Ex_ClassSubjectMarks;
import modules.exam.Ex_Repository.Ex_Grade;
import modules.exam.Ex_Repository.Ex_TypeNewEntry;
import modules.exam.Ex_Repository.Ex_View_ClassSubjectMarks;
import modules.exam.Ex_Repository.Ex_View_Grade;
import modules.exam.Ex_Repository.Ex_View_TypeNewEntry;
import modules.exam.Ex_Repository.Exam_Home;
import modules.exam.Ex_ResultEntry;
import modules.fees.Managment.Fee_Managment;
import modules.fees.Managment.Fee_Recipt;
import modules.fees.Managment.Fees_CollectNew;
import modules.fees.Managment.Fees_RACollectNew;
import modules.fees.Repositories.Fee_FeeAmount;
import modules.fees.Repositories.Fee_FeeNam;
import modules.fees.Repositories.Fee_FeeType;
import modules.fees.Repositories.Fee_Home1;
import modules.fees.Repositories.Fee_ViewFeeAmount;
import modules.fees.Repositories.Fee_ViewFeeNam;
import modules.fees.Repositories.Fee_ViewFeeType;
import modules.fees.Repositories.demo;
import modules.hrmanagement.Management.Hr_AttendanceEntry;
import modules.hrmanagement.Management.Hr_EmpMgmt;
import modules.hrmanagement.Management.Hr_EmpRegistration;
import modules.hrmanagement.Management.Hr_GenerateSalary;
import modules.hrmanagement.Repository.Hr_Add_Department;
import modules.hrmanagement.Repository.Hr_Add_Designation;
import modules.hrmanagement.Repository.Hr_Allowance_Distribution;
import modules.hrmanagement.Repository.Hr_Home;
import modules.hrmanagement.Repository.Hr_View_Allowance;
import modules.hrmanagement.Repository.Hr_View_Department;
import modules.hrmanagement.Repository.Hr_View_Designation;
import modules.librarymanagement.repository.Lm_AddBookDuration;
import modules.librarymanagement.repository.Lm_AddCategory;
import modules.librarymanagement.repository.Lm_AddIssuableBook;
import modules.librarymanagement.repository.Lm_AddOverDueCharge;
import modules.librarymanagement.repository.Lm_AddSubCategory;
import modules.librarymanagement.repository.Lm_Home;
import modules.librarymanagement.repository.Lm_NewEntry;
import modules.librarymanagement.repository.Lm_ViewBookDuration;
import modules.librarymanagement.repository.Lm_ViewCategory;
import modules.librarymanagement.repository.Lm_ViewEntry;
import modules.librarymanagement.repository.Lm_ViewIssuableBook;
import modules.librarymanagement.repository.Lm_ViewOverDueCharge;
import modules.librarymanagement.repository.Lm_ViewSubcategory;
import modules.storesandinventory.repository.Si_CategoryNewEntry;
import modules.storesandinventory.repository.Si_Home;
import modules.storesandinventory.repository.Si_SubCategoryNewEntry;
import modules.storesandinventory.repository.Si_SupplierNewEntry;
import modules.storesandinventory.repository.Si_ViewCategory;
import modules.storesandinventory.repository.Si_ViewSubCategory;
import modules.storesandinventory.repository.Si_ViewSupplier;
import modules.studentmanagment.StuMgmt_Home;
import modules.studentmanagment.admission.Admission;
import modules.studentmanagment.admission.Stu_AdmissionHome;
import modules.studentmanagment.admission.View_Admission;
import modules.studentmanagment.registration.Student_Registration;
import modules.studentmanagment.registration.View_Student_Registration;
import modules.studentmanagment.report.Stu_Report;
import modules.transport.Tr_Repository.Tr_Home;
import modules.transport.Tr_Repository.Tr_RouteDetail;
import modules.transport.Tr_Repository.Tr_StopDetail;
import modules.transport.Tr_Repository.Tr_VehicleDetail;
import modules.transport.Tr_VehicleInformation;

public class ConfigMgr {    

    public boolean loadDatabase() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String userName = "root";
            String password = "root";
            String url = "jdbc:mysql://localhost:3307/edify";
            Config.conn = DriverManager.getConnection(url, userName, password);
            Config.stmt = Config.conn.createStatement();            
            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }


    
    public boolean loadClassManager() {
        try {  
            
            
            Config.endn = new EnDn();
//            Student 
            
            Config.xml_stu_registrationmgr = new Xml_Stu_RegistrationMgr();
            Config.xml_stu_addmissionmgr = new Xml_Stu_AddmissionMgr();
            
            Config.stu_cm_registration = new Stu_Cm_Registration();
            Config.stu_cm_admission = new managers.clsmgr.daoMgr.student.Stu_Cm_Admission();
            
            
            //Library
            Config.lm_cm_bookdetailsmgr = new Lm_Cm_BookDetailsMgr();
            Config.config_lm_cm_addsubcategorymgr = new Config_Lm_Cm_AddSubCategoryMgr();
            Config.config_lm_cm_addcategorymgr = new Config_Lm_Cm_AddCategoryMgr();
            Config.config_lm_cm_addissuablebookmgr = new Config_Lm_Cm_AddIssuableBookMgr();
            Config.config_lm_cm_addbookdurationmgr = new Config_Lm_Cm_AddBookDurationMgr();
            Config.config_lm_cm_addoverduechargemgr = new Config_Lm_Cm_AddOverDueChargeMgr();
            
            //Library
            Config.xml_config_lm_categorymgr = new Xml_Config_Lm_CategoryMgr();
            Config.xml_config_lm_subcategorymgr = new Xml_Config_Lm_SubcategoryMgr();
            Config.xml_lm_bookdetailsmgr = new Xml_Lm_BookDetailsMgr();
            Config.xml_config_lm_issuablebookmgr = new Xml_Config_Lm_IssuableBookMgr();
            Config.xml_config_lm_bookdurationmgr = new Xml_Config_Lm_BookDurationMgr();
            Config.xml_config_lm_overduechargemgr = new Xml_Config_Lm_OverdueChargeMgr();
            

            //Hr XML
            Config.xml_config_hr_departmentmgr = new Xml_Config_Hr_DepartmentMgr();
            Config.xml_config_hr_designationmgr = new Xml_Config_Hr_DesignationMgr();
            Config.xml_config_hr_allowancemgr = new Xml_Config_Hr_AllowanceMgr();
            Config.xml_hr_emprecordmgr = new Xml_Hr_EmpRecordMgr();
            Config.xml_hr_empattendancemgr = new Xml_Hr_EmpAttendanceMgr();
            Config.xml_hr_salarymgr = new Xml_Hr_SalaryMgr();
            Config.xml_hr_advancemgr = new Xml_Hr_AdvanceMgr();
            Config.xml_hr_deductionmgr = new Xml_Hr_DeductionMgr();
            Config.xml_hr_generatesalarymgr = new Xml_Hr_GenerateSalaryMgr();
            
//            HR ClassMgr
            Config.config_hr_cm_add_departmentmgr = new Config_Hr_Cm_AddDepartmentMgr();
            Config.config_hr_cm_add_designationMgr = new Config_Hr_Cm_AddDesignationMgr();
            Config.config_hr_cm_allowancemgr = new Config_Hr_Cm_AllowanceMgr();            
            Config.hr_cm_emprecordmgr = new Hr_Cm_EmpRecordMgr();
            Config.hr_cm_empattendancemgr = new Hr_Cm_EmpAttendanceMgr();
            Config.Hr_Cm_salarymgr = new Hr_Cm_SalaryMgr();
            Config.hr_cm_advancemgr = new Hr_Cm_AdvanceMgr();
            Config.hr_cm_deductionmgr = new Hr_Cm_DeductionMgr();
            Config.hr_cm_generatesalarymgr = new Hr_Cm_GenerateSalaryMgr();
            
            
            
//            Stock Insert
            Config.si_cm_supplierprofilemgr = new Si_Cm_SupplierProfileMgr();
            Config.si_cm_categorymgr=new Si_Cm_CategoryMgr();
//            Config.si_cm_stockinsertmgr = new Si_Cm_StockInsertMgr();
            Config.si_cm_subcategorymgr=new Si_Cm_SubCategoryMgr();
            Config.xml_supplierprofilemgr= new Xml_SupplierProfileMgr();
            Config.xml_si_categorymgr=new Xml_Si_CategoryMgr();
            Config.xml_si_subcategorymgr=new Xml_Si_SubCategoryMgr();
            
//            Transport
            Config.xml_config_tr_vehicledetailmgr = new Xml_Config_Tr_VehicleDetailMgr();
            Config.xml_config_tr_routedetailmgr = new Xml_Config_Tr_RouteDetailMgr();
            Config.xml_config_tr_stopdetailmgr = new Xml_Config_Tr_StopDetailMgr();
            
            Config.config_tr_cm_vehicledetailmgr = new Config_Tr_Cm_VehicleDetailMgr();
            Config.config_tr_cm_routedetailmgr = new Config_Tr_Cm_RouteDetailMgr();
            Config.config_tr_cm_stopdetailmgr = new Config_Tr_Cm_StopDetailMgr();
                    

            
//            Admin Xml
            
            Config.xml_config_ad_schoolcategorymgr=new Xml_Config_Ad_SchoolCategoryMgr();
            Config.xml_config_ad_classmgr =new Xml_Config_Ad_ClassMgr();
            Config.xml_config_ad_classsectionmgr=new Xml_Config_Ad_ClassSectionMgr();
            Config.xml_config_ad_subjectmgr=new Xml_Config_Ad_SubjectMgr();
            Config.xml_config_ad_electivegroupmgr=new Xml_Config_Ad_ElectiveGroupMgr();
            Config.xml_config_ad_electivegroupsubjectmgr=new Xml_Config_Ad_ElectiveGroupSubjectMgr();
            Config.xml_config_ad_resulttypemgr=new Xml_Config_Ad_ResultTypeMgr();
            Config.xml_config_ad_sessionmgr=new Xml_Config_Ad_SessionMgr();
            
//            Admin Class managers
            
            Config.config_ad_cm_sessionmgr = new Config_Ad_Cm_SessionMgr();
            Config.config_ad_cm_schoolcategorymgr = new Config_Ad_Cm_SchoolCategoryMgr();
            Config.config_ad_cm_classmgr = new Config_Ad_Cm_ClassMgr();
            Config.config_ad_cm_class_sectionmgr = new Config_Ad_Cm_Class_SectionMgr();
            Config.config_ad_cm_subjectmgr = new Config_Ad_Cm_SubjectMgr();
            Config.config_ad_cm_electivegroupmgr = new Config_Ad_Cm_ElectiveGroupMgr();
            Config.config_ad_cm_electivegroupsubjectmgr = new Config_Ad_Cm_ElectiveGroupSubjectMgr();
            
//            Exam Type
            Config.xml_config_examtypemgr = new Xml_Config_ExamTypeMgr();
            Config.xml_config_examgrademgr = new Xml_Config_ExamGradeMgr();
            Config.xml_config_examclasssubmgr = new Xml_Config_ExamClassSubMgr();
            
            Config.config_exam_cm_type = new Config_Exam_Cm_Type();
            Config.config_exam_cm_grade = new Config_Exam_Cm_Grade();
            Config.config_exam_cm_classsub = new Config_Exam_Cm_ClassSub();


//          Fees
            Config.xml_config_fee_feetypemgr = new Xml_Config_Fee_FeeTypeMgr();
            Config.xml_config_fee_feenammgr = new Xml_Config_Fee_FeeNamMgr();
            Config.xml_config_fee_feeamountmgr = new Xml_Config_Fee_FeeAmountMgr();
            
            Config.config_fee_cm_feetypemgr = new Config_Fee_Cm_FeeTypeMgr();
            Config.config_fee_cm_feenammgr = new Config_Fee_Cm_FeeNamMgr();
            Config.config_fee_cm_feeamountmgr = new Config_Fee_Cm_FeeAmountMgr();
            Config.fee_Cm_FeeCollection = new Fee_Cm_FeeCollection();
            
            
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadForms() {
        try {
            //dashboard
            Config.dashlets = new Dashlets(null, true);
            Config.panel_login = new panel_login();
            Config.panel_home = new panel_home();
            Config.homepage = new Home();   

//            Admin
            Config.ad_home = new Admin_Home(null, true);
            
            Config.ad_classnewentry = new Admin_ClassNewEntry(null, true);
            Config.ad_electivegroup =new Admin_ElectiveGroup(null, true);
            Config.ad_electivegroupsubject=new Admin_ElectiveGroSub(null, true);
            Config.ad_scnewentry =new Admin_SchoolCategoryNewEntry(null, true);
            Config.ad_sectionnewentry=new Admin_SectionNewEntry(null, true);
            Config.ad_subjectnewentry= new Admin_SubjectNewEntry(null, true);
            Config.ad_sessionnewentry =new Admin_SessionNewEntry(null, true);
            
            Config.ad_viewclass=new Admin_ViewClass(null, true);
            Config.ad_viewelectivegroup=new Admin_ViewElectiveGroup(null, true);
            Config.ad_viewelectivegroupsubject=new Admin_ViewElectiveGroupSubject(null, true);
            Config.ad_viewschoolcategory=new Admin_ViewSchoolCategory(null, true);
            Config.ad_viewsection=new Admin_ViewSection(null, true);
            Config.ad_viewsession=new Admin_ViewSession(null, true);
            Config.ad_viewsubject=new Admin_ViewSubject(null, true);

            //Hr
            Config.hr_home = new Hr_Home(null, true);
            Config.hr_add_department = new Hr_Add_Department(null, true);
            Config.hr_add_designation = new Hr_Add_Designation(null, true);
            Config.hr_allowance_distribution = new Hr_Allowance_Distribution(null, true);
            Config.hr_view_department = new Hr_View_Department(null, true);
            Config.hr_view_designation = new Hr_View_Designation(null, true);
            Config.hr_view_allowance = new Hr_View_Allowance(null, true);
            Config.hr_empregistration = new Hr_EmpRegistration(null, true);
            Config.hr_empmgmt = new Hr_EmpMgmt(null, true);
            Config.hr_attendanceEntry = new Hr_AttendanceEntry(null, true);
            Config.hr_generatesalary = new Hr_GenerateSalary(null, true);
            
//            Exam
            Config.exam_home = new Exam_Home();
            Config.ex_typenewentry = new Ex_TypeNewEntry(null, true);
            Config.ex_grade = new Ex_Grade(null, true);
            Config.ex_classsubjectmarks = new Ex_ClassSubjectMarks(null, true);
            Config.ex_view_typenewentry = new Ex_View_TypeNewEntry(null, true);
            Config.ex_view_grade = new Ex_View_Grade(null, true);
            Config.ex_view_classsubmarks = new Ex_View_ClassSubjectMarks(null, true);
            
            
            Config.ex_resultentry = new Ex_ResultEntry();
            
            //library management
            Config.lm_repository_home = new Lm_Home();
            Config.lm_newentry = new Lm_NewEntry(null, true);
            Config.lm_viewentry = new Lm_ViewEntry(null, true);
            Config.lm_addsubcategory =new Lm_AddSubCategory(null,true);
            Config.lm_addcategory = new Lm_AddCategory(null,true);
            Config.lm_viewcategory = new Lm_ViewCategory(null, true);
            Config.lm_viewsubcategory = new Lm_ViewSubcategory(null, true);
            Config.lm_addbookduration = new Lm_AddBookDuration(null,true);
            Config.lm_addissuablebook = new Lm_AddIssuableBook(null,true);
            Config.lm_addoverduecharge = new Lm_AddOverDueCharge(null,true);
            Config.lm_viewbookduration = new Lm_ViewBookDuration(null,true);
            Config.lm_viewissuablebook = new Lm_ViewIssuableBook(null,true);
            Config.lm_viewoverduecharge = new Lm_ViewOverDueCharge(null,true);           
             //Stock And Inventory
            Config.si_home = new Si_Home();
            Config.si_subcategorynewentry = new Si_SubCategoryNewEntry(null, true);
            Config.si_categorynewentry=new Si_CategoryNewEntry(null, true);
            Config.si_suppliernewentry=new Si_SupplierNewEntry(null, true);
            Config.si_viewcategory=new Si_ViewCategory(null, true);
            Config.si_viewsupplier=new Si_ViewSupplier(null, true);
            Config.si_viewsubcategory=new Si_ViewSubCategory(null, true);  
            
            
            //Transport
            Config.tr_home = new Tr_Home();
            Config.tr_vehicledetail = new Tr_VehicleDetail(null, true);
            Config.tr_routedetail = new Tr_RouteDetail(null, true);
            Config.tr_stopdetail = new Tr_StopDetail(null, true);
            Config.tr_vehicleinformation = new Tr_VehicleInformation(null, true);
            
//            Student Managment
            Config.stumgmt_home = new StuMgmt_Home();
            Config.student_registration = new Student_Registration();
            Config.admission = new Admission(null, true);
            Config.stu_admissionhome = new Stu_AdmissionHome(null, true);
            Config.view_stu_registration = new View_Student_Registration();
            Config.view_admission = new View_Admission(null, true);
            
            Config.stu_Report = new Stu_Report(null, true);
            
//            Fees
            Config.fee_home1 = new Fee_Home1();
            Config.fee_feetype = new Fee_FeeType(null, true);
            Config.fee_feenam = new Fee_FeeNam(null, true);
            Config.fee_feeamount = new Fee_FeeAmount(null, true);
            
            Config.fee_managment = new Fee_Managment(null, true);
            Config.fees_racollectnew = new Fees_RACollectNew(null, true);
            Config.fees_collectnew = new Fees_CollectNew(null, true);
            Config.fee_Recipt = new Fee_Recipt(null, true);
            
            Config.fee_viewfeetype = new Fee_ViewFeeType(null, true);
            Config.fee_viewfeenam = new Fee_ViewFeeNam(null, true);
            Config.fee_viewfeeamount = new Fee_ViewFeeAmount(null, true);
            Config.Demo = new demo(null, true);
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true  ;
    }      
}
