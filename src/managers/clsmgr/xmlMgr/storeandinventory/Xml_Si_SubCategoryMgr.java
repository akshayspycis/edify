package managers.clsmgr.xmlMgr.storeandinventory;

import java.io.File;
import java.sql.ResultSetMetaData;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


public class Xml_Si_SubCategoryMgr {
            
    public File file = null;    
    public DocumentBuilder document_builder = null;    
    public DOMSource dom_source = null;
    public StreamResult result = null;
    public Document doc = null;
    public Element element = null;
    public Transformer transformer = null;
    public XPath xpath = null;
    public XPathExpression xpath_exp = null;

    public Xml_Si_SubCategoryMgr(){
        try {
            file = new File("src\\managers\\datamgr\\xml\\Si_subcategory");        
            document_builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = document_builder.newDocument();
            element = doc.createElement("Edify");
            doc.appendChild(element);            
            transformer = TransformerFactory.newInstance().newTransformer();
            xpath = XPathFactory.newInstance().newXPath();
            
            load_Si_SubCategory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    
    }

    public boolean load_Si_SubCategory() {
        try {
            Config.sql = "select * from config_si_subcategory";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                Element si_subcategory = doc.createElement("si_subcategory");
                element.appendChild(si_subcategory);

                ResultSetMetaData rsmd = Config.rs.getMetaData();                
                si_subcategory.setAttribute("subcategory_id", Config.rs.getString("subcategory_id"));
                for (int i = 2; i <= rsmd.getColumnCount(); i++) {
                    Node node = doc.createElement(rsmd.getColumnName(i));
                    if (Config.rs.getString(rsmd.getColumnName(i)) != null) {
                        node.appendChild(doc.createTextNode(Config.rs.getString(rsmd.getColumnName(i))));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }                    
                   si_subcategory.appendChild(node);
                }
            }
            
            transformer.transform(new DOMSource(doc), new StreamResult(file));            
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

 
    
}
