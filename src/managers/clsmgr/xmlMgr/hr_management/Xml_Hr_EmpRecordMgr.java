

package managers.clsmgr.xmlMgr.hr_management;

import java.io.File;
import java.sql.ResultSetMetaData;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Xml_Hr_EmpRecordMgr {
    public File file = null;    
    public DocumentBuilder document_builder = null;    
    public DOMSource dom_source = null;
    public StreamResult result = null;
    public Document doc = null;
    public Element element = null;
    public Transformer transformer = null;
    public XPath xpath = null;
    public XPathExpression xpath_exp = null;
    
    public Xml_Hr_EmpRecordMgr(){
        try {
            file = new File("src\\managers\\datamgr\\xml\\Hr_EmpRecord");        
            document_builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = document_builder.newDocument();
            element = doc.createElement("HumanResource");
            doc.appendChild(element);            
            transformer = TransformerFactory.newInstance().newTransformer();
            xpath = XPathFactory.newInstance().newXPath();
            
            load_EmpRecord();
        } catch (Exception e) {
            e.printStackTrace();
        }
}

    public boolean load_EmpRecord() {
        try {
            NodeList nList = Config.xml_hr_emprecordmgr.doc.getElementsByTagName("emprecord");
            int len = nList.getLength();
            for (int i = 0; i < len; i++) {
                Node nNode = nList.item(0);
                nNode.getParentNode().removeChild(nNode);
            }
        } catch (Exception e) {
        }
        try {
            Config.sql = "select * from hr_emprecord";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            int a = 0;
            while (Config.rs.next()) {
                a++;
                Element hr_emprecord = doc.createElement("emprecord");
                element.appendChild(hr_emprecord);
                ResultSetMetaData rsmd = Config.rs.getMetaData();                
                hr_emprecord.setAttribute("emp_id", Config.rs.getString("emp_id"));
                hr_emprecord.setAttribute("sno", String.valueOf(a));
                for (int i = 2; i <= rsmd.getColumnCount(); i++) {
                    Node node = doc.createElement(rsmd.getColumnName(i));
                    if (Config.rs.getString(rsmd.getColumnName(i)) != null) {
                        node.appendChild(doc.createTextNode(Config.rs.getString(rsmd.getColumnName(i))));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }                    
                    hr_emprecord.appendChild(node);
                }
            }
            
            transformer.transform(new DOMSource(doc), new StreamResult(file));            
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
