package managers.clsmgr.xmlMgr.library;

import java.io.File;
import java.sql.ResultSetMetaData;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Xml_Lm_BookDetailsMgr {
     public File file = null;    
    public DocumentBuilder document_builder = null;    
    public DOMSource dom_source = null;
    public StreamResult result = null;
    public Document doc = null;
    public Element element = null;
    public Transformer transformer = null;
    public XPath xpath = null;
    public XPathExpression xpath_exp = null;

    public Xml_Lm_BookDetailsMgr() {
        try {
            file = new File("src\\managers\\datamgr\\xml\\Lm_bookdetails");        
            document_builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = document_builder.newDocument();
            element = doc.createElement("Library");
            doc.appendChild(element);            
            transformer = TransformerFactory.newInstance().newTransformer();
            xpath = XPathFactory.newInstance().newXPath();
            
            load_BookDetails();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    }

    public boolean load_BookDetails() {
        try {
            NodeList nList = Config.xml_lm_bookdetailsmgr.doc.getElementsByTagName("bookdetails");
            int len = nList.getLength();
            for (int i = 0; i < len; i++) {
                Node nNode = nList.item(0);
                nNode.getParentNode().removeChild(nNode);
            }
        } catch (Exception e) {
        }
        try {
            Config.sql = "select * from lm_bookdetails";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            int a= 0;
            while (Config.rs.next()) {
                a++;
                Element book_details = doc.createElement("lm_bookdetails");
                element.appendChild(book_details);

                ResultSetMetaData rsmd = Config.rs.getMetaData();
                book_details.setAttribute("sno", String.valueOf(a));
                book_details.setAttribute("book_id", Config.rs.getString("book_id"));                
                for (int i = 2; i <= rsmd.getColumnCount(); i++) {
                    Node node = doc.createElement(rsmd.getColumnName(i));
                    if (Config.rs.getString(rsmd.getColumnName(i)) != null) {
                        node.appendChild(doc.createTextNode(Config.rs.getString(rsmd.getColumnName(i))));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }                    
                    book_details.appendChild(node);
                }
            }
            
            transformer.transform(new DOMSource(doc), new StreamResult(file));            
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    }
    

