
package managers.clsmgr.xmlMgr.library;

import java.io.File;
import java.sql.ResultSetMetaData;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Xml_Config_Lm_BookDurationMgr {
    public File file = null;    
    public DocumentBuilder document_builder = null;    
    public DOMSource dom_source = null;
    public StreamResult result = null;
    public Document doc = null;
    public Element element = null;
    public Transformer transformer = null;
    public XPath xpath = null;
    public XPathExpression xpath_exp = null;  
    
    public Xml_Config_Lm_BookDurationMgr(){
        try {
            file = new File("src\\managers\\datamgr\\xml\\Lm_BookDuration");        
            document_builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = document_builder.newDocument();
            element = doc.createElement("Library");
            doc.appendChild(element);            
            transformer = TransformerFactory.newInstance().newTransformer();
            xpath = XPathFactory.newInstance().newXPath();
            
            load_BookDuration();
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean load_BookDuration() {
     try {
            NodeList nList = Config.xml_config_lm_bookdurationmgr.doc.getElementsByTagName("bookduration");
            int len = nList.getLength();
            for (int i = 0; i < len; i++) {
                Node nNode = nList.item(0);
                nNode.getParentNode().removeChild(nNode);
            }
        } catch (Exception e) {
        }
        try {
            Config.sql = "select * from config_lm_addduration";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            int a =0;
            while (Config.rs.next()) {
                a++;
              
                Element book_duration = doc.createElement("bookduration");
                element.appendChild(book_duration);

                ResultSetMetaData rsmd = Config.rs.getMetaData();  
                book_duration.setAttribute("sno", String.valueOf(a));
                book_duration.setAttribute("duration_id", Config.rs.getString("duration_id"));
                for (int i = 2; i <= rsmd.getColumnCount(); i++) {
                    Node node = doc.createElement(rsmd.getColumnName(i));
                    if (Config.rs.getString(rsmd.getColumnName(i)) != null) {
                        node.appendChild(doc.createTextNode(Config.rs.getString(rsmd.getColumnName(i))));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }                    
                    book_duration.appendChild(node);
                }
            }
            
            transformer.transform(new DOMSource(doc), new StreamResult(file));            
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
