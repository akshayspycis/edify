package managers.clsmgr.xmlMgr.examination;

import java.io.File;
import java.sql.ResultSetMetaData;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ashok
 */
public class Xml_Config_ExamTypeMgr {

    public File file = null;    
    public DocumentBuilder document_builder = null;    
    public DOMSource dom_source = null;
    public StreamResult result = null;
    public Document doc = null;
    public Element element = null;
    public Transformer transformer = null;
    public XPath xpath = null;
    public XPathExpression xpath_exp = null;

    public Xml_Config_ExamTypeMgr() {
        
         try {
            file = new File("src\\managers\\datamgr\\xml\\Config_Exam_Type");        
            document_builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = document_builder.newDocument();
            element = doc.createElement("Examination");
            doc.appendChild(element);            
            transformer = TransformerFactory.newInstance().newTransformer();
            xpath = XPathFactory.newInstance().newXPath();
            
            load_ExamType();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    
    }
    
    public boolean load_ExamType() {
        try {
            NodeList nList = Config.xml_config_examtypemgr.doc.getElementsByTagName("config_exam_type");
            int len = nList.getLength();
            for (int i = 0; i < len; i++) {
                Node nNode = nList.item(0);
                nNode.getParentNode().removeChild(nNode);
            }
        } catch (Exception e) {
       }
        try {
            Config.sql = "select * from config_exam_type";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            int a = 0;
            while (Config.rs.next()) {
                a++;
                Element Exam_type = doc.createElement("config_exam_type");
                element.appendChild(Exam_type);
                
                ResultSetMetaData rsmd = Config.rs.getMetaData(); 
                
                Exam_type.setAttribute("exam_id", Config.rs.getString("exam_id"));
                Exam_type.setAttribute("sno", String.valueOf(a));
                for (int i = 2; i <= rsmd.getColumnCount(); i++) {
                    Node node = doc.createElement(rsmd.getColumnName(i));
                    if (Config.rs.getString(rsmd.getColumnName(i)) != null) {
                        node.appendChild(doc.createTextNode(Config.rs.getString(rsmd.getColumnName(i))));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }                    
                    Exam_type.appendChild(node);
                }
            }
            
            transformer.transform(new DOMSource(doc), new StreamResult(file));            
            
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
        
    }
        
    
    

