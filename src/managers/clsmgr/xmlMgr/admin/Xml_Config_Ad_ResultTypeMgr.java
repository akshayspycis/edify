

package managers.clsmgr.xmlMgr.admin;

import java.io.File;
import java.sql.ResultSetMetaData;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Xml_Config_Ad_ResultTypeMgr {
        public File file = null;    
    public DocumentBuilder document_builder = null;    
    public DOMSource dom_source = null;
    public StreamResult result = null;
    public Document doc = null;
    public Element element = null;
    public Transformer transformer = null;
    public XPath xpath = null;
    public XPathExpression xpath_exp = null;

    public Xml_Config_Ad_ResultTypeMgr(){
        try {
            file = new File("src\\managers\\datamgr\\xml\\config_ad_result_type");        
            document_builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = document_builder.newDocument();
            element = doc.createElement("Edify");
            doc.appendChild(element);            
            transformer = TransformerFactory.newInstance().newTransformer();
            xpath = XPathFactory.newInstance().newXPath();
            
            load_Ad_ResultType();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    
    }

    public boolean load_Ad_ResultType() {
        try {
            NodeList nList = Config.xml_config_ad_resulttypemgr.doc.getElementsByTagName("config_ad_result_type");
            int ctr = nList.getLength();
            for (int i = 0; i < ctr; i++) {                
                Node nNode = nList.item(0);  
                nNode.getParentNode().removeChild(nNode);
            }            
        } catch (Exception e) {} 
        try {
            Config.sql = "select * from config_ad_result_type";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            int ctr = 0;
            while (Config.rs.next()) {
                ctr++;
                Element ad_resulttype = doc.createElement("config_ad_result_type");
                element.appendChild(ad_resulttype);
                ResultSetMetaData rsmd = Config.rs.getMetaData();                
                ad_resulttype.setAttribute("sno", String.valueOf(ctr));
                ad_resulttype.setAttribute("result_id", Config.rs.getString("result_id"));
                for (int i = 2; i <= rsmd.getColumnCount(); i++) {
                    Node node = doc.createElement(rsmd.getColumnName(i));
                    if (Config.rs.getString(rsmd.getColumnName(i)) != null) {
                        node.appendChild(doc.createTextNode(Config.rs.getString(rsmd.getColumnName(i))));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }                    
                   ad_resulttype.appendChild(node);
                }
            }
            
            transformer.transform(new DOMSource(doc), new StreamResult(file));            
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    
}
