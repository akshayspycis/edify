

package managers.clsmgr.xmlMgr.admin;

import java.io.File;
import java.sql.ResultSetMetaData;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @Author Bhanvendra Chaturvedi
 */
public class Xml_Config_Ad_ClassSectionMgr {
    public File file = null;    
    public DocumentBuilder document_builder = null;    
    public DOMSource dom_source = null;
    public StreamResult result = null;
    public Document doc = null;
    public Element element = null;
    public Transformer transformer = null;
    public XPath xpath = null;
    public XPathExpression xpath_exp = null;

    public Xml_Config_Ad_ClassSectionMgr (){
        try {
            file = new File("src\\managers\\datamgr\\xml\\config_ad_class_section");        
            document_builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            doc = document_builder.newDocument();
            element = doc.createElement("Edify");
            doc.appendChild(element);            
            transformer = TransformerFactory.newInstance().newTransformer();
            xpath = XPathFactory.newInstance().newXPath();
            
            load_Ad_ClassSection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    
    }

    public boolean load_Ad_ClassSection() {
        try {
            NodeList nList = Config.xml_config_ad_classsectionmgr.doc.getElementsByTagName("config_ad_class_section");
            int ctr = nList.getLength();
            for (int i = 0; i < ctr; i++) {                
                Node nNode = nList.item(0);  
                nNode.getParentNode().removeChild(nNode);
            }            
        } catch (Exception e) {} 
        try {
            Config.sql = "select * from config_ad_class_section";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            int ctr=0;
            while (Config.rs.next()) {
                 ctr++;
                Element ad_classsection = doc.createElement("config_ad_class_section");
                element.appendChild(ad_classsection);

                ResultSetMetaData rsmd = Config.rs.getMetaData(); 
                 ad_classsection.setAttribute("sno", String.valueOf(ctr));
                ad_classsection.setAttribute("section_id", Config.rs.getString("section_id"));
                for (int i = 2; i <= rsmd.getColumnCount(); i++) {
                    Node node = doc.createElement(rsmd.getColumnName(i));
                    if (Config.rs.getString(rsmd.getColumnName(i)) != null) {
                        node.appendChild(doc.createTextNode(Config.rs.getString(rsmd.getColumnName(i))));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }                    
                   ad_classsection.appendChild(node);
                }
            }
            
            transformer.transform(new DOMSource(doc), new StreamResult(file));            
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

 
}
