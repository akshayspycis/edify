package managers.datamgr.configuration;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import managers.clsmgr.configuration.ConfigMgr;
import managers.clsmgr.configuration.EnDn;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_ClassMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_Class_SectionMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_ElectiveGroupMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_ElectiveGroupSubjectMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_SchoolCategoryMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_SessionMgr;
import managers.clsmgr.daoMgr.admin.Config_Ad_Cm_SubjectMgr;
import managers.clsmgr.daoMgr.examination.Config_Exam_Cm_ClassSub;
import managers.clsmgr.daoMgr.examination.Config_Exam_Cm_Grade;
import managers.clsmgr.daoMgr.examination.Config_Exam_Cm_Type;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_AmountMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_DamageTypeMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FeeAmountMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FeeNamMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FeeTypeMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FineAmountMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_FineTypeMgr;
import managers.clsmgr.daoMgr.fees.Config_Fee_Cm_TypeMgr;
import managers.clsmgr.daoMgr.fees.Fee_Cm_FeeCollection;
import managers.clsmgr.daoMgr.hrmanagement.Config_Hr_Cm_AddDepartmentMgr;
import managers.clsmgr.daoMgr.hrmanagement.Config_Hr_Cm_AddDesignationMgr;
import managers.clsmgr.daoMgr.hrmanagement.Config_Hr_Cm_AllowanceMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_AdvanceMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_DeductionMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_EmpAttendanceMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_EmpRecordMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_GenerateSalaryMgr;
import managers.clsmgr.daoMgr.hrmanagement.Hr_Cm_SalaryMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddBookDurationMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddCategoryMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddIssuableBookMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddOverDueChargeMgr;
import managers.clsmgr.daoMgr.librarymanagement.Config_Lm_Cm_AddSubCategoryMgr;
import managers.clsmgr.daoMgr.librarymanagement.Lm_Cm_BookDetailsMgr;
import managers.clsmgr.daoMgr.storesandinventory.Si_Cm_CategoryMgr;
import managers.clsmgr.daoMgr.storesandinventory.Si_Cm_SubCategoryMgr;
import managers.clsmgr.daoMgr.storesandinventory.Si_Cm_SupplierProfileMgr;
import managers.clsmgr.daoMgr.student.Stu_Cm_Admission;
import managers.clsmgr.daoMgr.student.Stu_Cm_Registration;
import managers.clsmgr.daoMgr.transport.Config_Tr_Cm_RouteDetailMgr;
import managers.clsmgr.daoMgr.transport.Config_Tr_Cm_StopDetailMgr;
import managers.clsmgr.daoMgr.transport.Config_Tr_Cm_VehicleDetailMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ClassMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ClassSectionMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ElectiveGroupMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ElectiveGroupSubjectMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_ResultTypeMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_SchoolCategoryMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_SessionMgr;
import managers.clsmgr.xmlMgr.admin.Xml_Config_Ad_SubjectMgr;
import managers.clsmgr.xmlMgr.examination.Xml_Config_ExamClassSubMgr;
import managers.clsmgr.xmlMgr.examination.Xml_Config_ExamGradeMgr;
import managers.clsmgr.xmlMgr.examination.Xml_Config_ExamTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_AmountMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_DamageTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FeeAmountMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FeeNamMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FeeTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FineAmountMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_FineTypeMgr;
import managers.clsmgr.xmlMgr.fees.Xml_Config_Fee_TypeMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Config_Hr_AllowanceMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Config_Hr_DepartmentMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Config_Hr_DesignationMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_AdvanceMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_DeductionMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_EmpAttendanceMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_EmpRecordMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_GenerateSalaryMgr;
import managers.clsmgr.xmlMgr.hr_management.Xml_Hr_SalaryMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_BookDurationMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_CategoryMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_IssuableBookMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_OverdueChargeMgr;
import managers.clsmgr.xmlMgr.library.Xml_Config_Lm_SubcategoryMgr;
import managers.clsmgr.xmlMgr.library.Xml_Lm_BookDetailsMgr;
import managers.clsmgr.xmlMgr.storeandinventory.Xml_Si_CategoryMgr;
import managers.clsmgr.xmlMgr.storeandinventory.Xml_Si_SubCategoryMgr;
import managers.clsmgr.xmlMgr.storeandinventory.Xml_SupplierProfileMgr;
import managers.clsmgr.xmlMgr.student.Xml_Stu_AddmissionMgr;
import managers.clsmgr.xmlMgr.student.Xml_Stu_RegistrationMgr;

import managers.clsmgr.xmlMgr.transport.Xml_Config_Tr_RouteDetailMgr;
import managers.clsmgr.xmlMgr.transport.Xml_Config_Tr_StopDetailMgr;
import managers.clsmgr.xmlMgr.transport.Xml_Config_Tr_VehicleDetailMgr;
import managers.datamgr.dao.student.Stu_Dm_Admission;
import managers.datamgr.dao.student.Stu_Dm_Registration;
import modules.admin.repository.Admin_ClassNewEntry;
import modules.admin.repository.Admin_ElectiveGroSub;
import modules.admin.repository.Admin_ElectiveGroup;
import modules.admin.repository.Admin_Home;
import modules.admin.repository.Admin_SchoolCategoryNewEntry;
import modules.admin.repository.Admin_SectionNewEntry;
import modules.admin.repository.Admin_SessionNewEntry;
import modules.admin.repository.Admin_SubjectNewEntry;
import modules.admin.repository.Admin_ViewClass;
import modules.admin.repository.Admin_ViewElectiveGroup;
import modules.admin.repository.Admin_ViewElectiveGroupSubject;
import modules.admin.repository.Admin_ViewSchoolCategory;
import modules.admin.repository.Admin_ViewSection;
import modules.admin.repository.Admin_ViewSession;
import modules.admin.repository.Admin_ViewSubject;
import modules.dashboard.Dashlets;
import modules.dashboard.Home;
import modules.dashboard.panel_home;
import modules.dashboard.panel_login;
import modules.exam.Ex_Repository.Ex_ClassSubjectMarks;
import modules.exam.Ex_Repository.Ex_Grade;
import modules.exam.Ex_Repository.Ex_TypeNewEntry;
import modules.exam.Ex_Repository.Ex_View_ClassSubjectMarks;
import modules.exam.Ex_Repository.Ex_View_Grade;
import modules.exam.Ex_Repository.Ex_View_TypeNewEntry;
import modules.exam.Ex_Repository.Exam_Home;
import modules.exam.Ex_ResultEntry;
import modules.fees.Managment.Fee_Managment;
import modules.fees.Managment.Fee_Recipt;
import modules.fees.Managment.Fees_CollectNew;
import modules.fees.Managment.Fees_RACollectNew;
import modules.fees.Repositories.Fee_FeeAmount;
import modules.fees.Repositories.Fee_FeeNam;
import modules.fees.Repositories.Fee_FeeType;
import modules.fees.Repositories.Fee_Home1;
import modules.fees.Repositories.Fee_ViewFeeAmount;
import modules.fees.Repositories.Fee_ViewFeeNam;
import modules.fees.Repositories.Fee_ViewFeeType;
import modules.fees.Repositories.demo;
import modules.hrmanagement.Management.Hr_Advance;
import modules.hrmanagement.Management.Hr_AttendanceEntry;
import modules.hrmanagement.Management.Hr_Deduction;
import modules.hrmanagement.Management.Hr_EmpMgmt;
import modules.hrmanagement.Management.Hr_EmpRegistration;
import modules.hrmanagement.Management.Hr_GenerateSalary;
import modules.hrmanagement.Management.Hr_SalaryDistribution;
import modules.hrmanagement.Management.Hr_ViewEmployee;
import modules.hrmanagement.Repository.Hr_Add_Department;
import modules.hrmanagement.Repository.Hr_Add_Designation;
import modules.hrmanagement.Repository.Hr_Allowance_Distribution;
import modules.hrmanagement.Repository.Hr_Home;
import modules.hrmanagement.Repository.Hr_View_Allowance;
import modules.hrmanagement.Repository.Hr_View_Department;
import modules.hrmanagement.Repository.Hr_View_Designation;
import modules.librarymanagement.repository.Lm_AddBookDuration;
import modules.librarymanagement.repository.Lm_AddCategory;
import modules.librarymanagement.repository.Lm_AddIssuableBook;
import modules.librarymanagement.repository.Lm_AddOverDueCharge;
import modules.librarymanagement.repository.Lm_AddSubCategory;
import modules.librarymanagement.repository.Lm_Home;
import modules.librarymanagement.repository.Lm_NewEntry;
import modules.librarymanagement.repository.Lm_ViewBookDuration;
import modules.librarymanagement.repository.Lm_ViewCategory;
import modules.librarymanagement.repository.Lm_ViewEntry;
import modules.librarymanagement.repository.Lm_ViewIssuableBook;
import modules.librarymanagement.repository.Lm_ViewOverDueCharge;
import modules.librarymanagement.repository.Lm_ViewSubcategory;
import modules.storesandinventory.repository.Si_CategoryNewEntry;
import modules.storesandinventory.repository.Si_Home;
import modules.storesandinventory.repository.Si_SubCategoryNewEntry;
import modules.storesandinventory.repository.Si_SupplierNewEntry;
import modules.storesandinventory.repository.Si_ViewCategory;
import modules.storesandinventory.repository.Si_ViewSubCategory;
import modules.storesandinventory.repository.Si_ViewSupplier;
import modules.studentmanagment.StuMgmt_Home;
import modules.studentmanagment.admission.Admission;
import modules.studentmanagment.admission.Stu_AdmissionHome;
import modules.studentmanagment.admission.View_Admission;
import modules.studentmanagment.registration.Student_Registration;
import modules.studentmanagment.registration.View_Student_Registration;
import modules.studentmanagment.report.Stu_Report;
import modules.transport.Tr_Repository.Tr_Home;
import modules.transport.Tr_Repository.Tr_RouteDetail;
import modules.transport.Tr_Repository.Tr_StopDetail;
import modules.transport.Tr_Repository.Tr_VehicleDetail;
import modules.transport.Tr_VehicleInformation;

public class Config {

    //database variables
    public static Connection conn = null;
    public static PreparedStatement pstmt = null;
    public static Statement stmt = null;
    public static ResultSet rs = null;
    public static String sql = null;    
    
    public static EnDn endn = null; 
    
//    Student Management
    
    public static Stu_Cm_Admission stu_cm_admission = null; 
    public static Stu_Dm_Registration stu_dm_registration = null; 
    public static Stu_Dm_Admission stu_dm_admission = null; 
    
    
//    Xml Library Managers
    public static Xml_Config_Lm_CategoryMgr xml_config_lm_categorymgr = null;   
    public static Xml_Config_Lm_SubcategoryMgr xml_config_lm_subcategorymgr = null;
    public static Xml_Lm_BookDetailsMgr xml_lm_bookdetailsmgr = null;   
    public static Xml_Config_Lm_IssuableBookMgr xml_config_lm_issuablebookmgr = null;
    public static Xml_Config_Lm_BookDurationMgr xml_config_lm_bookdurationmgr = null;
    public static Xml_Config_Lm_OverdueChargeMgr xml_config_lm_overduechargemgr = null;
    
    
    // HR Xml
    public static Xml_Config_Hr_DepartmentMgr xml_config_hr_departmentmgr = null;   
    public static Xml_Config_Hr_DesignationMgr xml_config_hr_designationmgr = null;   
    public static Xml_Config_Hr_AllowanceMgr xml_config_hr_allowancemgr = null;
    public static Xml_Hr_EmpRecordMgr xml_hr_emprecordmgr = null;
    public static Xml_Hr_EmpAttendanceMgr xml_hr_empattendancemgr = null;
    public static Xml_Hr_SalaryMgr xml_hr_salarymgr = null;
    public static Xml_Hr_AdvanceMgr xml_hr_advancemgr = null;
    public static Xml_Hr_DeductionMgr xml_hr_deductionmgr = null;
    public static Xml_Hr_GenerateSalaryMgr xml_hr_generatesalarymgr = null;
    

   

    
    //class managers
        public static ConfigMgr configmgr = null;
        
        
//      Student class managers 
        public static Xml_Stu_RegistrationMgr xml_stu_registrationmgr = null;
        public static Xml_Stu_AddmissionMgr xml_stu_addmissionmgr = null; 
        
        public static Stu_Cm_Admission stu_cm_addmission = null;
        public static Stu_Cm_Registration stu_cm_registration = null;
        
        
        // Library class managers  
        public static Lm_Cm_BookDetailsMgr lm_cm_bookdetailsmgr = null;
        public static Config_Lm_Cm_AddSubCategoryMgr config_lm_cm_addsubcategorymgr = null;
        public static Config_Lm_Cm_AddCategoryMgr config_lm_cm_addcategorymgr = null;
        public static Config_Lm_Cm_AddBookDurationMgr config_lm_cm_addbookdurationmgr = null;
        public static Config_Lm_Cm_AddIssuableBookMgr config_lm_cm_addissuablebookmgr = null;
        public static Config_Lm_Cm_AddOverDueChargeMgr config_lm_cm_addoverduechargemgr = null;
    
        //HR
        public static Config_Hr_Cm_AddDepartmentMgr config_hr_cm_add_departmentmgr = null;
        public static Config_Hr_Cm_AddDesignationMgr config_hr_cm_add_designationMgr = null;
        public static Config_Hr_Cm_AllowanceMgr config_hr_cm_allowancemgr = null;
        public static Hr_Cm_EmpRecordMgr hr_cm_emprecordmgr = null;
        public static Hr_Cm_EmpAttendanceMgr hr_cm_empattendancemgr = null;
        public static Hr_Cm_SalaryMgr Hr_Cm_salarymgr = null;
        public static Hr_Cm_AdvanceMgr hr_cm_advancemgr = null;
        public static Hr_Cm_GenerateSalaryMgr hr_cm_generatesalarymgr = null;
        public static Hr_Cm_DeductionMgr hr_cm_deductionmgr = null;
        
        
//        Transport
        public static Config_Tr_Cm_VehicleDetailMgr config_tr_cm_vehicledetailmgr = null;
        public static Config_Tr_Cm_RouteDetailMgr config_tr_cm_routedetailmgr = null;
        public static Config_Tr_Cm_StopDetailMgr config_tr_cm_stopdetailmgr = null; 

//      Transport
        public static Xml_Config_Tr_VehicleDetailMgr xml_config_tr_vehicledetailmgr = null;
        public static Xml_Config_Tr_RouteDetailMgr xml_config_tr_routedetailmgr = null;
        public static Xml_Config_Tr_StopDetailMgr xml_config_tr_stopdetailmgr = null;     
//        Examination
        public static Config_Exam_Cm_Type config_exam_cm_type = null;
        public static Config_Exam_Cm_Grade config_exam_cm_grade = null;
        public static Config_Exam_Cm_ClassSub config_exam_cm_classsub = null;
        

//  Exam Xml
        public static Xml_Config_ExamTypeMgr xml_config_examtypemgr = null;
        public static Xml_Config_ExamGradeMgr xml_config_examgrademgr = null;
        public static Xml_Config_ExamClassSubMgr xml_config_examclasssubmgr = null;
        
        
        public static Xml_SupplierProfileMgr xml_supplierprofilemgr=null;
        public static Xml_Si_CategoryMgr xml_si_categorymgr=null;
        public static Xml_Si_SubCategoryMgr xml_si_subcategorymgr=null;
        
        //stockAndInventory ClassMgr       
        public static Si_Cm_SupplierProfileMgr si_cm_supplierprofilemgr=null;
        public static Si_Cm_CategoryMgr si_cm_categorymgr=null;
        public static Si_Cm_SubCategoryMgr si_cm_subcategorymgr=null;
        
    //  Admin xml
        public static Xml_Config_Ad_SchoolCategoryMgr xml_config_ad_schoolcategorymgr =null;
        public static Xml_Config_Ad_ClassMgr xml_config_ad_classmgr =null;
        public static Xml_Config_Ad_ClassSectionMgr xml_config_ad_classsectionmgr =null;
        public static Xml_Config_Ad_SubjectMgr xml_config_ad_subjectmgr =null;
        public static Xml_Config_Ad_ElectiveGroupMgr xml_config_ad_electivegroupmgr =null;
        public static Xml_Config_Ad_ElectiveGroupSubjectMgr xml_config_ad_electivegroupsubjectmgr=null;
        public static Xml_Config_Ad_ResultTypeMgr xml_config_ad_resulttypemgr=null;
        public static Xml_Config_Ad_SessionMgr xml_config_ad_sessionmgr=null;

    //  Admin ClassMgr
        public static Config_Ad_Cm_SchoolCategoryMgr config_ad_cm_schoolcategorymgr=null;
        public static Config_Ad_Cm_ClassMgr config_ad_cm_classmgr =null;
        public static Config_Ad_Cm_Class_SectionMgr config_ad_cm_class_sectionmgr=null;
        public static Config_Ad_Cm_SubjectMgr config_ad_cm_subjectmgr =null;
        public static Config_Ad_Cm_ElectiveGroupMgr config_ad_cm_electivegroupmgr=null;
        public static Config_Ad_Cm_ElectiveGroupSubjectMgr config_ad_cm_electivegroupsubjectmgr=null;
        public static Config_Ad_Cm_SessionMgr config_ad_cm_sessionmgr =null;
         
//   Fees
       public static  Xml_Config_Fee_FeeAmountMgr xml_config_fee_feeamountmgr = null;
        public static  Xml_Config_Fee_FeeNamMgr xml_config_fee_feenammgr = null;
        public static  Xml_Config_Fee_FeeTypeMgr xml_config_fee_feetypemgr = null;
        
        
        public static Config_Fee_Cm_FeeTypeMgr config_fee_cm_feetypemgr = null;
        public static Config_Fee_Cm_FeeNamMgr config_fee_cm_feenammgr = null;
        public static Config_Fee_Cm_FeeAmountMgr config_fee_cm_feeamountmgr = null;
        public static Fee_Cm_FeeCollection fee_Cm_FeeCollection = null;
        
        
        
        
       
/*******************************************************************************************************/
        
            
    //forms
        //dashboard
        public static Home homepage = null;
        public static panel_login panel_login = null;
        public static panel_home panel_home = null;      
        public static Dashlets dashlets = null;       

        
//        Admin
        public static Admin_Home ad_home=null;
        public static Admin_SchoolCategoryNewEntry ad_scnewentry =null;
        public static Admin_ClassNewEntry ad_classnewentry =null;
        
        public static Admin_ElectiveGroup ad_electivegroup =null;
        public static Admin_SectionNewEntry ad_sectionnewentry=null;
        public static Admin_SessionNewEntry ad_sessionnewentry=null;
        public static Admin_SubjectNewEntry ad_subjectnewentry =null;
        public static Admin_ElectiveGroSub ad_electivegroupsubject = null;
        
        public static Admin_ViewClass ad_viewclass=null;
        public static Admin_ViewElectiveGroup ad_viewelectivegroup =null;
        public static Admin_ViewElectiveGroupSubject ad_viewelectivegroupsubject=null;
        public static Admin_ViewSchoolCategory ad_viewschoolcategory=null;
        public static Admin_ViewSection ad_viewsection=null;
        public static Admin_ViewSession ad_viewsession=null;
        public static Admin_ViewSubject ad_viewsubject=null;
        
//Hr
        public static Hr_Home hr_home = null;
        public static Hr_Allowance_Distribution hr_allowance_distribution = null;
        public static Hr_Add_Department hr_add_department = null;
        public static Hr_Add_Designation hr_add_designation = null;
        public static Hr_View_Designation hr_view_designation = null;
        public static Hr_View_Department hr_view_department = null;
        public static Hr_View_Allowance hr_view_allowance = null;
        public static Hr_EmpRegistration hr_empregistration = null;
        public static Hr_EmpMgmt hr_empmgmt = null;
        public static Hr_GenerateSalary hr_generatesalary = null;
        public static Hr_AttendanceEntry hr_attendanceEntry = null;
        public static Hr_SalaryDistribution hr_salarydistribution = null;
        public static Hr_Advance hr_advance = null;
        public static Hr_Deduction hr_deduction = null;
        public static Hr_ViewEmployee hr_viewemployee = null;
        
//        Exam
        public static Exam_Home exam_home = null;
        public static Ex_TypeNewEntry ex_typenewentry = null;
        public static Ex_Grade ex_grade = null;
        public static Ex_ClassSubjectMarks ex_classsubjectmarks = null;
        public static Ex_View_TypeNewEntry ex_view_typenewentry = null;
        public static Ex_View_Grade ex_view_grade = null;
        public static Ex_View_ClassSubjectMarks ex_view_classsubmarks = null;
        
        
        public static Ex_ResultEntry ex_resultentry = null;
        
        //library management
        public static Lm_Home lm_repository_home = null;
        public static Lm_NewEntry lm_newentry = null;
        public static Lm_ViewEntry lm_viewentry = null;
        
//        public static Lm_IssueBook lm_issuebook = null;        
//        public static Lm_GenerateLibraryCard lm_generatelibrarycard = null;
        public static Lm_AddSubCategory lm_addsubcategory = null;
        public static Lm_AddCategory lm_addcategory = null;
        public static Lm_ViewCategory lm_viewcategory = null;
        public static Lm_ViewSubcategory lm_viewsubcategory = null;
        
        public static Lm_AddBookDuration lm_addbookduration = null;
        public static Lm_AddIssuableBook lm_addissuablebook = null;
        public static Lm_AddOverDueCharge lm_addoverduecharge = null;
        public static Lm_ViewBookDuration lm_viewbookduration = null;
        public static Lm_ViewIssuableBook lm_viewissuablebook = null;
        public static Lm_ViewOverDueCharge lm_viewoverduecharge = null;
       

        //Stock And Inventory 
        public static Si_Home si_home = null;
        public static Si_SubCategoryNewEntry si_subcategorynewentry = null;
        public static Si_SupplierNewEntry si_suppliernewentry=null;
        public static Si_CategoryNewEntry si_categorynewentry =null;
        public static Si_ViewCategory si_viewcategory=null;
        public static Si_ViewSubCategory si_viewsubcategory=null;
        public static Si_ViewSupplier si_viewsupplier=null;
        
        
        
        //Transport
        public static Tr_Home tr_home = null;
        public static Tr_VehicleDetail tr_vehicledetail = null;
        public static Tr_RouteDetail tr_routedetail = null;
        public static Tr_StopDetail tr_stopdetail = null;
        
        public static Tr_VehicleInformation tr_vehicleinformation = null;
        
        
//        Student Managment
        public static StuMgmt_Home stumgmt_home = null;
        public static Student_Registration student_registration = null;
        public static Admission admission = null;
        public static Stu_AdmissionHome stu_admissionhome = null;
        public static View_Student_Registration view_stu_registration = null;
        public static View_Admission view_admission = null;
        
        public static Stu_Report stu_Report = null;
        
        
        
//        Fees
        public static Fee_Home1 fee_home1 = null;
        public static Fee_FeeType fee_feetype = null;
        public static Fee_FeeNam fee_feenam = null;
        public static Fee_FeeAmount fee_feeamount = null;
        public static Fee_Managment fee_managment = null;
        public static Fees_RACollectNew fees_racollectnew = null;
        public static Fees_CollectNew fees_collectnew = null;
        public static Fee_Recipt fee_Recipt = null;
        
        public static Fee_ViewFeeType fee_viewfeetype = null;
        public static Fee_ViewFeeNam fee_viewfeenam = null;
        public static Fee_ViewFeeAmount fee_viewfeeamount = null;
        
        public static demo Demo = null;
        
        
       
       
       
       
       
       
}
