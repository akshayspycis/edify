package managers.datamgr.dao.examination;

public class Config_Exam_Dm_ClassSub {

      String classsubject_id = null;
      String class_id = null;
      String subject_id = null;
      String total_theorymarks = null;
      String theory_passingmarks = null;
      String tototal_practicalmarks = null;
      String practical_passingmarks = null;

    public String getClasssubject_id() {
        return classsubject_id;
    }

    public void setClasssubject_id(String classsubject_id) {
        this.classsubject_id = classsubject_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getTotal_theorymarks() {
        return total_theorymarks;
    }

    public void setTotal_theorymarks(String total_theorymarks) {
        this.total_theorymarks = total_theorymarks;
    }

    public String getTheory_passingmarks() {
        return theory_passingmarks;
    }

    public void setTheory_passingmarks(String theory_passingmarks) {
        this.theory_passingmarks = theory_passingmarks;
    }

    public String getTototal_practicalmarks() {
        return tototal_practicalmarks;
    }

    public void setTototal_practicalmarks(String tototal_practicalmarks) {
        this.tototal_practicalmarks = tototal_practicalmarks;
    }

    public String getPractical_passingmarks() {
        return practical_passingmarks;
    }

    public void setPractical_passingmarks(String practical_passingmarks) {
        this.practical_passingmarks = practical_passingmarks;
    }
      
      
}
