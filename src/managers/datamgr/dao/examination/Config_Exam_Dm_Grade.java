package managers.datamgr.dao.examination;

public class Config_Exam_Dm_Grade {
    
    String grade_id = null;
    String grade_name = null;
    String min_marks = null;
    String max_marks = null;
    String grade_point = null;
    String remarks = null;

    public String getGrade_id() {
        return grade_id;
    }

    public void setGrade_id(String grade_id) {
        this.grade_id = grade_id;
    }

    public String getGrade_name() {
        return grade_name;
    }

    public void setGrade_name(String grade_name) {
        this.grade_name = grade_name;
    }

    public String getMin_marks() {
        return min_marks;
    }

    public void setMin_marks(String min_marks) {
        this.min_marks = min_marks;
    }

    public String getMax_marks() {
        return max_marks;
    }

    public void setMax_marks(String max_marks) {
        this.max_marks = max_marks;
    }

    public String getGrade_point() {
        return grade_point;
    }

    public void setGrade_point(String grade_point) {
        this.grade_point = grade_point;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    
}
