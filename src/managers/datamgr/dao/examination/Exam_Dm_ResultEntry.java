package managers.datamgr.dao.examination;


public class Exam_Dm_ResultEntry {

    String student_rollno = null;
    String class_id = null;
    String subject_id = null;
    String marks_theory = null;
    String markes_prectical = null;
    String total_marks = null;

    public String getStudent_rollno() {
        return student_rollno;
    }

    public void setStudent_rollno(String student_rollno) {
        this.student_rollno = student_rollno;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getMarks_theory() {
        return marks_theory;
    }

    public void setMarks_theory(String marks_theory) {
        this.marks_theory = marks_theory;
    }

    public String getMarkes_prectical() {
        return markes_prectical;
    }

    public void setMarkes_prectical(String markes_prectical) {
        this.markes_prectical = markes_prectical;
    }

    public String getTotal_marks() {
        return total_marks;
    }

    public void setTotal_marks(String total_marks) {
        this.total_marks = total_marks;
    }
    
    
}
