package managers.datamgr.dao.examination;

public class Exam_Dm_RollNo {

     String calss_id = null;
     String stu_id = null;
     String roll_no = null;
     String section = null;

    public String getCalss_id() {
        return calss_id;
    }

    public void setCalss_id(String calss_id) {
        this.calss_id = calss_id;
    }

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
    
     
}
