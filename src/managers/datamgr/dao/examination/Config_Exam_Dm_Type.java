package managers.datamgr.dao.examination;

public class Config_Exam_Dm_Type {

    String exam_id = null;
    String examtype_name = null;

    public String getExam_id() {
        return exam_id;
    }

    public void setExam_id(String exam_id) {
        this.exam_id = exam_id;
    }

    public String getExamtype_name() {
        return examtype_name;
    }

    public void setExamtype_name(String examtype_name) {
        this.examtype_name = examtype_name;
    }
    
}
