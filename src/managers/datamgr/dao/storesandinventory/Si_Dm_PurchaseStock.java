/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managers.datamgr.dao.storesandinventory;

/**
 *
 * @author user
 */
public class Si_Dm_PurchaseStock {
    String purchaseitem_id=null;
    String billno=null;
    String category=null;
    String itemname=null;
    String free=null;
    String quantity=null;
    String rate=null;
    String mrp=null;
    String amount=null;

    public String getPurchaseitem_id() {
        return purchaseitem_id;
    }

    public void setPurchaseitem_id(String purchaseitem_id) {
        this.purchaseitem_id = purchaseitem_id;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
   
    
    
}
