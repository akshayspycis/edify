package managers.datamgr.dao.storesandinventory;

public class Si_Dm_Category {
    String category_id =null;
    String categoryname =null;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    
    
}
