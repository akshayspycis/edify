package managers.datamgr.dao.storesandinventory;
public class Si_Dm_PurchaseReturn {
    String billno = null;
    String category = null;
    String itemname = null;
    String free = null;
    String quantity = null;
    String returndate = null;
    String returnamount = null;

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getReturndate() {
        return returndate;
    }

    public void setReturndate(String returndate) {
        this.returndate = returndate;
    }

    public String getReturnamount() {
        return returnamount;
    }

    public void setReturnamount(String returnamount) {
        this.returnamount = returnamount;
    }
    
}
