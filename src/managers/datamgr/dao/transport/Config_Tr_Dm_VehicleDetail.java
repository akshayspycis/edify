package managers.datamgr.dao.transport;

public class Config_Tr_Dm_VehicleDetail {
    
    String vehicle_id = null;
    String vehiclename = null;
    String vehicleno = null;
    String vehicleseatcapacity = null;

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getVehiclename() {
        return vehiclename;
    }

    public void setVehiclename(String vehiclename) {
        this.vehiclename = vehiclename;
    }

    public String getVehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        this.vehicleno = vehicleno;
    }

    public String getVehicleseatcapacity() {
        return vehicleseatcapacity;
    }

    public void setVehicleseatcapacity(String vehicleseatcapacity) {
        this.vehicleseatcapacity = vehicleseatcapacity;
    }

    
}
