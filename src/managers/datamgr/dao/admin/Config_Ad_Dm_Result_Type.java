package managers.datamgr.dao.admin;

public class Config_Ad_Dm_Result_Type {

    
     String result_id = null;
     String resulttype_name = null;

    public String getResult_id() {
        return result_id;
    }

    public void setResult_id(String result_id) {
        this.result_id = result_id;
    }

    public String getResulttype_name() {
        return resulttype_name;
    }

    public void setResulttype_name(String resulttype_name) {
        this.resulttype_name = resulttype_name;
    }
     
}
