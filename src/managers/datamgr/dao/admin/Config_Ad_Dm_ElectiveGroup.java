package managers.datamgr.dao.admin;

public class Config_Ad_Dm_ElectiveGroup {
    
     String electivegroup_id = null;
     String electivegroupname = null;
     String session_id = null;

    public String getElectivegroup_id() {
        return electivegroup_id;
    }

    public void setElectivegroup_id(String electivegroup_id) {
        this.electivegroup_id = electivegroup_id;
    }

    public String getElectivegroupname() {
        return electivegroupname;
    }

    public void setElectivegroupname(String electivegroupname) {
        this.electivegroupname = electivegroupname;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
     
     
}
