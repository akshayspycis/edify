package managers.datamgr.dao.admin;

public class Config_Ad_Dm_Session {
   
     String session_id = null;
     String session_name = null;

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getSession_name() {
        return session_name;
    }

    public void setSession_name(String session_name) {
        this.session_name = session_name;
    }
    
     
}
