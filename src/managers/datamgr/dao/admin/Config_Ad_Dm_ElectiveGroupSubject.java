package managers.datamgr.dao.admin;

public class Config_Ad_Dm_ElectiveGroupSubject {
    String electivegroupsubject_id=null;
    String subject_id = null;
    String electivegroup_id = null;
    String session_id = null;

    public String getElectivegroupsubject_id() {
        return electivegroupsubject_id;
    }

    public void setElectivegroupsubject_id(String electivegroupsubject_id) {
        this.electivegroupsubject_id = electivegroupsubject_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getElectivegroup_id() {
        return electivegroup_id;
    }

    public void setElectivegroup_id(String electivegroup_id) {
        this.electivegroup_id = electivegroup_id;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
    
    
}
