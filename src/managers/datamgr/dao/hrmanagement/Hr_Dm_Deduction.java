package managers.datamgr.dao.hrmanagement;

public class Hr_Dm_Deduction {
    String deduction_id = null;
    String emp_id = null;
    String advance_id = null;
    String amount = null;
    String deduction_date = null;
    String balance = null;

    public String getDeduction_id() {
        return deduction_id;
    }

    public void setDeduction_id(String deduction_id) {
        this.deduction_id = deduction_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getAdvance_id() {
        return advance_id;
    }

    public void setAdvance_id(String advance_id) {
        this.advance_id = advance_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDeduction_date() {
        return deduction_date;
    }

    public void setDeduction_date(String deduction_date) {
        this.deduction_date = deduction_date;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
    
    
}
