

package managers.datamgr.dao.hrmanagement;

public class Hr_Dm_EmpRecord {
    int emp_id;
    String fname = null;
    String midname = null;
    String lname = null;
    String dob = null;
    String fathername = null;    
    String gender = null;
    String category = null;
    String religion = null;
    String qualification = null;
    String otherQualification = null;
    String Raddress = null;
    String Rcity = null;
    String Rstate = null;
    String Rcountry = null;
    String Rpincode = null;
    String Paddress = null;
    String Pcity = null;
    String Pstate = null;
    String Pcountry = null;
    String Ppincode = null;
    String mobno = null;
    String contactno = null;
    String emailid = null;
    String doj = null;
    String department_id = null;
    String designation_id = null;
    String basicpay = null;
    String identity_type = null;
    String identityno = null;
    String bank_accountno = null;
    String pf_accountno = null;
    

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

   

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMidname() {
        return midname;
    }

    public void setMidname(String midname) {
        this.midname = midname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getOtherQualification() {
        return otherQualification;
    }

    public void setOtherQualification(String otherQualification) {
        this.otherQualification = otherQualification;
    }

    public String getRaddress() {
        return Raddress;
    }

    public void setRaddress(String Raddress) {
        this.Raddress = Raddress;
    }

    public String getRcity() {
        return Rcity;
    }

    public void setRcity(String Rcity) {
        this.Rcity = Rcity;
    }

    public String getRstate() {
        return Rstate;
    }

    public void setRstate(String Rstate) {
        this.Rstate = Rstate;
    }

    public String getRcountry() {
        return Rcountry;
    }

    public void setRcountry(String Rcountry) {
        this.Rcountry = Rcountry;
    }

    public String getRpincode() {
        return Rpincode;
    }

    public void setRpincode(String Rpincode) {
        this.Rpincode = Rpincode;
    }

    public String getPaddress() {
        return Paddress;
    }

    public void setPaddress(String Paddress) {
        this.Paddress = Paddress;
    }

    public String getPcity() {
        return Pcity;
    }

    public void setPcity(String Pcity) {
        this.Pcity = Pcity;
    }

    public String getPstate() {
        return Pstate;
    }

    public void setPstate(String Pstate) {
        this.Pstate = Pstate;
    }

    public String getPcountry() {
        return Pcountry;
    }

    public void setPcountry(String Pcountry) {
        this.Pcountry = Pcountry;
    }

    public String getPpincode() {
        return Ppincode;
    }

    public void setPpincode(String Ppincode) {
        this.Ppincode = Ppincode;
    }

    public String getMobno() {
        return mobno;
    }

    public void setMobno(String mobno) {
        this.mobno = mobno;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getDoj() {
        return doj;
    }

    public void setDoj(String doj) {
        this.doj = doj;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(String designation_id) {
        this.designation_id = designation_id;
    }    

    public String getBasicpay() {
        return basicpay;
    }

    public void setBasicpay(String basicpay) {
        this.basicpay = basicpay;
    }

    public String getIdentity_type() {
        return identity_type;
    }

    public void setIdentity_type(String identity_type) {
        this.identity_type = identity_type;
    }

    public String getIdentityno() {
        return identityno;
    }

    public void setIdentityno(String identityno) {
        this.identityno = identityno;
    }

    public String getBank_accountno() {
        return bank_accountno;
    }

    public void setBank_accountno(String bank_accountno) {
        this.bank_accountno = bank_accountno;
    }

    public String getPf_accountno() {
        return pf_accountno;
    }

    public void setPf_accountno(String pf_accountno) {
        this.pf_accountno = pf_accountno;
    }

    
    
}
