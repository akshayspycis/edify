package managers.datamgr.dao.hrmanagement;

public class Config_Hr_Dm_AddDesignation {

    String designation_id = null;
    String designationname = null;
   

    public String getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(String designation_id) {
        this.designation_id = designation_id;
    }

    public String getDesignationname() {
        return designationname;
    }

    public void setDesignationname(String designationname) {
        this.designationname = designationname;
    }    
    
}
