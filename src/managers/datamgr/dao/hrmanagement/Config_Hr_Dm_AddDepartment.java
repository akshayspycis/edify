package managers.datamgr.dao.hrmanagement;

public class Config_Hr_Dm_AddDepartment {

    String department_id = null;
    String departmentname = null;

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getDepartmentname() {
        return departmentname;
    }

    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }
    
    
}
