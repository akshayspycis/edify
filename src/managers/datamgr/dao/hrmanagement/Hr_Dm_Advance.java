
package managers.datamgr.dao.hrmanagement;

public class Hr_Dm_Advance {
    String advance_id = null;
    String emp_id = null;
    String advance_amt = null;
    String advancedate = null;
    
    

    public String getAdvance_id() {
        return advance_id;
    }

    public void setAdvance_id(String advance_id) {
        this.advance_id = advance_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getAdvance_amt() {
        return advance_amt;
    }

    public void setAdvance_amt(String advance_amt) {
        this.advance_amt = advance_amt;
    }

    public String getAdvancedate() {
        return advancedate;
    }

    public void setAdvancedate(String advancedate) {
        this.advancedate = advancedate;
    }
   
}
