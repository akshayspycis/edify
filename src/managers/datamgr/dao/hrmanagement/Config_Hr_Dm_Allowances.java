package managers.datamgr.dao.hrmanagement;

public class Config_Hr_Dm_Allowances {

    String allowance_id = null;       
    String hra = null;
    String da = null;
    String medical = null;
    String conveyance = null;
    String travel = null;
    String special = null;    
    String pf = null;    
    String designation_id = null;

    public String getAllowance_id() {
        return allowance_id;
    }

    public void setAllowance_id(String allowance_id) {
        this.allowance_id = allowance_id;
    }

    public String getHra() {
        return hra;
    }

    public void setHra(String hra) {
        this.hra = hra;
    }

    public String getDa() {
        return da;
    }

    public void setDa(String da) {
        this.da = da;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getConveyance() {
        return conveyance;
    }

    public void setConveyance(String conveyance) {
        this.conveyance = conveyance;
    }

    public String getTravel() {
        return travel;
    }

    public void setTravel(String travel) {
        this.travel = travel;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getPf() {
        return pf;
    }

    public void setPf(String pf) {
        this.pf = pf;
    }

        
    public String getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(String designation_id) {
        this.designation_id = designation_id;
    }

    

    

  

   
   
        
}
