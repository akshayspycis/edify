

package managers.datamgr.dao.hrmanagement;


public class Hr_Dm_Salary {
    String salary_id = null;
    String emp_id = null;   
    String advance_id = null;
    String emp_attendance_id = null;
    String salarydate = null;

    public String getSalary_id() {
        return salary_id;
    }

    public void setSalary_id(String salary_id) {
        this.salary_id = salary_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }


    public String getAdvance_id() {
        return advance_id;
    }

    public void setAdvance_id(String advance_id) {
        this.advance_id = advance_id;
    }

    public String getEmp_attendance_id() {
        return emp_attendance_id;
    }

    public void setEmp_attendance_id(String emp_attendance_id) {
        this.emp_attendance_id = emp_attendance_id;
    }

    public String getSalarydate() {
        return salarydate;
    }

    public void setSalarydate(String salarydate) {
        this.salarydate = salarydate;
    }
    
    
}
