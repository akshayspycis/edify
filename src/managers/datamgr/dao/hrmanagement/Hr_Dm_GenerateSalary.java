

package managers.datamgr.dao.hrmanagement;

public class Hr_Dm_GenerateSalary {
   String salary_id = null; 
   String emp_id = null;
   String checkno = null;
   String paydate = null;
   String incometax = null;   
   String hra = null;
   String da = null;
   String medical = null;
   String conveyance = null;
   String travel = null;
   String special = null;
   String pf = null;
   String netsal = null;
   String advance_id = null;
   String attendance_id = null;

    public String getSalary_id() {
        return salary_id;
    }

    public void setSalary_id(String salary_id) {
        this.salary_id = salary_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getCheckno() {
        return checkno;
    }

    public void setCheckno(String checkno) {
        this.checkno = checkno;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate = paydate;
    }

    public String getIncometax() {
        return incometax;
    }

    public void setIncometax(String incometax) {
        this.incometax = incometax;
    }

    public String getHra() {
        return hra;
    }

    public void setHra(String hra) {
        this.hra = hra;
    }

    public String getDa() {
        return da;
    }

    public void setDa(String da) {
        this.da = da;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    public String getConveyance() {
        return conveyance;
    }

    public void setConveyance(String conveyance) {
        this.conveyance = conveyance;
    }

    public String getTravel() {
        return travel;
    }

    public void setTravel(String travel) {
        this.travel = travel;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getPf() {
        return pf;
    }

    public void setPf(String pf) {
        this.pf = pf;
    }

    public String getNetsal() {
        return netsal;
    }

    public void setNetsal(String netsal) {
        this.netsal = netsal;
    }

    public String getAdvance_id() {
        return advance_id;
    }

    public void setAdvance_id(String advance_id) {
        this.advance_id = advance_id;
    }

    public String getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(String attendance_id) {
        this.attendance_id = attendance_id;
    }
   
   
   
   
}
