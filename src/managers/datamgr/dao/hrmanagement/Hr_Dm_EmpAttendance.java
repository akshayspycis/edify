package managers.datamgr.dao.hrmanagement;

public class Hr_Dm_EmpAttendance {

    String emp_attendance_id = null;
    String emp_id = null;
    String status = null;
    String timein = null;
    String timeout = null;
    String date = null;

    public String getEmp_attendance_id() {
        return emp_attendance_id;
    }

    public void setEmp_attendance_id(String emp_attendance_id) {
        this.emp_attendance_id = emp_attendance_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimein() {
        return timein;
    }

    public void setTimein(String timein) {
        this.timein = timein;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    

   
}
