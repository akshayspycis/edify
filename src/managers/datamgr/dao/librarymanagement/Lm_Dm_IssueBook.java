package managers.datamgr.dao.librarymanagement;

/**
 *
 * @author dell
 */
public class Lm_Dm_IssueBook {
    String issuebook_id = null;
    String sno = null;
    String bookid = null;
    String bookname = null;
    String duration = null;
    String issuedate = null;
    String duedate = null;
    String returndate = null;
    String fine = null;

    public String getIssuebook_id() {
        return issuebook_id;
    }

    public void setIssuebook_id(String issuebook_id) {
        this.issuebook_id = issuebook_id;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getBookid() {
        return bookid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getIssuedate() {
        return issuedate;
    }

    public void setIssuedate(String issuedate) {
        this.issuedate = issuedate;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getReturndate() {
        return returndate;
    }

    public void setReturndate(String returndate) {
        this.returndate = returndate;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    
}
