package managers.datamgr.dao.librarymanagement;

/**
 *
 * @author Archi
 */
public class Config_Lm_Dm_AddOverDueCharge {
    String overduecharge_id = null;
    String category = null;
    String subcategory = null;
    String forstudent = null;
    String forstaff = null;

    public String getOverduecharge_id() {
        return overduecharge_id;
    }

    public void setOverduecharge_id(String overduecharge_id) {
        this.overduecharge_id = overduecharge_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getForstudent() {
        return forstudent;
    }

    public void setForstudent(String forstudent) {
        this.forstudent = forstudent;
    }

    public String getForstaff() {
        return forstaff;
    }

    public void setForstaff(String forstaff) {
        this.forstaff = forstaff;
    }

    
    
}
