package managers.datamgr.dao.librarymanagement;


public class Config_Lm_Dm_AddIssuableBook {
    
    String issuablebook_id = null;
    String categoryname = null;
    String subcategoryname = null;
    String forstudent = null;
    String forstaff = null;

    public String getIssuablebook_id() {
        return issuablebook_id;
    }

    public void setIssuablebook_id(String issuablebook_id) {
        this.issuablebook_id = issuablebook_id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getSubcategoryname() {
        return subcategoryname;
    }

    public void setSubcategoryname(String subcategoryname) {
        this.subcategoryname = subcategoryname;
    }

    public String getForstudent() {
        return forstudent;
    }

    public void setForstudent(String forstudent) {
        this.forstudent = forstudent;
    }

    public String getForstaff() {
        return forstaff;
    }

    public void setForstaff(String forstaff) {
        this.forstaff = forstaff;
    }    
}
