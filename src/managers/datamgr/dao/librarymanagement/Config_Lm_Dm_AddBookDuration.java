package managers.datamgr.dao.librarymanagement;

public class Config_Lm_Dm_AddBookDuration {
    String duration_id = null;
    String category = null;
    String subcategory = null;
    String forstudent = null;
    String forstaff = null;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getForstudent() {
        return forstudent;
    }

    public void setForstudent(String forstudent) {
        this.forstudent = forstudent;
    }

    public String getForstaff() {
        return forstaff;
    }

    public void setForstaff(String forstaff) {
        this.forstaff = forstaff;
    }

    public String getDuration_id() {
        return duration_id;
    }

    public void setDuration_id(String duration_id) {
        this.duration_id = duration_id;
    }

 
}
