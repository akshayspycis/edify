package managers.datamgr.dao.librarymanagement;

/**
 *
 * @author Archi
 */
public class Config_Lm_Dm_AddSubCategory {
    String subcategory_id = null;
    String subcategoryname = null;
    String category_id = null;

    public String getSubcategoryname() {
        return subcategoryname;
    }

    public void setSubcategoryname(String subcategoryname) {
        this.subcategoryname = subcategoryname;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

   
}
