package managers.datamgr.dao.student;

public class Stu_Dm_Admission {
    
    String admission_id = null;
    String registration_id = null;
    String addmission_date = null;
    String ssm_id = null;
    String fm_id=null;
    String aadharcard_no = null;
    String subject_id= null;
    String bus = null;
    String hostal = null;   
    String tc = null;
    String cc = null;
    String birth = null;
    String domicile = null;
    String cast = null;
    String income = null;
    String guar_name = null;
    String relation = null;
    String address = null;
    String mob_no = null;
    String phone_no = null;
    String email_id = null;
    String session = null;
    String tcno = null;
    String ccno = null;
    String birthno = null;        
    String domicileno = null;
    String casteno = null;
    String incomeno = null;
    String section_id = null;
    String route_id = null;
    

    public String getAdmission_id() {
        return admission_id;
    }

    public void setAdmission_id(String admission_id) {
        this.admission_id = admission_id;
    }

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getAddmission_date() {
        return addmission_date;
    }

    public void setAddmission_date(String addmission_date) {
        this.addmission_date = addmission_date;
    }

    public String getSsm_id() {
        return ssm_id;
    }

    public void setSsm_id(String ssm_id) {
        this.ssm_id = ssm_id;
    }

    public String getFm_id() {
        return fm_id;
    }

    public void setFm_id(String fm_id) {
        this.fm_id = fm_id;
    }

    public String getAadharcard_no() {
        return aadharcard_no;
    }

    public void setAadharcard_no(String aadharcard_no) {
        this.aadharcard_no = aadharcard_no;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getHostal() {
        return hostal;
    }

    public void setHostal(String hostal) {
        this.hostal = hostal;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getDomicile() {
        return domicile;
    }

    public void setDomicile(String domicile) {
        this.domicile = domicile;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getGuar_name() {
        return guar_name;
    }

    public void setGuar_name(String guar_name) {
        this.guar_name = guar_name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMob_no() {
        return mob_no;
    }

    public void setMob_no(String mob_no) {
        this.mob_no = mob_no;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getTcno() {
        return tcno;
    }

    public void setTcno(String tcno) {
        this.tcno = tcno;
    }

    public String getCcno() {
        return ccno;
    }

    public void setCcno(String ccno) {
        this.ccno = ccno;
    }

    public String getBirthno() {
        return birthno;
    }

    public void setBirthno(String birthno) {
        this.birthno = birthno;
    }

    public String getDomicileno() {
        return domicileno;
    }

    public void setDomicileno(String domicileno) {
        this.domicileno = domicileno;
    }

    public String getCasteno() {
        return casteno;
    }

    public void setCasteno(String casteno) {
        this.casteno = casteno;
    }

    public String getIncomeno() {
        return incomeno;
    }

    public void setIncomeno(String incomeno) {
        this.incomeno = incomeno;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }
    
    
    
    
    
}
