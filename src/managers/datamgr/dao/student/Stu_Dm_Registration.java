package managers.datamgr.dao.student;


public class Stu_Dm_Registration {

    String registration_id = null;
    String form_no = null;
    String date = null;
    String firstname = null;
    String middlename = null;
    String lastname = null;
    String fathername = null;
    String mothername = null;
    String dob = null;   
    String gender = null;
    String class_id = null;
    String category = null;
    String religion = null;
    String nationality = null;
    String fatherqualification = null;
    String fatheroccupation = null;
    String fatherincome = null;
    String f_emailid = null;
    String motherqualification = null;
    String motheroccupation = null;
    String motherincome = null;
    String m_emailid = null;
    String r_address = null;
    String r_city = null;
    String r_state = null;
    String r_country = null;
    String r_pincode = null;
    String r_phoneno = null;
    String father_mobno = null;
    String mother_mobno = null;
    String p_address = null;
    String p_city = null;
    String p_state = null;
    String p_country = null;
    String p_pincode = null;
    String p_phoneno = null;
    String pre_class = null;
    String pre_year = null;
    String pre_result = null;
    String pre_percentage = null;
    String pre_schoolname = null;
    String pre_boardname = null;
    String bloodgroup = null;
    String height = null;   
    String weight = null;
    String handicapped_status = null;
    String handicapped_description = null;
    String session_id = null;

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getForm_no() {
        return form_no;
    }

    public void setForm_no(String form_no) {
        this.form_no = form_no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getMothername() {
        return mothername;
    }

    public void setMothername(String mothername) {
        this.mothername = mothername;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFatherqualification() {
        return fatherqualification;
    }

    public void setFatherqualification(String fatherqualification) {
        this.fatherqualification = fatherqualification;
    }

    public String getFatheroccupation() {
        return fatheroccupation;
    }

    public void setFatheroccupation(String fatheroccupation) {
        this.fatheroccupation = fatheroccupation;
    }

    public String getFatherincome() {
        return fatherincome;
    }

    public void setFatherincome(String fatherincome) {
        this.fatherincome = fatherincome;
    }

    public String getF_emailid() {
        return f_emailid;
    }

    public void setF_emailid(String f_emailid) {
        this.f_emailid = f_emailid;
    }

    public String getMotherqualification() {
        return motherqualification;
    }

    public void setMotherqualification(String motherqualification) {
        this.motherqualification = motherqualification;
    }

    public String getMotheroccupation() {
        return motheroccupation;
    }

    public void setMotheroccupation(String motheroccupation) {
        this.motheroccupation = motheroccupation;
    }

    public String getMotherincome() {
        return motherincome;
    }

    public void setMotherincome(String motherincome) {
        this.motherincome = motherincome;
    }

    public String getM_emailid() {
        return m_emailid;
    }

    public void setM_emailid(String m_emailid) {
        this.m_emailid = m_emailid;
    }

    public String getR_address() {
        return r_address;
    }

    public void setR_address(String r_address) {
        this.r_address = r_address;
    }

    public String getR_city() {
        return r_city;
    }

    public void setR_city(String r_city) {
        this.r_city = r_city;
    }

    public String getR_state() {
        return r_state;
    }

    public void setR_state(String r_state) {
        this.r_state = r_state;
    }

    public String getR_country() {
        return r_country;
    }

    public void setR_country(String r_country) {
        this.r_country = r_country;
    }

    public String getR_pincode() {
        return r_pincode;
    }

    public void setR_pincode(String r_pincode) {
        this.r_pincode = r_pincode;
    }

    public String getR_phoneno() {
        return r_phoneno;
    }

    public void setR_phoneno(String r_phoneno) {
        this.r_phoneno = r_phoneno;
    }

    public String getFather_mobno() {
        return father_mobno;
    }

    public void setFather_mobno(String father_mobno) {
        this.father_mobno = father_mobno;
    }

    public String getMother_mobno() {
        return mother_mobno;
    }

    public void setMother_mobno(String mother_mobno) {
        this.mother_mobno = mother_mobno;
    }

    public String getP_address() {
        return p_address;
    }

    public void setP_address(String p_address) {
        this.p_address = p_address;
    }

    public String getP_city() {
        return p_city;
    }

    public void setP_city(String p_city) {
        this.p_city = p_city;
    }

    public String getP_state() {
        return p_state;
    }

    public void setP_state(String p_state) {
        this.p_state = p_state;
    }

    public String getP_country() {
        return p_country;
    }

    public void setP_country(String p_country) {
        this.p_country = p_country;
    }

    public String getP_pincode() {
        return p_pincode;
    }

    public void setP_pincode(String p_pincode) {
        this.p_pincode = p_pincode;
    }

    public String getP_phoneno() {
        return p_phoneno;
    }

    public void setP_phoneno(String p_phoneno) {
        this.p_phoneno = p_phoneno;
    }

    public String getPre_class() {
        return pre_class;
    }

    public void setPre_class(String pre_class) {
        this.pre_class = pre_class;
    }

    public String getPre_year() {
        return pre_year;
    }

    public void setPre_year(String pre_year) {
        this.pre_year = pre_year;
    }

    public String getPre_result() {
        return pre_result;
    }

    public void setPre_result(String pre_result) {
        this.pre_result = pre_result;
    }

    public String getPre_percentage() {
        return pre_percentage;
    }

    public void setPre_percentage(String pre_percentage) {
        this.pre_percentage = pre_percentage;
    }

    public String getPre_schoolname() {
        return pre_schoolname;
    }

    public void setPre_schoolname(String pre_schoolname) {
        this.pre_schoolname = pre_schoolname;
    }

    public String getPre_boardname() {
        return pre_boardname;
    }

    public void setPre_boardname(String pre_boardname) {
        this.pre_boardname = pre_boardname;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHandicapped_status() {
        return handicapped_status;
    }

    public void setHandicapped_status(String handicapped_status) {
        this.handicapped_status = handicapped_status;
    }

    public String getHandicapped_description() {
        return handicapped_description;
    }

    public void setHandicapped_description(String handicapped_description) {
        this.handicapped_description = handicapped_description;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }
    
    
}
