package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Dm_SubmissionDate {
    String config_fee_datesubmission_id ;
    String jan_date ;
    String feb_date ;
    String mar_date ;
    String apr_date ;
    String may_date ;
    String jun_date ;
    String jul_date ;
    String aug_date ;
    String sep_date ;
    String oct_date ;

    public String getConfig_fee_datesubmission_id() {
        return config_fee_datesubmission_id;
    }

    public void setConfig_fee_datesubmission_id(String config_fee_datesubmission_id) {
        this.config_fee_datesubmission_id = config_fee_datesubmission_id;
    }

    public String getJan_date() {
        return jan_date;
    }

    public void setJan_date(String jan_date) {
        this.jan_date = jan_date;
    }

    public String getFeb_date() {
        return feb_date;
    }

    public void setFeb_date(String feb_date) {
        this.feb_date = feb_date;
    }

    public String getMar_date() {
        return mar_date;
    }

    public void setMar_date(String mar_date) {
        this.mar_date = mar_date;
    }

    public String getApr_date() {
        return apr_date;
    }

    public void setApr_date(String apr_date) {
        this.apr_date = apr_date;
    }

    public String getMay_date() {
        return may_date;
    }

    public void setMay_date(String may_date) {
        this.may_date = may_date;
    }

    public String getJun_date() {
        return jun_date;
    }

    public void setJun_date(String jun_date) {
        this.jun_date = jun_date;
    }

    public String getJul_date() {
        return jul_date;
    }

    public void setJul_date(String jul_date) {
        this.jul_date = jul_date;
    }

    public String getAug_date() {
        return aug_date;
    }

    public void setAug_date(String aug_date) {
        this.aug_date = aug_date;
    }

    public String getSep_date() {
        return sep_date;
    }

    public void setSep_date(String sep_date) {
        this.sep_date = sep_date;
    }

    public String getOct_date() {
        return oct_date;
    }

    public void setOct_date(String oct_date) {
        this.oct_date = oct_date;
    }

    public String getNov_date() {
        return nov_date;
    }

    public void setNov_date(String nov_date) {
        this.nov_date = nov_date;
    }

    public String getDec_date() {
        return dec_date;
    }

    public void setDec_date(String dec_date) {
        this.dec_date = dec_date;
    }
    String nov_date ;
    String dec_date ;
    
    
    
}
