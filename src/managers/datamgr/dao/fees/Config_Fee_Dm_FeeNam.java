package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Dm_FeeNam {
    String feename_id ;
    String session_id ;
    String feetype_id ;
    String feenmae ;

    public String getFeename_id() {
        return feename_id;
    }

    public void setFeename_id(String feename_id) {
        this.feename_id = feename_id;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getFeetype_id() {
        return feetype_id;
    }

    public void setFeetype_id(String feetype_id) {
        this.feetype_id = feetype_id;
    }

    public String getFeenmae() {
        return feenmae;
    }

    public void setFeenmae(String feenmae) {
        this.feenmae = feenmae;
    }
    
}
