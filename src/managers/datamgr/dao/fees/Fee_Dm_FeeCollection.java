package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Fee_Dm_FeeCollection {
       String feecollection_id;
       String registration_id;
       String session_id;
       String class_id;
       String feetype_id;
       String feeamount_id;
       String date;
       String modepay;
       String bankname;
       String cheqdate;
       String cheqno;
       String totalamount;
       String discount;
       String netamount;
       String payable;
       String balance;
       String recepitno;

    public String getFeecollection_id() {
        return feecollection_id;
    }

    public void setFeecollection_id(String feecollection_id) {
        this.feecollection_id = feecollection_id;
    }

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getFeetype_id() {
        return feetype_id;
    }

    public void setFeetype_id(String feetype_id) {
        this.feetype_id = feetype_id;
    }

    public String getFeeamount_id() {
        return feeamount_id;
    }

    public void setFeeamount_id(String feeamount_id) {
        this.feeamount_id = feeamount_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModepay() {
        return modepay;
    }

    public void setModepay(String modepay) {
        this.modepay = modepay;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getCheqdate() {
        return cheqdate;
    }

    public void setCheqdate(String cheqdate) {
        this.cheqdate = cheqdate;
    }

    public String getCheqno() {
        return cheqno;
    }

    public void setCheqno(String cheqno) {
        this.cheqno = cheqno;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getNetamount() {
        return netamount;
    }

    public void setNetamount(String netamount) {
        this.netamount = netamount;
    }

    public String getPayable() {
        return payable;
    }

    public void setPayable(String payable) {
        this.payable = payable;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRecepitno() {
        return recepitno;
    }

    public void setRecepitno(String recepitno) {
        this.recepitno = recepitno;
    }
    
       
       
}
