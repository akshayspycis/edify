package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Dm_DamageType {
    String damage_id ;
    String damagename ;

    public String getDamage_id() {
        return damage_id;
    }

    public void setDamage_id(String damage_id) {
        this.damage_id = damage_id;
    }

    public String getDamagename() {
        return damagename;
    }

    public void setDamagename(String damagename) {
        this.damagename = damagename;
    }
    
            
    
}
