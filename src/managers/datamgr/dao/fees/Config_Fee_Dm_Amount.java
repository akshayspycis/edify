package managers.datamgr.dao.fees;


public class Config_Fee_Dm_Amount {
    String feeamount_id = null ;
    String class_id = null ;
    String fee_id = null ;
    String amount = null ;

    public String getFeeamount_id() {
        return feeamount_id;
    }

    public void setFeeamount_id(String feeamount_id) {
        this.feeamount_id = feeamount_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getFee_id() {
        return fee_id;
    }

    public void setFee_id(String fee_id) {
        this.fee_id = fee_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    
          
    
}
