package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Dm_FineType {
    String fine_id ;
    String finename ;

    public String getFine_id() {
        return fine_id;
    }

    public void setFine_id(String fine_id) {
        this.fine_id = fine_id;
    }

    public String getFinename() {
        return finename;
    }

    public void setFinename(String finename) {
        this.finename = finename;
    }
    
    
    
}
