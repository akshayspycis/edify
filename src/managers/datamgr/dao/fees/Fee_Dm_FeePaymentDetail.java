package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Fee_Dm_FeePaymentDetail {
    String payment_detail_id;
    String paymentmode;
    String bankname;
    String paydate;
    String paynumber;

    public String getPayment_detail_id() {
        return payment_detail_id;
    }

    public void setPayment_detail_id(String payment_detail_id) {
        this.payment_detail_id = payment_detail_id;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate = paydate;
    }

    public String getPaynumber() {
        return paynumber;
    }

    public void setPaynumber(String paynumber) {
        this.paynumber = paynumber;
    }
    

    
}
