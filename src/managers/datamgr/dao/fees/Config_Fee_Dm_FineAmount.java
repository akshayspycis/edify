package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Dm_FineAmount {
    String fineamount_id ;
    String class_id ;
    String fine_id ;
    String fineamount ;

    public String getFineamount_id() {
        return fineamount_id;
    }

    public void setFineamount_id(String fineamount_id) {
        this.fineamount_id = fineamount_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getFine_id() {
        return fine_id;
    }

    public void setFine_id(String fine_id) {
        this.fine_id = fine_id;
    }

    public String getFineamount() {
        return fineamount;
    }

    public void setFineamount(String fineamount) {
        this.fineamount = fineamount;
    }
    
    
    
}
