package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Dm_FeeType {
    String feetype_id ;
    String session_id ;
    String typename ;

    public String getFeetype_id() {
        return feetype_id;
    }

    public void setFeetype_id(String feetype_id) {
        this.feetype_id = feetype_id;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }
    
    
}
