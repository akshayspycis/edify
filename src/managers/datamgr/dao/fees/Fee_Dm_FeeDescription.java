package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Fee_Dm_FeeDescription {
    String fee_description_id;
    String feeamount_id;
    String totalamount;
    String discount;
    String netamount;
    String payableamount;
    String balanceamount;

    public String getFee_description_id() {
        return fee_description_id;
    }

    public void setFee_description_id(String fee_description_id) {
        this.fee_description_id = fee_description_id;
    }

    public String getFeeamount_id() {
        return feeamount_id;
    }

    public void setFeeamount_id(String feeamount_id) {
        this.feeamount_id = feeamount_id;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getNetamount() {
        return netamount;
    }

    public void setNetamount(String netamount) {
        this.netamount = netamount;
    }

    public String getPayableamount() {
        return payableamount;
    }

    public void setPayableamount(String payableamount) {
        this.payableamount = payableamount;
    }

    public String getBalanceamount() {
        return balanceamount;
    }

    public void setBalanceamount(String balanceamount) {
        this.balanceamount = balanceamount;
    }
    
    

    
}
