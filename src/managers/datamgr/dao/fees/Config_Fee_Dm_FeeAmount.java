package managers.datamgr.dao.fees;

/**
 *
 * @author Ashok
 */
public class Config_Fee_Dm_FeeAmount {
    String feeamount_id ;
    String session_id ;
    String class_id ;
    String feename_id ;
    String amount ;
    String feetype_id;

    public String getFeeamount_id() {
        return feeamount_id;
    }

    public void setFeeamount_id(String feeamount_id) {
        this.feeamount_id = feeamount_id;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getFeename_id() {
        return feename_id;
    }

    public void setFeename_id(String feename_id) {
        this.feename_id = feename_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFeetype_id() {
        return feetype_id;
    }

    public void setFeetype_id(String feetype_id) {
        this.feetype_id = feetype_id;
    }

    
    

    
}
