package edify;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import managers.clsmgr.configuration.ConfigMgr;
import managers.datamgr.configuration.Config;

public class Edify {
    
    Banner banner;

    public Edify() {
        banner = new Banner();
        Config.configmgr = new ConfigMgr();
    }
    
    private void showbanner() {        
        banner.setVisible(true);
        banner.setBannerLabel("Checking system configuration...");        
    }
    
    private void connserver() {
        banner.setBannerLabel("Connecting to server...");        
    }
    
    private void conndatabase() {
        banner.setBannerLabel("Checking database connection...");
        if (!Config.configmgr.loadDatabase()) {
            JOptionPane.showMessageDialog(banner,"Database connectivity problem, product can not be launch.","Error : 1003.",JOptionPane.ERROR_MESSAGE);            
            System.exit(0);
        }
    }
    
    private void initcomponent() {
        banner.setBannerLabel("Initialising components...");        
        int x=0;
        if (Config.configmgr.loadClassManager()){x++;} else {System.out.println("Error : 1, Class Manager loading problem.");}
        if (Config.configmgr.loadForms()){x++;} else {System.out.println("Error : 2, form loading problem.");}  
        if (x!=2) {
            JOptionPane.showMessageDialog(banner,"Product initialisation problem, product can not be launch.","Error.",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }
    
    private void hidebanner() {
        banner.dispose();        
        Config.homepage.onloadReset();
        Config.homepage.setVisible(true);
    }
    
    public static void main(String[] args) {        
        try {            
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Edify.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Edify obj = new Edify();
            obj.showbanner();
            Thread.sleep(1000);
            obj.connserver();
            Thread.sleep(1000);
            obj.conndatabase();
            Thread.sleep(1000);
            obj.initcomponent();
            Thread.sleep(1000);
            obj.hidebanner();
        } catch (InterruptedException ex) {
            System.out.println(ex);
            Logger.getLogger(Edify.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}