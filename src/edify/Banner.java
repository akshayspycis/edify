package edify;

import java.awt.Toolkit;

/**
 *
 * @author AMS
 */
public class Banner extends javax.swing.JFrame {
    /**
     * Creates new form Banner
     */
    public Banner() {
        initComponents();        
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/icon.png")));
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblShow = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Edify - Initialising Modules [INFOPARK INNOVATIONS]");
        setUndecorated(true);
        getContentPane().setLayout(null);

        lblShow.setForeground(new java.awt.Color(255, 255, 255));
        lblShow.setText("...");
        getContentPane().add(lblShow);
        lblShow.setBounds(340, 246, 360, 14);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/banner.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 720, 268);

        setSize(new java.awt.Dimension(719, 268));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblShow;
    // End of variables declaration//GEN-END:variables
    
    void setBannerLabel(String lable) {
        lblShow.setText(lable);
    }
}
