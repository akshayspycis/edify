package modules.storesandinventory.repository;


import javax.swing.JOptionPane;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.storesandinventory.Si_Dm_SubCategory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
 
public class Si_ViewSubCategory extends javax.swing.JFrame {

    public Si_ViewSubCategory(Object object, boolean b) {
       initComponents();
        this.setLocationRelativeTo(null);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_viewsubcat = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cmb_viewcateg = new javax.swing.JComboBox();
        btn_deletesubcat = new javax.swing.JButton();
        btn_updatesubcat = new javax.swing.JButton();
        lbl_subcatid = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "View SubCategory", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Sub Category Name :");

        jLabel2.setText("Category Name :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_viewcateg, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_viewsubcat)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmb_viewcateg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_viewsubcat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_deletesubcat.setText("Delete");
        btn_deletesubcat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deletesubcatActionPerformed(evt);
            }
        });

        btn_updatesubcat.setText("Update");
        btn_updatesubcat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updatesubcatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_subcatid, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_updatesubcat)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_deletesubcat)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbl_subcatid, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_updatesubcat)
                        .addComponent(btn_deletesubcat)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_updatesubcatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updatesubcatActionPerformed
            Si_Dm_SubCategory cs = new Si_Dm_SubCategory();
            cs.setSubcategory_id(lbl_subcatid.getText());
//            cs.setCategory_id(Config.si_dm_category.get(cmb_viewcateg.getSelectedIndex()).getCategory_id());
            cs.setSubcategoryname(txt_viewsubcat.getText());
            if(Config.si_cm_subcategorymgr.updsSiSubCategory(cs)){ 
                JOptionPane.showMessageDialog(this, "SubCategory updated Successfully", "Success",JOptionPane.NO_OPTION);
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "Error in Updation", "Error",JOptionPane.ERROR_MESSAGE );
            }       
    }//GEN-LAST:event_btn_updatesubcatActionPerformed

    private void btn_deletesubcatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deletesubcatActionPerformed
       
                              
             int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Subcategory?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {
             
                if(Config.si_cm_subcategorymgr.delStockSubCategory(lbl_subcatid.getText())){ 
//                Config.lm_repository_home.setCombo(cmb_categoryname.getSelectedIndex());
                JOptionPane.showMessageDialog(this, "SubCategory Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                dispose();
            
                 }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } else{
                   dispose();     
                   
        }
    }//GEN-LAST:event_btn_deletesubcatActionPerformed

  
    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Si_ViewSubCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Si_ViewSubCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Si_ViewSubCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Si_ViewSubCategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
               
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_deletesubcat;
    private javax.swing.JButton btn_updatesubcat;
    private javax.swing.JComboBox cmb_viewcateg;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lbl_subcatid;
    private javax.swing.JTextField txt_viewsubcat;
    // End of variables declaration//GEN-END:variables

void onloadReset(String subcategoryid) {
    lbl_subcatid.setText(subcategoryid);
    String catid = null;
    String catname = null;
    NodeList nList = Config.xml_si_subcategorymgr.doc.getElementsByTagName("si_subcategory");
    for (int i = 0; i < nList.getLength(); i++) {
        Node nNode = nList.item(i);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            Element eElement = (Element) nNode;
            if(eElement.getAttribute("subcategory_id").equals(subcategoryid))
                catid=eElement.getElementsByTagName("category_id").item(0).getTextContent();
        }
    }
    NodeList nList1 = Config.xml_si_categorymgr.doc.getElementsByTagName("si_category");
    for (int j = 0; j < nList1.getLength(); j++) {
        Node nNode1 = nList1.item(j);
        if (nNode1.getNodeType() == Node.ELEMENT_NODE) {
            Element eElement = (Element) nNode1;
            cmb_viewcateg.addItem(eElement.getElementsByTagName("categoryname").item(0).getTextContent());
            if(catid.equals(eElement.getAttribute("category_id"))){
                catname=eElement.getElementsByTagName("catgoryname").item(0).getTextContent();
            }
        }
        cmb_viewcateg.setSelectedItem(catname);
        for (int k= 0; k < nList.getLength(); k++) {
            Node nNode2 = nList.item(k);
            if (nNode2.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode2;
                if(eElement.getAttribute("subcategory_id").equals(subcategoryid))
                    txt_viewsubcat.setText(eElement.getElementsByTagName("subcategoryname").item(0).getTextContent());
                break;
            }
        }
    }
}
}


