package modules.storesandinventory.repository;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.storesandinventory.Si_Dm_SubCategory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Si_SubCategoryNewEntry extends javax.swing.JDialog {
     DefaultTableModel tbl_subcatSiModel;
  
    public Si_SubCategoryNewEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_subcatSiModel=(DefaultTableModel)tbl_subcategory.getModel();

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private Si_SubCategoryNewEntry() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        btn_SubCat_Cancel = new javax.swing.JButton();
        btn_SubCat_Save = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lbl_SubCat_CatName = new javax.swing.JLabel();
        cmb_categoryname = new javax.swing.JComboBox();
        lbl_SubCat_Name = new javax.swing.JLabel();
        txt_subcatname = new javax.swing.JTextField();
        btn_SubCat_Add = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_subcategory = new javax.swing.JTable();
        lbl_categoryid = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_SubCat_Cancel.setText("Cancel");
        btn_SubCat_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubCat_CancelActionPerformed(evt);
            }
        });

        btn_SubCat_Save.setText("Save");
        btn_SubCat_Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubCat_SaveActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Add Subcategory", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        lbl_SubCat_CatName.setText("Category Name :");

        lbl_SubCat_Name.setText("Sub Category Name");

        btn_SubCat_Add.setText("Add");
        btn_SubCat_Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubCat_AddActionPerformed(evt);
            }
        });

        tbl_subcategory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No.", "Sub Category Name :"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbl_subcategory);
        if (tbl_subcategory.getColumnModel().getColumnCount() > 0) {
            tbl_subcategory.getColumnModel().getColumn(0).setResizable(false);
            tbl_subcategory.getColumnModel().getColumn(0).setPreferredWidth(300);
            tbl_subcategory.getColumnModel().getColumn(1).setResizable(false);
            tbl_subcategory.getColumnModel().getColumn(1).setPreferredWidth(700);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lbl_SubCat_Name, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_SubCat_CatName, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_subcatname)
                            .addComponent(cmb_categoryname, 0, 204, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubCat_Add, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 435, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_SubCat_CatName)
                    .addComponent(cmb_categoryname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_SubCat_Name)
                    .addComponent(txt_subcatname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_SubCat_Add))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 472, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_categoryid, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_SubCat_Save)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubCat_Cancel)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SubCat_Cancel, btn_SubCat_Save});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_categoryid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_SubCat_Save)
                        .addComponent(btn_SubCat_Cancel)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_SubCat_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubCat_CancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_SubCat_CancelActionPerformed

  
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_SubCat_SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubCat_SaveActionPerformed
//       String str = Validity();
//        if(str.equals("ok"))
//       { 
//           managrs.datamgr.configuration.Config.si_dm_subcategory s=new  managrs.datamgr.configuration.Config.si_dm_subcategory ();
       ArrayList<Si_Dm_SubCategory> si = new ArrayList<>();
        try {
            for (int i = 0; i < tbl_subcategory.getRowCount(); i++) {
                managers.datamgr.dao.storesandinventory.Si_Dm_SubCategory s =new managers.datamgr.dao.storesandinventory.Si_Dm_SubCategory ();
//           s.setCategory_id(Config.xml_si_categorymgr.get(cmb_categoryname.getSelectedIndex()).getCategory_id());     
                s.setSubcategoryname(tbl_subcategory.getValueAt(i, 1).toString());
                si.add(s);
               }
            if (Config.si_cm_subcategorymgr.insStockSubCategory(si)) {
                onloadReset();
                Config.si_home.onloadReset();
                JOptionPane.showMessageDialog(this,"Subcategory Insertion successfully.","Subcategory Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in Insertion Subcategory","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
//        }
    }//GEN-LAST:event_btn_SubCat_SaveActionPerformed

    private void btn_SubCat_AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubCat_AddActionPerformed
        try {
        if (!txt_subcatname.getText().trim().equals("")) {        
            int i;
            for (i = 0; i < tbl_subcatSiModel.getRowCount(); i++) 
            {
                if (txt_subcatname.getText().trim().equals(tbl_subcatSiModel.getValueAt(i, 1).toString())){  
                    break;
                }
            }
            if (i == tbl_subcatSiModel.getRowCount()) {
                tbl_subcatSiModel.addRow(new Object[] {String.valueOf(i+1),txt_subcatname.getText().trim()});
                txt_subcatname.setText(""); 
                txt_subcatname.requestFocus();
            }else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_subcatname.setText("");
                txt_subcatname.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_subcatname.setText("");
        }
        } catch (Exception e) {
            e.printStackTrace();
        }    
    }//GEN-LAST:event_btn_SubCat_AddActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_SubCat_Add;
    private javax.swing.JButton btn_SubCat_Cancel;
    private javax.swing.JButton btn_SubCat_Save;
    private javax.swing.JComboBox cmb_categoryname;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_SubCat_CatName;
    private javax.swing.JLabel lbl_SubCat_Name;
    private javax.swing.JLabel lbl_categoryid;
    private javax.swing.JTable tbl_subcategory;
    private javax.swing.JTextField txt_subcatname;
    // End of variables declaration//GEN-END:variables

    void onloadReset() {
        try {
            
             NodeList nList = Config.xml_si_categorymgr.doc.getElementsByTagName("si_category");
             cmb_categoryname.removeAllItems();
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_categoryname.addItem(eElement.getElementsByTagName("categoryname").item(0).getTextContent().toString());
                }
            }
        txt_subcatname.setText("");
        tbl_subcatSiModel.setRowCount(0);
        } catch (Exception e) {
        e.printStackTrace();
        }     
    }  
}
