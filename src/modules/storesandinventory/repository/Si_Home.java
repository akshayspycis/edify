package modules.storesandinventory.repository;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Si_Home extends javax.swing.JFrame {
DefaultTableModel tbl_sup_Model;
   DefaultTableModel tbl_CategModel;
    DefaultTableModel tbl_Si_SubCatModel;
    public Si_Home() {
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_sup_Model = (DefaultTableModel) tbl_sup.getModel();
        tbl_CategModel = (DefaultTableModel) tbl_Categ.getModel();
        tbl_Si_SubCatModel=(DefaultTableModel) tbl_Si_subcat.getModel();
    }
 
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lbl_SearchBy = new javax.swing.JLabel();
        btn_SupplierSearch = new javax.swing.JButton();
        btn_NewEntry = new javax.swing.JButton();
        cmb_SearchBy = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        txt_SearchSupplier = new javax.swing.JTextField();
        lbl_TotalCount = new javax.swing.JLabel();
        txt_TotalCount = new javax.swing.JTextField();
        btn_SupplierView = new javax.swing.JButton();
        btn_Delete = new javax.swing.JButton();
        btn_Export = new javax.swing.JButton();
        btn_Print = new javax.swing.JButton();
        btn_Cancel = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_sup = new javax.swing.JTable();
        btn_Refresh = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        btn_StoPro_SubCatPrint = new javax.swing.JButton();
        btn_StoPro_SubCatCancel = new javax.swing.JButton();
        btn_StoPro_SubCatRefresh = new javax.swing.JButton();
        jSplitPane3 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        lbl_StoPro_CatSearch = new javax.swing.JLabel();
        txt_StockcateSearch = new javax.swing.JTextField();
        btn_StockCatSearch = new javax.swing.JButton();
        btn_Sto_CatNewEntry = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_Categ = new javax.swing.JTable();
        btn_CatView = new javax.swing.JButton();
        btn_CatDelete = new javax.swing.JButton();
        lbl_StoPro_SubCatTotlCount = new javax.swing.JLabel();
        txt_CatTotlCount = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cmb_category = new javax.swing.JComboBox();
        lbl_StoPro_SubCatSearch = new javax.swing.JLabel();
        txt_searchsubcategory = new javax.swing.JTextField();
        btn_SubCatSearch = new javax.swing.JButton();
        btn_Sto_SubCatNewEntry = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_Si_subcat = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txt_subtotalcount = new javax.swing.JTextField();
        btn_viewsubcategory = new javax.swing.JButton();
        btn_sideletesub = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel7.setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Data Repository");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        lbl_SearchBy.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_SearchBy.setText("Search By :");

        btn_SupplierSearch.setText("Search");
        btn_SupplierSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SupplierSearchActionPerformed(evt);
            }
        });

        btn_NewEntry.setText("New Entry");
        btn_NewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_NewEntryActionPerformed(evt);
            }
        });

        cmb_SearchBy.setMaximumRowCount(5);
        cmb_SearchBy.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Supplier_id", "Name", "Orgname", "City", "Locality" }));
        cmb_SearchBy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_SearchByActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        txt_SearchSupplier.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_SearchSupplierCaretUpdate(evt);
            }
        });
        txt_SearchSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_SearchSupplierActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_SearchBy)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_SearchBy, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_SearchSupplier)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_SupplierSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_NewEntry)
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_NewEntry, btn_SupplierSearch});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_SearchBy)
                        .addComponent(cmb_SearchBy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SupplierSearch)
                        .addComponent(btn_NewEntry)
                        .addComponent(txt_SearchSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_NewEntry, btn_SupplierSearch, cmb_SearchBy, jSeparator1, lbl_SearchBy});

        lbl_TotalCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_TotalCount.setText("Total Count :");

        txt_TotalCount.setEditable(false);

        btn_SupplierView.setText("View");
        btn_SupplierView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SupplierViewActionPerformed(evt);
            }
        });

        btn_Delete.setLabel("Delete");
        btn_Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DeleteActionPerformed(evt);
            }
        });

        btn_Export.setLabel("Export");

        btn_Print.setLabel("Print");
        btn_Print.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_PrintActionPerformed(evt);
            }
        });

        btn_Cancel.setLabel("Cancel");
        btn_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CancelActionPerformed(evt);
            }
        });

        tbl_sup.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S. No.", "Supplier ID", "Supplier Name", "Organization Name ", "Contact No.", "Address", "Email"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_sup.getTableHeader().setReorderingAllowed(false);
        tbl_sup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_supMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_sup);
        if (tbl_sup.getColumnModel().getColumnCount() > 0) {
            tbl_sup.getColumnModel().getColumn(0).setResizable(false);
            tbl_sup.getColumnModel().getColumn(0).setPreferredWidth(10);
            tbl_sup.getColumnModel().getColumn(1).setResizable(false);
            tbl_sup.getColumnModel().getColumn(1).setPreferredWidth(40);
            tbl_sup.getColumnModel().getColumn(2).setResizable(false);
            tbl_sup.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbl_sup.getColumnModel().getColumn(3).setResizable(false);
            tbl_sup.getColumnModel().getColumn(3).setPreferredWidth(100);
            tbl_sup.getColumnModel().getColumn(4).setResizable(false);
            tbl_sup.getColumnModel().getColumn(4).setPreferredWidth(70);
            tbl_sup.getColumnModel().getColumn(5).setResizable(false);
            tbl_sup.getColumnModel().getColumn(5).setPreferredWidth(300);
            tbl_sup.getColumnModel().getColumn(6).setResizable(false);
            tbl_sup.getColumnModel().getColumn(6).setPreferredWidth(80);
        }

        btn_Refresh.setText("Refresh");
        btn_Refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_RefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(lbl_TotalCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_TotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
                        .addComponent(btn_Refresh, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SupplierView, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Delete, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Export, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Print, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Cancel, btn_Delete, btn_Export, btn_Print, btn_Refresh, btn_SupplierView});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_TotalCount)
                    .addComponent(txt_TotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_SupplierView)
                    .addComponent(btn_Delete)
                    .addComponent(btn_Export)
                    .addComponent(btn_Print)
                    .addComponent(btn_Cancel)
                    .addComponent(btn_Refresh))
                .addGap(6, 6, 6))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Supplier Management", jPanel4);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        btn_StoPro_SubCatPrint.setLabel("Print");

        btn_StoPro_SubCatCancel.setLabel("Cancel");
        btn_StoPro_SubCatCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StoPro_SubCatCancelActionPerformed(evt);
            }
        });

        btn_StoPro_SubCatRefresh.setText("Refresh");
        btn_StoPro_SubCatRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StoPro_SubCatRefreshActionPerformed(evt);
            }
        });

        jSplitPane3.setBorder(null);
        jSplitPane3.setDividerLocation(473);
        jSplitPane3.setDividerSize(2);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Category", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        lbl_StoPro_CatSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_StoPro_CatSearch.setText("Category Name:");

        txt_StockcateSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_StockcateSearchCaretUpdate(evt);
            }
        });

        btn_StockCatSearch.setText("Search");
        btn_StockCatSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StockCatSearchActionPerformed(evt);
            }
        });

        btn_Sto_CatNewEntry.setText("New Entry");
        btn_Sto_CatNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sto_CatNewEntryActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        tbl_Categ.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Category ID", "Category Name"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Categ.getTableHeader().setReorderingAllowed(false);
        tbl_Categ.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_CategMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_Categ);
        if (tbl_Categ.getColumnModel().getColumnCount() > 0) {
            tbl_Categ.getColumnModel().getColumn(0).setResizable(false);
            tbl_Categ.getColumnModel().getColumn(0).setPreferredWidth(150);
            tbl_Categ.getColumnModel().getColumn(1).setResizable(false);
            tbl_Categ.getColumnModel().getColumn(1).setPreferredWidth(600);
        }

        btn_CatView.setText("View");
        btn_CatView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CatViewActionPerformed(evt);
            }
        });

        btn_CatDelete.setLabel("Delete");
        btn_CatDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CatDeleteActionPerformed(evt);
            }
        });

        lbl_StoPro_SubCatTotlCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_StoPro_SubCatTotlCount.setText("Total Count :");

        txt_CatTotlCount.setEditable(false);
        txt_CatTotlCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_CatTotlCountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lbl_StoPro_SubCatTotlCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_CatTotlCount, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_CatView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_CatDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lbl_StoPro_CatSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_StockcateSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StockCatSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_Sto_CatNewEntry)))
                .addGap(10, 10, 10))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_CatDelete, btn_CatView});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Sto_CatNewEntry, btn_StockCatSearch});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_StoPro_CatSearch)
                        .addComponent(txt_StockcateSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_StockCatSearch)
                        .addComponent(btn_Sto_CatNewEntry, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_StoPro_SubCatTotlCount)
                        .addComponent(txt_CatTotlCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_CatView)
                        .addComponent(btn_CatDelete))))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_Sto_CatNewEntry, btn_StockCatSearch, jSeparator2, lbl_StoPro_CatSearch});

        jSplitPane3.setLeftComponent(jPanel3);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SubCategory", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel2.setText("Category Name :");

        cmb_category.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_categoryActionPerformed(evt);
            }
        });

        lbl_StoPro_SubCatSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_StoPro_SubCatSearch.setText("Subcat. Name :");

        txt_searchsubcategory.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_searchsubcategoryCaretUpdate(evt);
            }
        });

        btn_SubCatSearch.setText("Search");
        btn_SubCatSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubCatSearchActionPerformed(evt);
            }
        });

        btn_Sto_SubCatNewEntry.setText("New Entry");
        btn_Sto_SubCatNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sto_SubCatNewEntryActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        tbl_Si_subcat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sub Category ID", "Sub Category Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Si_subcat.getTableHeader().setReorderingAllowed(false);
        tbl_Si_subcat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_Si_subcatMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tbl_Si_subcat);
        if (tbl_Si_subcat.getColumnModel().getColumnCount() > 0) {
            tbl_Si_subcat.getColumnModel().getColumn(0).setResizable(false);
            tbl_Si_subcat.getColumnModel().getColumn(0).setPreferredWidth(200);
            tbl_Si_subcat.getColumnModel().getColumn(1).setResizable(false);
            tbl_Si_subcat.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Total Count :");

        txt_subtotalcount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_subtotalcountActionPerformed(evt);
            }
        });

        btn_viewsubcategory.setText("View");
        btn_viewsubcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewsubcategoryActionPerformed(evt);
            }
        });

        btn_sideletesub.setText("Delete");
        btn_sideletesub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sideletesubActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_category, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(lbl_StoPro_SubCatSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_searchsubcategory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubCatSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_Sto_SubCatNewEntry, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_subtotalcount, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 110, Short.MAX_VALUE)
                        .addComponent(btn_viewsubcategory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_sideletesub)))
                .addContainerGap())
            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel2, lbl_StoPro_SubCatSearch});

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Sto_SubCatNewEntry, btn_SubCatSearch});

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_sideletesub, btn_viewsubcategory});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmb_category, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_StoPro_SubCatSearch)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_searchsubcategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SubCatSearch))
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_Sto_SubCatNewEntry))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_subtotalcount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_viewsubcategory)
                    .addComponent(btn_sideletesub)))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_Sto_SubCatNewEntry, btn_SubCatSearch, jSeparator3, lbl_StoPro_SubCatSearch});

        jSplitPane3.setRightComponent(jPanel6);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSplitPane3)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_StoPro_SubCatRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StoPro_SubCatPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StoPro_SubCatCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_StoPro_SubCatPrint)
                    .addComponent(btn_StoPro_SubCatCancel)
                    .addComponent(btn_StoPro_SubCatRefresh))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Stock Products", jPanel8);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Supplier");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmb_SearchByActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_SearchByActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_SearchByActionPerformed

    private void btn_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_CancelActionPerformed

    private void btn_RefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_RefreshActionPerformed
        txt_SearchSupplier.setText("");
        cmb_SearchBy.setToolTipText("");
        onloadReset();
    }//GEN-LAST:event_btn_RefreshActionPerformed

    private void btn_StoPro_SubCatCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StoPro_SubCatCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_StoPro_SubCatCancelActionPerformed

    private void btn_CatViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CatViewActionPerformed
        Config.si_viewcategory.onloadReset(tbl_CategModel.getValueAt(tbl_Categ.getSelectedRow(), 0).toString());
        Config.si_viewcategory.setVisible(true); 
    }//GEN-LAST:event_btn_CatViewActionPerformed

    private void btn_PrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_PrintActionPerformed

    }//GEN-LAST:event_btn_PrintActionPerformed

    private void btn_NewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_NewEntryActionPerformed
         Config.si_suppliernewentry.onloadReset();
         Config.si_suppliernewentry.setVisible(true);
    }//GEN-LAST:event_btn_NewEntryActionPerformed

    private void btn_Sto_CatNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sto_CatNewEntryActionPerformed
         Config.si_categorynewentry.onloadReset();
         Config.si_categorynewentry.setVisible(true);
    }//GEN-LAST:event_btn_Sto_CatNewEntryActionPerformed

    private void btn_Sto_SubCatNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sto_SubCatNewEntryActionPerformed
         Config.si_subcategorynewentry.onloadReset();
         Config.si_subcategorynewentry.setVisible(true);
    }//GEN-LAST:event_btn_Sto_SubCatNewEntryActionPerformed

    private void btn_SupplierViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SupplierViewActionPerformed
        Config.si_viewsupplier.onloadReset(tbl_sup_Model.getValueAt(tbl_sup.getSelectedRow(), 1).toString());
        Config.si_viewsupplier.setVisible(true);      
    }//GEN-LAST:event_btn_SupplierViewActionPerformed

    private void btn_StockCatSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StockCatSearchActionPerformed
          try {
            tbl_CategModel.setRowCount(0);            
                NodeList nList = Config.xml_si_categorymgr.doc.getElementsByTagName("si_category");
            for (int i = 0; i < nList.getLength(); i++){
                  Node nNode = nList.item(i);
                  Element eElement = (Element) nNode;
                   if (!txt_StockcateSearch.getText().equals("")) {
                    if(eElement.getElementsByTagName("categoryname").item(0).getTextContent().toUpperCase().startsWith(txt_StockcateSearch.getText().toUpperCase())) {
                        tbl_CategModel.addRow(new Object[] {eElement.getAttribute("category_id"),eElement.getElementsByTagName("categoryname").item(0).getTextContent()
                            });
                    }
            }else {
                       tbl_CategModel.addRow(new Object[] {   
                           eElement.getAttribute("category_id"),
                           eElement.getElementsByTagName("categoryname").item(0).getTextContent()
                      });          
                }
            }
            
        } catch (Exception e) {
        e.printStackTrace();
        }
    }//GEN-LAST:event_btn_StockCatSearchActionPerformed

    private void txt_StockcateSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_StockcateSearchCaretUpdate
        btn_StockCatSearchActionPerformed(null);
    }//GEN-LAST:event_txt_StockcateSearchCaretUpdate

    private void btn_SupplierSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SupplierSearchActionPerformed
        try {
             tbl_sup_Model.setRowCount(0);
     NodeList nList = Config.xml_supplierprofilemgr.doc.getElementsByTagName("supplierprofile");
            for (int i = 0; i < nList.getLength(); i++){
                 Node nNode = nList.item(i);
                  Element eElement = (Element) nNode;
                  if (cmb_SearchBy.getSelectedItem().equals("Supplier_id")) {             
                    if(eElement.getAttribute("supplier_id").startsWith(txt_SearchSupplier.getText())) {
                        tbl_sup_Model.addRow(new Object[] {i+1,
                            eElement.getAttribute("supplier_id"),
                            eElement.getElementsByTagName("name").item(0).getTextContent(),
                            eElement.getElementsByTagName("orgname").item(0).getTextContent(),
                            eElement.getElementsByTagName("contactno").item(0).getTextContent(), 
                            eElement.getElementsByTagName("locality").item(0).getTextContent()+","+ eElement.getElementsByTagName("city").item(0).getTextContent()+"," +eElement.getElementsByTagName("state").item(0).getTextContent()+","+ eElement.getElementsByTagName("country").item(0).getTextContent(),
                            eElement.getElementsByTagName("emailid").item(0).getTextContent()  
                        });
                    }
                  }else if(cmb_SearchBy.getSelectedItem().equals("Name")){
                      if(eElement.getElementsByTagName("name").item(0).getTextContent().toUpperCase().startsWith(txt_SearchSupplier.getText().toUpperCase())) {
                          tbl_sup_Model.addRow(new Object[] {i+1,
                              eElement.getAttribute("supplier_id"),
                              eElement.getElementsByTagName("name").item(0).getTextContent(),
                              eElement.getElementsByTagName("orgname").item(0).getTextContent(),
                              eElement.getElementsByTagName("contactno").item(0).getTextContent(), 
                              eElement.getElementsByTagName("locality").item(0).getTextContent()+","+ eElement.getElementsByTagName("city").item(0).getTextContent()+"," +eElement.getElementsByTagName("state").item(0).getTextContent()+","+ eElement.getElementsByTagName("country").item(0).getTextContent(),
                              eElement.getElementsByTagName("emailid").item(0).getTextContent() 
                          });
                      }
                  }else if(cmb_SearchBy.getSelectedItem().equals("Orgname")){
                      if(eElement.getElementsByTagName("orgname").item(0).getTextContent().toUpperCase().startsWith(txt_SearchSupplier.getText().toUpperCase())){
                          tbl_sup_Model.addRow(new Object[] {i+1,
                              eElement.getAttribute("supplier_id"),
                              eElement.getElementsByTagName("name").item(0).getTextContent(),
                              eElement.getElementsByTagName("orgname").item(0).getTextContent(),
                              eElement.getElementsByTagName("contactno").item(0).getTextContent(), 
                              eElement.getElementsByTagName("locality").item(0).getTextContent()+","+ eElement.getElementsByTagName("city").item(0).getTextContent()+"," +eElement.getElementsByTagName("state").item(0).getTextContent()+","+ eElement.getElementsByTagName("country").item(0).getTextContent(),
                              eElement.getElementsByTagName("emailid").item(0).getTextContent() 
                          }); 
                      }
                  }else if(cmb_SearchBy.getSelectedItem().equals("City")){
                      if(eElement.getElementsByTagName("city").item(0).getTextContent().toUpperCase().startsWith(txt_SearchSupplier.getText().toUpperCase())) {
                          tbl_sup_Model.addRow(new Object[] {i+1,
                              eElement.getAttribute("supplier_id"),
                              eElement.getElementsByTagName("name").item(0).getTextContent(),
                              eElement.getElementsByTagName("orgname").item(0).getTextContent(),
                              eElement.getElementsByTagName("contactno").item(0).getTextContent(), 
                              eElement.getElementsByTagName("locality").item(0).getTextContent()+","+ eElement.getElementsByTagName("city").item(0).getTextContent()+"," +eElement.getElementsByTagName("state").item(0).getTextContent()+","+ eElement.getElementsByTagName("country").item(0).getTextContent(),
                              eElement.getElementsByTagName("emailid").item(0).getTextContent() 
                          });
                      }
                  }else if(cmb_SearchBy.getSelectedItem().equals("Locality")){
                      if(eElement.getElementsByTagName("locality").item(0).getTextContent().toUpperCase().startsWith(txt_SearchSupplier.getText().toUpperCase())) {
                          tbl_sup_Model.addRow(new Object[] {i+1,
                              eElement.getAttribute("supplier_id"),
                              eElement.getElementsByTagName("name").item(0).getTextContent(),
                              eElement.getElementsByTagName("orgname").item(0).getTextContent(),
                              eElement.getElementsByTagName("contactno").item(0).getTextContent(), 
                              eElement.getElementsByTagName("locality").item(0).getTextContent()+","+ eElement.getElementsByTagName("city").item(0).getTextContent()+"," +eElement.getElementsByTagName("state").item(0).getTextContent()+","+ eElement.getElementsByTagName("country").item(0).getTextContent(),
                              eElement.getElementsByTagName("emailid").item(0).getTextContent() 
                          });
                      }
                  }else if(cmb_SearchBy.getSelectedIndex()==0){
                      if(eElement.getElementsByTagName("orgname").item(0).getTextContent().toUpperCase().startsWith(txt_SearchSupplier.getText().toUpperCase())) {
                          tbl_sup_Model.addRow(new Object[] {i+1,
                              eElement.getAttribute("supplier_id"),
                              eElement.getElementsByTagName("name").item(0).getTextContent(),
                              eElement.getElementsByTagName("orgname").item(0).getTextContent(),
                              eElement.getElementsByTagName("contactno").item(0).getTextContent(), 
                              eElement.getElementsByTagName("locality").item(0).getTextContent()+","+ eElement.getElementsByTagName("city").item(0).getTextContent()+"," +eElement.getElementsByTagName("state").item(0).getTextContent()+","+ eElement.getElementsByTagName("country").item(0).getTextContent(),
                              eElement.getElementsByTagName("emailid").item(0).getTextContent() 
                          });     
                      }
                  }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_SupplierSearchActionPerformed

    private void txt_SearchSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_SearchSupplierActionPerformed
     
    }//GEN-LAST:event_txt_SearchSupplierActionPerformed

    private void txt_SearchSupplierCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_SearchSupplierCaretUpdate
        btn_SupplierSearchActionPerformed(null);
    }//GEN-LAST:event_txt_SearchSupplierCaretUpdate

    private void tbl_supMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_supMouseClicked
     if(evt.getClickCount()==2){
         btn_SupplierViewActionPerformed(null);
     }
    }//GEN-LAST:event_tbl_supMouseClicked

    private void tbl_CategMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_CategMouseClicked
       if(evt.getClickCount()==2){
           btn_CatViewActionPerformed(null);
       }
    }//GEN-LAST:event_tbl_CategMouseClicked

    private void btn_SubCatSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubCatSearchActionPerformed
//        try {
//            tbl_Si_SubCatModel.setRowCount(0);
//            if (!txt_searchsubcategory.getText().equalsIgnoreCase("")) {
//                NodeList nList = Config.xml_si_subcategorymgr.doc.getElementsByTagName("si_subcategory");
//                for (int i = 0; i < nList.getLength(); i++) {
//                    Node nNode=nList.item(i);
//                    Element eElement=(Element) nNode;
//                    if(eElement.getAttribute("subcategory_id").startsWith(Config.xml_si_categorymgr.get(cmb_category.getSelectedIndex()).getCategory_id()) && eElement.getElementsByTagName("subcategoryname").equals(txt_searchsubcategory.getText() )) {
//                        tbl_Si_SubCatModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});
//                    }
//                }
//            } else {
//                NodeList nList = Config.xml_si_subcategorymgr.doc.getElementsByTagName("si_subcategory");
//                for (int i = 0; i < nList.getLength(); i++) {
//                    Node nNode=nList.item(i);
//                    Element eElement=(Element) nNode;
//                    if(eElement.getAttribute("subcategory_id").startsWith(Config.xml_si_categorymgr.get(cmb_category.getSelectedIndex()).getCategory_id()) && eElement.getElementsByTagName("subcategoryname").equals(txt_searchsubcategory.getText() )) {
//                        tbl_Si_SubCatModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});
//                    }
//                }
//            }for (int i = 0; i < tbl_Si_SubCatModel.getRowCount(); i++){
//                tbl_Si_SubCatModel.setValueAt(i+1, i,0);
//            }
//        } catch (Exception e){
//            e.printStackTrace();
//        }      
        
    }//GEN-LAST:event_btn_SubCatSearchActionPerformed

    private void txt_subtotalcountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_subtotalcountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_subtotalcountActionPerformed

    private void btn_viewsubcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewsubcategoryActionPerformed
        Config.si_viewsubcategory.onloadReset(tbl_Si_SubCatModel.getValueAt(tbl_Si_subcat.getSelectedRow(), 0).toString());
        Config.si_viewsubcategory.setVisible(true);       
    }//GEN-LAST:event_btn_viewsubcategoryActionPerformed

    private void cmb_categoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_categoryActionPerformed
        try {
            int i=0;       
            tbl_Si_SubCatModel.setRowCount(0);
             NodeList nList1 = Config.xml_si_categorymgr.doc.getElementsByTagName("si_category");
             Element element = (Element)Config.xml_si_categorymgr.xpath.evaluate("//*[@category_id='']",Config.xml_si_categorymgr.doc,XPathConstants.NODE);
             NodeList nList = Config.xml_si_subcategorymgr.doc.getElementsByTagName("si_subcategory");
             for (int k = 0; k < nList.getLength(); k++){
                 Node nNode = nList.item(k);
                 if(nNode.getNodeType()==Node.ELEMENT_NODE){
                     Element eElement = (Element) nNode;              
                     int j=cmb_category.getSelectedIndex();
                     if(j==k){
                         tbl_Si_SubCatModel.addRow(new Object[] {eElement.getAttribute("subcategory_id").toString(),eElement.getElementsByTagName("subcategoryname").toString()});
                     }
                 }
             }                  
        }catch (Exception e){
            e.printStackTrace();                
        }        
    }//GEN-LAST:event_cmb_categoryActionPerformed

    private void txt_searchsubcategoryCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_searchsubcategoryCaretUpdate
        btn_SubCatSearchActionPerformed(null);
    }//GEN-LAST:event_txt_searchsubcategoryCaretUpdate

    private void btn_StoPro_SubCatRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StoPro_SubCatRefreshActionPerformed
       onloadReset();
    }//GEN-LAST:event_btn_StoPro_SubCatRefreshActionPerformed

    private void txt_CatTotlCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_CatTotlCountActionPerformed
        
    
    }//GEN-LAST:event_txt_CatTotlCountActionPerformed

    private void btn_DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DeleteActionPerformed
         String catid = tbl_sup.getValueAt(tbl_sup.getSelectedRow(),1).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Supplier?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.si_cm_supplierprofilemgr.delSi_SupplierProfile(catid)){ 
                    JOptionPane.showMessageDialog(this, "Supplier Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                 }else{
                    JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } 
    }//GEN-LAST:event_btn_DeleteActionPerformed

    private void btn_CatDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CatDeleteActionPerformed
        String catid = tbl_Categ.getValueAt(tbl_Categ.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Category?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {    
                 if(Config.si_cm_categorymgr.delStockCategory(catid)){
                     JOptionPane.showMessageDialog(this, "Category Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                     }else{
                     JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             }
    }//GEN-LAST:event_btn_CatDeleteActionPerformed

    private void tbl_Si_subcatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_Si_subcatMouseClicked
         if(evt.getClickCount()==2){
             btn_viewsubcategoryActionPerformed(null);
         }
    }//GEN-LAST:event_tbl_Si_subcatMouseClicked

    private void btn_sideletesubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sideletesubActionPerformed
         String subid = tbl_Si_subcat.getValueAt(tbl_Si_subcat.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Subcategory?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.si_cm_subcategorymgr.delStockSubCategory(subid)){ 
                JOptionPane.showMessageDialog(this, "SubCategory Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                }
             } 
    }//GEN-LAST:event_btn_sideletesubActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Cancel;
    private javax.swing.JButton btn_CatDelete;
    private javax.swing.JButton btn_CatView;
    private javax.swing.JButton btn_Delete;
    private javax.swing.JButton btn_Export;
    private javax.swing.JButton btn_NewEntry;
    private javax.swing.JButton btn_Print;
    private javax.swing.JButton btn_Refresh;
    private javax.swing.JButton btn_StoPro_SubCatCancel;
    private javax.swing.JButton btn_StoPro_SubCatPrint;
    private javax.swing.JButton btn_StoPro_SubCatRefresh;
    private javax.swing.JButton btn_Sto_CatNewEntry;
    private javax.swing.JButton btn_Sto_SubCatNewEntry;
    private javax.swing.JButton btn_StockCatSearch;
    private javax.swing.JButton btn_SubCatSearch;
    private javax.swing.JButton btn_SupplierSearch;
    private javax.swing.JButton btn_SupplierView;
    private javax.swing.JButton btn_sideletesub;
    private javax.swing.JButton btn_viewsubcategory;
    private javax.swing.JComboBox cmb_SearchBy;
    private javax.swing.JComboBox cmb_category;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JSplitPane jSplitPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbl_SearchBy;
    private javax.swing.JLabel lbl_StoPro_CatSearch;
    private javax.swing.JLabel lbl_StoPro_SubCatSearch;
    private javax.swing.JLabel lbl_StoPro_SubCatTotlCount;
    private javax.swing.JLabel lbl_TotalCount;
    private javax.swing.JTable tbl_Categ;
    private javax.swing.JTable tbl_Si_subcat;
    private javax.swing.JTable tbl_sup;
    private javax.swing.JTextField txt_CatTotlCount;
    private javax.swing.JTextField txt_SearchSupplier;
    private javax.swing.JTextField txt_StockcateSearch;
    private javax.swing.JTextField txt_TotalCount;
    private javax.swing.JTextField txt_searchsubcategory;
    private javax.swing.JTextField txt_subtotalcount;
    // End of variables declaration//GEN-END:variables
 public void onloadReset(){
     tbl_sup_Model.setRowCount(0);
     NodeList nList = Config.xml_supplierprofilemgr.doc.getElementsByTagName("supplierprofile");
            for (int i = 0; i < nList.getLength(); i++){
                 Node nNode = nList.item(i);
                  Element eElement = (Element) nNode;
                  tbl_sup_Model.addRow(new Object[] {i+1,
                      eElement.getAttribute("supplier_id").toString(),                
                      eElement.getElementsByTagName("name").item(0).getTextContent(),
                      eElement.getElementsByTagName("orgname").item(0).getTextContent(),
                      eElement.getElementsByTagName("contactno").item(0).getTextContent(),
                      eElement.getElementsByTagName("locality").item(0).getTextContent()
                              +","+ eElement.getElementsByTagName("city").item(0).getTextContent()
                              +"," +eElement.getElementsByTagName("state").item(0).getTextContent()
                              +","+ eElement.getElementsByTagName("country").item(0).getTextContent(),
                      eElement.getElementsByTagName("emailid").item(0).getTextContent()
                  });
            }
            cmb_category.removeAllItems();
            tbl_CategModel.setRowCount(0);   
            NodeList nList1=Config.xml_si_categorymgr.doc.getElementsByTagName("si_category");
            for (int i = 0; i < nList1.getLength(); i++){
                Node nNode = nList1.item(i);
                if(nNode.getNodeType()==Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    tbl_CategModel.addRow(new Object[] {eElement.getAttribute("category_id"),
                        eElement.getElementsByTagName("categoryname").item(0).getTextContent()});                         
                        cmb_category.addItem(eElement.getElementsByTagName("categoryname").item(0).getTextContent());
                    }
                }
            tbl_Si_SubCatModel.setRowCount(0);
            NodeList nList2=Config.xml_si_subcategorymgr.doc.getElementsByTagName("si_subcategory");
            for (int i = 0; i < nList2.getLength(); i++){
                Node nNode = nList2.item(i);
                if(nNode.getNodeType()==Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    tbl_Si_SubCatModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),
                        eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});
                }
            }              
            txt_StockcateSearch.setText("");
            txt_searchsubcategory.setText("");
            txt_SearchSupplier.setText("");
 }
}