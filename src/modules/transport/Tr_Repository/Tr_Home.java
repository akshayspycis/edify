package modules.transport.Tr_Repository;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.table.DefaultTableModel;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ashok
 */
public class Tr_Home extends javax.swing.JDialog {
    
    DefaultTableModel tbl_vehicledetailmodel; 
    DefaultTableModel tbl_routedetailmodel;
    DefaultTableModel tbl_stopdetailmodel;

   
    
    
    public Tr_Home() {
        
        initComponents();
        Dimension scrnsize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Double(scrnsize.getWidth()).intValue()-150, new Double(scrnsize.getHeight()).intValue()-100);
        setLocationRelativeTo(null);
        
        tbl_vehicledetailmodel = (DefaultTableModel) tbl_VehicleDetail.getModel();
        tbl_routedetailmodel = (DefaultTableModel) tbl_RouteDetail.getModel();
        tbl_stopdetailmodel = (DefaultTableModel) tbl_StopDetail.getModel();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        lbl_SearchVehicle = new javax.swing.JLabel();
        txt_VehicleDetailSearch = new javax.swing.JTextField();
        btn_Search = new javax.swing.JButton();
        btn_VehicleDetailNewEntry = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_VehicleDetail = new javax.swing.JTable();
        btn_VehicleDetailDelete = new javax.swing.JButton();
        btn_VehicleDetailView = new javax.swing.JButton();
        btn_VehicleDetailRefresh = new javax.swing.JButton();
        btn_VehicleDetailPrint = new javax.swing.JButton();
        btn_VehicleDetailCancel = new javax.swing.JButton();
        lbl_VehicleDetailTotalCount = new javax.swing.JLabel();
        txt_VehicleDetailTotalCount = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        lbl_RouteDetailSearch = new javax.swing.JLabel();
        txt_RouteDetailSearch = new javax.swing.JTextField();
        btn_RouteDetailSearch = new javax.swing.JButton();
        btn_RouteDetailNewEntry = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_RouteDetail = new javax.swing.JTable();
        btn_RouteDetailDelete = new javax.swing.JButton();
        btn_RouteDetailView = new javax.swing.JButton();
        btn_RouteDetailRefresh = new javax.swing.JButton();
        btn_RouteDetailPrint = new javax.swing.JButton();
        btn_RouteDetailCancel = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        lbl_StopDetailSearch = new javax.swing.JLabel();
        txt_StopDetailSearch = new javax.swing.JTextField();
        btn_StopDetailSearch = new javax.swing.JButton();
        btn_StopDetailNewEntry = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_StopDetail = new javax.swing.JTable();
        btn_StopDetailDelete = new javax.swing.JButton();
        btn_StopDetailView = new javax.swing.JButton();
        btn_StopDetailRefresh = new javax.swing.JButton();
        btn_StopDetailPrint = new javax.swing.JButton();
        btn_StopDetailCancel = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Data Repository");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        lbl_SearchVehicle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_SearchVehicle.setText("Search By Vehicle :");

        txt_VehicleDetailSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_VehicleDetailSearchCaretUpdate(evt);
            }
        });

        btn_Search.setText("Search");
        btn_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SearchActionPerformed(evt);
            }
        });

        btn_VehicleDetailNewEntry.setText("New Entry");
        btn_VehicleDetailNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_VehicleDetailNewEntryActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_SearchVehicle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_VehicleDetailSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_Search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_VehicleDetailNewEntry)
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Search, btn_VehicleDetailNewEntry});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_SearchVehicle, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_VehicleDetailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_Search)
                        .addComponent(btn_VehicleDetailNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_Search, btn_VehicleDetailNewEntry, jSeparator1, lbl_SearchVehicle, txt_VehicleDetailSearch});

        tbl_VehicleDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "vehicle_id", "S.No.", "Vehicle Name", "Vehicle Number", "Vehicle Seat Capacity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tbl_VehicleDetail);
        if (tbl_VehicleDetail.getColumnModel().getColumnCount() > 0) {
            tbl_VehicleDetail.getColumnModel().getColumn(0).setResizable(false);
            tbl_VehicleDetail.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_VehicleDetail.getColumnModel().getColumn(1).setResizable(false);
            tbl_VehicleDetail.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_VehicleDetail.getColumnModel().getColumn(2).setResizable(false);
            tbl_VehicleDetail.getColumnModel().getColumn(2).setPreferredWidth(250);
            tbl_VehicleDetail.getColumnModel().getColumn(3).setResizable(false);
            tbl_VehicleDetail.getColumnModel().getColumn(4).setResizable(false);
        }

        btn_VehicleDetailDelete.setText("Delete");

        btn_VehicleDetailView.setText("View");

        btn_VehicleDetailRefresh.setText("Refresh");

        btn_VehicleDetailPrint.setText("Print");

        btn_VehicleDetailCancel.setText("Cancel");

        lbl_VehicleDetailTotalCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_VehicleDetailTotalCount.setText("Total Count :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 806, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lbl_VehicleDetailTotalCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_VehicleDetailTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_VehicleDetailView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_VehicleDetailDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_VehicleDetailRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_VehicleDetailPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_VehicleDetailCancel)))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_VehicleDetailCancel, btn_VehicleDetailDelete, btn_VehicleDetailPrint, btn_VehicleDetailRefresh, btn_VehicleDetailView});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_VehicleDetailCancel)
                    .addComponent(btn_VehicleDetailPrint)
                    .addComponent(btn_VehicleDetailRefresh)
                    .addComponent(btn_VehicleDetailDelete)
                    .addComponent(btn_VehicleDetailView)
                    .addComponent(lbl_VehicleDetailTotalCount)
                    .addComponent(txt_VehicleDetailTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_VehicleDetailView, lbl_VehicleDetailTotalCount, txt_VehicleDetailTotalCount});

        jTabbedPane1.addTab("Vehicle Detail", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        lbl_RouteDetailSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_RouteDetailSearch.setText("Search By Route :");
        lbl_RouteDetailSearch.setPreferredSize(new java.awt.Dimension(105, 14));

        btn_RouteDetailSearch.setText("Search");
        btn_RouteDetailSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_RouteDetailSearchActionPerformed(evt);
            }
        });

        btn_RouteDetailNewEntry.setText("New Entry");
        btn_RouteDetailNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_RouteDetailNewEntryActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_RouteDetailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_RouteDetailSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_RouteDetailSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_RouteDetailNewEntry)
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_RouteDetailNewEntry, btn_RouteDetailSearch});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_RouteDetailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_RouteDetailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_RouteDetailSearch)
                        .addComponent(btn_RouteDetailNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_RouteDetailNewEntry, btn_RouteDetailSearch, jSeparator2, lbl_RouteDetailSearch, txt_RouteDetailSearch});

        tbl_RouteDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "route_id", "S.No.", "Vehicle Name", "Area Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tbl_RouteDetail);
        if (tbl_RouteDetail.getColumnModel().getColumnCount() > 0) {
            tbl_RouteDetail.getColumnModel().getColumn(0).setResizable(false);
            tbl_RouteDetail.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_RouteDetail.getColumnModel().getColumn(1).setResizable(false);
            tbl_RouteDetail.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_RouteDetail.getColumnModel().getColumn(2).setResizable(false);
            tbl_RouteDetail.getColumnModel().getColumn(2).setPreferredWidth(200);
            tbl_RouteDetail.getColumnModel().getColumn(3).setResizable(false);
            tbl_RouteDetail.getColumnModel().getColumn(3).setPreferredWidth(350);
        }

        btn_RouteDetailDelete.setText("Delete");

        btn_RouteDetailView.setText("View");

        btn_RouteDetailRefresh.setText("Refresh");

        btn_RouteDetailPrint.setText("Print");

        btn_RouteDetailCancel.setText("Cancel");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Total Count :");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 806, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_RouteDetailView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_RouteDetailDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_RouteDetailRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_RouteDetailPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_RouteDetailCancel)))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_RouteDetailCancel, btn_RouteDetailDelete, btn_RouteDetailPrint, btn_RouteDetailRefresh, btn_RouteDetailView});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_RouteDetailCancel)
                        .addComponent(btn_RouteDetailPrint)
                        .addComponent(btn_RouteDetailRefresh)
                        .addComponent(btn_RouteDetailDelete)
                        .addComponent(btn_RouteDetailView)))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_RouteDetailView, jLabel3, jTextField2});

        jTabbedPane1.addTab("Route Detail", jPanel4);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        lbl_StopDetailSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_StopDetailSearch.setText("Search By Stop :");
        lbl_StopDetailSearch.setPreferredSize(new java.awt.Dimension(105, 14));

        btn_StopDetailSearch.setText("Search");

        btn_StopDetailNewEntry.setText("New Entry");
        btn_StopDetailNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StopDetailNewEntryActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_StopDetailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_StopDetailSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_StopDetailSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_StopDetailNewEntry)
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_StopDetailNewEntry, btn_StopDetailSearch});

        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_StopDetailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_StopDetailSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_StopDetailSearch)
                        .addComponent(btn_StopDetailNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_StopDetailNewEntry, btn_StopDetailSearch, jSeparator3, lbl_StopDetailSearch, txt_StopDetailSearch});

        tbl_StopDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Stop Id", "S.No.", "Area Name", "Stop Name", "Pick Up Time", "Drop Time"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(tbl_StopDetail);
        if (tbl_StopDetail.getColumnModel().getColumnCount() > 0) {
            tbl_StopDetail.getColumnModel().getColumn(0).setResizable(false);
            tbl_StopDetail.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_StopDetail.getColumnModel().getColumn(1).setResizable(false);
            tbl_StopDetail.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_StopDetail.getColumnModel().getColumn(2).setResizable(false);
            tbl_StopDetail.getColumnModel().getColumn(3).setResizable(false);
            tbl_StopDetail.getColumnModel().getColumn(4).setResizable(false);
            tbl_StopDetail.getColumnModel().getColumn(5).setResizable(false);
        }

        btn_StopDetailDelete.setText("Delete");

        btn_StopDetailView.setText("View");

        btn_StopDetailRefresh.setText("Refresh");

        btn_StopDetailPrint.setText("Print");

        btn_StopDetailCancel.setText("Cancel");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Total Count :");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 806, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_StopDetailView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StopDetailDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StopDetailRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StopDetailPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StopDetailCancel)))
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_StopDetailCancel, btn_StopDetailDelete, btn_StopDetailPrint, btn_StopDetailRefresh, btn_StopDetailView});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_StopDetailCancel)
                        .addComponent(btn_StopDetailPrint)
                        .addComponent(btn_StopDetailRefresh)
                        .addComponent(btn_StopDetailDelete)
                        .addComponent(btn_StopDetailView)))
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_StopDetailView, jLabel4, jTextField3});

        jTabbedPane1.addTab("Stop Detail", jPanel5);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
       
    }//GEN-LAST:event_closeDialog

    private void btn_StopDetailNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StopDetailNewEntryActionPerformed
        // TODO add your handling code here:
        Config.tr_stopdetail.onloadReset();
        Config.tr_stopdetail.setVisible(true);
        
    }//GEN-LAST:event_btn_StopDetailNewEntryActionPerformed

    private void btn_VehicleDetailNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_VehicleDetailNewEntryActionPerformed
        // TODO add your handling code here:
        Config.tr_vehicledetail.onloadReset();
        Config.tr_vehicledetail.setVisible(true);
    }//GEN-LAST:event_btn_VehicleDetailNewEntryActionPerformed

    private void btn_RouteDetailNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_RouteDetailNewEntryActionPerformed

        
// TODO add your handling code here:
        Config.tr_routedetail.onloadReset();
        Config.tr_routedetail.setVisible(true);
    }//GEN-LAST:event_btn_RouteDetailNewEntryActionPerformed

    private void txt_VehicleDetailSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_VehicleDetailSearchCaretUpdate
        // TODO add your handling code here:
        btn_SearchActionPerformed(null);
    }//GEN-LAST:event_txt_VehicleDetailSearchCaretUpdate

    private void btn_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SearchActionPerformed
        // TODO add your handling code here:
        try {
        tbl_vehicledetailmodel.setRowCount(0);    
            NodeList nodeList = Config.xml_config_tr_vehicledetailmgr.doc.getElementsByTagName("config_tr_vehicledetail");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                Element element = (Element) node;
                if (!txt_VehicleDetailSearch.getText().equals("")) {
                    if (element.getElementsByTagName("vehiclename").item(0).getTextContent().toUpperCase().startsWith(txt_VehicleDetailSearch.getText().toUpperCase())) {
                        tbl_vehicledetailmodel.addRow(new Object[] {element.getAttribute("vehicle_id"),
                            i+1,
                            element.getElementsByTagName("vehiclename").item(0).getTextContent(),
                            element.getElementsByTagName("vehicleno").item(0).getTextContent(),
                            element.getElementsByTagName("vehicleseatcapacity").item(0).getTextContent()});
                         }
                   } else{
                    tbl_vehicledetailmodel.addRow(new Object[] {element.getAttribute("vehicle_id"),i+1,
                        element.getElementsByTagName("vehiclename").item(0).getTextContent(),
                         element.getElementsByTagName("vehicleno").item(0).getTextContent(),
                            element.getElementsByTagName("vehicleseatcapacity").item(0).getTextContent()
                    });
                    int a = tbl_VehicleDetail.getRowCount();
                    txt_VehicleDetailTotalCount.setText(Integer.toString(a));
                }
              }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_SearchActionPerformed
    private void btn_RouteDetailSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_RouteDetailSearchActionPerformed

        tbl_routedetailmodel.setRowCount(0);
            try {
                String vid = null;
                String vname = null;
                NodeList nList = Config.xml_config_tr_routedetailmgr.doc.getElementsByTagName("config_tr_routedetail");
             for (int i = 0; i < nList.getLength(); i++) {
                 Node node = nList.item(i);
                 Element element = (Element) node;
                 if (!txt_RouteDetailSearch.getText().equals("")) {
                     if (element.getElementsByTagName("areaname").item(0).getTextContent().toUpperCase().startsWith(txt_RouteDetailSearch.getText().toUpperCase())) {
                         tbl_routedetailmodel.addRow(new Object[] {element.getAttribute("route_id"),
                             i+1,
                             element.getElementsByTagName("vehicle_id").item(0).getTextContent(),
                             element.getElementsByTagName("areaname").item(0).getTextContent()
                         });
                         
                     } else{
                         tbl_routedetailmodel.addRow(new Object[] {element.getAttribute("route_id"),
                             i+1,
                             element.getElementsByTagName("vehicle_id").item(0).getTextContent(),
                             element.getElementsByTagName("areaname").item(0).getTextContent()
                         });
                         
                         
                         
                     }
                 }
                
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_RouteDetailSearchActionPerformed
    
  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Tr_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Tr_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Tr_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Tr_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

      
     
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_RouteDetailCancel;
    private javax.swing.JButton btn_RouteDetailDelete;
    private javax.swing.JButton btn_RouteDetailNewEntry;
    private javax.swing.JButton btn_RouteDetailPrint;
    private javax.swing.JButton btn_RouteDetailRefresh;
    private javax.swing.JButton btn_RouteDetailSearch;
    private javax.swing.JButton btn_RouteDetailView;
    private javax.swing.JButton btn_Search;
    private javax.swing.JButton btn_StopDetailCancel;
    private javax.swing.JButton btn_StopDetailDelete;
    private javax.swing.JButton btn_StopDetailNewEntry;
    private javax.swing.JButton btn_StopDetailPrint;
    private javax.swing.JButton btn_StopDetailRefresh;
    private javax.swing.JButton btn_StopDetailSearch;
    private javax.swing.JButton btn_StopDetailView;
    private javax.swing.JButton btn_VehicleDetailCancel;
    private javax.swing.JButton btn_VehicleDetailDelete;
    private javax.swing.JButton btn_VehicleDetailNewEntry;
    private javax.swing.JButton btn_VehicleDetailPrint;
    private javax.swing.JButton btn_VehicleDetailRefresh;
    private javax.swing.JButton btn_VehicleDetailView;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JLabel lbl_RouteDetailSearch;
    private javax.swing.JLabel lbl_SearchVehicle;
    private javax.swing.JLabel lbl_StopDetailSearch;
    private javax.swing.JLabel lbl_VehicleDetailTotalCount;
    private javax.swing.JTable tbl_RouteDetail;
    private javax.swing.JTable tbl_StopDetail;
    private javax.swing.JTable tbl_VehicleDetail;
    private javax.swing.JTextField txt_RouteDetailSearch;
    private javax.swing.JTextField txt_StopDetailSearch;
    private javax.swing.JTextField txt_VehicleDetailSearch;
    private javax.swing.JTextField txt_VehicleDetailTotalCount;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(){
        
            tbl_vehicledetailmodel.setRowCount(0);       
            NodeList nList = Config.xml_config_tr_vehicledetailmgr.doc.getElementsByTagName("config_tr_vehicledetail");
            for (int i = 0; i < nList.getLength(); i++) {
                
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element)nNode;
                    tbl_vehicledetailmodel.addRow(new Object[] {eElement.getAttribute("vehicle_id"),i+1,eElement.getElementsByTagName("vehiclename").item(0).getTextContent(),eElement.getElementsByTagName("vehicleno").item(0).getTextContent(),eElement.getElementsByTagName("vehicleseatcapacity").item(0).getTextContent()});                                     
                }
            }

            tbl_routedetailmodel.setRowCount(0);
            try {
                String vid = null;
                String vname = null;
                NodeList nlist = Config.xml_config_tr_routedetailmgr.doc.getElementsByTagName("config_tr_routedetail");
                    for(int i = 0; i<nlist.getLength(); i++){
                        Node node = nlist.item(i);
                        Element elmt = (Element)node;
                        if(node.getNodeType() == Node.ELEMENT_NODE){
                            vid=String.valueOf(elmt.getElementsByTagName("vehicle_id").item(0).getTextContent());                   
                                Element element = (Element) Config.xml_config_tr_vehicledetailmgr.xpath.evaluate("//*[@vehicle_id='"+vid+"']", Config.xml_config_tr_vehicledetailmgr.doc, XPathConstants.NODE);
                                vname = element.getElementsByTagName("vehiclename").item(0).getTextContent(); 
                                    tbl_routedetailmodel.addRow(new Object[] {elmt.getAttribute("route_id"),
                                    i+1,
                                    vname,
                                    elmt.getElementsByTagName("areaname").item(0).getTextContent()});
                        }
                    }                     
            } catch (Exception e) {
                e.printStackTrace();
            }
                   
            tbl_stopdetailmodel.setRowCount(0);
            try {
                String rid = null;
                String rname = null;
                NodeList nnodeList = Config.xml_config_tr_stopdetailmgr.doc.getElementsByTagName("config_tr_stopdetail");
                for (int i = 0; i < nnodeList.getLength(); i++) {
                    Node nnode = nnodeList.item(i);
                    Element eelement = (Element)nnode;
                    if (nnode.getNodeType() == Node.ELEMENT_NODE) {
                        rid = String.valueOf(eelement.getElementsByTagName("route_id").item(0).getTextContent());
                        Element element1 = (Element) Config.xml_config_tr_routedetailmgr.xpath.evaluate("//*[@route_id = '"+rid+"']", Config.xml_config_tr_routedetailmgr.doc, XPathConstants.NODE);
                            rname = element1.getElementsByTagName("areaname").item(0).getTextContent();

                            tbl_stopdetailmodel.addRow(new Object[] {eelement.getAttribute("stop_id"),
                            i+1,
                            rname,
                            eelement.getElementsByTagName("stopname").item(0).getTextContent(),
                            eelement.getElementsByTagName("pickuptime").item(0).getTextContent(),
                            eelement.getElementsByTagName("droptime").item(0).getTextContent()});
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
                    
        
    }
   
}
