package modules.librarymanagement.repository;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddSubCategory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Archi
 */
public class Lm_ViewSubcategory extends javax.swing.JDialog {

    public Lm_ViewSubcategory(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }



    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txt_categoryname = new javax.swing.JLabel();
        cmb_categoryname = new javax.swing.JComboBox();
        lbl_subcatname = new javax.swing.JLabel();
        txt_subcatname = new javax.swing.JTextField();
        btn_updatesubcategory = new javax.swing.JButton();
        btn_cancelsubcategory = new javax.swing.JButton();
        lbl_subcategoryid = new javax.swing.JLabel();
        btn_deleteSubcategory = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "View Subcategory", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        txt_categoryname.setText("Category Name :");

        lbl_subcatname.setText("Sub Category Name :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 10, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbl_subcatname, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_categoryname, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_subcatname)
                    .addComponent(cmb_categoryname, 0, 225, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_categoryname)
                    .addComponent(cmb_categoryname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_subcatname)
                    .addComponent(txt_subcatname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_updatesubcategory.setText("Update");
        btn_updatesubcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updatesubcategoryActionPerformed(evt);
            }
        });

        btn_cancelsubcategory.setText("Cancel");
        btn_cancelsubcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelsubcategoryActionPerformed(evt);
            }
        });

        lbl_subcategoryid.setText("id");

        btn_deleteSubcategory.setText("Delete");
        btn_deleteSubcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteSubcategoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(lbl_subcategoryid)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_updatesubcategory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_deleteSubcategory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancelsubcategory)
                        .addGap(15, 15, 15)))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_updatesubcategory)
                    .addComponent(lbl_subcategoryid)
                    .addComponent(btn_deleteSubcategory)
                    .addComponent(btn_cancelsubcategory))
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_updatesubcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updatesubcategoryActionPerformed
        String str = checkValidity();
            if (str.equals("ok")){
                Config_Lm_Dm_AddSubCategory cs = new Config_Lm_Dm_AddSubCategory();
                cs.setSubcategory_id(lbl_subcategoryid.getText());
//                cs.setCategory_id(cmb_categoryname.getSelectedIndex());
                cs.setSubcategoryname(txt_subcatname.getText());
                if(Config.config_lm_cm_addsubcategorymgr.updLm_SubCategory(cs)){ 
//                Config.lm_repository_home.setCombo(cmb_categoryname.getSelectedIndex());
                    JOptionPane.showMessageDialog(this, "SubCategory updated Successfully", "Success",JOptionPane.NO_OPTION);
                    dispose();
                }else{
                    JOptionPane.showMessageDialog(this, "Error in Updation", "Error",JOptionPane.ERROR_MESSAGE );
                    }
            }else{            
                JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
             }
             
    }//GEN-LAST:event_btn_updatesubcategoryActionPerformed

    private void btn_cancelsubcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelsubcategoryActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelsubcategoryActionPerformed

    private void btn_deleteSubcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteSubcategoryActionPerformed
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure,you want to delete this record ? ","Warning",JOptionPane.YES_NO_OPTION);             
            if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.config_lm_cm_addsubcategorymgr.delLm_Subcategory(lbl_subcategoryid.getText())){ 
                    JOptionPane.showMessageDialog(this, "Recorded Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                    dispose();            
                }else{
                    JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                    }
            }else{
                dispose();     
                }
    }//GEN-LAST:event_btn_deleteSubcategoryActionPerformed
         
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Lm_ViewSubcategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Lm_ViewSubcategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Lm_ViewSubcategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Lm_ViewSubcategory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Lm_ViewSubcategory dialog = new Lm_ViewSubcategory(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelsubcategory;
    private javax.swing.JButton btn_deleteSubcategory;
    private javax.swing.JButton btn_updatesubcategory;
    private javax.swing.JComboBox cmb_categoryname;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_subcategoryid;
    private javax.swing.JLabel lbl_subcatname;
    private javax.swing.JLabel txt_categoryname;
    private javax.swing.JTextField txt_subcatname;
    // End of variables declaration//GEN-END:variables


     private String checkValidity() {
        if(txt_categoryname.getText().equals("")){
            return "Subcategory";
        }else{
            return "ok";
            }
    }

    private String checksubValidity() {
        if(txt_subcatname.getText().equals("")){
            return "Category";
        }else{
            return "ok";
            }
    }


    void onloadReset(String subcatid) {
        lbl_subcategoryid.setText(subcatid);        
        String catid = null;
        String catname = null;
        String subcatname = null;        
        
        cmb_categoryname.removeAllItems();                
        NodeList nList = Config.xml_config_lm_categorymgr.doc.getElementsByTagName("category");
        for(int i=0; i < nList.getLength();i++){
            Node nNode = nList.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE){
                Element eElement = (Element)nNode;
                if(eElement.getAttribute("category_id").equals(catid)){
                    catname = eElement.getElementsByTagName("catname").item(i).getTextContent();                    
                }
                cmb_categoryname.addItem(eElement.getElementsByTagName("categoryname").item(0).getTextContent());                
            }
        
        }
        NodeList nList2 = Config.xml_config_lm_subcategorymgr.doc.getElementsByTagName("subcategory");        
        for (int i = 0; i < nList2.getLength(); i++) {
            Node nNode = nList2.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if(eElement.getAttribute("subcategory_id").equals(subcatid)){                    
                    catid = eElement.getElementsByTagName("category_id").item(0).getTextContent();                    
                    subcatname = eElement.getElementsByTagName("subcategoryname").item(0).getTextContent();                    
                }
                
            }
        }
        NodeList nList1 = Config.xml_config_lm_categorymgr.doc.getElementsByTagName("category");
        for(int i=0; i < nList1.getLength();i++){
            Node nNode = nList1.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE){
                Element eElement = (Element)nNode;
                if(eElement.getAttribute("category_id").equals(catid)){
                    catname = eElement.getElementsByTagName("categoryname").item(0).getTextContent();                    
                }                
                
            }
        
        }
        
        cmb_categoryname.setSelectedItem(catname);
        txt_subcatname.setText(subcatname);
    }
        
        
    
    }

