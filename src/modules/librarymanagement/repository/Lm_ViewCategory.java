package modules.librarymanagement.repository;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.librarymanagement.Config_Lm_Dm_AddCategory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Lm_ViewCategory extends javax.swing.JDialog {


    public Lm_ViewCategory(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        lbl_categoryname = new javax.swing.JLabel();
        txt_categoryname = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        lbl_categoryid = new javax.swing.JLabel();
        btn_deletecategory = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "View Category", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        lbl_categoryname.setText("Category Name :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_categoryname)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_categoryname)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_categoryname)
                    .addComponent(txt_categoryname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setText("Update");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        lbl_categoryid.setBackground(new java.awt.Color(255, 255, 255));
        lbl_categoryid.setText("id");

        btn_deletecategory.setText("Delete");
        btn_deletecategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deletecategoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_categoryid, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                .addGap(29, 29, 29)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_deletecategory)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_deletecategory, jButton1, jButton2});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(lbl_categoryid)
                    .addComponent(btn_deletecategory))
                .addGap(6, 6, 6))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String str = checkValidity();
        if (str.equals("ok")){
            Config_Lm_Dm_AddCategory cs = new Config_Lm_Dm_AddCategory();            
            cs.setCategory_id(lbl_categoryid.getText());
            cs.setCategoryname(txt_categoryname.getText());
            if(Config.config_lm_cm_addcategorymgr.updLm_Category(cs)){                
                JOptionPane.showMessageDialog(this, "Category updated Successfully", "Successfull",JOptionPane.NO_OPTION);
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "Error in updation", "Error",JOptionPane.ERROR_MESSAGE );
            }
        }else{            
            JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btn_deletecategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deletecategoryActionPerformed
        int i = 0;
        String str = checkValidity();
        String catid = lbl_categoryid.getText();
        if(str.equals("ok")){                          
            int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure, You want to Delete category?","Warning",JOptionPane.YES_NO_OPTION);             
            if(dialogButton == JOptionPane.YES_OPTION) {
                NodeList nList = Config.xml_config_lm_subcategorymgr.doc.getElementsByTagName("subcategory");
                for ( i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if(eElement.getElementsByTagName("category_id").item(0).getTextContent().equals(catid)){             
                            JOptionPane.showMessageDialog(this, "First you will have to delete all its subcategories", "Error",JOptionPane.ERROR_MESSAGE);
                            break;
                        }
                    } 
                }if(i==nList.getLength()){
                    if(Config.config_lm_cm_addcategorymgr.delLm_category(lbl_categoryid.getText())){                              
                        JOptionPane.showMessageDialog(this, "Category Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                        dispose();            
                    }else{
                        JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                        }           
                    }
                }else{
                    dispose();     
                    }
                }else{            
                 JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
                }
    }//GEN-LAST:event_btn_deletecategoryActionPerformed
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_deletecategory;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lbl_categoryid;
    private javax.swing.JLabel lbl_categoryname;
    private javax.swing.JTextField txt_categoryname;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(String categoryid) {
        try {
            lbl_categoryid.setText(categoryid);
            Element element = (Element) Config.xml_config_lm_categorymgr.xpath.evaluate("//*[@category_id='"+categoryid+"']", Config.xml_config_lm_categorymgr.doc, XPathConstants.NODE);
            txt_categoryname.setText(element.getElementsByTagName("categoryname").item(0).getTextContent());            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error",JOptionPane.ERROR_MESSAGE );
        }
        
        
        
        
//         NodeList nList = Config.xml_categorymgr.doc.getElementsByTagName("category");
//         for (int i = 0; i < nList.getLength(); i++) {
//             Node nNode = nList.item(i);
//             if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//                 Element eElement = (Element) nNode;
//                 if(eElement.getAttribute("category_id").equals(categoryid)){             
//                    
//                    }
//             }
//         }
    }
    
     private String checkValidity() {
     if(txt_categoryname.getText().equals("")){
        return "Category";
     }else{
         return "ok";
     }
    }
    
}
