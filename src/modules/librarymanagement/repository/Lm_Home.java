package modules.librarymanagement.repository;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class Lm_Home extends javax.swing.JFrame {

    DefaultTableModel tbl_catagoryModel;
    DefaultTableModel tbl_bookdetailsModel;
    DefaultTableModel tbl_subcategoryModel;
    DefaultTableModel tbl_IssuableBookModel;
    DefaultTableModel tbl_DurationModel;
    DefaultTableModel tbl_OverdueModel;
    
     
    public Lm_Home() {
        initComponents();
        this.setLocationRelativeTo(null);        
        
        tbl_catagoryModel = (DefaultTableModel) tbl_category.getModel();
        tbl_bookdetailsModel = (DefaultTableModel) tbl_bookdetails.getModel();
        tbl_subcategoryModel = (DefaultTableModel) tbl_subcategory.getModel();
        tbl_IssuableBookModel = (DefaultTableModel) tbl_IssuableBook.getModel();
        tbl_DurationModel = (DefaultTableModel) tbl_Duration.getModel();
        tbl_OverdueModel = (DefaultTableModel) tbl_Overdue.getModel();
    }

      
    @SuppressWarnings("unchecked")
  
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane7 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        lbl_DR = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        btn_addcategory = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_category = new javax.swing.JTable();
        btn_deletecategory = new javax.swing.JButton();
        btn_viewcategory = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        btn_searchCategory = new javax.swing.JButton();
        txt_category = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_subcategory = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        cmb_category = new javax.swing.JComboBox();
        btn_addsubcategory = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_subcategory = new javax.swing.JTable();
        btn_deletesubcategoryHome = new javax.swing.JButton();
        btn_viewsubcategory = new javax.swing.JButton();
        btn_searchSubcategory = new javax.swing.JButton();
        btn_cancelCategory = new javax.swing.JButton();
        btn_refreshCategory = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel11 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txt_issuablebook = new javax.swing.JTextField();
        btn_searchIssuablebook = new javax.swing.JButton();
        cmb_searchIssuable = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_IssuableBook = new javax.swing.JTable();
        btn_StoPro_SubCatPrint = new javax.swing.JButton();
        btn_addIssuable = new javax.swing.JButton();
        btn_deleteIssuable = new javax.swing.JButton();
        btn_viewIssuable = new javax.swing.JButton();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txt_searchduration = new javax.swing.JTextField();
        btn_searchDuration = new javax.swing.JButton();
        cmb_searchDuration = new javax.swing.JComboBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_Duration = new javax.swing.JTable();
        btn_StoPro_SubCatPrint1 = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();
        btn_viewDuration = new javax.swing.JButton();
        btn_addDuration = new javax.swing.JButton();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_searchoverdue = new javax.swing.JTextField();
        btn_searchOverdue = new javax.swing.JButton();
        cmb_searchOverdue = new javax.swing.JComboBox();
        btn_addOverdue = new javax.swing.JButton();
        btn_viewOverdue = new javax.swing.JButton();
        btn_deleteOverdue = new javax.swing.JButton();
        btn_StoPro_SubCatPrint2 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_Overdue = new javax.swing.JTable();
        btn_refreshCirculation = new javax.swing.JButton();
        btn_cancelCirculation = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        btn_cancelBook = new javax.swing.JButton();
        btn_refreshBook = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_bookdetails = new javax.swing.JTable();
        btn_DR_Bdprnt = new javax.swing.JButton();
        btn_DR_BdExpt = new javax.swing.JButton();
        btn_DeleteBookdetails = new javax.swing.JButton();
        btn_BDView = new javax.swing.JButton();
        btn_NewEntry = new javax.swing.JButton();
        txt_DR_Bdtblcont = new javax.swing.JTextField();
        lbl_DR_BdtblCont = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lbl_DR_BdSrch = new javax.swing.JLabel();
        cmb_booksearch = new javax.swing.JComboBox();
        btn_search = new javax.swing.JButton();
        txt_booksearch = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setPreferredSize(new java.awt.Dimension(950, 540));

        jPanel7.setBackground(new java.awt.Color(0, 153, 153));

        lbl_DR.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbl_DR.setForeground(new java.awt.Color(255, 255, 255));
        lbl_DR.setText("Data Repository");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_DR)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_DR)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(458);
        jSplitPane1.setDividerSize(2);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Category", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        btn_addcategory.setText("Add");
        btn_addcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addcategoryActionPerformed(evt);
            }
        });

        tbl_category.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Category ID", "S. No.", "Category Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_category.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_categoryMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_category);
        if (tbl_category.getColumnModel().getColumnCount() > 0) {
            tbl_category.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_category.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_category.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_category.getColumnModel().getColumn(1).setResizable(false);
            tbl_category.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_category.getColumnModel().getColumn(2).setResizable(false);
            tbl_category.getColumnModel().getColumn(2).setPreferredWidth(900);
        }

        btn_deletecategory.setText("Delete");
        btn_deletecategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deletecategoryActionPerformed(evt);
            }
        });

        btn_viewcategory.setText("View");
        btn_viewcategory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_viewcategoryMouseClicked(evt);
            }
        });
        btn_viewcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewcategoryActionPerformed(evt);
            }
        });

        jLabel6.setText("Category Name :");

        btn_searchCategory.setText("Search");
        btn_searchCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchCategoryActionPerformed(evt);
            }
        });

        txt_category.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_categoryCaretUpdate(evt);
            }
        });
        txt_category.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_categoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_category)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_searchCategory))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(btn_addcategory, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_deletecategory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_viewcategory)))
                .addContainerGap())
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_addcategory, btn_deletecategory, btn_viewcategory});

        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(btn_searchCategory)
                    .addComponent(txt_category, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_addcategory)
                    .addComponent(btn_viewcategory)
                    .addComponent(btn_deletecategory))
                .addGap(11, 11, 11))
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_searchCategory, jLabel6, txt_category});

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel9);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sub Category", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLabel2.setText("Sub Category Name :");

        txt_subcategory.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_subcategoryCaretUpdate(evt);
            }
        });

        jLabel12.setText("Category :");

        cmb_category.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_categoryActionPerformed(evt);
            }
        });

        btn_addsubcategory.setText("Add");
        btn_addsubcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addsubcategoryActionPerformed(evt);
            }
        });

        tbl_subcategory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Subcategory ID", "S. No.", "Sub Category Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_subcategory.getTableHeader().setReorderingAllowed(false);
        tbl_subcategory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_subcategoryMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_subcategory);
        if (tbl_subcategory.getColumnModel().getColumnCount() > 0) {
            tbl_subcategory.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_subcategory.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_subcategory.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_subcategory.getColumnModel().getColumn(1).setResizable(false);
            tbl_subcategory.getColumnModel().getColumn(1).setPreferredWidth(100);
            tbl_subcategory.getColumnModel().getColumn(2).setResizable(false);
            tbl_subcategory.getColumnModel().getColumn(2).setPreferredWidth(900);
        }

        btn_deletesubcategoryHome.setText("Delete");
        btn_deletesubcategoryHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deletesubcategoryHomeActionPerformed(evt);
            }
        });

        btn_viewsubcategory.setText("View");
        btn_viewsubcategory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_viewsubcategoryMouseClicked(evt);
            }
        });
        btn_viewsubcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewsubcategoryActionPerformed(evt);
            }
        });

        btn_searchSubcategory.setText("Search");
        btn_searchSubcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchSubcategoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(btn_addsubcategory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_deletesubcategoryHome, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_viewsubcategory, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(txt_subcategory)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_searchSubcategory))
                            .addComponent(cmb_category, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_addsubcategory, btn_deletesubcategoryHome, btn_viewsubcategory});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(cmb_category, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_subcategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_searchSubcategory))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_deletesubcategoryHome)
                    .addComponent(btn_viewsubcategory)
                    .addComponent(btn_addsubcategory))
                .addGap(11, 11, 11))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_searchSubcategory, cmb_category, jLabel12, jLabel2, txt_subcategory});

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSplitPane1.setRightComponent(jPanel12);

        btn_cancelCategory.setText("Cancel");
        btn_cancelCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelCategoryActionPerformed(evt);
            }
        });

        btn_refreshCategory.setText("Refresh");
        btn_refreshCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_refreshCategoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSplitPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(btn_refreshCategory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_cancelCategory)
                        .addContainerGap())))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancelCategory, btn_refreshCategory});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jSplitPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancelCategory)
                    .addComponent(btn_refreshCategory))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Category/Subcategory", jPanel3);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane2.setMinimumSize(new java.awt.Dimension(105, 100));

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Search :");

        txt_issuablebook.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_issuablebookCaretUpdate(evt);
            }
        });
        txt_issuablebook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_issuablebookActionPerformed(evt);
            }
        });

        btn_searchIssuablebook.setText("Search ");
        btn_searchIssuablebook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchIssuablebookActionPerformed(evt);
            }
        });

        cmb_searchIssuable.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Category", "Subcategory" }));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_searchIssuable, 0, 145, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_issuablebook, javax.swing.GroupLayout.PREFERRED_SIZE, 637, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_searchIssuablebook)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_issuablebook, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_searchIssuablebook)
                    .addComponent(cmb_searchIssuable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel14Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_searchIssuablebook, cmb_searchIssuable, jLabel3, txt_issuablebook});

        tbl_IssuableBook.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "issuebook_id", "S.No.", "Category", "SubCategory", "For Student", "For Staff"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_IssuableBook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_IssuableBookMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_IssuableBook);
        if (tbl_IssuableBook.getColumnModel().getColumnCount() > 0) {
            tbl_IssuableBook.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_IssuableBook.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_IssuableBook.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_IssuableBook.getColumnModel().getColumn(1).setResizable(false);
            tbl_IssuableBook.getColumnModel().getColumn(2).setResizable(false);
            tbl_IssuableBook.getColumnModel().getColumn(3).setResizable(false);
            tbl_IssuableBook.getColumnModel().getColumn(4).setResizable(false);
            tbl_IssuableBook.getColumnModel().getColumn(5).setResizable(false);
        }

        btn_StoPro_SubCatPrint.setLabel("Print");

        btn_addIssuable.setText("Add");
        btn_addIssuable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addIssuableActionPerformed(evt);
            }
        });

        btn_deleteIssuable.setLabel("Delete");
        btn_deleteIssuable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteIssuableActionPerformed(evt);
            }
        });

        btn_viewIssuable.setText("View");
        btn_viewIssuable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewIssuableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 912, Short.MAX_VALUE)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(btn_addIssuable, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_viewIssuable)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_deleteIssuable)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StoPro_SubCatPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel11Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_StoPro_SubCatPrint, btn_addIssuable, btn_deleteIssuable, btn_viewIssuable});

        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_StoPro_SubCatPrint)
                    .addComponent(btn_viewIssuable)
                    .addComponent(btn_deleteIssuable)
                    .addComponent(btn_addIssuable))
                .addContainerGap())
        );

        jTabbedPane2.addTab("No. of Issuable Book", jPanel11);

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Search :");

        txt_searchduration.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_searchdurationCaretUpdate(evt);
            }
        });

        btn_searchDuration.setText("Search ");
        btn_searchDuration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchDurationActionPerformed(evt);
            }
        });

        cmb_searchDuration.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Category", "Subcategory" }));

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_searchDuration, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_searchduration)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_searchDuration)
                .addContainerGap())
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_searchduration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_searchDuration)
                    .addComponent(cmb_searchDuration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel17Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_searchDuration, cmb_searchDuration, jLabel4, txt_searchduration});

        tbl_Duration.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "", "S.No.", "Category", "SubCategory", "For Student", "For Staff"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Duration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_DurationMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tbl_Duration);
        if (tbl_Duration.getColumnModel().getColumnCount() > 0) {
            tbl_Duration.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_Duration.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_Duration.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_Duration.getColumnModel().getColumn(1).setResizable(false);
            tbl_Duration.getColumnModel().getColumn(2).setResizable(false);
            tbl_Duration.getColumnModel().getColumn(3).setResizable(false);
            tbl_Duration.getColumnModel().getColumn(4).setResizable(false);
            tbl_Duration.getColumnModel().getColumn(5).setResizable(false);
        }

        btn_StoPro_SubCatPrint1.setLabel("Print");

        btn_delete.setLabel("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });

        btn_viewDuration.setText("View");
        btn_viewDuration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewDurationActionPerformed(evt);
            }
        });

        btn_addDuration.setText("Add");
        btn_addDuration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addDurationActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 912, Short.MAX_VALUE)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(btn_addDuration, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_viewDuration, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StoPro_SubCatPrint1)))
                .addContainerGap())
        );

        jPanel16Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_StoPro_SubCatPrint1, btn_addDuration, btn_delete, btn_viewDuration});

        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_addDuration)
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_delete)
                        .addComponent(btn_StoPro_SubCatPrint1)
                        .addComponent(btn_viewDuration)))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Duration of Issue Book", jPanel16);

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Search :");

        txt_searchoverdue.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_searchoverdueCaretUpdate(evt);
            }
        });

        btn_searchOverdue.setText("Search ");
        btn_searchOverdue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchOverdueActionPerformed(evt);
            }
        });

        cmb_searchOverdue.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Category", "Subcategory" }));
        cmb_searchOverdue.setPreferredSize(new java.awt.Dimension(150, 20));

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_searchOverdue, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_searchoverdue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_searchOverdue)
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txt_searchoverdue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_searchOverdue)
                    .addComponent(cmb_searchOverdue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel20Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_searchOverdue, cmb_searchOverdue, jLabel5, txt_searchoverdue});

        btn_addOverdue.setText("Add");
        btn_addOverdue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addOverdueActionPerformed(evt);
            }
        });

        btn_viewOverdue.setText("View");
        btn_viewOverdue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewOverdueActionPerformed(evt);
            }
        });

        btn_deleteOverdue.setLabel("Delete");
        btn_deleteOverdue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteOverdueActionPerformed(evt);
            }
        });

        btn_StoPro_SubCatPrint2.setLabel("Print");

        tbl_Overdue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "", "S.No.", "Category", "SubCategory", "For Student", "For Staff"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Overdue.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_OverdueMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(tbl_Overdue);
        if (tbl_Overdue.getColumnModel().getColumnCount() > 0) {
            tbl_Overdue.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_Overdue.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_Overdue.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_Overdue.getColumnModel().getColumn(1).setResizable(false);
            tbl_Overdue.getColumnModel().getColumn(2).setResizable(false);
            tbl_Overdue.getColumnModel().getColumn(3).setResizable(false);
            tbl_Overdue.getColumnModel().getColumn(4).setResizable(false);
            tbl_Overdue.getColumnModel().getColumn(5).setResizable(false);
        }

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addComponent(btn_addOverdue, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_viewOverdue, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_deleteOverdue, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StoPro_SubCatPrint2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 912, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_StoPro_SubCatPrint2)
                    .addComponent(btn_viewOverdue)
                    .addComponent(btn_deleteOverdue)
                    .addComponent(btn_addOverdue))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Overdue Charge", jPanel19);

        btn_refreshCirculation.setText("Refresh");
        btn_refreshCirculation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_refreshCirculationActionPerformed(evt);
            }
        });

        btn_cancelCirculation.setText("Cancel");
        btn_cancelCirculation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelCirculationActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_refreshCirculation)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_cancelCirculation)
                .addContainerGap())
            .addComponent(jTabbedPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancelCirculation, btn_refreshCirculation});

        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancelCirculation)
                    .addComponent(btn_refreshCirculation))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Circulation", jPanel8);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        btn_cancelBook.setText("Cancel");
        btn_cancelBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelBookActionPerformed(evt);
            }
        });

        btn_refreshBook.setText("Refresh");
        btn_refreshBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_refreshBookActionPerformed(evt);
            }
        });

        tbl_bookdetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "S.no.", "Book Id", "Book Category", "Book SubCategory", "Standard", "Subject", "Book Title"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_bookdetails.getTableHeader().setReorderingAllowed(false);
        tbl_bookdetails.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_bookdetailsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_bookdetails);
        if (tbl_bookdetails.getColumnModel().getColumnCount() > 0) {
            tbl_bookdetails.getColumnModel().getColumn(0).setResizable(false);
            tbl_bookdetails.getColumnModel().getColumn(0).setPreferredWidth(8);
            tbl_bookdetails.getColumnModel().getColumn(1).setResizable(false);
            tbl_bookdetails.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_bookdetails.getColumnModel().getColumn(2).setResizable(false);
            tbl_bookdetails.getColumnModel().getColumn(3).setResizable(false);
            tbl_bookdetails.getColumnModel().getColumn(4).setResizable(false);
            tbl_bookdetails.getColumnModel().getColumn(5).setResizable(false);
            tbl_bookdetails.getColumnModel().getColumn(6).setResizable(false);
        }

        btn_DR_Bdprnt.setLabel("Print");

        btn_DR_BdExpt.setLabel("Export");

        btn_DeleteBookdetails.setLabel("Delete");
        btn_DeleteBookdetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DeleteBookdetailsActionPerformed(evt);
            }
        });

        btn_BDView.setText("View");
        btn_BDView.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_BDViewMouseClicked(evt);
            }
        });
        btn_BDView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BDViewActionPerformed(evt);
            }
        });

        btn_NewEntry.setText("New Entry");
        btn_NewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_NewEntryActionPerformed(evt);
            }
        });

        txt_DR_Bdtblcont.setEditable(false);

        lbl_DR_BdtblCont.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_DR_BdtblCont.setText("Total Count :");

        lbl_DR_BdSrch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_DR_BdSrch.setText("Search :");

        cmb_booksearch.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Book Id", "Category", "Subcategory", "Standard", "Subject", "Book Title", "Author Name" }));
        cmb_booksearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_booksearchActionPerformed(evt);
            }
        });
        cmb_booksearch.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                cmb_booksearchInputMethodTextChanged(evt);
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                cmb_booksearchCaretPositionChanged(evt);
            }
        });

        btn_search.setText("Search");
        btn_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchActionPerformed(evt);
            }
        });

        txt_booksearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_booksearchCaretUpdate(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_DR_BdSrch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_booksearch, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_booksearch, javax.swing.GroupLayout.PREFERRED_SIZE, 633, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_search)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_search)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_DR_BdSrch)
                        .addComponent(cmb_booksearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_booksearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_search, cmb_booksearch, lbl_DR_BdSrch, txt_booksearch});

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btn_refreshBook)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_DR_BdtblCont)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_DR_Bdtblcont, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_NewEntry)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_BDView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_DeleteBookdetails)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_DR_BdExpt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_DR_Bdprnt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancelBook))
                    .addComponent(jScrollPane1))
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_BDView, btn_DR_BdExpt, btn_DR_Bdprnt, btn_DeleteBookdetails, btn_NewEntry, btn_cancelBook});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancelBook)
                    .addComponent(btn_refreshBook)
                    .addComponent(btn_DR_Bdprnt)
                    .addComponent(btn_DR_BdExpt)
                    .addComponent(btn_DeleteBookdetails)
                    .addComponent(btn_BDView)
                    .addComponent(btn_NewEntry)
                    .addComponent(lbl_DR_BdtblCont)
                    .addComponent(txt_DR_Bdtblcont, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Book Details", jPanel4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Supplier");

        jScrollPane7.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 964, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        setBounds(0, 0, 980, 580);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_NewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_NewEntryActionPerformed
        Config.lm_newentry.onloadReset(); 
        Config.lm_newentry.setVisible(true);        
    }//GEN-LAST:event_btn_NewEntryActionPerformed

    private void btn_BDViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BDViewActionPerformed
        Config.lm_viewentry.onloadReset(tbl_bookdetailsModel.getValueAt(tbl_bookdetails.getSelectedRow(), 0).toString());
        Config.lm_viewentry.setVisible(true);
    }//GEN-LAST:event_btn_BDViewActionPerformed

    private void btn_cancelCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelCategoryActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelCategoryActionPerformed

    private void btn_addcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addcategoryActionPerformed
        Config.lm_addcategory.onloadReset(); 
        Config.lm_addcategory.setVisible(true);
    }//GEN-LAST:event_btn_addcategoryActionPerformed

    private void btn_addsubcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addsubcategoryActionPerformed
        Config.lm_addsubcategory.onloadReset();
        Config.lm_addsubcategory.setVisible(true);
    }//GEN-LAST:event_btn_addsubcategoryActionPerformed

    private void btn_viewcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewcategoryActionPerformed
        Config.lm_viewcategory.onloadReset(tbl_catagoryModel.getValueAt(tbl_category.getSelectedRow(), 0).toString());
        Config.lm_viewcategory.setVisible(true);
    }//GEN-LAST:event_btn_viewcategoryActionPerformed

    private void btn_viewIssuableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewIssuableActionPerformed
        Config.lm_viewissuablebook.onloadReset(tbl_IssuableBookModel.getValueAt(tbl_IssuableBook.getSelectedRow(), 0).toString());
        Config.lm_viewissuablebook.setVisible(true);        
    }//GEN-LAST:event_btn_viewIssuableActionPerformed

    private void btn_viewDurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewDurationActionPerformed
        Config.lm_viewbookduration.onloadReset(tbl_DurationModel.getValueAt(tbl_Duration.getSelectedRow(), 0).toString());
        Config.lm_viewbookduration.setVisible(true);
    }//GEN-LAST:event_btn_viewDurationActionPerformed

    private void btn_viewOverdueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewOverdueActionPerformed
        Config.lm_viewoverduecharge.onloadReset(tbl_OverdueModel.getValueAt(tbl_Overdue.getSelectedRow(), 0).toString());
        Config.lm_viewoverduecharge.setVisible(true);
    }//GEN-LAST:event_btn_viewOverdueActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
         String bookid = tbl_Duration.getValueAt(tbl_Duration.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are you sure, You want to delete this record? ","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.config_lm_cm_addbookdurationmgr.del_BookDuration(bookid)){ //               
                JOptionPane.showMessageDialog(this, "Record deleted successfully", "Success",JOptionPane.NO_OPTION);                               
                 }else{
                JOptionPane.showMessageDialog(this, "Error in deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             }
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void btn_addDurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addDurationActionPerformed
        Config.lm_addbookduration.onLoadReset();
        Config.lm_addbookduration.setVisible(true);
    }//GEN-LAST:event_btn_addDurationActionPerformed

    private void btn_ibaddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ibaddActionPerformed
        Config.lm_addissuablebook.setVisible(true);
    }//GEN-LAST:event_btn_ibaddActionPerformed

    private void btn_addOverdueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addOverdueActionPerformed
        Config.lm_addoverduecharge.onLoadReset();   
        Config.lm_addoverduecharge.setVisible(true);  
    }//GEN-LAST:event_btn_addOverdueActionPerformed

    private void btn_cancelCirculationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelCirculationActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelCirculationActionPerformed

    private void btn_cancelBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelBookActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelBookActionPerformed

    private void btn_searchCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchCategoryActionPerformed
        try {
            tbl_catagoryModel.setRowCount(0);
            NodeList nList = Config.xml_config_lm_categorymgr.doc.getElementsByTagName("category");
            if(!txt_category.getText().equals("")){
               
            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode; 
                    if(eElement.getElementsByTagName("categoryname").item(0).getTextContent().toUpperCase().startsWith(txt_category.getText().toUpperCase())){
                        tbl_catagoryModel.addRow( new Object [] {eElement.getAttribute("category_id"),i+1, eElement.getElementsByTagName("categoryname").item(0).getTextContent()});
                    }
                }
            }
            }else {
             
                    for (int i = 0; i < nList.getLength(); i++) {
                      Node nNode = nList.item(i);
                      if(nNode.getNodeType() == Node.ELEMENT_NODE){
                        Element eElement = (Element)nNode; 
                        tbl_catagoryModel.addRow(new Object[] {eElement.getAttribute("category_id"),i+1,eElement.getElementsByTagName("categoryname").item(0).getTextContent()});
                     }
                    }
                
                
        }
//            for (int i = 0; i < tbl_catagoryModel.getRowCount(); i++) {
//                tbl_subcategoryModel.setValueAt(i+1, i,1);
//            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_searchCategoryActionPerformed

    private void btn_searchIssuablebookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchIssuablebookActionPerformed
        try {
               tbl_IssuableBookModel.setRowCount(0);
               NodeList nList = Config.xml_config_lm_issuablebookmgr.doc.getElementsByTagName("issuablebook");
               if (cmb_searchIssuable.getSelectedItem().equals("Category")) {
               for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("categoryname").item(0).getTextContent().toUpperCase().startsWith(txt_issuablebook.getText().toUpperCase())) {
                        tbl_IssuableBookModel.addRow(new Object[] {                            
                            eElement.getAttribute("issuablebook_id"),                            
                            i+1,
                            eElement.getElementsByTagName("categoryname").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategoryname").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstudent").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstaff").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                }
              } else if (cmb_searchIssuable.getSelectedItem().equals("Subcategory")) {
                 for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("subcategoryname").item(0).getTextContent().toUpperCase().startsWith(txt_issuablebook.getText().toUpperCase())) {
                        tbl_IssuableBookModel.addRow(new Object[] {                            
                            eElement.getAttribute("issuablebook_id"),                            
                            i+1,
                            eElement.getElementsByTagName("categoryname").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategoryname").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstudent").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstaff").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                } 
              }else {}   
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_searchIssuablebookActionPerformed

    private void cmb_booksearchCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_cmb_booksearchCaretPositionChanged
         
    }//GEN-LAST:event_cmb_booksearchCaretPositionChanged

    private void cmb_booksearchInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_cmb_booksearchInputMethodTextChanged
        
    }//GEN-LAST:event_cmb_booksearchInputMethodTextChanged

    private void btn_searchSubcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchSubcategoryActionPerformed
        tbl_subcategoryModel.setRowCount(0);        
        try {
            if(cmb_category.getSelectedIndex() !=0){
                Element element = (Element) Config.xml_config_lm_categorymgr.xpath.evaluate("//*[@sno='"+cmb_category.getSelectedIndex()+"']", Config.xml_config_lm_categorymgr.doc, XPathConstants.NODE); 
                String catid = element.getAttribute("category_id");                                
                NodeList nList = Config.xml_config_lm_subcategorymgr.doc.getElementsByTagName("subcategory");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);                
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if (eElement.getElementsByTagName("category_id").item(0).getTextContent().equals(catid)) {
                            if(eElement.getElementsByTagName("subcategoryname").item(0).getTextContent().toUpperCase().startsWith(txt_subcategory.getText().toUpperCase()))
                            tbl_subcategoryModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),i+1,eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});

                        }                    
                    }
                }
            }else if(cmb_category.getSelectedItem().equals("- Select -")){
                                                
                NodeList nList = Config.xml_config_lm_subcategorymgr.doc.getElementsByTagName("subcategory");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);                
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;                         
                            if(eElement.getElementsByTagName("subcategoryname").item(0).getTextContent().toUpperCase().startsWith(txt_subcategory.getText().toUpperCase())){
                            tbl_subcategoryModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),i+1,eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});
                            }     
                    }
                }
            } else  {}            
        }catch (Exception e) {}    
               
               
               
               
               
               
               
               
               
               
               
               
               
               
//               NodeList nList = Config.xml_subcategorymgr.doc.getElementsByTagName("subcategory");
//               if (cmb_category.getSelectedIndex()==0) {
//               for (int i = 0; i < nList.getLength(); i++) {
//                Node nNode = nList.item(i);
//                if(nNode.getNodeType() == Node.ELEMENT_NODE){
//                    Element eElement = (Element)nNode;                    
//                    if(eElement.getElementsByTagName("subcategoryname").item(0).getTextContent().toUpperCase().startsWith(txt_subcategory.getText().toUpperCase())) {
//                        tbl_subcategoryModel.addRow(new Object[] {                            
//                            eElement.getAttribute("subcategory_id"),                            
//                            i+1,
//                            eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()
//                        });                        
//                    }
//                  }                
//                }
//              } else if (cmb_category.getSelectedIndex()==1) {
//                 for (int i = 0; i < nList.getLength(); i++) {
//                Node nNode = nList.item(i);
//                if(nNode.getNodeType() == Node.ELEMENT_NODE){
//                    Element eElement = (Element)nNode;                    
//                    if(eElement.getElementsByTagName("subcategoryname").item(0).getTextContent().toUpperCase().startsWith(txt_issuablebook.getText().toUpperCase())) {
//                        tbl_subcategoryModel.addRow(new Object[] {
//                            eElement.getAttribute("subcategory_id"),                            
//                            i+1,
//                            eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()
//                        });                                        
//                    }
//                  }                
//                } 
//              }else if (cmb_category.getSelectedIndex()==2) {
//                 for (int i = 0; i < nList.getLength(); i++) {
//                Node nNode = nList.item(i);
//                if(nNode.getNodeType() == Node.ELEMENT_NODE){
//                    Element eElement = (Element)nNode;                    
//                    if(eElement.getElementsByTagName("subcategoryname").item(0).getTextContent().toUpperCase().startsWith(txt_issuablebook.getText().toUpperCase())) {
//                        tbl_subcategoryModel.addRow(new Object[] {
//                            eElement.getAttribute("subcategory_id"),                            
//                            i+1,
//                            eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()
//                        });                                        
//                    }
//                  }                
//                } 
//              }
//              else {
//                  for (int i = 0; i < nList.getLength(); i++) {
//                  Node nNode = nList.item(i);
//                  if(nNode.getNodeType() == Node.ELEMENT_NODE){
//                    Element eElement = (Element)nNode;  
//                    tbl_subcategoryModel.addRow(new Object[] {
//                        eElement.getAttribute("subcategory_id"),                            
//                        i+1,
//                        eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()
//                                                   
//                    });                   
//                }
//            }  
//          }    
//        } catch (Exception e) {
//        }




           
                           
    }//GEN-LAST:event_btn_searchSubcategoryActionPerformed

    private void txt_subcategoryCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_subcategoryCaretUpdate
          btn_searchSubcategoryActionPerformed(null);
    }//GEN-LAST:event_txt_subcategoryCaretUpdate

    private void txt_booksearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_booksearchCaretUpdate
        btn_searchActionPerformed(null);
    }//GEN-LAST:event_txt_booksearchCaretUpdate

    private void tbl_categoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_categoryMouseClicked
        if(evt.getClickCount() == 2){
            btn_viewcategoryActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_categoryMouseClicked

    private void btn_viewsubcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewsubcategoryActionPerformed
        Config.lm_viewsubcategory.onloadReset(tbl_subcategoryModel.getValueAt(tbl_subcategory.getSelectedRow(), 0).toString()) ;
        Config.lm_viewsubcategory.setVisible(true);
    }//GEN-LAST:event_btn_viewsubcategoryActionPerformed

    private void cmb_categoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_categoryActionPerformed
//        txt_subcategory.setText(null);
//        tbl_subcategoryModel.setRowCount(0);        
//        try {
//            if(cmb_category.getSelectedIndex() !=0){
//                Element element = (Element) Config.xml_categorymgr.xpath.evaluate("//*[@sno='"+cmb_category.getSelectedIndex()+"']", Config.xml_categorymgr.doc, XPathConstants.NODE); 
//                String catid = element.getAttribute("category_id");                                
//                NodeList nList = Config.xml_subcategorymgr.doc.getElementsByTagName("subcategory");
//                for (int i = 0; i < nList.getLength(); i++) {
//                    Node nNode = nList.item(i);                
//                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//                        Element eElement = (Element) nNode;
//                        if (eElement.getElementsByTagName("category_id").item(0).getTextContent().equals(catid)) {                            
//                            tbl_subcategoryModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),i+1,eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});
//                        }                    
//                    }
//                }
//            }
//        else {
//                NodeList nList = Config.xml_subcategorymgr.doc.getElementsByTagName("subcategory");
//                for (int i = 0; i < nList.getLength(); i++) {
//                    Node nNode = nList.item(i);                
//                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//                        Element eElement = (Element) nNode;
//                        tbl_subcategoryModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),i+1,eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});
//
//                    }
//                }            
//            }            
//        }catch (Exception e) {}    
        btn_searchSubcategoryActionPerformed(null);
    }//GEN-LAST:event_cmb_categoryActionPerformed

    private void cmb_booksearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_booksearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmb_booksearchActionPerformed

    private void btn_addIssuableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addIssuableActionPerformed
        Config.lm_addissuablebook.onLoadReset();
        Config.lm_addissuablebook.setVisible(true);
    }//GEN-LAST:event_btn_addIssuableActionPerformed

    private void btn_refreshBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_refreshBookActionPerformed
        Config.lm_repository_home.onloadReset();
    }//GEN-LAST:event_btn_refreshBookActionPerformed

    private void tbl_bookdetailsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_bookdetailsMouseClicked
        if(evt.getClickCount() == 2){
            btn_BDViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_bookdetailsMouseClicked

    private void btn_viewsubcategoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_viewsubcategoryMouseClicked
       try {
//        Config.lm_viewsubcategory.onloadReset(tbl_subcategory.getValueAt(tbl_subcategory.getSelectedRow(),0).toString());
        Config.lm_viewsubcategory.setVisible(true);
        } catch (Exception e) {
        }
       
        
    }//GEN-LAST:event_btn_viewsubcategoryMouseClicked

    private void tbl_subcategoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_subcategoryMouseClicked
      if(evt.getClickCount() == 2){
          btn_viewsubcategoryActionPerformed(null);
      }
    }//GEN-LAST:event_tbl_subcategoryMouseClicked

    private void btn_deletesubcategoryHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deletesubcategoryHomeActionPerformed
         String catid = tbl_subcategory.getValueAt(tbl_subcategory.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are you sure, You want to delete this record? ","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.config_lm_cm_addsubcategorymgr.delLm_Subcategory(catid)){ //               
                JOptionPane.showMessageDialog(this, "Record deleted successfully", "Success",JOptionPane.NO_OPTION);                               
                 }else{
                JOptionPane.showMessageDialog(this, "Error in deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             }
         
         
       
    }//GEN-LAST:event_btn_deletesubcategoryHomeActionPerformed

    private void btn_deletecategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deletecategoryActionPerformed
        String catid = tbl_category.getValueAt(tbl_category.getSelectedRow(),0).toString();
        int i = 0;
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are you sure, You want to delete category?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) { 
                 NodeList nList = Config.xml_config_lm_subcategorymgr.doc.getElementsByTagName("subcategory");
                 for ( i = 0; i < nList.getLength(); i++) {
                     Node nNode = nList.item(i);
                     if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                         Element eElement = (Element) nNode;
                         if(eElement.getElementsByTagName("category_id").item(0).getTextContent().equals(catid)){             
                             JOptionPane.showMessageDialog(this, "First you will have to delete all its subcategories", "Error",JOptionPane.ERROR_MESSAGE);
                             break;
                         }
                     } 
                 }if(i==nList.getLength()){
                    if(Config.config_lm_cm_addcategorymgr.delLm_category(catid)){ 
                        JOptionPane.showMessageDialog(this, "Category deleted successfully", "Success",JOptionPane.NO_OPTION);                               
                         }else{
                        JOptionPane.showMessageDialog(this, "Error in deletion", "Error",JOptionPane.ERROR_MESSAGE );
                        }
                 }
             } 
    }//GEN-LAST:event_btn_deletecategoryActionPerformed

    private void txt_issuablebookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_issuablebookActionPerformed
        
    }//GEN-LAST:event_txt_issuablebookActionPerformed

    private void txt_categoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_categoryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_categoryActionPerformed

    private void txt_issuablebookCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_issuablebookCaretUpdate
        btn_searchIssuablebookActionPerformed(null);
    }//GEN-LAST:event_txt_issuablebookCaretUpdate

    private void btn_searchDurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchDurationActionPerformed
            try {
               tbl_DurationModel.setRowCount(0);
               NodeList nList = Config.xml_config_lm_bookdurationmgr.doc.getElementsByTagName("bookduration");
               if (cmb_searchDuration.getSelectedItem().equals("Category")) {
               for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("category").item(0).getTextContent().toUpperCase().startsWith(txt_searchduration.getText().toUpperCase())) {
                        tbl_DurationModel.addRow(new Object[] {                            
                            eElement.getAttribute("duration_id"),                            
                            i+1,
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstudent").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstaff").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                }
              } else if (cmb_searchDuration.getSelectedItem().equals("Subcategory")) {
                 for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("subcategory").item(0).getTextContent().toUpperCase().startsWith(txt_searchduration.getText().toUpperCase())) {
                        tbl_DurationModel.addRow(new Object[] {                            
                            eElement.getAttribute("duration_id"),   
                            i+1,
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstudent").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstaff").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                } 
              }else {}    
        } catch (Exception e) {
        }
            
    }//GEN-LAST:event_btn_searchDurationActionPerformed

    private void txt_searchdurationCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_searchdurationCaretUpdate
        btn_searchDurationActionPerformed(null);
    }//GEN-LAST:event_txt_searchdurationCaretUpdate

    private void btn_searchOverdueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchOverdueActionPerformed
        try {
               tbl_OverdueModel.setRowCount(0);
               NodeList nList = Config.xml_config_lm_overduechargemgr.doc.getElementsByTagName("lm_overduecharge");
               if (cmb_searchOverdue.getSelectedItem().equals("Category")) {
               for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("category").item(0).getTextContent().toUpperCase().startsWith(txt_searchoverdue.getText().toUpperCase())) {
                        tbl_OverdueModel.addRow(new Object[] {                            
                            eElement.getAttribute("overduecharge_id"),    
                            i+1,
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstudent").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstaff").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                }
              } else if (cmb_searchOverdue.getSelectedItem().equals("Subcategory")) {
                 for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("subcategory").item(0).getTextContent().toUpperCase().startsWith(txt_searchoverdue.getText().toUpperCase())) {
                        tbl_OverdueModel.addRow(new Object[] {                            
                            eElement.getAttribute("overduecharge_id"),        
                            i+1,
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstudent").item(0).getTextContent(),
                            eElement.getElementsByTagName("forstaff").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                } 
              }else {}    
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_searchOverdueActionPerformed

    private void txt_searchoverdueCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_searchoverdueCaretUpdate
        btn_searchOverdueActionPerformed(null);
    }//GEN-LAST:event_txt_searchoverdueCaretUpdate

    private void btn_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchActionPerformed
        try {
               tbl_bookdetailsModel.setRowCount(0);
               NodeList nList = Config.xml_lm_bookdetailsmgr.doc.getElementsByTagName("lm_bookdetails");
               if (cmb_booksearch.getSelectedItem().equals("Book Id")) {
               for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getAttribute("book_id").toUpperCase().startsWith(txt_booksearch.getText().toUpperCase())) {
                        tbl_bookdetailsModel.addRow(new Object[] {
                            i+1,
                            eElement.getAttribute("book_id"),                            
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("standard").item(0).getTextContent(),
                            eElement.getElementsByTagName("subject").item(0).getTextContent(),
                            eElement.getElementsByTagName("booktitle").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                }
              } else if (cmb_booksearch.getSelectedItem().equals("Category")) {
                 for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("category").item(0).getTextContent().toUpperCase().startsWith(txt_booksearch.getText().toUpperCase())) {
                        tbl_bookdetailsModel.addRow(new Object[] {
                            i+1,
                            eElement.getAttribute("book_id"),                            
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("standard").item(0).getTextContent(),
                            eElement.getElementsByTagName("subject").item(0).getTextContent(),
                            eElement.getElementsByTagName("booktitle").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                } 
              } else if (cmb_booksearch.getSelectedItem().equals("Subcategory")) {
                    for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("subcategory").item(0).getTextContent().toUpperCase().startsWith(txt_booksearch.getText().toUpperCase())) {
                        tbl_bookdetailsModel.addRow(new Object[] {
                            i+1,
                            eElement.getAttribute("book_id"),                            
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("standard").item(0).getTextContent(),
                            eElement.getElementsByTagName("subject").item(0).getTextContent(),
                            eElement.getElementsByTagName("booktitle").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                } 
              }else if (cmb_booksearch.getSelectedItem().equals("Standard")) {
                for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("standard").item(0).getTextContent().toUpperCase().startsWith(txt_booksearch.getText().toUpperCase())) {
                        tbl_bookdetailsModel.addRow(new Object[] {
                            i+1,
                            eElement.getAttribute("book_id"),                            
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("standard").item(0).getTextContent(),
                            eElement.getElementsByTagName("subject").item(0).getTextContent(),
                            eElement.getElementsByTagName("booktitle").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                }   
              }else if (cmb_booksearch.getSelectedItem().equals("Subject")) {
                  for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("subject").item(0).getTextContent().toUpperCase().startsWith(txt_booksearch.getText().toUpperCase())) {
                        tbl_bookdetailsModel.addRow(new Object[] {
                            i+1,
                            eElement.getAttribute("book_id"),                            
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("standard").item(0).getTextContent(),
                            eElement.getElementsByTagName("subject").item(0).getTextContent(),
                            eElement.getElementsByTagName("booktitle").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                }  
              }else if (cmb_booksearch.getSelectedItem().equals("Book Title")) {
                    for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("booktitle").item(0).getTextContent().toUpperCase().startsWith(txt_booksearch.getText().toUpperCase())) {
                        tbl_bookdetailsModel.addRow(new Object[] {
                            i+1,
                            eElement.getAttribute("book_id"),                            
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("standard").item(0).getTextContent(),
                            eElement.getElementsByTagName("subject").item(0).getTextContent(),
                            eElement.getElementsByTagName("booktitle").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                } 
              }else if (cmb_booksearch.getSelectedItem().equals("Author Name")) {
                   for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;                    
                    if(eElement.getElementsByTagName("authorname").item(0).getTextContent().toUpperCase().startsWith(txt_booksearch.getText().toUpperCase())) {
                        tbl_bookdetailsModel.addRow(new Object[] {
                            i+1,
                            eElement.getAttribute("book_id"),                            
                            eElement.getElementsByTagName("category").item(0).getTextContent(),   
                            eElement.getElementsByTagName("subcategory").item(0).getTextContent(),
                            eElement.getElementsByTagName("standard").item(0).getTextContent(),
                            eElement.getElementsByTagName("subject").item(0).getTextContent(),
                            eElement.getElementsByTagName("booktitle").item(0).getTextContent()
                            
                        });                        
                    }
                  }                
                }
              }
               else {}    
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btn_searchActionPerformed

    private void txt_categoryCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_categoryCaretUpdate
        btn_searchCategoryActionPerformed(null);
    }//GEN-LAST:event_txt_categoryCaretUpdate

    private void btn_deleteIssuableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteIssuableActionPerformed
        String bookid = tbl_IssuableBook.getValueAt(tbl_IssuableBook.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are you sure, You want to delete this record? ","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.config_lm_cm_addissuablebookmgr.delIssuablebook(bookid)){ //               
                JOptionPane.showMessageDialog(this, "Record deleted successfully", "Success",JOptionPane.NO_OPTION);                               
                 }else{
                JOptionPane.showMessageDialog(this, "Error in deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             }
    }//GEN-LAST:event_btn_deleteIssuableActionPerformed

    private void btn_deleteOverdueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteOverdueActionPerformed
         String bookid = tbl_Overdue.getValueAt(tbl_Overdue.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are you sure, You want to delete this record? ","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.config_lm_cm_addoverduechargemgr.del_OverdueCharge(bookid)){ //               
                JOptionPane.showMessageDialog(this, "Record deleted successfully", "Success",JOptionPane.NO_OPTION);                               
                 }else{
                JOptionPane.showMessageDialog(this, "Error in deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             }
    }//GEN-LAST:event_btn_deleteOverdueActionPerformed

    private void btn_DeleteBookdetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DeleteBookdetailsActionPerformed
        String bookid = tbl_bookdetails.getValueAt(tbl_bookdetails.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are you sure, You want to delete this record? ","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.lm_cm_bookdetailsmgr.delBookDetails(bookid)){ //               
                JOptionPane.showMessageDialog(this, "Record deleted successfully", "Success",JOptionPane.NO_OPTION);                               
                 }else{
                JOptionPane.showMessageDialog(this, "Error in deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             }
    }//GEN-LAST:event_btn_DeleteBookdetailsActionPerformed

    private void btn_BDViewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_BDViewMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_BDViewMouseClicked

    private void btn_viewcategoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_viewcategoryMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_viewcategoryMouseClicked

    private void tbl_IssuableBookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_IssuableBookMouseClicked
        if(evt.getClickCount() == 2){
            btn_viewIssuableActionPerformed(null);
        }
        
    }//GEN-LAST:event_tbl_IssuableBookMouseClicked

    private void tbl_DurationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_DurationMouseClicked
        if(evt.getClickCount() == 2){
            btn_viewDurationActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_DurationMouseClicked

    private void tbl_OverdueMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_OverdueMouseClicked
        if(evt.getClickCount() == 2){
            btn_viewOverdueActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_OverdueMouseClicked

    private void btn_refreshCirculationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_refreshCirculationActionPerformed
        Config.lm_repository_home.onloadReset();
    }//GEN-LAST:event_btn_refreshCirculationActionPerformed

    private void btn_refreshCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_refreshCategoryActionPerformed
        Config.lm_repository_home.onloadReset();
    }//GEN-LAST:event_btn_refreshCategoryActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_BDView;
    private javax.swing.JButton btn_DR_BdExpt;
    private javax.swing.JButton btn_DR_Bdprnt;
    private javax.swing.JButton btn_DeleteBookdetails;
    private javax.swing.JButton btn_NewEntry;
    private javax.swing.JButton btn_StoPro_SubCatPrint;
    private javax.swing.JButton btn_StoPro_SubCatPrint1;
    private javax.swing.JButton btn_StoPro_SubCatPrint2;
    private javax.swing.JButton btn_addDuration;
    private javax.swing.JButton btn_addIssuable;
    private javax.swing.JButton btn_addOverdue;
    private javax.swing.JButton btn_addcategory;
    private javax.swing.JButton btn_addsubcategory;
    private javax.swing.JButton btn_cancelBook;
    private javax.swing.JButton btn_cancelCategory;
    private javax.swing.JButton btn_cancelCirculation;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_deleteIssuable;
    private javax.swing.JButton btn_deleteOverdue;
    private javax.swing.JButton btn_deletecategory;
    private javax.swing.JButton btn_deletesubcategoryHome;
    private javax.swing.JButton btn_refreshBook;
    private javax.swing.JButton btn_refreshCategory;
    private javax.swing.JButton btn_refreshCirculation;
    private javax.swing.JButton btn_search;
    private javax.swing.JButton btn_searchCategory;
    private javax.swing.JButton btn_searchDuration;
    private javax.swing.JButton btn_searchIssuablebook;
    private javax.swing.JButton btn_searchOverdue;
    private javax.swing.JButton btn_searchSubcategory;
    private javax.swing.JButton btn_viewDuration;
    private javax.swing.JButton btn_viewIssuable;
    private javax.swing.JButton btn_viewOverdue;
    private javax.swing.JButton btn_viewcategory;
    private javax.swing.JButton btn_viewsubcategory;
    private javax.swing.JComboBox cmb_booksearch;
    private javax.swing.JComboBox cmb_category;
    private javax.swing.JComboBox cmb_searchDuration;
    private javax.swing.JComboBox cmb_searchIssuable;
    private javax.swing.JComboBox cmb_searchOverdue;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JLabel lbl_DR;
    private javax.swing.JLabel lbl_DR_BdSrch;
    private javax.swing.JLabel lbl_DR_BdtblCont;
    private javax.swing.JTable tbl_Duration;
    private javax.swing.JTable tbl_IssuableBook;
    private javax.swing.JTable tbl_Overdue;
    private javax.swing.JTable tbl_bookdetails;
    private javax.swing.JTable tbl_category;
    private javax.swing.JTable tbl_subcategory;
    private javax.swing.JTextField txt_DR_Bdtblcont;
    private javax.swing.JTextField txt_booksearch;
    private javax.swing.JTextField txt_category;
    private javax.swing.JTextField txt_issuablebook;
    private javax.swing.JTextField txt_searchduration;
    private javax.swing.JTextField txt_searchoverdue;
    private javax.swing.JTextField txt_subcategory;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {
 
        tbl_catagoryModel.setRowCount(0);       
        NodeList nList = Config.xml_config_lm_categorymgr.doc.getElementsByTagName("category");
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element)nNode;
                tbl_catagoryModel.addRow(new Object[] {eElement.getAttribute("category_id"),i+1,eElement.getElementsByTagName("categoryname").item(0).getTextContent()});                                     
            }
        }
        cmb_category.removeAllItems();  
        cmb_category.addItem("- Select -");
//        NodeList nList = Config.xml_categorymgr.doc.getElementsByTagName("category");
        for(int i=0; i < nList.getLength();i++){
            Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element)nNode;
                    cmb_category.addItem(eElement.getElementsByTagName("categoryname").item(0).getTextContent());
                }         
        } 
        
        txt_subcategory.setText("");        
        tbl_subcategoryModel.setRowCount(0);
        System.out.println("12");
        NodeList nList1 = Config.xml_config_lm_subcategorymgr.doc.getElementsByTagName("subcategory");
        for (int i = 0; i < nList1.getLength(); i++) {
            Node nNode = nList1.item(i);                
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                tbl_subcategoryModel.addRow(new Object[] {eElement.getAttribute("subcategory_id"),i+1,eElement.getElementsByTagName("subcategoryname").item(0).getTextContent()});
            }
        } 
        
        tbl_bookdetailsModel.setRowCount(0);       
        NodeList bList = Config.xml_lm_bookdetailsmgr.doc.getElementsByTagName("lm_bookdetails");
        for (int i = 0; i < bList.getLength(); i++) {
            Node nNode = bList.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE){
                Element eElement = (Element)nNode;
                tbl_bookdetailsModel.addRow(new Object[] {i+1,eElement.getAttribute("book_id"),eElement.getElementsByTagName("category").item(0).getTextContent(),eElement.getElementsByTagName("subcategory").item(0).getTextContent(),eElement.getElementsByTagName("standard").item(0).getTextContent(),eElement.getElementsByTagName("subject").item(0).getTextContent(),eElement.getElementsByTagName("booktitle").item(0).getTextContent()});                  
            }
        }
            
        tbl_IssuableBookModel.setRowCount(0);       
        NodeList iList = Config.xml_config_lm_issuablebookmgr.doc.getElementsByTagName("issuablebook");
        for (int i = 0; i < iList.getLength(); i++) {
            Node nNode = iList.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE){
                Element eElement = (Element)nNode;
                tbl_IssuableBookModel.addRow(new Object[] {eElement.getAttribute("issuablebook_id"),i+1,eElement.getElementsByTagName("categoryname").item(0).getTextContent(),eElement.getElementsByTagName("subcategoryname").item(0).getTextContent(),eElement.getElementsByTagName("forstudent").item(0).getTextContent(),eElement.getElementsByTagName("forstaff").item(0).getTextContent()});                   
            }
        }
        tbl_DurationModel.setRowCount(0);       
        NodeList dList = Config.xml_config_lm_bookdurationmgr.doc.getElementsByTagName("bookduration");
        for (int i = 0; i < dList.getLength(); i++) {
            Node nNode = dList.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE){
                Element eElement = (Element)nNode;
                tbl_DurationModel.addRow(new Object[] {eElement.getAttribute("duration_id"),i+1,eElement.getElementsByTagName("category").item(0).getTextContent(),eElement.getElementsByTagName("subcategory").item(0).getTextContent(),eElement.getElementsByTagName("forstudent").item(0).getTextContent(),eElement.getElementsByTagName("forstaff").item(0).getTextContent()});
            }
        }


        tbl_OverdueModel.setRowCount(0);       
        NodeList oList = Config.xml_config_lm_overduechargemgr.doc.getElementsByTagName("lm_overduecharge");
        for (int i = 0; i < oList.getLength(); i++) {
            Node nNode = oList.item(i);
            if(nNode.getNodeType() == Node.ELEMENT_NODE){
                Element eElement = (Element)nNode;
                tbl_OverdueModel.addRow(new Object[] {eElement.getAttribute("overduecharge_id"),i+1,eElement.getElementsByTagName("category").item(0).getTextContent(),eElement.getElementsByTagName("subcategory").item(0).getTextContent(),eElement.getElementsByTagName("forstudent").item(0).getTextContent(),eElement.getElementsByTagName("forstaff").item(0).getTextContent()});
            }
        }
    }
}

        

  
    

