package modules.exam.Ex_Repository;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.examination.Config_Exam_Dm_ClassSub;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ashok
 */
public class Ex_View_ClassSubjectMarks extends javax.swing.JDialog {

    /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1;

    /**
     * Creates new form Exam_ClassSubject
     */
    public Ex_View_ClassSubjectMarks(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
    }

    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btn_Cancel = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmb_Class = new javax.swing.JComboBox();
        cmb_Subject = new javax.swing.JComboBox();
        txt_TheMarks = new javax.swing.JTextField();
        txt_ThePassMarks = new javax.swing.JTextField();
        txt_PracMarks = new javax.swing.JTextField();
        txt_PraPassMark = new javax.swing.JTextField();
        btn_Update = new javax.swing.JButton();
        btn_Delete = new javax.swing.JButton();
        lbl_ClSubMar = new javax.swing.JLabel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_Cancel.setText("Cancel");
        btn_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CancelActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Exam Class/Subject Marks", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Class Name :");

        jLabel2.setText("Subject Name :");

        jLabel3.setText("Theory Marks :");

        jLabel4.setText("The. Passing Marks :");

        jLabel5.setText("Pra. Passing Marks :");

        jLabel6.setText("Practical Marks :");

        txt_PraPassMark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_PraPassMarkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Class, 0, 181, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Subject, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ThePassMarks))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel6)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_TheMarks)
                            .addComponent(txt_PracMarks)
                            .addComponent(txt_PraPassMark))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmb_Class, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmb_Subject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_TheMarks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ThePassMarks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txt_PracMarks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txt_PraPassMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmb_Class, cmb_Subject, jLabel1, jLabel2, jLabel3, jLabel5, jLabel6});

        btn_Update.setText("Update");
        btn_Update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_UpdateActionPerformed(evt);
            }
        });

        btn_Delete.setText("Delete");
        btn_Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_ClSubMar, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_Update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Cancel)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Cancel, btn_Update});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_Cancel)
                        .addComponent(btn_Update)
                        .addComponent(btn_Delete))
                    .addComponent(lbl_ClSubMar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CancelActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_btn_CancelActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void txt_PraPassMarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_PraPassMarkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_PraPassMarkActionPerformed

    private void btn_UpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_UpdateActionPerformed
        // TODO add your handling code here:
        
         try {
            
            String str = checkValidity();
        if (str.equals("ok")){
            
               int classindex = cmb_Class.getSelectedIndex()+1; 
               int subjectindex = cmb_Subject.getSelectedIndex()+1; 
               String classid = null;
               String subjectid = null;
               
               Config_Exam_Dm_ClassSub classub = new Config_Exam_Dm_ClassSub();
            
               Element element = (Element)Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno="+classindex+"]", Config.xml_config_ad_classmgr.doc,XPathConstants.NODE);
               classid = element.getAttribute("class_id");
               
               Element element1 = (Element)Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@sno='"+subjectindex+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
               subjectid = element1.getAttribute("subject_id");
               
               classub.setClasssubject_id(lbl_ClSubMar.getText());
               classub.setClass_id(classid);
               classub.setSubject_id(subjectid);
               classub.setTotal_theorymarks(txt_TheMarks.getText().trim());
               classub.setTheory_passingmarks(txt_ThePassMarks.getText().trim());
               classub.setTototal_practicalmarks(txt_PracMarks.getText().trim());
               classub.setPractical_passingmarks(txt_PraPassMark.getText().trim());
            
            if(Config.config_exam_cm_classsub.upd_ClasSub(classub)){                
                JOptionPane.showMessageDialog(this, "Record updated Successfully", "Successfull",JOptionPane.NO_OPTION);
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "Error in updation", "Error",JOptionPane.ERROR_MESSAGE );
            }
        }else{            
            JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
        }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
//        try {
//            
//         String str = checkValidity();
//        if (str.equals("ok")){
//            
//            int classindex = cmb_Class.getSelectedIndex()+1; 
//                System.out.println(classindex);
//               int subjectindex = cmb_Subject.getSelectedIndex()+1; 
//               String classid = null;
//               String subid = null;
//                
//                Config_Exam_Dm_ClassSub classub = new Config_Exam_Dm_ClassSub();
//                
//                
//                Element element = (Element)Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno="+classindex+"]", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
//                classid = element.getAttribute("class_id");
//                System.out.println(classid);
//                
//                Element element1 = (Element)Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@sno='"+subjectindex+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
//                subid = element1.getAttribute("subject_id");
//                
//           
//            
//            classub.setClasssubject_id(lbl_ClSubMar.getText());
//            classub.setClass_id(classid);
//            classub.setClasssubject_id(subid);
//            classub.setTotal_theorymarks(txt_TheMarks.getText());
//            classub.setTheory_passingmarks(txt_ThePassMarks.getText());
//            classub.setTototal_practicalmarks(txt_PracMarks.getText());
//            classub.setPractical_passingmarks(txt_PraPassMark.getText());
//            
//            if(Config.config_exam_cm_classsub.upd_ClasSub(classub)){                
//                JOptionPane.showMessageDialog(this, "Record updated Successfully", "Successfull",JOptionPane.NO_OPTION);
//                dispose();
//            }else{
//                JOptionPane.showMessageDialog(this, "Error in updation", "Error",JOptionPane.ERROR_MESSAGE );
//            }
//        }else{            
//            JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
//        }
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        
    }//GEN-LAST:event_btn_UpdateActionPerformed

    private void btn_DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DeleteActionPerformed
        // TODO add your handling code here:
         String str = checkValidity();
        if (str.equals("ok")){
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure,you want to delete this record ? ","Warning",JOptionPane.YES_NO_OPTION);             
            if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.config_exam_cm_classsub.del_ClasSub(lbl_ClSubMar.getText())){ 
                    
                    JOptionPane.showMessageDialog(this, "Recorded Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                    dispose();            
                }else{
                    JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                    }
            }else{                
                dispose();     
                }
            }else{            
                JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
                }
        
    }//GEN-LAST:event_btn_DeleteActionPerformed
    
    private void doClose(int retStatus) {
        returnStatus = retStatus;
        setVisible(false);
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ex_View_ClassSubjectMarks.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ex_View_ClassSubjectMarks.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ex_View_ClassSubjectMarks.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ex_View_ClassSubjectMarks.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Ex_View_ClassSubjectMarks dialog = new Ex_View_ClassSubjectMarks(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Cancel;
    private javax.swing.JButton btn_Delete;
    private javax.swing.JButton btn_Update;
    private javax.swing.JComboBox cmb_Class;
    private javax.swing.JComboBox cmb_Subject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lbl_ClSubMar;
    private javax.swing.JTextField txt_PraPassMark;
    private javax.swing.JTextField txt_PracMarks;
    private javax.swing.JTextField txt_TheMarks;
    private javax.swing.JTextField txt_ThePassMarks;
    // End of variables declaration//GEN-END:variables

    private int returnStatus = RET_CANCEL;
    
    void onloadReset(String classubid) {
        try {
            lbl_ClSubMar.setText(classubid);
            
           
            
            NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
             cmb_Class.removeAllItems();
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Class.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent().toString());
                }
             }
             
             NodeList nodeList = Config.xml_config_ad_subjectmgr.doc.getElementsByTagName("config_ad_subject");
             cmb_Subject.removeAllItems();
             for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                 if (node.getNodeType() == Node.ELEMENT_NODE) {
                     Element element1 = (Element) node;
                     cmb_Subject.addItem(element1.getElementsByTagName("subject_name").item(0).getTextContent().toString());
                     
                 }
             }
           
            Element element = (Element) Config.xml_config_examclasssubmgr.xpath.evaluate("//*[@classsubject_id='"+classubid+"']", Config.xml_config_examclasssubmgr.doc, XPathConstants.NODE);
            
            String classid = element.getElementsByTagName("class_id").item(0).getTextContent();
            
            Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
            String classname = element1.getElementsByTagName("class_name").item(0).getTextContent();
            cmb_Class.setSelectedItem(classname);
            
            String subjectid = element.getElementsByTagName("subject_id").item(0).getTextContent();
            
            Element element2 = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subjectid+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
            String subjectname = element2.getElementsByTagName("subject_name").item(0).getTextContent();
            cmb_Subject.setSelectedItem(subjectname);
            
            txt_TheMarks.setText(element.getElementsByTagName("total_theorymarks").item(0).getTextContent());
            txt_ThePassMarks.setText(element.getElementsByTagName("theory_passingmarks").item(0).getTextContent());            
            txt_PracMarks.setText(element.getElementsByTagName("tototal_practicalmarks").item(0).getTextContent());
            txt_PraPassMark.setText(element.getElementsByTagName("practical_passingmarks").item(0).getTextContent());
            
            
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error",JOptionPane.ERROR_MESSAGE );
        }
        
       
    }

    private String checkValidity() {
       if(txt_TheMarks.getText().equals("")){
        return "Grade";
        }else{
         return "ok";
        }
    }
}
