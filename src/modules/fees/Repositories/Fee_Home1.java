package modules.fees.Repositories;


import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.table.DefaultTableModel;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ashok
 */
public class Fee_Home1 extends javax.swing.JDialog {
    
    DefaultTableModel tbl_feetypemodel; 
    DefaultTableModel tbl_feenamemodel;
    DefaultTableModel tbl_feeamountmodel; 
//    DefaultTableModel tbl_feesubmissionmodel;
//    DefaultTableModel tbl_damagemodel;

   
    
    
    public Fee_Home1() {
        
        initComponents();
        Dimension scrnsize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Double(scrnsize.getWidth()).intValue()-150, new Double(scrnsize.getHeight()).intValue()-100);
        setLocationRelativeTo(null);
        
        tbl_feetypemodel = (DefaultTableModel) tbl_FeeType.getModel();
        tbl_feenamemodel = (DefaultTableModel) tbl_FeeName.getModel();
        tbl_feeamountmodel = (DefaultTableModel) tbl_FeeAmount.getModel();
//        tbl_feesubmissionmodel = (DefaultTableModel) tbl_SubmissionDate.getModel();
//        tbl_damagemodel = (DefaultTableModel) tbl_Damage.getModel();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        lbl_SearchFeeType = new javax.swing.JLabel();
        txt_FeeTypeSearch = new javax.swing.JTextField();
        btn_Search = new javax.swing.JButton();
        btn_FeeTypeNewEntry = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_FeeType = new javax.swing.JTable();
        btn_FeeTypeDelete = new javax.swing.JButton();
        btn_FeeTypeView = new javax.swing.JButton();
        btn_FeeTypeRefresh = new javax.swing.JButton();
        btn_FeeTypePrint = new javax.swing.JButton();
        btn_FeeTypeCancel = new javax.swing.JButton();
        lbl_FeeTypeTotalCount = new javax.swing.JLabel();
        txt_FeeTypeTotalCount = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        lbl_FeeAmountSearch = new javax.swing.JLabel();
        txt_FeeAmountSearch = new javax.swing.JTextField();
        btn_FeeAmountSearch = new javax.swing.JButton();
        btn_FeeAmountNewEntry = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jComboBox1 = new javax.swing.JComboBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_FeeName = new javax.swing.JTable();
        btn_FeeAmountDelete = new javax.swing.JButton();
        btn_FeeNameView = new javax.swing.JButton();
        btn_FeeAmountRefresh = new javax.swing.JButton();
        btn_FeeAmountPrint = new javax.swing.JButton();
        btn_FeeAmountlCancel = new javax.swing.JButton();
        lbl_FeeAmountCount = new javax.swing.JLabel();
        txt_FeeAmountCount = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        lbl_fineSearch = new javax.swing.JLabel();
        txt__fineSearch = new javax.swing.JTextField();
        btn__fineSearch = new javax.swing.JButton();
        btn__fineNewEntry = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_FeeAmount = new javax.swing.JTable();
        btn_FineDelete = new javax.swing.JButton();
        btn_FineView = new javax.swing.JButton();
        btn_FineRefresh = new javax.swing.JButton();
        btn_FinePrint = new javax.swing.JButton();
        btn_FineCancel = new javax.swing.JButton();
        lbl_FineCount = new javax.swing.JLabel();
        txt_FineCount = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        lbl_FineAmountSearch = new javax.swing.JLabel();
        txt_FineAmountSearch2 = new javax.swing.JTextField();
        btn_FineAmountSearch = new javax.swing.JButton();
        btn_FineAmountNewEntry = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_SubmissionDate = new javax.swing.JTable();
        btn_FineAmountDelete = new javax.swing.JButton();
        btn_FineAmountView = new javax.swing.JButton();
        btn_FineAmountRefresh = new javax.swing.JButton();
        btn_FineAmountPrint = new javax.swing.JButton();
        btn_FineAmountlCancel = new javax.swing.JButton();
        lbl_FineAmountCount = new javax.swing.JLabel();
        txt_FineAmountCount = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        lbl_DamageSearch = new javax.swing.JLabel();
        txt_DamageSearch = new javax.swing.JTextField();
        btn_DamageSearch = new javax.swing.JButton();
        btn_DamageNewEntry = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_Damage = new javax.swing.JTable();
        btn_DamageDelete = new javax.swing.JButton();
        btn_DamageView = new javax.swing.JButton();
        btn_DamageRefresh = new javax.swing.JButton();
        btn_DamagePrint = new javax.swing.JButton();
        btn_DamageCancel = new javax.swing.JButton();
        lbl_DamageCount = new javax.swing.JLabel();
        txt_DamageCount = new javax.swing.JTextField();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Data Repository");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        lbl_SearchFeeType.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_SearchFeeType.setText("Search By Fees Type :");

        txt_FeeTypeSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_FeeTypeSearchCaretUpdate(evt);
            }
        });

        btn_Search.setText("Search");
        btn_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SearchActionPerformed(evt);
            }
        });

        btn_FeeTypeNewEntry.setText("New Entry");
        btn_FeeTypeNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FeeTypeNewEntryActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_SearchFeeType)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_FeeTypeSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_Search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_FeeTypeNewEntry)
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FeeTypeNewEntry, btn_Search});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_SearchFeeType, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_FeeTypeSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_Search)
                        .addComponent(btn_FeeTypeNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_FeeTypeNewEntry, btn_Search, jSeparator1, lbl_SearchFeeType, txt_FeeTypeSearch});

        tbl_FeeType.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "fee_id", "S.No.", "Fee type"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_FeeType.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_FeeTypeMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_FeeType);
        if (tbl_FeeType.getColumnModel().getColumnCount() > 0) {
            tbl_FeeType.getColumnModel().getColumn(0).setResizable(false);
            tbl_FeeType.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_FeeType.getColumnModel().getColumn(1).setResizable(false);
            tbl_FeeType.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_FeeType.getColumnModel().getColumn(2).setResizable(false);
            tbl_FeeType.getColumnModel().getColumn(2).setPreferredWidth(250);
        }

        btn_FeeTypeDelete.setText("Delete");

        btn_FeeTypeView.setText("View");
        btn_FeeTypeView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FeeTypeViewActionPerformed(evt);
            }
        });

        btn_FeeTypeRefresh.setText("Refresh");

        btn_FeeTypePrint.setText("Print");

        btn_FeeTypeCancel.setText("Cancel");
        btn_FeeTypeCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FeeTypeCancelActionPerformed(evt);
            }
        });

        lbl_FeeTypeTotalCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_FeeTypeTotalCount.setText("Total Count :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lbl_FeeTypeTotalCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_FeeTypeTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_FeeTypeView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeTypeDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeTypeRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeTypePrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeTypeCancel)))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FeeTypeCancel, btn_FeeTypeDelete, btn_FeeTypePrint, btn_FeeTypeRefresh, btn_FeeTypeView});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_FeeTypeCancel)
                    .addComponent(btn_FeeTypePrint)
                    .addComponent(btn_FeeTypeRefresh)
                    .addComponent(btn_FeeTypeDelete)
                    .addComponent(btn_FeeTypeView)
                    .addComponent(lbl_FeeTypeTotalCount)
                    .addComponent(txt_FeeTypeTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_FeeTypeView, lbl_FeeTypeTotalCount, txt_FeeTypeTotalCount});

        jTabbedPane1.addTab("Fees Type", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        lbl_FeeAmountSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_FeeAmountSearch.setText("Search By  :");
        lbl_FeeAmountSearch.setPreferredSize(new java.awt.Dimension(105, 14));

        btn_FeeAmountSearch.setText("Search");
        btn_FeeAmountSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FeeAmountSearchActionPerformed(evt);
            }
        });

        btn_FeeAmountNewEntry.setText("New Entry");
        btn_FeeAmountNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FeeAmountNewEntryActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select By -", "Class Name", "Fee Name" }));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_FeeAmountSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_FeeAmountSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_FeeAmountSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_FeeAmountNewEntry)
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FeeAmountNewEntry, btn_FeeAmountSearch});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_FeeAmountSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_FeeAmountSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_FeeAmountSearch)
                        .addComponent(btn_FeeAmountNewEntry)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_FeeAmountNewEntry, btn_FeeAmountSearch, jSeparator2, lbl_FeeAmountSearch, txt_FeeAmountSearch});

        tbl_FeeName.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "feename_id", "S.No.", "Fee Type", "Fee Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_FeeName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_FeeNameMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_FeeName);
        if (tbl_FeeName.getColumnModel().getColumnCount() > 0) {
            tbl_FeeName.getColumnModel().getColumn(0).setResizable(false);
            tbl_FeeName.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_FeeName.getColumnModel().getColumn(1).setResizable(false);
            tbl_FeeName.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_FeeName.getColumnModel().getColumn(2).setResizable(false);
            tbl_FeeName.getColumnModel().getColumn(2).setPreferredWidth(200);
            tbl_FeeName.getColumnModel().getColumn(3).setResizable(false);
            tbl_FeeName.getColumnModel().getColumn(3).setPreferredWidth(350);
        }

        btn_FeeAmountDelete.setText("Delete");

        btn_FeeNameView.setText("View");
        btn_FeeNameView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FeeNameViewActionPerformed(evt);
            }
        });

        btn_FeeAmountRefresh.setText("Refresh");

        btn_FeeAmountPrint.setText("Print");

        btn_FeeAmountlCancel.setText("Cancel");
        btn_FeeAmountlCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FeeAmountlCancelActionPerformed(evt);
            }
        });

        lbl_FeeAmountCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_FeeAmountCount.setText("Total Count :");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(lbl_FeeAmountCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_FeeAmountCount, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_FeeNameView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeAmountDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeAmountRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeAmountPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FeeAmountlCancel)))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FeeAmountDelete, btn_FeeAmountPrint, btn_FeeAmountRefresh, btn_FeeAmountlCancel, btn_FeeNameView});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_FeeAmountCount)
                        .addComponent(txt_FeeAmountCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_FeeAmountlCancel)
                        .addComponent(btn_FeeAmountPrint)
                        .addComponent(btn_FeeAmountRefresh)
                        .addComponent(btn_FeeAmountDelete)
                        .addComponent(btn_FeeNameView)))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_FeeNameView, lbl_FeeAmountCount, txt_FeeAmountCount});

        jTabbedPane1.addTab("Fees Name", jPanel4);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setPreferredSize(new java.awt.Dimension(843, 386));

        lbl_fineSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_fineSearch.setText("Search By Fine Type :");
        lbl_fineSearch.setPreferredSize(new java.awt.Dimension(105, 14));

        btn__fineSearch.setText("Search");
        btn__fineSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn__fineSearchActionPerformed(evt);
            }
        });

        btn__fineNewEntry.setText("New Entry");
        btn__fineNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn__fineNewEntryActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_fineSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt__fineSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn__fineSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn__fineNewEntry)
                .addContainerGap())
        );

        jPanel11Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn__fineNewEntry, btn__fineSearch});

        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_fineSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt__fineSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn__fineSearch)
                        .addComponent(btn__fineNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel11Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn__fineSearch, lbl_fineSearch, txt__fineSearch});

        tbl_FeeAmount.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "feeamount_id", "S.No.", "Session Name", "Class Name", "Fee type", "Fee Name", "Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_FeeAmount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_FeeAmountMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_FeeAmount);
        if (tbl_FeeAmount.getColumnModel().getColumnCount() > 0) {
            tbl_FeeAmount.getColumnModel().getColumn(0).setResizable(false);
            tbl_FeeAmount.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_FeeAmount.getColumnModel().getColumn(1).setResizable(false);
            tbl_FeeAmount.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_FeeAmount.getColumnModel().getColumn(2).setResizable(false);
            tbl_FeeAmount.getColumnModel().getColumn(3).setResizable(false);
            tbl_FeeAmount.getColumnModel().getColumn(4).setResizable(false);
        }

        btn_FineDelete.setText("Delete");

        btn_FineView.setText("View");
        btn_FineView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FineViewActionPerformed(evt);
            }
        });

        btn_FineRefresh.setText("Refresh");

        btn_FinePrint.setText("Print");

        btn_FineCancel.setText("Cancel");
        btn_FineCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FineCancelActionPerformed(evt);
            }
        });

        lbl_FineCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_FineCount.setText("Total Count :");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addComponent(lbl_FineCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_FineCount, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 310, Short.MAX_VALUE)
                        .addComponent(btn_FineView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FineDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FineRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FinePrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FineCancel)))
                .addContainerGap())
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FineCancel, btn_FineDelete, btn_FinePrint, btn_FineRefresh, btn_FineView});

        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_FineCount)
                        .addComponent(txt_FineCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_FineCancel)
                        .addComponent(btn_FinePrint)
                        .addComponent(btn_FineRefresh)
                        .addComponent(btn_FineDelete)
                        .addComponent(btn_FineView)))
                .addContainerGap())
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_FineCancel, btn_FineDelete, btn_FinePrint, btn_FineRefresh, btn_FineView, lbl_FineCount, txt_FineCount});

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 874, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 418, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Fees Amount", jPanel5);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        lbl_FineAmountSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_FineAmountSearch.setText("Search By Route :");
        lbl_FineAmountSearch.setPreferredSize(new java.awt.Dimension(105, 14));

        btn_FineAmountSearch.setText("Search");
        btn_FineAmountSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FineAmountSearchActionPerformed(evt);
            }
        });

        btn_FineAmountNewEntry.setText("New Entry");
        btn_FineAmountNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FineAmountNewEntryActionPerformed(evt);
            }
        });

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_FineAmountSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_FineAmountSearch2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_FineAmountSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_FineAmountNewEntry)
                .addContainerGap())
        );

        jPanel13Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FineAmountNewEntry, btn_FineAmountSearch});

        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_FineAmountSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_FineAmountSearch2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_FineAmountSearch)
                        .addComponent(btn_FineAmountNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel13Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_FineAmountSearch, lbl_FineAmountSearch, txt_FineAmountSearch2});

        tbl_SubmissionDate.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fees_submissionDate_id", "S.No.", "Jan_Date", "Feb_Date", "Mar_Date", "Apr_Date", "May_Date", "Jun_Date", "Jul_Date", "Aug_Date", "Sep_Date", "Oct_Date", "Nov_Date", "Dec_Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_SubmissionDate.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_SubmissionDate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_SubmissionDateMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tbl_SubmissionDate);
        if (tbl_SubmissionDate.getColumnModel().getColumnCount() > 0) {
            tbl_SubmissionDate.getColumnModel().getColumn(0).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(1).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(2).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(3).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(4).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(5).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(6).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(7).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(8).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(9).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(10).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(11).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(12).setResizable(false);
            tbl_SubmissionDate.getColumnModel().getColumn(13).setResizable(false);
        }

        btn_FineAmountDelete.setText("Delete");

        btn_FineAmountView.setText("View");
        btn_FineAmountView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FineAmountViewActionPerformed(evt);
            }
        });

        btn_FineAmountRefresh.setText("Refresh");

        btn_FineAmountPrint.setText("Print");

        btn_FineAmountlCancel.setText("Cancel");
        btn_FineAmountlCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FineAmountlCancelActionPerformed(evt);
            }
        });

        lbl_FineAmountCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_FineAmountCount.setText("Total Count :");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                        .addComponent(lbl_FineAmountCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_FineAmountCount, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 310, Short.MAX_VALUE)
                        .addComponent(btn_FineAmountView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FineAmountDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FineAmountRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FineAmountPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_FineAmountlCancel)))
                .addContainerGap())
        );

        jPanel12Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FineAmountDelete, btn_FineAmountPrint, btn_FineAmountRefresh, btn_FineAmountView, btn_FineAmountlCancel});

        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_FineAmountCount)
                        .addComponent(txt_FineAmountCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_FineAmountlCancel)
                        .addComponent(btn_FineAmountPrint)
                        .addComponent(btn_FineAmountRefresh)
                        .addComponent(btn_FineAmountDelete)
                        .addComponent(btn_FineAmountView)))
                .addContainerGap())
        );

        jPanel12Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_FineAmountView, lbl_FineAmountCount, txt_FineAmountCount});

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 874, Short.MAX_VALUE)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 418, Short.MAX_VALUE)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("tab", jPanel8);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setPreferredSize(new java.awt.Dimension(843, 386));

        lbl_DamageSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_DamageSearch.setText("Search By Route :");
        lbl_DamageSearch.setPreferredSize(new java.awt.Dimension(105, 14));

        btn_DamageSearch.setText("Search");
        btn_DamageSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DamageSearchActionPerformed(evt);
            }
        });

        btn_DamageNewEntry.setText("New Entry");
        btn_DamageNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DamageNewEntryActionPerformed(evt);
            }
        });

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_DamageSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_DamageSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_DamageSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_DamageNewEntry)
                .addContainerGap())
        );

        jPanel15Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_DamageNewEntry, btn_DamageSearch});

        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_DamageSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_DamageSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_DamageSearch)
                        .addComponent(btn_DamageNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel15Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_DamageSearch, lbl_DamageSearch, txt_DamageSearch});

        tbl_Damage.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Damage_id", "S.No.", "Damage Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Damage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_DamageMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(tbl_Damage);
        if (tbl_Damage.getColumnModel().getColumnCount() > 0) {
            tbl_Damage.getColumnModel().getColumn(0).setResizable(false);
            tbl_Damage.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_Damage.getColumnModel().getColumn(1).setResizable(false);
            tbl_Damage.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_Damage.getColumnModel().getColumn(2).setResizable(false);
            tbl_Damage.getColumnModel().getColumn(2).setPreferredWidth(200);
        }

        btn_DamageDelete.setText("Delete");

        btn_DamageView.setText("View");
        btn_DamageView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DamageViewActionPerformed(evt);
            }
        });

        btn_DamageRefresh.setText("Refresh");

        btn_DamagePrint.setText("Print");

        btn_DamageCancel.setText("Cancel");
        btn_DamageCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DamageCancelActionPerformed(evt);
            }
        });

        lbl_DamageCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_DamageCount.setText("Total Count :");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(lbl_DamageCount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_DamageCount, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 310, Short.MAX_VALUE)
                        .addComponent(btn_DamageView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_DamageDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_DamageRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_DamagePrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_DamageCancel)))
                .addContainerGap())
        );

        jPanel14Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_DamageCancel, btn_DamageDelete, btn_DamagePrint, btn_DamageRefresh, btn_DamageView});

        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_DamageCount)
                        .addComponent(txt_DamageCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_DamageCancel)
                        .addComponent(btn_DamagePrint)
                        .addComponent(btn_DamageRefresh)
                        .addComponent(btn_DamageDelete)
                        .addComponent(btn_DamageView)))
                .addContainerGap())
        );

        jPanel14Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_DamageDelete, btn_DamageView, lbl_DamageCount, txt_DamageCount});

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 874, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 418, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("tab", jPanel9);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
       
    }//GEN-LAST:event_closeDialog

    private void btn_FeeTypeNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FeeTypeNewEntryActionPerformed

        Config.fee_feetype.onloadReset();
        Config.fee_feetype.setVisible(true);
//        Config.fee_typename.onloadReset();
//        Config.fee_typename.setVisible(true);
    }//GEN-LAST:event_btn_FeeTypeNewEntryActionPerformed

    private void btn_FeeAmountNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FeeAmountNewEntryActionPerformed

        Config.fee_feenam.onloadReset();
        Config.fee_feenam.setVisible(true);
        
    }//GEN-LAST:event_btn_FeeAmountNewEntryActionPerformed

    private void txt_FeeTypeSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_FeeTypeSearchCaretUpdate
        // TODO add your handling code here:
        btn_SearchActionPerformed(null);
    }//GEN-LAST:event_txt_FeeTypeSearchCaretUpdate

    private void btn_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SearchActionPerformed
        // TODO add your handling code here:
//        try {
//        tbl_feetypemodel.setRowCount(0);    
//            NodeList nodeList = Config.xml_config_fee_typemgr.doc.getElementsByTagName("config_fee_feename");
//            for (int i = 0; i < nodeList.getLength(); i++) {
//                Node node = nodeList.item(i);
//                Element element = (Element) node;
//                if (!txt_FeeTypeSearch.getText().equals("")) {
//                    if (element.getElementsByTagName("feename").item(0).getTextContent().toUpperCase().startsWith(txt_FeeTypeSearch.getText().toUpperCase())) {
//                        tbl_feetypemodel.addRow(new Object[] {element.getAttribute("fee_id"),
//                            i+1,
//                            element.getElementsByTagName("feename").item(0).getTextContent()});
//                         }
//                   } else{
//                    tbl_feetypemodel.addRow(new Object[] {element.getAttribute("fee_id"),i+1,
//                        element.getElementsByTagName("feename").item(0).getTextContent()});
//                    int a = tbl_FeeType.getRowCount();
//                    txt_FeeTypeTotalCount.setText(Integer.toString(a));
//                }
//              }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }//GEN-LAST:event_btn_SearchActionPerformed
    private void btn_FeeAmountSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FeeAmountSearchActionPerformed

    }//GEN-LAST:event_btn_FeeAmountSearchActionPerformed

    private void btn_FeeTypeViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FeeTypeViewActionPerformed
        // TODO add your handling code here:
        
        Config.fee_viewfeetype.onloadReset(tbl_feetypemodel.getValueAt(tbl_FeeType.getSelectedRow(), 0).toString());
        Config.fee_viewfeetype.setVisible (true);
    }//GEN-LAST:event_btn_FeeTypeViewActionPerformed

    private void btn_FeeNameViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FeeNameViewActionPerformed
        // TODO add your handling code here:
        Config.fee_viewfeenam.onloadReset(tbl_feenamemodel.getValueAt(tbl_FeeName.getSelectedRow(), 0).toString());
        Config.fee_viewfeenam.setVisible(true);
    }//GEN-LAST:event_btn_FeeNameViewActionPerformed

    private void tbl_FeeTypeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_FeeTypeMouseClicked
        // TODO add your handling code here:
        if(evt.getClickCount() == 2){
            btn_FeeTypeViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_FeeTypeMouseClicked

    private void tbl_FeeNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_FeeNameMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {
            btn_FeeNameViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_FeeNameMouseClicked

    private void btn__fineSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn__fineSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn__fineSearchActionPerformed

    private void btn__fineNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn__fineNewEntryActionPerformed
        Config.fee_feeamount.onloadReset();
        Config.fee_feeamount.setVisible(true);
    }//GEN-LAST:event_btn__fineNewEntryActionPerformed

    private void btn_FineViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FineViewActionPerformed
//        Config.fee_view_finetypename.onloadReset(tbl_finetypemodel.getValueAt(tbl_FineType.getSelectedRow(), 0).toString());
//        Config.fee_view_finetypename.setVisible(true);
    }//GEN-LAST:event_btn_FineViewActionPerformed

    private void btn_FineAmountSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FineAmountSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_FineAmountSearchActionPerformed

    private void btn_FineAmountNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FineAmountNewEntryActionPerformed
        // TODO add your handling code here:
//        Config.fee_submissiondate.onloadReset();
//        Config.fee_submissiondate.setVisible(true);
    }//GEN-LAST:event_btn_FineAmountNewEntryActionPerformed

    private void tbl_SubmissionDateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_SubmissionDateMouseClicked
        // TODO add your handling code here:
                if (evt.getClickCount() == 2) {
                    btn_FineAmountViewActionPerformed(null);
        }

    }//GEN-LAST:event_tbl_SubmissionDateMouseClicked

    private void btn_FineAmountViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FineAmountViewActionPerformed
        // TODO add your handling code here:
//        Config.fee_view_fineamount.onloadReset(tbl_feesubmissionmodel.getValueAt(tbl_SubmissionDate.getSelectedRow(), 0).toString());
//        Config.fee_view_fineamount.setVisible(true);
    }//GEN-LAST:event_btn_FineAmountViewActionPerformed

    private void btn_DamageSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DamageSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_DamageSearchActionPerformed

    private void btn_DamageNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DamageNewEntryActionPerformed
        // TODO add your handling code here:
//        Config.fee_damagetypename.onloadReset();
//        Config.fee_damagetypename.setVisible(true);
    }//GEN-LAST:event_btn_DamageNewEntryActionPerformed

    private void tbl_DamageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_DamageMouseClicked
        // TODO add your handling code here:
                if (evt.getClickCount() == 2) {
                    btn_DamageViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_DamageMouseClicked

    private void btn_DamageViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DamageViewActionPerformed
        // TODO add your handling code here:
//        Config.fee_View_damagetypename.onloadReset(tbl_damagemodel.getValueAt(tbl_Damage.getSelectedRow(), 0).toString());
//        Config.fee_View_damagetypename.setVisible(true);
    }//GEN-LAST:event_btn_DamageViewActionPerformed

    private void btn_DamageCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DamageCancelActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btn_DamageCancelActionPerformed

    private void btn_FineAmountlCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FineAmountlCancelActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btn_FineAmountlCancelActionPerformed

    private void btn_FineCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FineCancelActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btn_FineCancelActionPerformed

    private void btn_FeeAmountlCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FeeAmountlCancelActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btn_FeeAmountlCancelActionPerformed

    private void btn_FeeTypeCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FeeTypeCancelActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btn_FeeTypeCancelActionPerformed

    private void tbl_FeeAmountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_FeeAmountMouseClicked
        // TODO add your handling code here:
        if(evt.getClickCount() == 2){
            btn_FineViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_FeeAmountMouseClicked
    
  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Fee_Home1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Fee_Home1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Fee_Home1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Fee_Home1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

      
     
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_DamageCancel;
    private javax.swing.JButton btn_DamageDelete;
    private javax.swing.JButton btn_DamageNewEntry;
    private javax.swing.JButton btn_DamagePrint;
    private javax.swing.JButton btn_DamageRefresh;
    private javax.swing.JButton btn_DamageSearch;
    private javax.swing.JButton btn_DamageView;
    private javax.swing.JButton btn_FeeAmountDelete;
    private javax.swing.JButton btn_FeeAmountNewEntry;
    private javax.swing.JButton btn_FeeAmountPrint;
    private javax.swing.JButton btn_FeeAmountRefresh;
    private javax.swing.JButton btn_FeeAmountSearch;
    private javax.swing.JButton btn_FeeAmountlCancel;
    private javax.swing.JButton btn_FeeNameView;
    private javax.swing.JButton btn_FeeTypeCancel;
    private javax.swing.JButton btn_FeeTypeDelete;
    private javax.swing.JButton btn_FeeTypeNewEntry;
    private javax.swing.JButton btn_FeeTypePrint;
    private javax.swing.JButton btn_FeeTypeRefresh;
    private javax.swing.JButton btn_FeeTypeView;
    private javax.swing.JButton btn_FineAmountDelete;
    private javax.swing.JButton btn_FineAmountNewEntry;
    private javax.swing.JButton btn_FineAmountPrint;
    private javax.swing.JButton btn_FineAmountRefresh;
    private javax.swing.JButton btn_FineAmountSearch;
    private javax.swing.JButton btn_FineAmountView;
    private javax.swing.JButton btn_FineAmountlCancel;
    private javax.swing.JButton btn_FineCancel;
    private javax.swing.JButton btn_FineDelete;
    private javax.swing.JButton btn_FinePrint;
    private javax.swing.JButton btn_FineRefresh;
    private javax.swing.JButton btn_FineView;
    private javax.swing.JButton btn_Search;
    private javax.swing.JButton btn__fineNewEntry;
    private javax.swing.JButton btn__fineSearch;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbl_DamageCount;
    private javax.swing.JLabel lbl_DamageSearch;
    private javax.swing.JLabel lbl_FeeAmountCount;
    private javax.swing.JLabel lbl_FeeAmountSearch;
    private javax.swing.JLabel lbl_FeeTypeTotalCount;
    private javax.swing.JLabel lbl_FineAmountCount;
    private javax.swing.JLabel lbl_FineAmountSearch;
    private javax.swing.JLabel lbl_FineCount;
    private javax.swing.JLabel lbl_SearchFeeType;
    private javax.swing.JLabel lbl_fineSearch;
    private javax.swing.JTable tbl_Damage;
    private javax.swing.JTable tbl_FeeAmount;
    private javax.swing.JTable tbl_FeeName;
    private javax.swing.JTable tbl_FeeType;
    private javax.swing.JTable tbl_SubmissionDate;
    private javax.swing.JTextField txt_DamageCount;
    private javax.swing.JTextField txt_DamageSearch;
    private javax.swing.JTextField txt_FeeAmountCount;
    private javax.swing.JTextField txt_FeeAmountSearch;
    private javax.swing.JTextField txt_FeeTypeSearch;
    private javax.swing.JTextField txt_FeeTypeTotalCount;
    private javax.swing.JTextField txt_FineAmountCount;
    private javax.swing.JTextField txt_FineAmountSearch2;
    private javax.swing.JTextField txt_FineCount;
    private javax.swing.JTextField txt__fineSearch;
    // End of variables declaration//GEN-END:variables

    public void onloadReset(){
        tbl_feetypemodel.setRowCount(0);       
            NodeList nList = Config.xml_config_fee_feetypemgr.doc.getElementsByTagName("config_fee_feetype");
            for (int i = 0; i < nList.getLength(); i++) {
                
                Node nNode = nList.item(i);
                if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element)nNode;
                    tbl_feetypemodel.addRow(new Object[] {
                        eElement.getAttribute("feetype_id"),
                        i+1,
                        eElement.getElementsByTagName("typename").item(0).getTextContent()
                    });                                     
                }
            }
            
        tbl_feenamemodel.setRowCount(0);
            NodeList nodeList2 = Config.xml_config_fee_feenammgr.doc.getElementsByTagName("config_fee_feenam");

            for (int i = 0; i < nodeList2.getLength(); i++) {
                Node node2 = nodeList2.item(i);
                if (node2.getNodeType() == Node.ELEMENT_NODE) {
                    Element element2 = (Element)node2;
                    try {

                        String feetypeid = element2.getElementsByTagName("feetype_id").item(0).getTextContent();
                        
                        Element element3 = (Element)  Config.xml_config_fee_feetypemgr.xpath.evaluate("//*[@feetype_id='"+feetypeid+"']", Config.xml_config_fee_feetypemgr.doc, XPathConstants.NODE);            
                        String feetype = element3.getElementsByTagName("typename").item(0).getTextContent();
                        
                   
                        tbl_feenamemodel.addRow(new Object[]{
                        element2.getAttribute("feename_id"),
                        i+1,
                        feetype,
                        
                        element2.getElementsByTagName("feenmae").item(0).getTextContent()

                        });
                        } catch (Exception e) {
                        e.printStackTrace();
                    }
        
           
                }
            }
            
            tbl_feeamountmodel.setRowCount(0);
            NodeList nodeList = Config.xml_config_fee_feeamountmgr.doc.getElementsByTagName("config_fee_feeamount");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nod = nodeList.item(i);
                if (nod.getNodeType() == Node.ELEMENT_NODE) {
                    Element eleme = (Element)nod;
                    try {
                        
                        String sessionid = eleme.getElementsByTagName("session_id").item(0).getTextContent();
                        Element element3 = (Element)  Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@session_id='"+sessionid+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);            
                        String sessionname = element3.getElementsByTagName("session_name").item(0).getTextContent();
                        
                        
                        String classid = eleme.getElementsByTagName("class_id").item(0).getTextContent();
                        Element element4 = (Element)  Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);            
                        String classname = element4.getElementsByTagName("class_name").item(0).getTextContent();
                        

                        String feetypeid = eleme.getElementsByTagName("feetype_id").item(0).getTextContent();
                        Element element5 = (Element)  Config.xml_config_fee_feetypemgr.xpath.evaluate("//*[@feetype_id='"+feetypeid+"']", Config.xml_config_fee_feetypemgr.doc, XPathConstants.NODE);            
                        String feetype = element5.getElementsByTagName("typename").item(0).getTextContent();
                        
                        
                        String feenamid = eleme.getElementsByTagName("feename_id").item(0).getTextContent();
                        Element element6 = (Element)  Config.xml_config_fee_feenammgr.xpath.evaluate("//*[@feename_id='"+feenamid+"']", Config.xml_config_fee_feenammgr.doc, XPathConstants.NODE);            
                        String feenam = element6.getElementsByTagName("feenmae").item(0).getTextContent();
                        
                   
                        tbl_feeamountmodel.addRow(new Object[]{
                            eleme.getAttribute("feeamount_id"), 
                            i+1, 
                            sessionname, 
                            classname, 
                            feetype, 
                            feenam, 
                            eleme.getElementsByTagName("amount").item(0).getTextContent()

                        });
                        } catch (Exception e) {
                        e.printStackTrace();
                    }
        
           
                }
            }
    }
}
