package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_SchoolCategory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Admin_ViewSchoolCategory extends javax.swing.JDialog {

    
    public Admin_ViewSchoolCategory(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

   
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btn_scUpdate = new javax.swing.JButton();
        btn_scDelete = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_ViewSchoolCategoryName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();
        lbl_scId = new javax.swing.JLabel();
        btn_Cancel = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_scUpdate.setText("Update");
        btn_scUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_scUpdateActionPerformed(evt);
            }
        });

        btn_scDelete.setText("Delete");
        btn_scDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_scDeleteActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "School Category", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Category Name :");

        jLabel4.setText("Session :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ViewSchoolCategoryName, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Session, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel4});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_ViewSchoolCategoryName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel4, txt_ViewSchoolCategoryName});

        lbl_scId.setForeground(new java.awt.Color(240, 240, 240));

        btn_Cancel.setText("Cancel");
        btn_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbl_scId, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                        .addGap(23, 23, 23)
                        .addComponent(btn_scUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_scDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Cancel))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Cancel, btn_scDelete, btn_scUpdate});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_scDelete)
                    .addComponent(btn_scUpdate)
                    .addComponent(lbl_scId)
                    .addComponent(btn_Cancel))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_scUpdate, lbl_scId});

        getRootPane().setDefaultButton(btn_scUpdate);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_scUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_scUpdateActionPerformed
          try {
            String str = checkValidity();
            if (str.equals("ok")){
                int sessionindex = cmb_Session.getSelectedIndex()+1; 
                String sessionid = null;
                Config_Ad_Dm_SchoolCategory schoolcat = new Config_Ad_Dm_SchoolCategory();
                   Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno="+sessionindex+"]", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
                   sessionid = element.getAttribute("session_id");
                   schoolcat.setCategory_id(lbl_scId.getText());
                   schoolcat.setSession_id(sessionid);
                   schoolcat.setCategory_name(txt_ViewSchoolCategoryName.getText());
                    if(Config.config_ad_cm_schoolcategorymgr.updAd_SchoolCategory(schoolcat)){                
                        JOptionPane.showMessageDialog(this, "Record updated Successfully", "Successfull",JOptionPane.NO_OPTION);
                        dispose();
                    }else{
                        JOptionPane.showMessageDialog(this, "Error in updation", "Error",JOptionPane.ERROR_MESSAGE );
                    }
            }else{            
                JOptionPane.showMessageDialog(this, str, "Error",JOptionPane.ERROR_MESSAGE );
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }  
        
    }//GEN-LAST:event_btn_scUpdateActionPerformed

    private void btn_scDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_scDeleteActionPerformed
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure You want to Delete School Category?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {
             
                if(Config.config_ad_cm_schoolcategorymgr.delAd_SchoolCategory(lbl_scId.getText())){ 

                JOptionPane.showMessageDialog(this, "School Category Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                dispose();
            
                 }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } else{
                   dispose();     
                   }
    }//GEN-LAST:event_btn_scDeleteActionPerformed

  
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
     
    }//GEN-LAST:event_closeDialog

    private void btn_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CancelActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btn_CancelActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Cancel;
    private javax.swing.JButton btn_scDelete;
    private javax.swing.JButton btn_scUpdate;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lbl_scId;
    private javax.swing.JTextField txt_ViewSchoolCategoryName;
    // End of variables declaration//GEN-END:variables

    void onloadReset(String categoryid) {
     try {
            lbl_scId.setText(categoryid);
//            Element element1  = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate(feeid, null);
             NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
             cmb_Session.removeAllItems();
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                }
             }
            Element element = (Element) Config.xml_config_ad_schoolcategorymgr.xpath.evaluate("//*[@category_id='"+categoryid+"']", Config.xml_config_ad_schoolcategorymgr.doc, XPathConstants.NODE);
            
            String sessionid = element.getElementsByTagName("session_id").item(0).getTextContent();
            Element element1 = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@session_id='"+sessionid+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);
            String sessionname = element1.getElementsByTagName("session_name").item(0).getTextContent();
            cmb_Session.setSelectedItem(sessionname);
            
            txt_ViewSchoolCategoryName.setText(element.getElementsByTagName("category_name").item(0).getTextContent());
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error",JOptionPane.ERROR_MESSAGE );
        }
 }    

    

    private String checkValidity() {
         if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(txt_ViewSchoolCategoryName.getText().equals("")){
            return (" 'Category Name' should not be blank.");
            
        }else{
        return ("ok");
        }
    }
}
