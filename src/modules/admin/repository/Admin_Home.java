package modules.admin.repository;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Admin_Home extends javax.swing.JDialog {
   DefaultTableModel tbl_SessionModel;
   DefaultTableModel tbl_SchoolCategoryModel;
   DefaultTableModel tbl_ClassModel;
   DefaultTableModel tbl_SectionModel;
   DefaultTableModel tbl_SubjectModel;
   DefaultTableModel tbl_ElectiveGroupModel;
   DefaultTableModel tbl_ElectiveSubjectModel;
        public Admin_Home(java.awt.Frame parent, boolean modal) {                       
        super(parent, modal);
        initComponents();
        Dimension scrnsize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Double(scrnsize.getWidth()).intValue()-150, new Double(scrnsize.getHeight()).intValue()-100);
        setLocationRelativeTo(null);        
        this.setLocationRelativeTo(null);
         tbl_SessionModel = (DefaultTableModel) tbl_Session.getModel();
         tbl_SchoolCategoryModel=(DefaultTableModel) tbl_SchoolCategory.getModel();
         tbl_ClassModel=(DefaultTableModel) tbl_Class.getModel();
         tbl_SectionModel=(DefaultTableModel) tbl_Section.getModel();
         tbl_SubjectModel=(DefaultTableModel) tbl_subject.getModel();
         tbl_ElectiveGroupModel=(DefaultTableModel)tbl_ElectiveName.getModel();
         tbl_ElectiveSubjectModel=(DefaultTableModel) tbl_ElectiveSubject.getModel();         
         String cancelName = "cancel";
         InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
         inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
         ActionMap actionMap = getRootPane().getActionMap();    
    }  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txt_SessionSearch = new javax.swing.JTextField();
        btn_SessionSearch = new javax.swing.JButton();
        btn_SessionNewEntry = new javax.swing.JButton();
        jSeparator8 = new javax.swing.JSeparator();
        txt_SessionTotalCount = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        btn_SessionView = new javax.swing.JButton();
        btn_SessionDelete = new javax.swing.JButton();
        btn_SessionExport = new javax.swing.JButton();
        btn_SessionCancel = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        tbl_Session = new javax.swing.JTable();
        btn_SessionRefresh = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        txt_ScSearch = new javax.swing.JTextField();
        btn_Sc_Search = new javax.swing.JButton();
        btn_Sc_NewEntry = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JSeparator();
        btn_Sc_View = new javax.swing.JButton();
        btn_Sc_Delete = new javax.swing.JButton();
        btn_ScExport = new javax.swing.JButton();
        btn_Sc_Cancel = new javax.swing.JButton();
        txt_Sc_TotalCount = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tbl_SchoolCategory = new javax.swing.JTable();
        btn_Sc_Refresh = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jSplitPane3 = new javax.swing.JSplitPane();
        jPanel8 = new javax.swing.JPanel();
        txt_ClassSearch = new javax.swing.JTextField();
        btn_ClassSearch = new javax.swing.JButton();
        jSeparator10 = new javax.swing.JSeparator();
        btn_ClassNewEntry = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        btn_ClassDelete = new javax.swing.JButton();
        btn_ClassView = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        txt_ClassTotalCount = new javax.swing.JTextField();
        jScrollPane11 = new javax.swing.JScrollPane();
        tbl_Class = new javax.swing.JTable();
        btn_ClassExport = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        tbl_Section = new javax.swing.JTable();
        btn_SectionSearch = new javax.swing.JButton();
        btn_SectionNewEntry = new javax.swing.JButton();
        txt_SectionSearch = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        txt_SectionTotalCount = new javax.swing.JTextField();
        btn_SectionView = new javax.swing.JButton();
        btn_SectionDelete = new javax.swing.JButton();
        jSeparator11 = new javax.swing.JSeparator();
        jLabel26 = new javax.swing.JLabel();
        cmb_Class = new javax.swing.JComboBox();
        btn_SectionExport = new javax.swing.JButton();
        btn_ClassSectionRefresh = new javax.swing.JButton();
        btn_ClassSectionCancel = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel21 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txt_SubjectSearch = new javax.swing.JTextField();
        btn_SubjectSearch = new javax.swing.JButton();
        btn_SubjectNewEntry = new javax.swing.JButton();
        jSeparator12 = new javax.swing.JSeparator();
        jScrollPane13 = new javax.swing.JScrollPane();
        tbl_subject = new javax.swing.JTable();
        btn_SubjectCancel = new javax.swing.JButton();
        btn_SubjectExport = new javax.swing.JButton();
        btn_SubjectRefresh = new javax.swing.JButton();
        btn_SubjectDelete = new javax.swing.JButton();
        btn_SubjectView = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txt_SubjectTotalCount = new javax.swing.JTextField();
        jPanel23 = new javax.swing.JPanel();
        btn_ElectiveRefresh = new javax.swing.JButton();
        btn_ElectiveCancel = new javax.swing.JButton();
        jSplitPane4 = new javax.swing.JSplitPane();
        jPanel24 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        txt_EletiveNameSearch = new javax.swing.JTextField();
        btn_ElectiveSearch = new javax.swing.JButton();
        jSeparator13 = new javax.swing.JSeparator();
        btn_ElectiveNameNewEntry = new javax.swing.JButton();
        jScrollPane14 = new javax.swing.JScrollPane();
        tbl_ElectiveName = new javax.swing.JTable();
        btn_ElectiveNameView = new javax.swing.JButton();
        btn_ElectiveNameDelete = new javax.swing.JButton();
        txt_ElectiveGroupTotalCount = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        btn_EGName = new javax.swing.JButton();
        jPanel25 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        txt_ElectiveSubjectNameSearch = new javax.swing.JTextField();
        btn_ElectiveSubjectSearch = new javax.swing.JButton();
        jSeparator14 = new javax.swing.JSeparator();
        btn_ElectiveSubjectNewEntry = new javax.swing.JButton();
        jScrollPane15 = new javax.swing.JScrollPane();
        tbl_ElectiveSubject = new javax.swing.JTable();
        jLabel30 = new javax.swing.JLabel();
        txt_ElectiveSubjectTotalCount = new javax.swing.JTextField();
        btn_ElectiveSubjectView = new javax.swing.JButton();
        btn_ElectiveSubjectDelete = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        cmb_ElectiveGroupName = new javax.swing.JComboBox();
        btn_ESubName = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Data Repository");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Session Name :");

        txt_SessionSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_SessionSearchCaretUpdate(evt);
            }
        });

        btn_SessionSearch.setText("Search");
        btn_SessionSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SessionSearchActionPerformed(evt);
            }
        });

        btn_SessionNewEntry.setText("New Entry");
        btn_SessionNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SessionNewEntryActionPerformed(evt);
            }
        });

        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_SessionSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_SessionSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_SessionNewEntry)
                .addContainerGap())
        );

        jPanel17Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SessionNewEntry, btn_SessionSearch});

        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_SessionSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SessionSearch)
                        .addComponent(btn_SessionNewEntry)))
                .addGap(11, 11, 11))
        );

        jPanel17Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_SessionNewEntry, btn_SessionSearch, jLabel16, txt_SessionSearch});

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel18.setText("Total Count :");

        btn_SessionView.setText("View");
        btn_SessionView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SessionViewActionPerformed(evt);
            }
        });

        btn_SessionDelete.setText("Delete");
        btn_SessionDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SessionDeleteActionPerformed(evt);
            }
        });

        btn_SessionExport.setText("Export");
        btn_SessionExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SessionExportActionPerformed(evt);
            }
        });

        btn_SessionCancel.setText("Cancel");

        tbl_Session.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Session Id", "S.No.", "Session Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Session.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_Session.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_SessionMouseClicked(evt);
            }
        });
        jScrollPane9.setViewportView(tbl_Session);
        if (tbl_Session.getColumnModel().getColumnCount() > 0) {
            tbl_Session.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_Session.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_Session.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_Session.getColumnModel().getColumn(1).setResizable(false);
            tbl_Session.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_Session.getColumnModel().getColumn(2).setResizable(false);
            tbl_Session.getColumnModel().getColumn(2).setPreferredWidth(800);
        }

        btn_SessionRefresh.setText("Refresh");
        btn_SessionRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SessionRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 859, Short.MAX_VALUE)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_SessionTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_SessionRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SessionView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SessionDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SessionExport)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SessionCancel)))
                .addContainerGap())
        );

        jPanel16Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SessionCancel, btn_SessionDelete, btn_SessionExport, btn_SessionRefresh, btn_SessionView});

        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_SessionCancel)
                    .addComponent(btn_SessionExport)
                    .addComponent(btn_SessionDelete)
                    .addComponent(btn_SessionView)
                    .addComponent(txt_SessionTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(btn_SessionRefresh))
                .addContainerGap())
        );

        jPanel16Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_SessionView, jLabel18, txt_SessionTotalCount});

        jTabbedPane1.addTab("Session", jPanel16);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setText("School Category Name :");

        txt_ScSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_ScSearchCaretUpdate(evt);
            }
        });

        btn_Sc_Search.setText("Search");
        btn_Sc_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sc_SearchActionPerformed(evt);
            }
        });

        btn_Sc_NewEntry.setText("New Entry");
        btn_Sc_NewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sc_NewEntryActionPerformed(evt);
            }
        });

        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_ScSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 536, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_Sc_Search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_Sc_NewEntry)
                .addContainerGap())
        );

        jPanel19Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Sc_NewEntry, btn_Sc_Search});

        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addComponent(btn_Sc_NewEntry, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel19Layout.createSequentialGroup()
                        .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_ScSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_Sc_Search)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        btn_Sc_View.setText("View");
        btn_Sc_View.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sc_ViewActionPerformed(evt);
            }
        });

        btn_Sc_Delete.setText("Delete");
        btn_Sc_Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sc_DeleteActionPerformed(evt);
            }
        });

        btn_ScExport.setText("Export");
        btn_ScExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ScExportActionPerformed(evt);
            }
        });

        btn_Sc_Cancel.setText("Cancel");
        btn_Sc_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sc_CancelActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel21.setText("Total Count :");

        tbl_SchoolCategory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Category Id", "S.No.", "Category Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_SchoolCategory.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_SchoolCategory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_SchoolCategoryMouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(tbl_SchoolCategory);
        if (tbl_SchoolCategory.getColumnModel().getColumnCount() > 0) {
            tbl_SchoolCategory.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_SchoolCategory.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_SchoolCategory.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_SchoolCategory.getColumnModel().getColumn(1).setResizable(false);
            tbl_SchoolCategory.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_SchoolCategory.getColumnModel().getColumn(2).setResizable(false);
            tbl_SchoolCategory.getColumnModel().getColumn(2).setPreferredWidth(800);
        }

        btn_Sc_Refresh.setText("Refresh");
        btn_Sc_Refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Sc_RefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_Sc_TotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_Sc_Refresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Sc_View)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Sc_Delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ScExport)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Sc_Cancel)))
                .addContainerGap())
        );

        jPanel15Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ScExport, btn_Sc_Cancel, btn_Sc_Delete, btn_Sc_Refresh, btn_Sc_View});

        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_Sc_Cancel)
                    .addComponent(btn_ScExport)
                    .addComponent(btn_Sc_Delete)
                    .addComponent(btn_Sc_View)
                    .addComponent(jLabel21)
                    .addComponent(txt_Sc_TotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_Sc_Refresh))
                .addContainerGap())
        );

        jPanel15Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel21, txt_Sc_TotalCount});

        jTabbedPane1.addTab("School Category", jPanel15);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jSplitPane3.setBorder(null);
        jSplitPane3.setDividerLocation(420);
        jSplitPane3.setDividerSize(1);
        jSplitPane3.setPreferredSize(new java.awt.Dimension(806, 335));

        txt_ClassSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_ClassSearchCaretUpdate(evt);
            }
        });

        btn_ClassSearch.setText("Search");
        btn_ClassSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassSearchActionPerformed(evt);
            }
        });

        jSeparator10.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_ClassNewEntry.setText("New Entry");
        btn_ClassNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassNewEntryActionPerformed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel22.setText("Class Name :");

        btn_ClassDelete.setText("Delete");
        btn_ClassDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassDeleteActionPerformed(evt);
            }
        });

        btn_ClassView.setText("View");
        btn_ClassView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassViewActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Total Count :");

        tbl_Class.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Class Id", "S.No.", "Class Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Class.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_Class.getTableHeader().setReorderingAllowed(false);
        tbl_Class.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_ClassMouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(tbl_Class);
        if (tbl_Class.getColumnModel().getColumnCount() > 0) {
            tbl_Class.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_Class.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_Class.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_Class.getColumnModel().getColumn(0).setHeaderValue("Class Id");
            tbl_Class.getColumnModel().getColumn(1).setResizable(false);
            tbl_Class.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_Class.getColumnModel().getColumn(1).setHeaderValue("S.No.");
            tbl_Class.getColumnModel().getColumn(2).setResizable(false);
            tbl_Class.getColumnModel().getColumn(2).setPreferredWidth(200);
            tbl_Class.getColumnModel().getColumn(2).setHeaderValue("Class Name");
        }

        btn_ClassExport.setText("Export");
        btn_ClassExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassExportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ClassSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ClassSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ClassNewEntry))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ClassTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_ClassView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ClassDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ClassExport)))
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ClassNewEntry, btn_ClassSearch});

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ClassDelete, btn_ClassExport, btn_ClassView});

        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txt_ClassSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel22))
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_ClassNewEntry)
                        .addComponent(btn_ClassSearch)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ClassDelete)
                    .addComponent(btn_ClassView)
                    .addComponent(jLabel23)
                    .addComponent(txt_ClassTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_ClassExport))
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_ClassSearch, jLabel22, txt_ClassSearch});

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_ClassView, jLabel23, txt_ClassTotalCount});

        jSplitPane3.setLeftComponent(jPanel8);

        tbl_Section.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Section Id", "S.No.", "Section Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Section.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_Section.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_SectionMouseClicked(evt);
            }
        });
        jScrollPane12.setViewportView(tbl_Section);
        if (tbl_Section.getColumnModel().getColumnCount() > 0) {
            tbl_Section.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_Section.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_Section.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_Section.getColumnModel().getColumn(1).setResizable(false);
            tbl_Section.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_Section.getColumnModel().getColumn(2).setResizable(false);
            tbl_Section.getColumnModel().getColumn(2).setPreferredWidth(800);
        }

        btn_SectionSearch.setText("Search");
        btn_SectionSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SectionSearchActionPerformed(evt);
            }
        });

        btn_SectionNewEntry.setText("New Entry");
        btn_SectionNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SectionNewEntryActionPerformed(evt);
            }
        });

        txt_SectionSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_SectionSearchCaretUpdate(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setText("Section Name :");

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel25.setText("Total Count :");

        btn_SectionView.setText("View");
        btn_SectionView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SectionViewActionPerformed(evt);
            }
        });

        btn_SectionDelete.setText("Delete");
        btn_SectionDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SectionDeleteActionPerformed(evt);
            }
        });

        jSeparator11.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel26.setText("Class :");

        cmb_Class.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_ClassActionPerformed(evt);
            }
        });

        btn_SectionExport.setText("Export");
        btn_SectionExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SectionExportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel20Layout.createSequentialGroup()
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel20Layout.createSequentialGroup()
                                .addComponent(txt_SectionSearch)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_SectionSearch)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_SectionNewEntry))
                            .addComponent(cmb_Class, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel20Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_SectionTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_SectionView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SectionDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SectionExport)))
                .addContainerGap())
        );

        jPanel20Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SectionNewEntry, btn_SectionSearch});

        jPanel20Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SectionDelete, btn_SectionExport, btn_SectionView});

        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(cmb_Class, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel24)
                        .addComponent(txt_SectionSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SectionSearch)
                        .addComponent(btn_SectionNewEntry))
                    .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_SectionTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SectionView)
                        .addComponent(btn_SectionDelete)
                        .addComponent(btn_SectionExport))
                    .addComponent(jLabel25))
                .addContainerGap())
        );

        jPanel20Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_SectionSearch, jLabel24, jLabel26, txt_SectionSearch});

        jPanel20Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_SectionView, jLabel25, txt_SectionTotalCount});

        jSplitPane3.setRightComponent(jPanel20);

        btn_ClassSectionRefresh.setText("Refresh");
        btn_ClassSectionRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassSectionRefreshActionPerformed(evt);
            }
        });

        btn_ClassSectionCancel.setText("Cancel");
        btn_ClassSectionCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassSectionCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_ClassSectionRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ClassSectionCancel))
                    .addComponent(jSplitPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 859, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ClassSectionCancel, btn_ClassSectionRefresh});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ClassSectionCancel)
                    .addComponent(btn_ClassSectionRefresh))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Class/Section", jPanel6);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel21.setBackground(new java.awt.Color(255, 255, 255));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Subject Name :");

        txt_SubjectSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_SubjectSearchCaretUpdate(evt);
            }
        });

        btn_SubjectSearch.setText("Search");
        btn_SubjectSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectSearchActionPerformed(evt);
            }
        });

        btn_SubjectNewEntry.setText("New Entry");
        btn_SubjectNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectNewEntryActionPerformed(evt);
            }
        });

        jSeparator12.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_SubjectSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_SubjectSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_SubjectNewEntry)
                .addContainerGap())
        );

        jPanel22Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SubjectNewEntry, btn_SubjectSearch});

        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_SubjectNewEntry)
                    .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_SubjectSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SubjectSearch))
                    .addComponent(jSeparator12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tbl_subject.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Subject Id", "S.No.", "Subject Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_subject.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_subject.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_subjectMouseClicked(evt);
            }
        });
        jScrollPane13.setViewportView(tbl_subject);
        if (tbl_subject.getColumnModel().getColumnCount() > 0) {
            tbl_subject.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_subject.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_subject.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_subject.getColumnModel().getColumn(1).setResizable(false);
            tbl_subject.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_subject.getColumnModel().getColumn(2).setResizable(false);
            tbl_subject.getColumnModel().getColumn(2).setPreferredWidth(800);
        }

        btn_SubjectCancel.setText("Cancel");
        btn_SubjectCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectCancelActionPerformed(evt);
            }
        });

        btn_SubjectExport.setText("Export");
        btn_SubjectExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectExportActionPerformed(evt);
            }
        });

        btn_SubjectRefresh.setText("Refresh");
        btn_SubjectRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectRefreshActionPerformed(evt);
            }
        });

        btn_SubjectDelete.setText("Delete");
        btn_SubjectDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectDeleteActionPerformed(evt);
            }
        });

        btn_SubjectView.setText("View");
        btn_SubjectView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectViewActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Total Count :");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_SubjectTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_SubjectView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubjectDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubjectRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubjectExport)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubjectCancel)))
                .addContainerGap())
        );

        jPanel21Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SubjectCancel, btn_SubjectDelete, btn_SubjectExport, btn_SubjectRefresh, btn_SubjectView});

        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_SubjectCancel)
                    .addComponent(btn_SubjectExport)
                    .addComponent(btn_SubjectRefresh)
                    .addComponent(btn_SubjectDelete)
                    .addComponent(btn_SubjectView)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_SubjectTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane3.addTab("Subject", jPanel21);

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));

        btn_ElectiveRefresh.setText("Refresh");
        btn_ElectiveRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveRefreshActionPerformed(evt);
            }
        });

        btn_ElectiveCancel.setText("Cancel");
        btn_ElectiveCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveCancelActionPerformed(evt);
            }
        });

        jSplitPane4.setBorder(null);
        jSplitPane4.setDividerLocation(420);
        jSplitPane4.setDividerSize(1);
        jSplitPane4.setPreferredSize(new java.awt.Dimension(781, 296));

        jPanel24.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Elective Group", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setText("Elective Name :");

        txt_EletiveNameSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_EletiveNameSearchCaretUpdate(evt);
            }
        });

        btn_ElectiveSearch.setText("Search");
        btn_ElectiveSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveSearchActionPerformed(evt);
            }
        });

        jSeparator13.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_ElectiveNameNewEntry.setText("New Entry");
        btn_ElectiveNameNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveNameNewEntryActionPerformed(evt);
            }
        });

        tbl_ElectiveName.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Elective Id", "S.No.", "Elective Group Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_ElectiveName.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_ElectiveName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_ElectiveNameMouseClicked(evt);
            }
        });
        jScrollPane14.setViewportView(tbl_ElectiveName);
        if (tbl_ElectiveName.getColumnModel().getColumnCount() > 0) {
            tbl_ElectiveName.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_ElectiveName.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_ElectiveName.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_ElectiveName.getColumnModel().getColumn(1).setResizable(false);
            tbl_ElectiveName.getColumnModel().getColumn(1).setPreferredWidth(10);
            tbl_ElectiveName.getColumnModel().getColumn(2).setResizable(false);
            tbl_ElectiveName.getColumnModel().getColumn(2).setPreferredWidth(250);
        }

        btn_ElectiveNameView.setText("View");
        btn_ElectiveNameView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveNameViewActionPerformed(evt);
            }
        });

        btn_ElectiveNameDelete.setText("Delete");
        btn_ElectiveNameDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveNameDeleteActionPerformed(evt);
            }
        });

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel28.setText("Total Count :");

        btn_EGName.setText("Export");
        btn_EGName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_EGNameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_EletiveNameSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator13, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveNameNewEntry))
                    .addGroup(jPanel24Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ElectiveGroupTotalCount, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveNameView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveNameDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_EGName)))
                .addContainerGap())
        );

        jPanel24Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ElectiveNameNewEntry, btn_ElectiveSearch});

        jPanel24Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_EGName, btn_ElectiveNameDelete, btn_ElectiveNameView});

        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel27)
                        .addComponent(txt_EletiveNameSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_ElectiveSearch)
                        .addComponent(btn_ElectiveNameNewEntry))
                    .addComponent(jSeparator13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ElectiveNameDelete)
                    .addComponent(btn_ElectiveNameView)
                    .addComponent(jLabel28)
                    .addComponent(txt_ElectiveGroupTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_EGName))
                .addContainerGap())
        );

        jSplitPane4.setLeftComponent(jPanel24);

        jPanel25.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Elective Subject", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel29.setText("E Subject Name :");

        txt_ElectiveSubjectNameSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_ElectiveSubjectNameSearchCaretUpdate(evt);
            }
        });

        btn_ElectiveSubjectSearch.setText("Search");
        btn_ElectiveSubjectSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveSubjectSearchActionPerformed(evt);
            }
        });

        jSeparator14.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btn_ElectiveSubjectNewEntry.setText("New Entry");
        btn_ElectiveSubjectNewEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveSubjectNewEntryActionPerformed(evt);
            }
        });

        tbl_ElectiveSubject.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Elective Group Id", "S.No.", "Elective Subject"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_ElectiveSubject.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_ElectiveSubject.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_ElectiveSubjectMouseClicked(evt);
            }
        });
        jScrollPane15.setViewportView(tbl_ElectiveSubject);
        if (tbl_ElectiveSubject.getColumnModel().getColumnCount() > 0) {
            tbl_ElectiveSubject.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_ElectiveSubject.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_ElectiveSubject.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_ElectiveSubject.getColumnModel().getColumn(1).setResizable(false);
            tbl_ElectiveSubject.getColumnModel().getColumn(1).setPreferredWidth(200);
            tbl_ElectiveSubject.getColumnModel().getColumn(2).setResizable(false);
            tbl_ElectiveSubject.getColumnModel().getColumn(2).setPreferredWidth(800);
        }

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel30.setText("Total Count :");

        btn_ElectiveSubjectView.setText("View");
        btn_ElectiveSubjectView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveSubjectViewActionPerformed(evt);
            }
        });

        btn_ElectiveSubjectDelete.setText("Delete");
        btn_ElectiveSubjectDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveSubjectDeleteActionPerformed(evt);
            }
        });

        jLabel31.setText("Elective Group Name :");

        cmb_ElectiveGroupName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_ElectiveGroupNameActionPerformed(evt);
            }
        });

        btn_ESubName.setText("Export");
        btn_ESubName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ESubNameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addGap(18, 18, 18)
                        .addComponent(txt_ElectiveSubjectNameSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveSubjectSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveSubjectNewEntry))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ElectiveSubjectTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_ElectiveSubjectView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveSubjectDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ESubName))
                    .addGroup(jPanel25Layout.createSequentialGroup()
                        .addComponent(jLabel31)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_ElectiveGroupName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel25Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ElectiveSubjectNewEntry, btn_ElectiveSubjectSearch});

        jPanel25Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ESubName, btn_ElectiveSubjectDelete, btn_ElectiveSubjectView});

        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(cmb_ElectiveGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel29)
                        .addComponent(txt_ElectiveSubjectNameSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_ElectiveSubjectSearch)
                        .addComponent(btn_ElectiveSubjectNewEntry)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ElectiveSubjectDelete)
                    .addComponent(btn_ElectiveSubjectView)
                    .addComponent(jLabel30)
                    .addComponent(txt_ElectiveSubjectTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_ESubName))
                .addContainerGap())
        );

        jSplitPane4.setRightComponent(jPanel25);

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_ElectiveRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveCancel))
                    .addComponent(jSplitPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel23Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ElectiveCancel, btn_ElectiveRefresh});

        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ElectiveCancel)
                    .addComponent(btn_ElectiveRefresh))
                .addContainerGap())
        );

        jTabbedPane3.addTab("Elective Group", jPanel23);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane3)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jTabbedPane3)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Subject", jPanel4);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        
    }//GEN-LAST:event_closeDialog

    private void btn_SessionNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SessionNewEntryActionPerformed
        Config.ad_sessionnewentry.onloadReset();
        Config.ad_sessionnewentry.setVisible(true);
    }//GEN-LAST:event_btn_SessionNewEntryActionPerformed

    private void btn_SessionViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SessionViewActionPerformed
        // TODO add your handling code here:
        Config.ad_viewsession.onlodeReset(tbl_SessionModel.getValueAt(tbl_Session.getSelectedRow(), 0).toString());

        Config.ad_viewsession.setVisible(true);
    }//GEN-LAST:event_btn_SessionViewActionPerformed

    private void tbl_SessionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_SessionMouseClicked
        // TODO add your handling code here:
        if(evt.getClickCount()==2){
            btn_SessionViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_SessionMouseClicked

    private void txt_ScSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_ScSearchCaretUpdate
        btn_Sc_SearchActionPerformed(null);
    }//GEN-LAST:event_txt_ScSearchCaretUpdate

    private void btn_Sc_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sc_SearchActionPerformed
        try {
             tbl_SchoolCategoryModel.setRowCount(0);
             NodeList nList2 = Config.xml_config_ad_schoolcategorymgr.doc.getElementsByTagName("config_ad_schoolcategory");
             int ctr1 = 0;
             for (int i = 0; i < nList2.getLength(); i++) {
                 Node nNode2 = nList2.item(i);
                 Element eElement2 = (Element) nNode2;
                 if(!txt_ScSearch.getText().trim().equals("")){
                     if (eElement2.getElementsByTagName("category_name").item(0).getTextContent().toUpperCase().startsWith(txt_ScSearch.getText().toUpperCase())) {
                        ctr1++;
                        tbl_SchoolCategoryModel.addRow(new Object[]{
                            eElement2.getAttribute("category_id"),
                            ctr1,
                            eElement2.getElementsByTagName("category_name").item(0).getTextContent()
                        });
                     }
                 }else{tbl_SchoolCategoryModel.addRow(new  Object[]{
                     eElement2.getAttribute("category_id"),
                     i+1,
                     eElement2.getElementsByTagName("category_name").item(0).getTextContent()
                 });
               }
             }
             int c = tbl_SchoolCategoryModel.getRowCount();
             txt_Sc_TotalCount.setText(Integer.toString(c));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_Sc_SearchActionPerformed

    private void btn_Sc_NewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sc_NewEntryActionPerformed
        // TODO add your handling code here:
        Config.ad_scnewentry.onloadReset();
        Config.ad_scnewentry.setVisible(true);
    }//GEN-LAST:event_btn_Sc_NewEntryActionPerformed

    private void btn_Sc_ViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sc_ViewActionPerformed
        Config.ad_viewschoolcategory.onloadReset(tbl_SchoolCategoryModel.getValueAt(tbl_SchoolCategory.getSelectedRow(), 0).toString());
        Config.ad_viewschoolcategory.setVisible(true);
    }//GEN-LAST:event_btn_Sc_ViewActionPerformed

    private void btn_Sc_DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sc_DeleteActionPerformed
        String catid = tbl_SchoolCategoryModel.getValueAt(tbl_SchoolCategory.getSelectedRow(),0).toString();
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete School Category?","Warning",JOptionPane.YES_NO_OPTION);
        if(dialogButton == JOptionPane.YES_OPTION) {
            if(Config.config_ad_cm_schoolcategorymgr.delAd_SchoolCategory(catid)){
                JOptionPane.showMessageDialog(this, "School Category Deleted Successfully", "Success",JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
            }
        }

    }//GEN-LAST:event_btn_Sc_DeleteActionPerformed

    private void btn_ScExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ScExportActionPerformed
         if(evt.getSource() == btn_ScExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Admin_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_SchoolCategory, new File(file));
				}
			}        
    }//GEN-LAST:event_btn_ScExportActionPerformed

    private void btn_Sc_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sc_CancelActionPerformed
            dispose();
    }//GEN-LAST:event_btn_Sc_CancelActionPerformed

    private void tbl_SchoolCategoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_SchoolCategoryMouseClicked
        
        if(evt.getClickCount()==2){
            btn_Sc_ViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_SchoolCategoryMouseClicked

    private void txt_ClassSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_ClassSearchCaretUpdate
        btn_ClassSearchActionPerformed(null);
    }//GEN-LAST:event_txt_ClassSearchCaretUpdate

    private void btn_ClassSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassSearchActionPerformed
        try {
            tbl_ClassModel.setRowCount(0);            
            NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
            int ctr=0;
            for (int i = 0; i < nList.getLength(); i++){
                Node nNode = nList.item(i);
                Element eElement = (Element) nNode;
                if (!txt_ClassSearch.getText().equals("")) {
                    if(eElement.getElementsByTagName("class_name").item(0).getTextContent().toUpperCase().startsWith(txt_ClassSearch.getText().toUpperCase())) {
                        ctr++;
                        tbl_ClassModel.addRow(new Object[] {eElement.getAttribute("class_id"),
                            ctr,
                            eElement.getElementsByTagName("class_name").item(0).getTextContent()       
                        });
                    }
                }else {
                    tbl_ClassModel.addRow(new Object[] {eElement.getAttribute("class_id"),
                        i+1,   
                        eElement.getElementsByTagName("class_name").item(0).getTextContent()
                    });          
                }
            }
            int a=tbl_ClassModel.getRowCount();
            txt_ClassTotalCount.setText(Integer.toString(a));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_ClassSearchActionPerformed

    private void btn_ClassNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassNewEntryActionPerformed
        Config.ad_classnewentry.onloadReset();
        Config.ad_classnewentry.setVisible(true);
    }//GEN-LAST:event_btn_ClassNewEntryActionPerformed

    private void btn_ClassDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassDeleteActionPerformed
        String catid = tbl_ClassModel.getValueAt(tbl_Class.getSelectedRow(),0).toString();
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Class?","Warning",JOptionPane.YES_NO_OPTION);             
        if(dialogButton == JOptionPane.YES_OPTION) {             
            if(Config.config_ad_cm_classmgr.delAd_Class(catid)){ 
                JOptionPane.showMessageDialog(this, "Class  Deleted Successfully", "Success",JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Class Deletion", "Error",JOptionPane.ERROR_MESSAGE );
            }
        }    
    }//GEN-LAST:event_btn_ClassDeleteActionPerformed

    private void btn_ClassViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassViewActionPerformed
       Config.ad_viewclass.onloadReset(tbl_ClassModel.getValueAt(tbl_Class.getSelectedRow(), 0).toString());
        Config.ad_viewclass.setVisible(true);
    }//GEN-LAST:event_btn_ClassViewActionPerformed

    private void tbl_ClassMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_ClassMouseClicked
        if(evt.getClickCount()==2){
            btn_ClassViewActionPerformed(null);
     }
    }//GEN-LAST:event_tbl_ClassMouseClicked

    private void tbl_SectionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_SectionMouseClicked
        if(evt.getClickCount()==2){
            btn_SectionViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_SectionMouseClicked

    private void btn_SectionSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SectionSearchActionPerformed
        tbl_SectionModel.setRowCount(0);        
                try {
                    if(cmb_Class.getSelectedIndex() !=0){
                        Element element = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno='"+cmb_Class.getSelectedIndex()+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE); 
                        String classid = element.getAttribute("class_id");                                
                        NodeList nList = Config.xml_config_ad_classsectionmgr.doc.getElementsByTagName("config_ad_class_section");
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node nNode = nList.item(i);                
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element eElement = (Element) nNode;
                                if (eElement.getElementsByTagName("class_id").item(0).getTextContent().equals(classid)) {
                                    if(eElement.getElementsByTagName("section_name").item(0).getTextContent().toUpperCase().startsWith(txt_SectionSearch.getText().toUpperCase()))
                                    tbl_SectionModel.addRow(new Object[] {eElement.getAttribute("section_id"),i+1,eElement.getElementsByTagName("section_name").item(0).getTextContent()});

                                }                    
                            }
                        }
                    }else if(cmb_Class.getSelectedItem().equals("- Select -")){

                        NodeList nList = Config.xml_config_ad_classsectionmgr.doc.getElementsByTagName("config_ad_class_section");
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node nNode = nList.item(i);                
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element eElement = (Element) nNode;                         
                                    if(eElement.getElementsByTagName("section_name").item(0).getTextContent().toUpperCase().startsWith(txt_SectionSearch.getText().toUpperCase())){
                                    tbl_SectionModel.addRow(new Object[] {eElement.getAttribute("section_id"),i+1,eElement.getElementsByTagName("section_name").item(0).getTextContent()});
                                    }     
                            }
                        }
                    } else  {}  
                    int a=tbl_SectionModel.getRowCount();
                    txt_SectionTotalCount.setText(Integer.toString(a));
                }catch (Exception e) {
                    e.printStackTrace();
                }      
    }//GEN-LAST:event_btn_SectionSearchActionPerformed

    private void btn_SectionNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SectionNewEntryActionPerformed
        Config.ad_sectionnewentry.onloadReset();
        Config.ad_sectionnewentry.setVisible(true);
    }//GEN-LAST:event_btn_SectionNewEntryActionPerformed

    private void txt_SectionSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_SectionSearchCaretUpdate
        btn_SectionSearchActionPerformed(null);
    }//GEN-LAST:event_txt_SectionSearchCaretUpdate

    private void btn_SectionViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SectionViewActionPerformed
         Config.ad_viewsection.onloadReset(tbl_SectionModel.getValueAt(tbl_Section.getSelectedRow(), 0).toString());
         Config.ad_viewsection.setVisible(true);
    }//GEN-LAST:event_btn_SectionViewActionPerformed

    private void btn_SectionDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SectionDeleteActionPerformed
    String catid = tbl_SectionModel.getValueAt(tbl_Section.getSelectedRow(),0).toString();
    int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Section?","Warning",JOptionPane.YES_NO_OPTION);             
    if(dialogButton == JOptionPane.YES_OPTION) {             
        if(Config.config_ad_cm_class_sectionmgr.delAd_Class_Section(catid)){ 
            JOptionPane.showMessageDialog(this, "Section  Deleted Successfully", "Success",JOptionPane.NO_OPTION);
        }else{
            JOptionPane.showMessageDialog(this, "Error in Section Deletion", "Error",JOptionPane.ERROR_MESSAGE );
        }
    } 
    }//GEN-LAST:event_btn_SectionDeleteActionPerformed

    private void cmb_ClassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_ClassActionPerformed
        txt_SectionSearch.setText(null);
        tbl_SectionModel.setRowCount(0);
        String catid = null;
        try {   
            if (cmb_Class.getSelectedIndex() != 0) { 
                try {
                    Element element = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno='"+cmb_Class.getSelectedIndex()+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                    catid = element.getAttribute("class_id");   
                } catch (Exception e) {
                }
                                             
                NodeList nList = Config.xml_config_ad_classsectionmgr.doc.getElementsByTagName("config_ad_class_section");                
                int ctr=0;
                                         
                for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                Element eElement = (Element) nNode;                    
                    if (eElement.getElementsByTagName("class_id").item(0).getTextContent().equals(catid)) {  
//                        if (!txt_SectionSearch.getText().equals("")) {                                 
                            ctr++;
                            tbl_SectionModel.addRow(new Object[] {eElement.getAttribute("section_id"),
                            ctr,
                            eElement.getElementsByTagName("section_name").item(0).getTextContent()});
    //                            btn_SectionSearchActionPerformed(null);
//                        }else{
//                            
//                        }
                    }
                }     
                        
            }else{   
            NodeList nList = Config.xml_config_ad_classsectionmgr.doc.getElementsByTagName("config_ad_class_section");
            int ctr=0;
            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                Element eElement = (Element) nNode;                           
                ctr++;
                tbl_SectionModel.addRow(new Object[] {eElement.getAttribute("section_id"),
                ctr,
                eElement.getElementsByTagName("section_name").item(0).getTextContent()});
                                                                   
            } 
            
        }
            int a=tbl_SectionModel.getRowCount();
            txt_SectionTotalCount.setText(Integer.toString(a));  
            
                          
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_cmb_ClassActionPerformed

    private void btn_ClassSectionRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassSectionRefreshActionPerformed
        Config.ad_home.onloadReset();
    }//GEN-LAST:event_btn_ClassSectionRefreshActionPerformed

    private void btn_ClassSectionCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassSectionCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_ClassSectionCancelActionPerformed

    private void txt_SubjectSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_SubjectSearchCaretUpdate
        btn_SubjectSearchActionPerformed(null);
    }//GEN-LAST:event_txt_SubjectSearchCaretUpdate

    private void btn_SubjectSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectSearchActionPerformed
    try {
        tbl_SubjectModel.setRowCount(0);            
        NodeList nList = Config.xml_config_ad_subjectmgr.doc.getElementsByTagName("config_ad_subject");
        for (int i = 0; i < nList.getLength(); i++){
            Node nNode = nList.item(i);
            Element eElement = (Element) nNode;
            if (!txt_SubjectSearch.getText().equals("")) {
                if(eElement.getElementsByTagName("subject_name").item(0).getTextContent().toUpperCase().startsWith(txt_SubjectSearch.getText().toUpperCase())) {
                    tbl_SubjectModel.addRow(new Object[] {eElement.getAttribute("subject_id"),
                        i+1,
                        eElement.getElementsByTagName("subject_name").item(0).getTextContent()                       
                    });
                }
            }else {
                tbl_SubjectModel.addRow(new Object[] {eElement.getAttribute("subject_id"),
                    i+1,   
                    eElement.getElementsByTagName("subject_name").item(0).getTextContent()
                });          
            }
        }
        int a=tbl_SubjectModel.getRowCount();
        txt_SubjectTotalCount.setText(Integer.toString(a));
    } catch (Exception e) {
        e.printStackTrace();
    }
    }//GEN-LAST:event_btn_SubjectSearchActionPerformed

    private void btn_SubjectNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectNewEntryActionPerformed
        Config.ad_subjectnewentry.onloadReset();
        Config.ad_subjectnewentry.setVisible(true);
    }//GEN-LAST:event_btn_SubjectNewEntryActionPerformed

    private void tbl_subjectMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_subjectMouseClicked
        if(evt.getClickCount()==2){
            btn_SubjectViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_subjectMouseClicked

    private void btn_SubjectCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_SubjectCancelActionPerformed

    private void btn_SubjectRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectRefreshActionPerformed
        Config.ad_home.onloadReset();
    }//GEN-LAST:event_btn_SubjectRefreshActionPerformed

    private void btn_SubjectDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectDeleteActionPerformed
        String subjectid = tbl_SubjectModel.getValueAt(tbl_subject.getSelectedRow(),0).toString();
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Section?","Warning",JOptionPane.YES_NO_OPTION);             
        if(dialogButton == JOptionPane.YES_OPTION) {             
            if(Config.config_ad_cm_subjectmgr.delAd_Subject(subjectid)){ 
                JOptionPane.showMessageDialog(this, "Subject  Deleted Successfully", "Success",JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Section Deletion", "Error",JOptionPane.ERROR_MESSAGE );
            }
        }
    }//GEN-LAST:event_btn_SubjectDeleteActionPerformed

    private void btn_SubjectViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectViewActionPerformed
        Config.ad_viewsubject.onloadReset(tbl_SubjectModel.getValueAt(tbl_subject.getSelectedRow(), 0).toString());
        Config.ad_viewsubject.setVisible(true);
    }//GEN-LAST:event_btn_SubjectViewActionPerformed

    private void btn_ElectiveRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveRefreshActionPerformed
        Config.ad_home.onloadReset();
    }//GEN-LAST:event_btn_ElectiveRefreshActionPerformed

    private void btn_ElectiveCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_ElectiveCancelActionPerformed

    private void txt_EletiveNameSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_EletiveNameSearchCaretUpdate
        btn_ElectiveSearchActionPerformed(null);
    }//GEN-LAST:event_txt_EletiveNameSearchCaretUpdate

    private void btn_ElectiveSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveSearchActionPerformed
        try {
            tbl_ElectiveGroupModel.setRowCount(0);            
            NodeList nList = Config.xml_config_ad_electivegroupmgr.doc.getElementsByTagName("config_ad_electivegroup");
            int ctr=0;
            for (int i = 0; i < nList.getLength(); i++){
                Node nNode = nList.item(i);
                Element eElement = (Element) nNode;
                if (!txt_EletiveNameSearch.getText().equals("")) {
                    if(eElement.getElementsByTagName("electivegroupname").item(0).getTextContent().toUpperCase().startsWith(txt_EletiveNameSearch.getText().toUpperCase())) {
                        ctr++;
                        tbl_ElectiveGroupModel.addRow(new Object[] {eElement.getAttribute("electivegroup_id").toString(),
                            ctr,
                            eElement.getElementsByTagName("electivegroupname").item(0).getTextContent()                       
                        });
                    }
                }else {
                    tbl_ElectiveGroupModel.addRow(new Object[] {eElement.getAttribute("electivegroup_id").toString(),
                        i+1,   
                        eElement.getElementsByTagName("electivegroupname").item(0).getTextContent()
                    });          
                }
            }
            int a=tbl_ElectiveGroupModel.getRowCount();
            txt_ElectiveGroupTotalCount.setText(Integer.toString(a));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_ElectiveSearchActionPerformed

    private void btn_ElectiveNameNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveNameNewEntryActionPerformed
        Config.ad_electivegroup.onloadReset();
        Config.ad_electivegroup.setVisible(true);

    }//GEN-LAST:event_btn_ElectiveNameNewEntryActionPerformed

    private void tbl_ElectiveNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_ElectiveNameMouseClicked
        if(evt.getClickCount()==2){
            btn_ElectiveNameViewActionPerformed(null);}
    }//GEN-LAST:event_tbl_ElectiveNameMouseClicked

    private void btn_ElectiveNameViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveNameViewActionPerformed
        Config.ad_viewelectivegroup.onloadReset(tbl_ElectiveGroupModel.getValueAt(tbl_ElectiveName.getSelectedRow(), 0).toString());
        Config.ad_viewelectivegroup.setVisible(true);
    }//GEN-LAST:event_btn_ElectiveNameViewActionPerformed

    private void btn_ElectiveNameDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveNameDeleteActionPerformed
        String electiveid = tbl_ElectiveGroupModel.getValueAt(tbl_ElectiveName.getSelectedRow(),0).toString();
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Section?","Warning",JOptionPane.YES_NO_OPTION);             
        if(dialogButton == JOptionPane.YES_OPTION) {             
            if(Config.config_ad_cm_electivegroupmgr.delAd_electivegroup(electiveid)){ 
                JOptionPane.showMessageDialog(this, "Elective Group  Deleted Successfully", "Success",JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Section Elective Group", "Error",JOptionPane.ERROR_MESSAGE );
            }
        } 
    }//GEN-LAST:event_btn_ElectiveNameDeleteActionPerformed

    private void btn_ElectiveSubjectSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveSubjectSearchActionPerformed
        tbl_ElectiveSubjectModel.setRowCount(0);        
                try {
                    if(cmb_ElectiveGroupName.getSelectedIndex() !=0){
                        Element element = (Element) Config.xml_config_ad_electivegroupmgr.xpath.evaluate("//*[@sno='"+cmb_ElectiveGroupName.getSelectedIndex()+"']", Config.xml_config_ad_electivegroupmgr.doc, XPathConstants.NODE); 
                        String electivegroupid = element.getAttribute("electivegroup_id");                                
                        NodeList nList = Config.xml_config_ad_electivegroupsubjectmgr.doc.getElementsByTagName("config_ad_electivegroupsubject");
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node nNode = nList.item(i);                
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element eElement = (Element) nNode;
                                if (eElement.getElementsByTagName("electivegroup_id").item(0).getTextContent().equals(electivegroupid)) {
                                    
                                    String subject_id = eElement.getElementsByTagName("subject_id").item(0).getTextContent();
                                    Element ele = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subject_id+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
                                    String subjectname = ele.getElementsByTagName("subject_name").item(0).getTextContent();
                                    
                                    try {
                                        if(!txt_SectionSearch.getText().trim().equals("")){
                                            
                                            if(txt_SectionSearch.getText().toUpperCase().equals(subjectname.toUpperCase())){
                                            
                                                tbl_ElectiveSubjectModel.addRow(new Object[] {
                                                eElement.getAttribute("electivegroupsubject_id"),
                                                i+1,
                                                subjectname

                                                });
                                            }
                                        else{
//                                            tbl_ElectiveSubjectModel.addRow(new Object[] {
//                                            eElement.getAttribute("electivegroupsubject_id"),
//                                            i+1,
//                                            subjectname
//
//                                            });
                                            JOptionPane.showMessageDialog(this, "No Record Found", "Error",JOptionPane.ERROR_MESSAGE);
                                        }
                                    }else{
                                        tbl_ElectiveSubjectModel.addRow(new Object[] {
                                        eElement.getAttribute("electivegroupsubject_id"),
                                        i+1,
                                        subjectname
                                        });
                                    }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    

                                }                    
                            }
                        }
                    }else {
                        NodeList nList = Config.xml_config_ad_electivegroupsubjectmgr.doc.getElementsByTagName("config_ad_electivegroupsubject");
                        for (int i = 0; i < nList.getLength(); i++) {
                            Node nNode = nList.item(i);                
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element eElement = (Element) nNode;     
                                
                                String subject_id = eElement.getElementsByTagName("subject_id").item(0).getTextContent();
                                Element ele = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subject_id+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
                                String subjectname = ele.getElementsByTagName("subject_name").item(0).getTextContent();
                                
//                                    if(eElement.getElementsByTagName("section_name").item(0).getTextContent().toUpperCase().startsWith(txt_SectionSearch.getText().toUpperCase())){
                                tbl_ElectiveSubjectModel.addRow(new Object[] {eElement.getAttribute("electivegroupsubject_id"),
                                i+1,
                                subjectname                                        
                                });
                                        
//                                    }     
                            }
                        }
                    }
                    int a=tbl_ElectiveSubjectModel.getRowCount();
                    txt_ElectiveSubjectTotalCount.setText(Integer.toString(a));
                }catch (Exception e) {
                    e.printStackTrace();
                }      

    }//GEN-LAST:event_btn_ElectiveSubjectSearchActionPerformed

    private void btn_ElectiveSubjectNewEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveSubjectNewEntryActionPerformed
        Config.ad_electivegroupsubject.onloadReset();
        Config.ad_electivegroupsubject.setVisible(true);                
    }//GEN-LAST:event_btn_ElectiveSubjectNewEntryActionPerformed

    private void tbl_ElectiveSubjectMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_ElectiveSubjectMouseClicked
        if(evt.getClickCount()==2){
            btn_ElectiveSubjectViewActionPerformed(null);
        }

    }//GEN-LAST:event_tbl_ElectiveSubjectMouseClicked

    private void btn_ElectiveSubjectViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveSubjectViewActionPerformed
        Config.ad_viewelectivegroupsubject.onloadReset(tbl_ElectiveSubjectModel.getValueAt(tbl_ElectiveSubject.getSelectedRow(), 0).toString());
        Config.ad_viewelectivegroupsubject.setVisible(true);
    }//GEN-LAST:event_btn_ElectiveSubjectViewActionPerformed

    private void btn_ElectiveSubjectDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveSubjectDeleteActionPerformed
         String electiveSubjectid = tbl_ElectiveSubjectModel.getValueAt(tbl_ElectiveSubject.getSelectedRow(),0).toString();
         int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure to Delete Section?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.config_ad_cm_electivegroupsubjectmgr.delAd_electivesubject(electiveSubjectid)){ 
                    JOptionPane.showMessageDialog(this, "Elective Subject Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                 }else{
                    JOptionPane.showMessageDialog(this, "Error in Section Elective Group", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } 
    }//GEN-LAST:event_btn_ElectiveSubjectDeleteActionPerformed

    private void cmb_ElectiveGroupNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_ElectiveGroupNameActionPerformed
         tbl_ElectiveSubjectModel.setRowCount(0);
        String elective_id = null;
        try {
            int si = cmb_ElectiveGroupName.getSelectedIndex();
            if (cmb_ElectiveGroupName.getSelectedIndex() != 0) {
                try {
                    Element element = (Element) Config.xml_config_ad_electivegroupmgr.xpath.evaluate("//*[@sno='"+si+"']", Config.xml_config_ad_electivegroupmgr.doc, XPathConstants.NODE);
                    elective_id = element.getAttribute("electivegroup_id");
                } catch (Exception e) {

                }

                NodeList nList = Config.xml_config_ad_electivegroupsubjectmgr.doc.getElementsByTagName("config_ad_electivegroupsubject");
                int ctr=0;
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if (eElement.getElementsByTagName("electivegroup_id").item(0).getTextContent().equals(elective_id)) {
                            String subject_id = eElement.getElementsByTagName("subject_id").item(0).getTextContent();
                            Element ele = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subject_id+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
                            String subjectname = ele.getElementsByTagName("subject_name").item(0).getTextContent();
                            ctr++;
                            tbl_ElectiveSubjectModel.addRow(new Object[] {
                                eElement.getAttribute("electivegroupsubject_id"),
                                ctr,
                                subjectname,
                            });
                        }
                    }
                }
            
            }else{
                NodeList nList = Config.xml_config_ad_electivegroupsubjectmgr.doc.getElementsByTagName("config_ad_electivegroupsubject");
                int ctr=0;
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);                
                    Element eElement = (Element) nNode;  
                    String subject_id = eElement.getElementsByTagName("subject_id").item(0).getTextContent();
                    Element ele = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subject_id+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
                    String subjectname = ele.getElementsByTagName("subject_name").item(0).getTextContent();
                    ctr++;
                    
                    tbl_ElectiveSubjectModel.addRow(new Object[] {
                    eElement.getAttribute("electivegroupsubject_id"),
                    ctr,
                    subjectname
                    });
                                            
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_cmb_ElectiveGroupNameActionPerformed

    private void btn_SessionDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SessionDeleteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_SessionDeleteActionPerformed

    private void btn_SessionSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SessionSearchActionPerformed
         try {
             tbl_SessionModel.setRowCount(0);
             NodeList nList1 = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
             int ctr1 = 0;
             for (int i = 0; i < nList1.getLength(); i++) {
                 Node nNode1 = nList1.item(i);
                 Element eElement1 = (Element) nNode1;
                 if(!txt_SessionSearch.getText().equals("")){
                     if (eElement1.getElementsByTagName("session_name").item(0).getTextContent().toUpperCase().startsWith(txt_SessionSearch.getText().toUpperCase())) {
                        ctr1++;
                        tbl_SessionModel.addRow(new Object[]{
                            eElement1.getAttribute("session_id"),
                            ctr1,
                            eElement1.getElementsByTagName("session_name").item(0).getTextContent()
                        });
                     }
                 }else{tbl_SessionModel.addRow(new  Object[]{
                     eElement1.getAttribute("session_id"),
                     i+1,
                     eElement1.getElementsByTagName("session_name").item(0).getTextContent()
                 });
               }
             }
             int b = tbl_SessionModel.getRowCount();
             txt_SessionTotalCount.setText(Integer.toString(b));
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_btn_SessionSearchActionPerformed

    private void txt_SessionSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_SessionSearchCaretUpdate
        btn_SessionSearchActionPerformed(null);
    }//GEN-LAST:event_txt_SessionSearchCaretUpdate

    private void txt_ElectiveSubjectNameSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_ElectiveSubjectNameSearchCaretUpdate
        btn_ElectiveSubjectSearchActionPerformed(null);
    }//GEN-LAST:event_txt_ElectiveSubjectNameSearchCaretUpdate

    private void btn_ClassExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassExportActionPerformed
        if(evt.getSource() == btn_ClassExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Admin_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_Class, new File(file));
				}
			}                
    }//GEN-LAST:event_btn_ClassExportActionPerformed

    private void btn_SessionExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SessionExportActionPerformed
        if(evt.getSource() == btn_SessionExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Admin_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_Session, new File(file));
				}
			}
    }//GEN-LAST:event_btn_SessionExportActionPerformed

    private void btn_SectionExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SectionExportActionPerformed
        if(evt.getSource() == btn_SectionExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Admin_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_Section, new File(file));
				}
			}        
    }//GEN-LAST:event_btn_SectionExportActionPerformed

    private void btn_SubjectExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectExportActionPerformed
        if(evt.getSource() == btn_SubjectExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Admin_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_subject, new File(file));
				}
			}        
    }//GEN-LAST:event_btn_SubjectExportActionPerformed

    private void btn_EGNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_EGNameActionPerformed
        if(evt.getSource() == btn_EGName){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Admin_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_ElectiveName, new File(file));
				}
			}        
    }//GEN-LAST:event_btn_EGNameActionPerformed

    private void btn_ESubNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ESubNameActionPerformed
        if(evt.getSource() == btn_ESubName){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Admin_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_ElectiveSubject, new File(file));
				}
			}        
    }//GEN-LAST:event_btn_ESubNameActionPerformed

    private void btn_SessionRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SessionRefreshActionPerformed
        Config.ad_home.onloadReset();
    }//GEN-LAST:event_btn_SessionRefreshActionPerformed

    private void btn_Sc_RefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Sc_RefreshActionPerformed
        Config.ad_home.onloadReset();
    }//GEN-LAST:event_btn_Sc_RefreshActionPerformed
        
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Admin_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Admin_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Admin_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Admin_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Admin_Home dialog = new Admin_Home(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ClassDelete;
    private javax.swing.JButton btn_ClassExport;
    private javax.swing.JButton btn_ClassNewEntry;
    private javax.swing.JButton btn_ClassSearch;
    private javax.swing.JButton btn_ClassSectionCancel;
    private javax.swing.JButton btn_ClassSectionRefresh;
    private javax.swing.JButton btn_ClassView;
    private javax.swing.JButton btn_EGName;
    private javax.swing.JButton btn_ESubName;
    private javax.swing.JButton btn_ElectiveCancel;
    private javax.swing.JButton btn_ElectiveNameDelete;
    private javax.swing.JButton btn_ElectiveNameNewEntry;
    private javax.swing.JButton btn_ElectiveNameView;
    private javax.swing.JButton btn_ElectiveRefresh;
    private javax.swing.JButton btn_ElectiveSearch;
    private javax.swing.JButton btn_ElectiveSubjectDelete;
    private javax.swing.JButton btn_ElectiveSubjectNewEntry;
    private javax.swing.JButton btn_ElectiveSubjectSearch;
    private javax.swing.JButton btn_ElectiveSubjectView;
    private javax.swing.JButton btn_ScExport;
    private javax.swing.JButton btn_Sc_Cancel;
    private javax.swing.JButton btn_Sc_Delete;
    private javax.swing.JButton btn_Sc_NewEntry;
    private javax.swing.JButton btn_Sc_Refresh;
    private javax.swing.JButton btn_Sc_Search;
    private javax.swing.JButton btn_Sc_View;
    private javax.swing.JButton btn_SectionDelete;
    private javax.swing.JButton btn_SectionExport;
    private javax.swing.JButton btn_SectionNewEntry;
    private javax.swing.JButton btn_SectionSearch;
    private javax.swing.JButton btn_SectionView;
    private javax.swing.JButton btn_SessionCancel;
    private javax.swing.JButton btn_SessionDelete;
    private javax.swing.JButton btn_SessionExport;
    private javax.swing.JButton btn_SessionNewEntry;
    private javax.swing.JButton btn_SessionRefresh;
    private javax.swing.JButton btn_SessionSearch;
    private javax.swing.JButton btn_SessionView;
    private javax.swing.JButton btn_SubjectCancel;
    private javax.swing.JButton btn_SubjectDelete;
    private javax.swing.JButton btn_SubjectExport;
    private javax.swing.JButton btn_SubjectNewEntry;
    private javax.swing.JButton btn_SubjectRefresh;
    private javax.swing.JButton btn_SubjectSearch;
    private javax.swing.JButton btn_SubjectView;
    private javax.swing.JComboBox cmb_Class;
    private javax.swing.JComboBox cmb_ElectiveGroupName;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JSplitPane jSplitPane3;
    private javax.swing.JSplitPane jSplitPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTable tbl_Class;
    private javax.swing.JTable tbl_ElectiveName;
    private javax.swing.JTable tbl_ElectiveSubject;
    private javax.swing.JTable tbl_SchoolCategory;
    private javax.swing.JTable tbl_Section;
    private javax.swing.JTable tbl_Session;
    private javax.swing.JTable tbl_subject;
    private javax.swing.JTextField txt_ClassSearch;
    private javax.swing.JTextField txt_ClassTotalCount;
    private javax.swing.JTextField txt_ElectiveGroupTotalCount;
    private javax.swing.JTextField txt_ElectiveSubjectNameSearch;
    private javax.swing.JTextField txt_ElectiveSubjectTotalCount;
    private javax.swing.JTextField txt_EletiveNameSearch;
    private javax.swing.JTextField txt_ScSearch;
    private javax.swing.JTextField txt_Sc_TotalCount;
    private javax.swing.JTextField txt_SectionSearch;
    private javax.swing.JTextField txt_SectionTotalCount;
    private javax.swing.JTextField txt_SessionSearch;
    private javax.swing.JTextField txt_SessionTotalCount;
    private javax.swing.JTextField txt_SubjectSearch;
    private javax.swing.JTextField txt_SubjectTotalCount;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {
        cmb_ElectiveGroupName.removeAllItems();
             cmb_ElectiveGroupName.addItem("-Select-");
             NodeList nodeList2 = Config.xml_config_ad_electivegroupmgr.doc.getElementsByTagName("config_ad_electivegroup");                         
             for (int i = 0; i < nodeList2.getLength(); i++) {
                Node node = nodeList2.item(i);
                 if (node.getNodeType() == Node.ELEMENT_NODE) {
                     Element element = (Element) node;
                     cmb_ElectiveGroupName.addItem(element.getElementsByTagName("electivegroupname").item(0).getTextContent());
                     
                 }
             }
       
        try {
//            int sessionid = 0;
            Config.sql = "select session_id from config_ad_session";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while(Config.rs.next()){
                if(Config.rs.getString("session_id")==null ){
                    
                    jTabbedPane1.setEnabledAt(0, true);
                    jTabbedPane1.setEnabledAt(1, false);
                    jTabbedPane1.setEnabledAt(1, false);
                }
            }
            
       } catch (Exception e) {
           e.printStackTrace();
       }
     
     
       tbl_SessionModel.setRowCount(0);
       NodeList nodeList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
       for (int i = 0; i < nodeList.getLength(); i++) {
           Node n01 = nodeList.item(i);
           Element el01 = (Element) n01;
           tbl_SessionModel.addRow(new Object[] {el01.getAttribute("session_id"),
           i+1,
           el01.getElementsByTagName("session_name").item(0).getTextContent()
           });
       }
        btn_SessionSearchActionPerformed(null);
       
       
    tbl_SchoolCategoryModel.setRowCount(0);
    NodeList nList = Config.xml_config_ad_schoolcategorymgr.doc.getElementsByTagName("config_ad_schoolcategory");
    for (int i = 0; i < nList.getLength(); i++){
        Node nNode = nList.item(i);
        Element eElement = (Element) nNode;
        tbl_SchoolCategoryModel.addRow(new Object[] {eElement.getAttribute("category_id"),
            i+1,
            eElement.getElementsByTagName("category_name").item(0).getTextContent(),                
            
        });
    }
    btn_Sc_SearchActionPerformed(null);
    tbl_ClassModel.setRowCount(0);
    cmb_Class.removeAllItems();
    cmb_Class.addItem("- Select -");
    NodeList nList1 = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
    for (int i = 0; i < nList1.getLength(); i++){
        Node nNode = nList1.item(i);
        Element eElement = (Element) nNode;
        tbl_ClassModel.addRow(new Object[] {eElement.getAttribute("class_id"),
            i+1,
            eElement.getElementsByTagName("class_name").item(0).getTextContent(),
            eElement.getElementsByTagName("category_id").item(0).getTextContent()
        });
        cmb_Class.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());
    }
       btn_ClassSearchActionPerformed(null);
    tbl_SectionModel.setRowCount(0);
    NodeList nList2 = Config.xml_config_ad_classsectionmgr.doc.getElementsByTagName("config_ad_class_section");
    for (int i = 0; i < nList2.getLength(); i++){
        Node nNode = nList2.item(i);
        Element eElement = (Element) nNode;
        tbl_SectionModel.addRow(new Object[] {eElement.getAttribute("section_id"),
            i+1,
            eElement.getElementsByTagName("section_name").item(0).getTextContent()                     
        });
    }
    btn_SectionSearchActionPerformed(null);
    tbl_SubjectModel.setRowCount(0);
    NodeList nList3 = Config.xml_config_ad_subjectmgr.doc.getElementsByTagName("config_ad_subject");
    for (int i = 0; i < nList3.getLength(); i++){
        Node nNode = nList3.item(i);
        Element eElement = (Element) nNode;
        tbl_SubjectModel.addRow(new Object[] {eElement.getAttribute("subject_id"),
            i+1,
            eElement.getElementsByTagName("subject_name").item(0).getTextContent()
        });
                  btn_SubjectSearchActionPerformed(null);
    }
    tbl_ElectiveGroupModel.setRowCount(0);    
    NodeList nList4 = Config.xml_config_ad_electivegroupmgr.doc.getElementsByTagName("config_ad_electivegroup");
    for (int i = 0; i < nList4.getLength(); i++){
        Node nNode = nList4.item(i);
        Element eElement = (Element) nNode;                  
        tbl_ElectiveGroupModel.addRow(new Object[] {eElement.getAttribute("electivegroup_id"),
            i+1,
            eElement.getElementsByTagName("electivegroupname").item(0).getTextContent()
        });
//        cmb_ElectiveGroupName.addItem(eElement.getElementsByTagName("electivegroupname").item(0).getTextContent());
                  btn_ElectiveSearchActionPerformed(null);
     }
    tbl_ElectiveSubjectModel.setRowCount(0); 
        
        try {
            String subjectid = null;
            NodeList nList5 = Config.xml_config_ad_electivegroupsubjectmgr.doc.getElementsByTagName("config_ad_electivegroupsubject");
            
            for (int i = 0; i < nList5.getLength(); i++){
                Node nNode = nList5.item(i);
                Element eElement = (Element) nNode;   
                    try {
                        subjectid = eElement.getElementsByTagName("subject_id").item(0).getTextContent();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Element element = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subjectid+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
                    String subjectname = element.getElementsByTagName("subject_name").item(0).getTextContent();
                    tbl_ElectiveSubjectModel.addRow(new Object[] { eElement.getAttribute("electivegroupsubject_id"),
                        i+1,
//                        eElement.getElementsByTagName("electivegroup_id").item(0).getTextContent()
                    subjectname
                });
            }
            btn_ElectiveSubjectSearchActionPerformed(null);
        } catch (Exception e) {
            e.printStackTrace();
        }     
   }
    public void toExcel(JTable table, File file){
		try{
			TableModel model = table.getModel();
			FileWriter excel = new FileWriter(file);

			for(int i = 0; i < model.getColumnCount(); i++){
				excel.write(model.getColumnName(i) + "\t");
			}

			excel.write("\n");

			for(int i=0; i< model.getRowCount(); i++) {
				for(int j=0; j < model.getColumnCount(); j++) {
					excel.write(model.getValueAt(i,j).toString()+"\t");
				}
				excel.write("\n");
			}

			excel.close();
		}catch(IOException e){ 
                    e.printStackTrace();
                    
                }
	}
}