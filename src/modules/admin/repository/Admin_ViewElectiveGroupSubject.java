

package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_ElectiveGroupSubject;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Admin_ViewElectiveGroupSubject extends javax.swing.JDialog {

   
    public Admin_ViewElectiveGroupSubject(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_ElectiveSubjectUpdate = new javax.swing.JButton();
        btn_ElectiveSubjectDelete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmb_ElectiveSubjectGroupName = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        cmb_SubjectName = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();
        lbl_ElectiveSubjectId = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_ElectiveSubjectUpdate.setText("Update");
        btn_ElectiveSubjectUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveSubjectUpdateActionPerformed(evt);
            }
        });

        btn_ElectiveSubjectDelete.setText("Delete");
        btn_ElectiveSubjectDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveSubjectDeleteActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Elective Group Subject", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Elective Group Name :");

        jLabel2.setText("Subject Name :");

        jLabel3.setText("Session :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmb_ElectiveSubjectGroupName, 0, 96, Short.MAX_VALUE)
                            .addComponent(cmb_SubjectName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cmb_ElectiveSubjectGroupName, cmb_Session, cmb_SubjectName});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmb_ElectiveSubjectGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(cmb_SubjectName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmb_ElectiveSubjectGroupName, jLabel1, jLabel2, jLabel3});

        lbl_ElectiveSubjectId.setForeground(new java.awt.Color(240, 240, 240));

        jButton1.setText("Cancel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbl_ElectiveSubjectId, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_ElectiveSubjectUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveSubjectDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ElectiveSubjectDelete, btn_ElectiveSubjectUpdate, jButton1});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_ElectiveSubjectDelete)
                        .addComponent(btn_ElectiveSubjectUpdate)
                        .addComponent(jButton1))
                    .addComponent(lbl_ElectiveSubjectId, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(btn_ElectiveSubjectUpdate);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ElectiveSubjectUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveSubjectUpdateActionPerformed
   String str = Validation();
   if (str.equals("ok")){           
       try {
           Element element = (Element) Config.xml_config_ad_electivegroupmgr.xpath.evaluate("//*[@sno='"+(cmb_ElectiveSubjectGroupName.getSelectedIndex()+1)+"']", Config.xml_config_ad_electivegroupmgr.doc, XPathConstants.NODE);
            String electiveid = element.getAttribute("electivegroup_id");
           Element elmt = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@sno='"+(cmb_SubjectName.getSelectedIndex()+1)+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
           String subjectid = elmt.getAttribute("subject_id");
            
            Config_Ad_Dm_ElectiveGroupSubject  cs = new Config_Ad_Dm_ElectiveGroupSubject ();
            cs.setElectivegroupsubject_id(lbl_ElectiveSubjectId.getText());            
            cs.setSubject_id(subjectid);
            cs.setElectivegroup_id(electiveid);
            if(Config.config_ad_cm_electivegroupsubjectmgr.updAd_electivesubject(cs)){
                JOptionPane.showMessageDialog(this, "Section  updated Successfully", "Success",JOptionPane.NO_OPTION);
                dispose();
            }else{
                JOptionPane.showMessageDialog(this, "Error in Updation", "Error",JOptionPane.ERROR_MESSAGE );
            }                 
        } catch (Exception e) {
            e.printStackTrace();
        } 
        }else{            
            JOptionPane.showMessageDialog(this, str, "Error",JOptionPane.ERROR_MESSAGE );
        }
    
    }//GEN-LAST:event_btn_ElectiveSubjectUpdateActionPerformed

    private void btn_ElectiveSubjectDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveSubjectDeleteActionPerformed
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure You Want to Delete Elective Subject Group ? ","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {
             
                if(Config.config_ad_cm_electivegroupsubjectmgr.delAd_electivesubject(lbl_ElectiveSubjectId.getText())){ 

                JOptionPane.showMessageDialog(this, "Elective Subject Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                dispose();
            
                 }else{
                    
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } else{
                   dispose();     
                   }
    }//GEN-LAST:event_btn_ElectiveSubjectDeleteActionPerformed

   
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
      
    }//GEN-LAST:event_closeDialog

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ElectiveSubjectDelete;
    private javax.swing.JButton btn_ElectiveSubjectUpdate;
    private javax.swing.JComboBox cmb_ElectiveSubjectGroupName;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JComboBox cmb_SubjectName;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_ElectiveSubjectId;
    // End of variables declaration//GEN-END:variables

  public void onloadReset(String electivesubjectid) {
      
      try {
          lbl_ElectiveSubjectId.setText(electivesubjectid);

             NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
             cmb_Session.removeAllItems();
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                }
             }
             NodeList nList1 = Config.xml_config_ad_electivegroupmgr.doc.getElementsByTagName("config_ad_electivegroup");
             cmb_ElectiveSubjectGroupName.removeAllItems();
             for (int i = 0; i < nList1.getLength(); i++) {
                Node nNode = nList1.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_ElectiveSubjectGroupName.addItem(eElement.getElementsByTagName("electivegroupname").item(0).getTextContent().toString());
                }
             }
             NodeList nList2 = Config.xml_config_ad_subjectmgr.doc.getElementsByTagName("config_ad_subject");
             cmb_SubjectName.removeAllItems();
             for (int i = 0; i < nList2.getLength(); i++) {
                Node nNode2 = nList2.item(i);                
                if (nNode2.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement2 = (Element) nNode2;
                    cmb_SubjectName.addItem(eElement2.getElementsByTagName("subject_name").item(0).getTextContent().toString());
                }
             }
            Element element = (Element) Config.xml_config_ad_electivegroupsubjectmgr.xpath.evaluate("//*[@electivegroupsubject_id='"+electivesubjectid+"']", Config.xml_config_ad_electivegroupsubjectmgr.doc, XPathConstants.NODE);
            
            String sessionid = element.getElementsByTagName("session_id").item(0).getTextContent();
            Element element1 = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@session_id='"+sessionid+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);
            String sessionname = element1.getElementsByTagName("session_name").item(0).getTextContent();
            
            String elegroupid = element.getElementsByTagName("electivegroup_id").item(0).getTextContent();
            Element element2 = (Element) Config.xml_config_ad_electivegroupmgr.xpath.evaluate("//*[@electivegroup_id='"+elegroupid+"']", Config.xml_config_ad_electivegroupmgr.doc, XPathConstants.NODE);
            String groupnam = element2.getElementsByTagName("electivegroupname").item(0).getTextContent();
            
            String subjectid = element.getElementsByTagName("subject_id").item(0).getTextContent();
            Element element3 = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subjectid+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
            String subjectnam = element3.getElementsByTagName("subject_name").item(0).getTextContent();
            
            cmb_Session.setSelectedItem(sessionname);
            cmb_ElectiveSubjectGroupName.setSelectedItem(groupnam);
            cmb_SubjectName.setSelectedItem(subjectnam);
      } catch (Exception e) {
      }
  }
  private String Validation() {
      if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(cmb_ElectiveSubjectGroupName.getSelectedItem().equals("- Select -")){
            return (" 'Elective Group Name' should not be un-select.");
            
        }else if (cmb_SubjectName.getSelectedItem().equals("- Select -")) {
            return (" 'Elective Group Name' should not be un-select.");
        }
        else{
        return ("ok");
        }
  }
}