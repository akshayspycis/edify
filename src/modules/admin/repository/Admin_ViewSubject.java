

package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Subject;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class Admin_ViewSubject extends javax.swing.JDialog {

    
    public Admin_ViewSubject(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_SubjectUpdate = new javax.swing.JButton();
        btn_SubjectDelete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_SubjectName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();
        lbl_SubjectId = new javax.swing.JLabel();
        btn_Cnacel = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_SubjectUpdate.setText("Update");
        btn_SubjectUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectUpdateActionPerformed(evt);
            }
        });

        btn_SubjectDelete.setText("Delete");
        btn_SubjectDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SubjectDeleteActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Subject View", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Subject Name :");

        jLabel2.setText("Session :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_SubjectName))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_SubjectName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        lbl_SubjectId.setForeground(new java.awt.Color(240, 240, 240));

        btn_Cnacel.setText("Cancel");
        btn_Cnacel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CnacelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbl_SubjectId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(33, 33, 33)
                        .addComponent(btn_SubjectUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SubjectDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Cnacel))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_Cnacel, btn_SubjectDelete, btn_SubjectUpdate});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_SubjectDelete)
                    .addComponent(btn_SubjectUpdate)
                    .addComponent(lbl_SubjectId)
                    .addComponent(btn_Cnacel))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_SubjectUpdate, lbl_SubjectId});

        getRootPane().setDefaultButton(btn_SubjectUpdate);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
    }//GEN-LAST:event_closeDialog

    private void btn_SubjectUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectUpdateActionPerformed
         try {
            String str = checkValidity();
            if (str.equals("ok")){
                int i;
                String subjectname = txt_SubjectName.getText();   
                NodeList nList = Config.xml_config_ad_subjectmgr.doc.getElementsByTagName("config_ad_subject");
                for(i=0; i < nList.getLength();i++){
                    Node nNode = nList.item(i);
                    if(nNode.getNodeType() == Node.ELEMENT_NODE){
                        Element eElement = (Element)nNode;                   
                        if(subjectname.equalsIgnoreCase(eElement.getElementsByTagName("subject_name").item(0).getTextContent())){
                            JOptionPane.showMessageDialog(this, "Subject Name is already Exist");                
                            break;                        
                        }
                    }
                }
                if(i==nList.getLength()){
                int sessionindex = cmb_Session.getSelectedIndex()+1; 
                String sessionid = null;
                Config_Ad_Dm_Subject subject = new Config_Ad_Dm_Subject();
                   Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno="+sessionindex+"]", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
                   sessionid = element.getAttribute("session_id");
                   subject.setSubject_id(lbl_SubjectId.getText());
                   subject.setSession_id(sessionid);
                   subject.setSubject_name(txt_SubjectName.getText());
                    if(Config.config_ad_cm_subjectmgr.updAd_Subject(subject)){                
                        JOptionPane.showMessageDialog(this, "Record updated Successfully", "Successfull",JOptionPane.NO_OPTION);
                        dispose();
                    }else{
                        JOptionPane.showMessageDialog(this, "Error in updation", "Error",JOptionPane.ERROR_MESSAGE );
                    }
                }}else{            
                JOptionPane.showMessageDialog(this, str, "Error",JOptionPane.ERROR_MESSAGE );
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }//GEN-LAST:event_btn_SubjectUpdateActionPerformed

    private void btn_SubjectDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SubjectDeleteActionPerformed
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure, You Want to Delete Subject?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {
             
                if(Config.config_ad_cm_subjectmgr.delAd_Subject(lbl_SubjectId.getText())){ 

                JOptionPane.showMessageDialog(this, "Subject Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                dispose();
            
                 }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } else{
                   dispose();     
                   }
    }//GEN-LAST:event_btn_SubjectDeleteActionPerformed

    private void btn_CnacelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CnacelActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_btn_CnacelActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Cnacel;
    private javax.swing.JButton btn_SubjectDelete;
    private javax.swing.JButton btn_SubjectUpdate;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_SubjectId;
    private javax.swing.JTextField txt_SubjectName;
    // End of variables declaration//GEN-END:variables

    void onloadReset(String subjectid) {
        
        try {
            lbl_SubjectId.setText(subjectid);
//            Element element1  = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate(feeid, null);
             NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
             cmb_Session.removeAllItems();
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                }
             }
            Element element = (Element) Config.xml_config_ad_subjectmgr.xpath.evaluate("//*[@subject_id='"+subjectid+"']", Config.xml_config_ad_subjectmgr.doc, XPathConstants.NODE);
            
            String sessionid = element.getElementsByTagName("session_id").item(0).getTextContent();
            Element element1 = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@session_id='"+sessionid+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);
            String sessionname = element1.getElementsByTagName("session_name").item(0).getTextContent();
            cmb_Session.setSelectedItem(sessionname);
            
            txt_SubjectName.setText(element.getElementsByTagName("subject_name").item(0).getTextContent());
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error",JOptionPane.ERROR_MESSAGE );
        }
    }
     private String checkValidity() {
        if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(txt_SubjectName.getText().equals("")){
            return (" 'Subject Name' should not be blank.");
            
        }else{
        return ("ok");
        }
    }

}
