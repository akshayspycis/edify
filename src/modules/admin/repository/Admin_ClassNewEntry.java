

package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Class;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Admin_ClassNewEntry extends javax.swing.JDialog {

    
    public Admin_ClassNewEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btn_ClassNewEntrySave = new javax.swing.JButton();
        btn_ClassNewEntryCancel = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_ClassNewEntryName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cmb_ClassNewEntryCategoryName = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();

        jLabel2.setText("jLabel2");

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_ClassNewEntrySave.setText("Save");
        btn_ClassNewEntrySave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassNewEntrySaveActionPerformed(evt);
            }
        });

        btn_ClassNewEntryCancel.setText("Cancel");
        btn_ClassNewEntryCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassNewEntryCancelActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Class New Entry", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Class Name :");

        txt_ClassNewEntryName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_ClassNewEntryNameActionPerformed(evt);
            }
        });

        jLabel3.setText("School Category :");

        jLabel4.setText("Session :");

        cmb_Session.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_SessionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmb_ClassNewEntryCategoryName, 0, 200, Short.MAX_VALUE)
                            .addComponent(txt_ClassNewEntryName)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmb_Session, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel3, jLabel4});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmb_ClassNewEntryCategoryName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_ClassNewEntryName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel3, jLabel4, txt_ClassNewEntryName});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_ClassNewEntrySave, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ClassNewEntryCancel))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ClassNewEntryCancel, btn_ClassNewEntrySave});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ClassNewEntryCancel)
                    .addComponent(btn_ClassNewEntrySave))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(btn_ClassNewEntrySave);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ClassNewEntrySaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassNewEntrySaveActionPerformed
           try {
            String str = checkValidity();
            int i;
            if (str.equals("ok")) {
                String classname = txt_ClassNewEntryName.getText();   
                NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
                for(i=0; i < nList.getLength();i++){
                    Node nNode = nList.item(i);
                    if(nNode.getNodeType() == Node.ELEMENT_NODE){
                        Element eElement = (Element)nNode;                   
                        if(classname.equalsIgnoreCase(eElement.getElementsByTagName("class_name").item(0).getTextContent())){
                            JOptionPane.showMessageDialog(this, "Class Name is already Exist");                
                            break;                        
                        }
                    }
                }
                if(i==nList.getLength()){
                 int sessionindex = cmb_Session.getSelectedIndex(); 
                 String sessionid = null;
                 
                 int schoolcatindex = cmb_ClassNewEntryCategoryName.getSelectedIndex();
                 String schoolcatid = null;
               
                 Config_Ad_Dm_Class clas = new Config_Ad_Dm_Class();
                 
                 Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno='"+sessionindex+"']", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
                 sessionid = element.getAttribute("session_id");
                 
                 Element elemen = (Element)Config.xml_config_ad_schoolcategorymgr.xpath.evaluate("//*[@sno='"+schoolcatindex+"']", Config.xml_config_ad_schoolcategorymgr.doc,XPathConstants.NODE);
                 schoolcatid = elemen.getAttribute("category_id");
                 
                 clas.setSession_id(sessionid);
                 clas.setCategory_id(schoolcatid);
                 clas.setClass_name(txt_ClassNewEntryName.getText());
                 if(Config.config_ad_cm_classmgr.insAd_Class(clas)){
                     onloadReset();
                    JOptionPane.showMessageDialog(this, "Data Inserted Successfully", "success", JOptionPane.NO_OPTION);
                 }else{
                   JOptionPane.showMessageDialog(this, "Error in Insertion", "Error",JOptionPane.ERROR_MESSAGE);
                 }
                }
            }else{
               JOptionPane.showMessageDialog(this, str,"Error", JOptionPane.ERROR_MESSAGE);
           }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_ClassNewEntrySaveActionPerformed

    private void btn_ClassNewEntryCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassNewEntryCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_ClassNewEntryCancelActionPerformed

    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        
    }//GEN-LAST:event_closeDialog

    private void txt_ClassNewEntryNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_ClassNewEntryNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_ClassNewEntryNameActionPerformed

    private void cmb_SessionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_SessionActionPerformed
        int index = cmb_Session.getSelectedIndex();
        String sessionid = null;        
        if(index>0){        
            try {
                Element element = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno='"+index+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);
                sessionid = element.getAttribute("session_id");            
            } catch (Exception e) {
                e.printStackTrace();
            }        

            NodeList sList = Config.xml_config_ad_schoolcategorymgr.doc.getElementsByTagName("config_ad_schoolcategory");         
            cmb_ClassNewEntryCategoryName.removeAllItems();
            cmb_ClassNewEntryCategoryName.addItem("- Select -");
            for (int i = 0; i < sList.getLength(); i++) {       
                Node nNode = sList.item(i);            
                Element eElement = (Element)nNode;                 
                if(eElement.getElementsByTagName("session_id").item(0).getTextContent().equals(sessionid)){                    
                    cmb_ClassNewEntryCategoryName.addItem(eElement.getElementsByTagName("category_name").item(0).getTextContent());                    
                }           
            }
        }else{
           NodeList sList = Config.xml_config_ad_schoolcategorymgr.doc.getElementsByTagName("config_ad_schoolcategory");         
            cmb_ClassNewEntryCategoryName.removeAllItems();
            cmb_ClassNewEntryCategoryName.addItem("- Select -");
            for (int i = 0; i < sList.getLength(); i++) {       
                Node nNode = sList.item(i);            
                Element eElement = (Element)nNode;                  
                cmb_ClassNewEntryCategoryName.addItem(eElement.getElementsByTagName("category_name").item(0).getTextContent());                    
                           
            }
        }
        
        
    
        
    }//GEN-LAST:event_cmb_SessionActionPerformed
    
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ClassNewEntryCancel;
    private javax.swing.JButton btn_ClassNewEntrySave;
    private javax.swing.JComboBox cmb_ClassNewEntryCategoryName;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txt_ClassNewEntryName;
    // End of variables declaration//GEN-END:variables

   

    public void onloadReset() {
        txt_ClassNewEntryName.setText(null);
        try {
            txt_ClassNewEntryName.setText("");            
            NodeList nList1=Config.xml_config_ad_schoolcategorymgr.doc.getElementsByTagName("config_ad_schoolcategory");
            cmb_ClassNewEntryCategoryName.removeAllItems();
            cmb_ClassNewEntryCategoryName.addItem("- Select -");
            for (int i = 0; i < nList1.getLength(); i++){
                    Node nNode = nList1.item(i);
                    if(nNode.getNodeType()==Node.ELEMENT_NODE){
                        Element eElement = (Element) nNode;
                        cmb_ClassNewEntryCategoryName.addItem(eElement.getElementsByTagName("category_name").item(0).getTextContent());
                    }
                }
            NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
              cmb_Session.removeAllItems();
              cmb_Session.addItem("- Select -");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);                
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent());
                    }
                 }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
}
    private String checkValidity() {
        if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(cmb_ClassNewEntryCategoryName.getSelectedItem().equals("- Select -")){
            return (" 'School Category' should not be un-select.");
            
        }else if(txt_ClassNewEntryName.getText().equals("")){
            return ("'Class Name' should not be blank");
        }
        else{
        return ("ok");
        }

    }
}
