package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_ElectiveGroup;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Admin_ElectiveGroup extends javax.swing.JDialog {

    public Admin_ElectiveGroup(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_ElectiveGroupSave = new javax.swing.JButton();
        btn_ElectiveGroupCancel = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_ElectiveName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_ElectiveGroupSave.setText("Save");
        btn_ElectiveGroupSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveGroupSaveActionPerformed(evt);
            }
        });

        btn_ElectiveGroupCancel.setText("Cancel");
        btn_ElectiveGroupCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveGroupCancelActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Elective Group", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Elective Group Name :");

        txt_ElectiveName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_ElectiveNameActionPerformed(evt);
            }
        });

        jLabel2.setText("Session :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ElectiveName, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cmb_Session, txt_ElectiveName});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_ElectiveName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel2, txt_ElectiveName});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_ElectiveGroupSave, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveGroupCancel))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ElectiveGroupCancel, btn_ElectiveGroupSave});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ElectiveGroupCancel)
                    .addComponent(btn_ElectiveGroupSave))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(btn_ElectiveGroupSave);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ElectiveGroupSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveGroupSaveActionPerformed
      try {
            int i;
            String str = checkValidity();
            if (str.equals("ok")) {
               int sessionindex = cmb_Session.getSelectedIndex(); 
                
                String sessionid = null;
                
                String electivegroupname = txt_ElectiveName.getText();   
                NodeList nList = Config.xml_config_ad_electivegroupmgr.doc.getElementsByTagName("config_ad_electivegroup");
                for(i=0; i < nList.getLength();i++){
                    Node nNode = nList.item(i);
                    if(nNode.getNodeType() == Node.ELEMENT_NODE){
                        Element eElement = (Element)nNode;                   
                        if(electivegroupname.equalsIgnoreCase(eElement.getElementsByTagName("electivegroupname").item(0).getTextContent())){
                            JOptionPane.showMessageDialog(this, "Elective Group Name is already Exist");                
                            break;                        
                        }
                    }
                }
                if(i==nList.getLength()){
               
                 Config_Ad_Dm_ElectiveGroup elegroup = new Config_Ad_Dm_ElectiveGroup();
                 
                 Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno='"+sessionindex+"']", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
               
                 sessionid = element.getAttribute("session_id");
                 elegroup.setSession_id(sessionid);
                 elegroup.setElectivegroupname(txt_ElectiveName.getText());
                 if(Config.config_ad_cm_electivegroupmgr.insAd_electivegroup(elegroup)){
                     onloadReset();
                    JOptionPane.showMessageDialog(this, "Data Inserted Successfully", "success", JOptionPane.NO_OPTION);
                 }else{
                   JOptionPane.showMessageDialog(this, "Error in Insertion", "Error",JOptionPane.ERROR_MESSAGE);
                 }
                }
                }else{
               JOptionPane.showMessageDialog(this, str,"Error", JOptionPane.ERROR_MESSAGE);
           }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_ElectiveGroupSaveActionPerformed

    private void btn_ElectiveGroupCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveGroupCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_ElectiveGroupCancelActionPerformed

  
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
       
    }//GEN-LAST:event_closeDialog

    private void txt_ElectiveNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_ElectiveNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_ElectiveNameActionPerformed
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ElectiveGroupCancel;
    private javax.swing.JButton btn_ElectiveGroupSave;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txt_ElectiveName;
    // End of variables declaration//GEN-END:variables

   

   public void onloadReset() {
         try {
             NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
              cmb_Session.removeAllItems();
              cmb_Session.addItem("- Select -");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);                
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                    }
                 }
             txt_ElectiveName.setText("");
         } catch (Exception e) {
             e.printStackTrace();
             
         }
         
    }
    private String checkValidity() {
        if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(txt_ElectiveName.getText().equals("")){
            return (" 'Elective Name' should not be blank.");
            
        }else{
        return ("ok");
        }
    }
}
