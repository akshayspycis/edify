package modules.admin.repository;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Class;
import managers.datamgr.dao.admin.Config_Ad_Dm_Class_Section;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Admin_ViewClass extends javax.swing.JDialog {
         public Admin_ViewClass (java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
               
            }
        });
    }

    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_ClassUpdate = new javax.swing.JButton();
        btn_ViewClassDelete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_ViewClassName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cmb_ViewSchoolCategory = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();
        lbl_ClassId = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_ClassUpdate.setText("Update");
        btn_ClassUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ClassUpdateActionPerformed(evt);
            }
        });

        btn_ViewClassDelete.setText("Delete");
        btn_ViewClassDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ViewClassDeleteActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Class View Entry", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Class Name :");

        jLabel2.setText("School Category :");

        jLabel3.setText("Session :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_ViewClassName, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmb_ViewSchoolCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10))))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cmb_Session, cmb_ViewSchoolCategory, txt_ViewClassName});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmb_ViewSchoolCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_ViewClassName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, txt_ViewClassName});

        lbl_ClassId.setForeground(new java.awt.Color(240, 240, 240));

        jButton1.setText("Cancel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbl_ClassId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(24, 24, 24)
                        .addComponent(btn_ClassUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ViewClassDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ClassUpdate, btn_ViewClassDelete, jButton1});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ViewClassDelete)
                    .addComponent(btn_ClassUpdate)
                    .addComponent(lbl_ClassId)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_ClassUpdate, lbl_ClassId});

        getRootPane().setDefaultButton(btn_ClassUpdate);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ClassUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ClassUpdateActionPerformed

          try {
            String str = checkValidity();
            if (str.equals("ok")){
                int i;
                int sessionindex = cmb_Session.getSelectedIndex()+1; 
                String sessionid = null;
                int categoryindex = cmb_ViewSchoolCategory.getSelectedIndex()+1;
                String categorid = null; 
                
                Config_Ad_Dm_Class clas = new Config_Ad_Dm_Class();
                
                Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno="+sessionindex+"]", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
                sessionid = element.getAttribute("session_id");
                Element element1 = (Element)Config.xml_config_ad_schoolcategorymgr.xpath.evaluate("//*[@sno="+categoryindex+"]", Config.xml_config_ad_schoolcategorymgr.doc,XPathConstants.NODE);
                categorid = element1.getAttribute("category_id");
                   
                String classname = txt_ViewClassName.getText();   
                NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
                for(i=0; i < nList.getLength();i++){
                    Node nNode = nList.item(i);
                    if(nNode.getNodeType() == Node.ELEMENT_NODE){
                        Element eElement = (Element)nNode;                   
                        if(classname.equalsIgnoreCase(eElement.getElementsByTagName("class_name").item(0).getTextContent())){
                            JOptionPane.showMessageDialog(this, "Class Name is already Exist");                
                            break;                        
                        }
                    }
                }
                if(i==nList.getLength()){
                   
                   clas.setClass_id(lbl_ClassId.getText());
                   clas.setSession_id(sessionid);
                   clas.setCategory_id(categorid);
                   clas.setClass_name(txt_ViewClassName.getText());
                    if(Config.config_ad_cm_classmgr.updAd_Class(clas)){                
                        JOptionPane.showMessageDialog(this, "Record updated Successfully", "Successfull",JOptionPane.NO_OPTION);
                        dispose();
                    }else{
                        JOptionPane.showMessageDialog(this, "Error in updation", "Error",JOptionPane.ERROR_MESSAGE );
                    }
            }}else{            
                JOptionPane.showMessageDialog(this, str, "Error",JOptionPane.ERROR_MESSAGE );
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
        
    }//GEN-LAST:event_btn_ClassUpdateActionPerformed

    private void btn_ViewClassDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ViewClassDeleteActionPerformed
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure You Want to Delete Class","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {             
                 if(Config.config_ad_cm_classmgr.delAd_Class(lbl_ClassId.getText())){ 
                     JOptionPane.showMessageDialog(this, "Class Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                     dispose();            
                 }else{
                     JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } else{
                 dispose();     
             }
    }//GEN-LAST:event_btn_ViewClassDeleteActionPerformed
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
       
    }//GEN-LAST:event_closeDialog

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ClassUpdate;
    private javax.swing.JButton btn_ViewClassDelete;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JComboBox cmb_ViewSchoolCategory;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_ClassId;
    private javax.swing.JTextField txt_ViewClassName;
    // End of variables declaration//GEN-END:variables

    void onloadReset(String classid) {
        try {
            lbl_ClassId.setText(classid);
//            Element element1  = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate(feeid, null);
             NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
             cmb_Session.removeAllItems();
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                }
             }
             NodeList nList1 = Config.xml_config_ad_schoolcategorymgr.doc.getElementsByTagName("config_ad_schoolcategory");
             cmb_ViewSchoolCategory.removeAllItems();
             for (int i = 0; i < nList1.getLength(); i++) {
                Node nNode = nList1.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_ViewSchoolCategory.addItem(eElement.getElementsByTagName("category_name").item(0).getTextContent().toString());
                }
             }
            Element element = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
            
            String sessionid = element.getElementsByTagName("session_id").item(0).getTextContent();
            Element element1 = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@session_id='"+sessionid+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);
            String sessionname = element1.getElementsByTagName("session_name").item(0).getTextContent();
            
            String schoolcatid = element.getElementsByTagName("category_id").item(0).getTextContent();
            Element element2 = (Element) Config.xml_config_ad_schoolcategorymgr.xpath.evaluate("//*[@category_id='"+schoolcatid+"']", Config.xml_config_ad_schoolcategorymgr.doc, XPathConstants.NODE);
            String schoolcatnam = element2.getElementsByTagName("category_name").item(0).getTextContent();
            
            cmb_Session.setSelectedItem(sessionname);
            cmb_ViewSchoolCategory.setSelectedItem(schoolcatnam);
            txt_ViewClassName.setText(element.getElementsByTagName("class_name").item(0).getTextContent());
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error",JOptionPane.ERROR_MESSAGE );
        }
 }  
    private String checkValidity() {
        if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(cmb_ViewSchoolCategory.getSelectedItem().equals("- Select -")){
            return (" 'School Category' should not be un-select.");
            
        }else if(txt_ViewClassName.getText().equals("")){
            return ("'Class Name' should not be blank");
        }
        else{
        return ("ok");
        }

    }
}
