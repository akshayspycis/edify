package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_Class_Section;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Admin_SectionNewEntry extends javax.swing.JDialog {

    
    public Admin_SectionNewEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_SectionNewEntrySave = new javax.swing.JButton();
        btn_SectionNewEntryCancel = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmb_SectionNewEntryClassName = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txt_SectionName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_SectionNewEntrySave.setText("Save");
        btn_SectionNewEntrySave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SectionNewEntrySaveActionPerformed(evt);
            }
        });

        btn_SectionNewEntryCancel.setText("Cancel");
        btn_SectionNewEntryCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SectionNewEntryCancelActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Section New Entry", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Class Name :");

        jLabel2.setText("Section Name :");

        jLabel3.setText("Session :");

        cmb_Session.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_SessionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmb_SectionNewEntryClassName, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_SectionName))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cmb_SectionNewEntryClassName, cmb_Session, txt_SectionName});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmb_SectionNewEntryClassName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_SectionName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmb_SectionNewEntryClassName, jLabel1, jLabel2, jLabel3});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_SectionNewEntrySave, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_SectionNewEntryCancel))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SectionNewEntryCancel, btn_SectionNewEntrySave});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_SectionNewEntryCancel)
                    .addComponent(btn_SectionNewEntrySave))
                .addGap(11, 11, 11))
        );

        getRootPane().setDefaultButton(btn_SectionNewEntrySave);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_SectionNewEntrySaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SectionNewEntrySaveActionPerformed
          try {
            String str = checkValidity();
            if (str.equals("ok")) {
                 
                int sessionindex = cmb_Session.getSelectedIndex(); 
                   String sessionid = null;
                int classindex = cmb_SectionNewEntryClassName.getSelectedIndex();
                   String classid = null;
               
                   Config_Ad_Dm_Class_Section classec = new Config_Ad_Dm_Class_Section();
                 
                 Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno='"+sessionindex+"']", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
                 sessionid = element.getAttribute("session_id");
                 
                 Element elemen = (Element)Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno='"+classindex+"']", Config.xml_config_ad_classmgr.doc,XPathConstants.NODE);
                 classid = elemen.getAttribute("class_id");
                 
                 classec.setSession_id(sessionid);
                 classec.setClass_id(classid);
                 classec.setSection_name(txt_SectionName.getText());
                 if(Config.config_ad_cm_class_sectionmgr.insAd_Class_Section(classec)){
                     onloadReset();
                    JOptionPane.showMessageDialog(this, "Data Inserted Successfully", "success", JOptionPane.NO_OPTION);
                 }else{
                   JOptionPane.showMessageDialog(this, "Error in Insertion", "Error",JOptionPane.ERROR_MESSAGE);
                 }
           }else{
               JOptionPane.showMessageDialog(this, str,"Error", JOptionPane.ERROR_MESSAGE);
           }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_SectionNewEntrySaveActionPerformed

    private void btn_SectionNewEntryCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SectionNewEntryCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_SectionNewEntryCancelActionPerformed

    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        
    }//GEN-LAST:event_closeDialog

    private void cmb_SessionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_SessionActionPerformed
        int index = cmb_Session.getSelectedIndex();
        String sessionid = null;        
        if(index>0){        
            try {
                Element element = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno='"+index+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);
                sessionid = element.getAttribute("session_id");            
            } catch (Exception e) {
                e.printStackTrace();
            }        

            NodeList sList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");         
            cmb_SectionNewEntryClassName.removeAllItems();
            cmb_SectionNewEntryClassName.addItem("- Select -");
            for (int i = 0; i < sList.getLength(); i++) {       
                Node nNode = sList.item(i);            
                Element eElement = (Element)nNode;                 
                if(eElement.getElementsByTagName("session_id").item(0).getTextContent().equals(sessionid)){                    
                    cmb_SectionNewEntryClassName.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());                    
                }           
            }
        }else{
            NodeList sList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");         
            cmb_SectionNewEntryClassName.removeAllItems();
            cmb_SectionNewEntryClassName.addItem("- Select -");
            for (int i = 0; i < sList.getLength(); i++) {       
                Node nNode = sList.item(i);            
                Element eElement = (Element)nNode;                  
                cmb_SectionNewEntryClassName.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());                    
                           
            }
        }
    }//GEN-LAST:event_cmb_SessionActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_SectionNewEntryCancel;
    private javax.swing.JButton btn_SectionNewEntrySave;
    private javax.swing.JComboBox cmb_SectionNewEntryClassName;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txt_SectionName;
    // End of variables declaration//GEN-END:variables

        void onloadReset() {
           try {
            txt_SectionName.setText(null);
            
            NodeList nList1=Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
            cmb_SectionNewEntryClassName.removeAllItems();
            cmb_SectionNewEntryClassName.addItem("- Select -");
            for (int i = 0; i < nList1.getLength(); i++){
                    Node nNode = nList1.item(i);
                    if(nNode.getNodeType()==Node.ELEMENT_NODE){
                        Element eElement = (Element) nNode;
                        cmb_SectionNewEntryClassName.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());
                    }
                }
            NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
              cmb_Session.removeAllItems();
              cmb_Session.addItem("- Select -");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);                
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                    }
                 }

        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }
   private String checkValidity() {
        if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(cmb_SectionNewEntryClassName.getSelectedItem().equals("- Select -")){
            return (" 'Class Name' should not be un-select.");
            
        }else if(txt_SectionName.getText().equals("")){
            return ("'Section Name' should not be blank");
        }
        else{
        return ("ok");
        }

    }
}
