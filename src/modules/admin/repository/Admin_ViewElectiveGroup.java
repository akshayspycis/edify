package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_ElectiveGroup;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Admin_ViewElectiveGroup extends javax.swing.JDialog {

    
    public Admin_ViewElectiveGroup(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

   
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_ElectiveUpdate = new javax.swing.JButton();
        btn_ElectiveDelete = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_ElectiveName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();
        lbl_ElectiveId = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_ElectiveUpdate.setText("Update");
        btn_ElectiveUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveUpdateActionPerformed(evt);
            }
        });

        btn_ElectiveDelete.setText("Delete");
        btn_ElectiveDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ElectiveDeleteActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Elective Group", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Elective Group Name :");

        jLabel2.setText("Session :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ElectiveName, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Session, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_ElectiveName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel2, txt_ElectiveName});

        lbl_ElectiveId.setForeground(new java.awt.Color(240, 240, 240));

        jButton1.setText("Cancel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbl_ElectiveId, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_ElectiveUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_ElectiveDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ElectiveDelete, btn_ElectiveUpdate, jButton1});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ElectiveDelete)
                    .addComponent(btn_ElectiveUpdate)
                    .addComponent(lbl_ElectiveId)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_ElectiveUpdate, lbl_ElectiveId});

        getRootPane().setDefaultButton(btn_ElectiveUpdate);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ElectiveUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveUpdateActionPerformed
       
        try {
            String str = checkValidity();
            if (str.equals("ok")){
                int i;
                int sessionindex = cmb_Session.getSelectedIndex()+1; 
                String sessionid = null;
                String electivegroupname = txt_ElectiveName.getText();   
                NodeList nList = Config.xml_config_ad_electivegroupmgr.doc.getElementsByTagName("config_ad_electivegroup");
                for(i=0; i < nList.getLength();i++){
                    Node nNode = nList.item(i);
                    if(nNode.getNodeType() == Node.ELEMENT_NODE){
                        Element eElement = (Element)nNode;                   
                        if(electivegroupname.equalsIgnoreCase(eElement.getElementsByTagName("electivegroupname").item(0).getTextContent())){
                            JOptionPane.showMessageDialog(this, "Elective Group Name is already Exist");                
                            break;                        
                        }
                    }
                }
                if(i==nList.getLength()){
                Config_Ad_Dm_ElectiveGroup group = new Config_Ad_Dm_ElectiveGroup();
                   Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno="+sessionindex+"]", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
                   sessionid = element.getAttribute("session_id");
                   group.setElectivegroup_id(lbl_ElectiveId.getText());
                   group.setSession_id(sessionid);
                   group.setElectivegroupname(txt_ElectiveName.getText());
                    if(Config.config_ad_cm_electivegroupmgr.updAd_electivegroup(group)){                
                        JOptionPane.showMessageDialog(this, "Record updated Successfully", "Successfull",JOptionPane.NO_OPTION);
                        dispose();
                    }else{
                        JOptionPane.showMessageDialog(this, "Error in updation", "Error",JOptionPane.ERROR_MESSAGE );
                    }
            }
            }else{            
                JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }//GEN-LAST:event_btn_ElectiveUpdateActionPerformed

    private void btn_ElectiveDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ElectiveDeleteActionPerformed
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure You Want to Delete Elective Name?","Warning",JOptionPane.YES_NO_OPTION);             
             if(dialogButton == JOptionPane.YES_OPTION) {
             
                if(Config.config_ad_cm_electivegroupmgr.delAd_electivegroup(lbl_ElectiveId.getText())){ 

                JOptionPane.showMessageDialog(this, "Elective Name Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                dispose();
            
                 }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                 }
             } else{
                   dispose();     
                   }
    }//GEN-LAST:event_btn_ElectiveDeleteActionPerformed

    
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
       
    }//GEN-LAST:event_closeDialog

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ElectiveDelete;
    private javax.swing.JButton btn_ElectiveUpdate;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbl_ElectiveId;
    private javax.swing.JTextField txt_ElectiveName;
    // End of variables declaration//GEN-END:variables
 public void onloadReset(String Electiveidview) {
     try {
            lbl_ElectiveId.setText(Electiveidview);
//            Element element1  = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate(feeid, null);
             NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
             cmb_Session.removeAllItems();
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                }
             }
            Element element = (Element) Config.xml_config_ad_electivegroupmgr.xpath.evaluate("//*[@electivegroup_id='"+Electiveidview+"']", Config.xml_config_ad_electivegroupmgr.doc, XPathConstants.NODE);
            
            String sessionid = element.getElementsByTagName("session_id").item(0).getTextContent();
            Element element1 = (Element) Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@session_id='"+sessionid+"']", Config.xml_config_ad_sessionmgr.doc, XPathConstants.NODE);
            String sessionname = element1.getElementsByTagName("session_name").item(0).getTextContent();
            cmb_Session.setSelectedItem(sessionname);
            
            txt_ElectiveName.setText(element.getElementsByTagName("electivegroupname").item(0).getTextContent());
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error",JOptionPane.ERROR_MESSAGE );
        }
        
        
    }

     private String checkValidity() {
        if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if (txt_ElectiveName.getText().equals("")) {
            return (" 'Elective Group Name' should not be blank.");
        }
        else{
        return ("ok");
        }
    }
}
