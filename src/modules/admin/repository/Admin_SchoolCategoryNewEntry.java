

package modules.admin.repository;

import java.awt.event.KeyEvent;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.admin.Config_Ad_Dm_SchoolCategory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Admin_SchoolCategoryNewEntry extends javax.swing.JDialog {

   
    public Admin_SchoolCategoryNewEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btn_SchoolCategorySave = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_SchoolCategoryName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cmb_Session = new javax.swing.JComboBox();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        btn_SchoolCategorySave.setText("Save");
        btn_SchoolCategorySave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SchoolCategorySaveActionPerformed(evt);
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "School Category", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Category Name :");

        jLabel4.setText("Session :");

        cmb_Session.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_SessionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_SchoolCategoryName))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel4});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmb_Session, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_SchoolCategoryName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel4, txt_SchoolCategoryName});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_SchoolCategorySave, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelButton))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SchoolCategorySave, cancelButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(btn_SchoolCategorySave))
                .addContainerGap())
        );

        getRootPane().setDefaultButton(btn_SchoolCategorySave);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_SchoolCategorySaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SchoolCategorySaveActionPerformed
          try {
            String str = checkValidity();
            if (str.equals("ok")) {
               int sessionindex = cmb_Session.getSelectedIndex(); 
                 
                 String sessionid = null;
               
                 Config_Ad_Dm_SchoolCategory schoolcat = new Config_Ad_Dm_SchoolCategory();
                 
                 Element element = (Element)Config.xml_config_ad_sessionmgr.xpath.evaluate("//*[@sno='"+sessionindex+"']", Config.xml_config_ad_sessionmgr.doc,XPathConstants.NODE);
               
                 sessionid = element.getAttribute("session_id");
                 schoolcat.setSession_id(sessionid);
                 schoolcat.setCategory_name(txt_SchoolCategoryName.getText());
                 if(Config.config_ad_cm_schoolcategorymgr.insAd_SchoolCategory(schoolcat)){
                    JOptionPane.showMessageDialog(this, "Data Inserted Successfully", "success", JOptionPane.NO_OPTION);
                 }else{
                   JOptionPane.showMessageDialog(this, "Error in Insertion", "Error",JOptionPane.ERROR_MESSAGE);
                 }
           }else{
               JOptionPane.showMessageDialog(this, str, "Error",JOptionPane.ERROR_MESSAGE);
           }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_SchoolCategorySaveActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
    }//GEN-LAST:event_closeDialog

    private void cmb_SessionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_SessionActionPerformed
        
    }//GEN-LAST:event_cmb_SessionActionPerformed
    
    

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_SchoolCategorySave;
    private javax.swing.JButton cancelButton;
    private javax.swing.JComboBox cmb_Session;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txt_SchoolCategoryName;
    // End of variables declaration//GEN-END:variables

   

public void onloadReset() {
        
       try {
              
             NodeList nList = Config.xml_config_ad_sessionmgr.doc.getElementsByTagName("config_ad_session");
              cmb_Session.removeAllItems();
              cmb_Session.addItem("- Select -");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);                
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        cmb_Session.addItem(eElement.getElementsByTagName("session_name").item(0).getTextContent().toString());
                    }
                 }
             
             
             txt_SchoolCategoryName.setText("");
         } catch (Exception e) {
             e.printStackTrace();
             
         }
         
        
    }



    private String checkValidity() {
        if (cmb_Session.getSelectedItem().equals("- Select -")) {
        return (" 'Session Name' should not be un-select.");
            
        }else if(txt_SchoolCategoryName.getText().equals("")){
            return (" 'Category Name' should not be blank.");
            
        }else{
        return ("ok");
        }
    }


}
