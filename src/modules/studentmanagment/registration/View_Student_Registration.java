package modules.studentmanagment.registration;

import java.awt.CardLayout;
import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.student.Stu_Dm_Registration;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ashok
 */
public class View_Student_Registration extends javax.swing.JDialog  {
    
    CardLayout cl ;
    int currentcard = 1;
    
    

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdf1 = new SimpleDateFormat("MMM d, yyyy");
    DateFormat formatter = new SimpleDateFormat("MMM/d/yyyy");
    SimpleDateFormat sdf2 = new SimpleDateFormat("EEEE, MMMM dd, yyyy");
    public View_Student_Registration() {
        
        initComponents();
        this.setLocationRelativeTo(null);

        // Close the dialog when Esc is pressed
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        result = new javax.swing.ButtonGroup();
        handicapped = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_Form = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_FirstName = new javax.swing.JTextField();
        txt_MidileName = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txt_FatherName = new javax.swing.JTextField();
        txt_MotherName = new javax.swing.JTextField();
        txt_LstName = new javax.swing.JTextField();
        cmb_Gender = new javax.swing.JComboBox();
        cmb_Category = new javax.swing.JComboBox();
        cmb_Nationalty = new javax.swing.JComboBox();
        dtch_date = new com.toedter.calendar.JDateChooser();
        txt_MotherOccu = new javax.swing.JTextField();
        txt_MotherInco = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        txt_MotherQuli = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        txt_FatherQuli = new javax.swing.JTextField();
        txt_FatherOcc = new javax.swing.JTextField();
        txt_FatherInco = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txt_FatherEmail = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txt_MotherEmail = new javax.swing.JTextField();
        cmb_Class = new javax.swing.JComboBox();
        txt_Religion = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_FatherMob = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txt_MotherMob = new javax.swing.JTextField();
        dtch_dob = new com.toedter.calendar.JDateChooser();
        lbl_test = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txt_Raddress = new javax.swing.JTextField();
        txt_Rcity = new javax.swing.JTextField();
        txt_Rcountry = new javax.swing.JTextField();
        txt_Rstate = new javax.swing.JTextField();
        txt_Rpincode = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txt_Rphoneno = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        chkbox_address = new javax.swing.JCheckBox();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txt_Pphone_no = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        txt_Pcity = new javax.swing.JTextField();
        txt_Pcountry = new javax.swing.JTextField();
        txt_Pstate = new javax.swing.JTextField();
        txt_Ppincode = new javax.swing.JTextField();
        txt_Paddress = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        rdbtn_pass = new javax.swing.JRadioButton();
        rdbtn_fail = new javax.swing.JRadioButton();
        txt_preSchoolname = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txt_preYear = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        txt_percentage = new javax.swing.JTextField();
        txt_boardname = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        rdbtn_yes = new javax.swing.JRadioButton();
        rdbtn_no = new javax.swing.JRadioButton();
        jLabel47 = new javax.swing.JLabel();
        txt_height = new javax.swing.JTextField();
        txt_weight = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        cmb_bloodgroup = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        txt_handi_desc = new javax.swing.JTextField();
        cmb_PClass = new javax.swing.JComboBox();
        btn_next = new javax.swing.JButton();
        txt_StuInfoCancel = new javax.swing.JButton();
        btn_back = new javax.swing.JButton();
        lbl_registration_id = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("View Student Registration Details");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(440, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(11, 11, 11))
        );

        jTabbedPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel2.setText("Form No. :");
        jLabel2.setMaximumSize(new java.awt.Dimension(101, 14));
        jLabel2.setMinimumSize(new java.awt.Dimension(101, 14));
        jLabel2.setPreferredSize(new java.awt.Dimension(101, 14));

        jLabel3.setText("Date");

        jLabel4.setText("Student Name :");

        jLabel5.setText("Father Name :");

        jLabel6.setText("DOB : ");

        jLabel7.setText("Class :");

        jLabel8.setText("Religion :");

        jLabel9.setText("Mother Name :");

        jLabel10.setText("Gender :");

        txt_FirstName.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_FirstNameCaretUpdate(evt);
            }
        });
        txt_FirstName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_FirstNameFocusGained(evt);
            }
        });

        txt_MidileName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_MidileNameActionPerformed(evt);
            }
        });
        txt_MidileName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_MidileNameFocusGained(evt);
            }
        });

        jLabel11.setText("Category :");

        jLabel12.setText("Nationality :");

        txt_LstName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_LstNameActionPerformed(evt);
            }
        });
        txt_LstName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_LstNameFocusGained(evt);
            }
        });

        cmb_Gender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Male", "Female" }));

        cmb_Category.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "General", "Obc", "Sc", "St" }));

        cmb_Nationalty.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Indian", "Other" }));

        dtch_date.setDateFormatString("dd/MM/yyyy");

        jLabel49.setText("Mother Occupation :");

        jLabel50.setText("Mother Income :");

        jLabel51.setText("Mother Qualification :");

        jLabel52.setText("Father Income :");

        jLabel53.setText("Father Occupation :");

        jLabel54.setText("Father Qualification :");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("Student Details");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Parents Details");

        jLabel16.setText("Father Email-Id :");

        jLabel17.setText("Mother Email-Id :");

        jLabel18.setText("Father Mobile No :");

        jLabel35.setText("Mother Mobile No :");

        txt_MotherMob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_MotherMobActionPerformed(evt);
            }
        });

        dtch_dob.setDateFormatString("dd/MM/yyyy");

        jLabel25.setText("Last Name :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txt_FirstName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_MidileName))
                    .addComponent(txt_FatherName)
                    .addComponent(txt_Form)
                    .addComponent(cmb_Class, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_Religion)
                    .addComponent(dtch_dob, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel25))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(dtch_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(txt_MotherName, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_LstName)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addGap(5, 5, 5)
                            .addComponent(cmb_Gender, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmb_Nationalty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmb_Category, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(241, 241, 241))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel15)
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel16)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txt_FatherEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel53)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txt_FatherOcc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel54)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txt_FatherQuli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel52)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txt_FatherInco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(14, 14, 14)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(5, 5, 5)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_MotherOccu)
                                    .addComponent(txt_MotherInco, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_MotherQuli, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addComponent(txt_MotherEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel14)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(txt_FatherMob, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14)
                                .addComponent(jLabel35)
                                .addGap(5, 5, 5)
                                .addComponent(txt_MotherMob, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lbl_test, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cmb_Category, cmb_Class, cmb_Gender, cmb_Nationalty, dtch_date, txt_FatherEmail, txt_FatherInco, txt_FatherName, txt_FatherOcc, txt_FatherQuli, txt_Form, txt_LstName, txt_MotherEmail, txt_MotherInco, txt_MotherName, txt_MotherOccu, txt_MotherQuli, txt_Religion});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel10, jLabel11, jLabel12, jLabel16, jLabel17, jLabel18, jLabel2, jLabel25, jLabel3, jLabel35, jLabel4, jLabel49, jLabel5, jLabel50, jLabel51, jLabel52, jLabel53, jLabel54, jLabel6, jLabel7, jLabel8, jLabel9});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14)
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dtch_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_Form, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_FirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_MidileName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_LstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel25))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txt_FatherName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txt_MotherName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel10)
                            .addComponent(cmb_Gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(cmb_Category, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(cmb_Class, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(cmb_Nationalty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)
                            .addComponent(txt_Religion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(jLabel15)
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel54)
                            .addComponent(txt_MotherQuli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel51)
                            .addComponent(txt_FatherQuli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel53)
                            .addComponent(txt_MotherOccu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel49)
                            .addComponent(txt_FatherOcc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel52)
                                .addComponent(txt_FatherInco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_MotherInco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel50)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(txt_FatherEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17)
                            .addComponent(txt_MotherEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(txt_FatherMob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel35)
                            .addComponent(txt_MotherMob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(dtch_dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lbl_test, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmb_Category, cmb_Class, cmb_Gender, cmb_Nationalty, dtch_date, jLabel10, jLabel11, jLabel12, jLabel16, jLabel17, jLabel18, jLabel2, jLabel25, jLabel3, jLabel35, jLabel4, jLabel49, jLabel5, jLabel50, jLabel51, jLabel52, jLabel53, jLabel54, jLabel6, jLabel7, jLabel8, jLabel9, txt_FatherEmail, txt_FatherInco, txt_FatherName, txt_FatherOcc, txt_FatherQuli, txt_FirstName, txt_Form, txt_LstName, txt_MidileName, txt_MotherEmail, txt_MotherInco, txt_MotherName, txt_MotherOccu, txt_MotherQuli, txt_Religion});

        jTabbedPane1.addTab("Student Information", jPanel3);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N
        jPanel5.setEnabled(false);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setText("Residential Address");

        jLabel20.setText("Address :");
        jLabel20.setMaximumSize(new java.awt.Dimension(101, 14));
        jLabel20.setMinimumSize(new java.awt.Dimension(101, 14));
        jLabel20.setPreferredSize(new java.awt.Dimension(101, 14));

        jLabel21.setText("City :");

        jLabel22.setText("Country :");

        jLabel23.setText("State :");

        jLabel24.setText("Pincode :");

        txt_Raddress.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_RaddressCaretUpdate(evt);
            }
        });

        txt_Rcity.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_RcityCaretUpdate(evt);
            }
        });
        txt_Rcity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_RcityActionPerformed(evt);
            }
        });

        txt_Rcountry.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_RcountryCaretUpdate(evt);
            }
        });

        txt_Rstate.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_RstateCaretUpdate(evt);
            }
        });

        txt_Rpincode.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_RpincodeCaretUpdate(evt);
            }
        });

        jLabel26.setText("Phone No. :");

        txt_Rphoneno.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_RphonenoCaretUpdate(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel27.setText("Permanent Address");

        chkbox_address.setBackground(new java.awt.Color(255, 255, 255));
        chkbox_address.setText("Same as Residential Address");
        chkbox_address.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkbox_addressActionPerformed(evt);
            }
        });

        jLabel28.setText("Pincode :");

        jLabel29.setText("State :");

        jLabel30.setText("Address :");
        jLabel30.setPreferredSize(new java.awt.Dimension(75, 14));

        jLabel32.setText("Phone No. :");

        jLabel33.setText("Country :");

        jLabel34.setText("City :");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel19)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Rcity, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel22)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Rcountry)))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Rpincode))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel23)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Rstate, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkbox_address))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_Paddress))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel34)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Pcity, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel33)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Pcountry))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel32)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Pphone_no, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Pstate, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addGap(6, 6, 6)
                                .addComponent(txt_Ppincode))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_Rphoneno, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_Raddress)))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel20, jLabel21, jLabel22, jLabel23, jLabel24, jLabel26, jLabel28, jLabel29, jLabel30, jLabel32, jLabel33, jLabel34});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19)
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Raddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jLabel23)
                    .addComponent(txt_Rcity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Rstate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jLabel24)
                    .addComponent(txt_Rcountry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Rpincode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(txt_Rphoneno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(chkbox_address))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Paddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(jLabel29)
                    .addComponent(txt_Pcity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Pstate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(txt_Pcountry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Ppincode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(txt_Pphone_no, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(150, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel20, jLabel21, jLabel22, jLabel23, jLabel24, jLabel26, jLabel28, jLabel29, jLabel30, jLabel32, jLabel33, jLabel34, txt_Raddress});

        jTabbedPane1.addTab("Contact Details", jPanel5);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N

        jLabel37.setText("Previous Class :");
        jLabel37.setMaximumSize(new java.awt.Dimension(101, 14));
        jLabel37.setMinimumSize(new java.awt.Dimension(101, 14));
        jLabel37.setPreferredSize(new java.awt.Dimension(101, 14));

        jLabel38.setText("Result :");

        jLabel39.setText("Previous School Name :");

        rdbtn_pass.setBackground(new java.awt.Color(255, 255, 255));
        result.add(rdbtn_pass);
        rdbtn_pass.setText("Pass");

        rdbtn_fail.setBackground(new java.awt.Color(255, 255, 255));
        result.add(rdbtn_fail);
        rdbtn_fail.setText("Fail");

        jLabel40.setText("Year :");

        jLabel41.setText("Percentage :");

        jLabel42.setText("Board Name :");

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel43.setText("Health Details");

        jLabel44.setText("Blood Group :");

        jLabel45.setText("Height :");

        jLabel46.setText("Handicapped :");

        rdbtn_yes.setBackground(new java.awt.Color(255, 255, 255));
        handicapped.add(rdbtn_yes);
        rdbtn_yes.setText("Yes");
        rdbtn_yes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbtn_yesActionPerformed(evt);
            }
        });

        rdbtn_no.setBackground(new java.awt.Color(255, 255, 255));
        handicapped.add(rdbtn_no);
        rdbtn_no.setText("No");

        jLabel47.setText("Weight :");

        jLabel48.setText("If Yes Description :");

        cmb_bloodgroup.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "o+", "o-", "A+", "A-", "B+", "B-", "AB+", "AB-" }));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Previous Educational Deatail");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addComponent(jLabel48)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txt_handi_desc))
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addComponent(jLabel38)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(rdbtn_pass, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(rdbtn_fail, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel39)
                                        .addComponent(jLabel43)
                                        .addComponent(jLabel44)
                                        .addComponent(jLabel46))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel6Layout.createSequentialGroup()
                                            .addComponent(rdbtn_yes, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(rdbtn_no, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(txt_preSchoolname)
                                        .addComponent(cmb_bloodgroup, 0, 185, Short.MAX_VALUE)))
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cmb_PClass, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel40)
                                .addComponent(jLabel41)
                                .addComponent(jLabel42)
                                .addComponent(jLabel45)
                                .addComponent(jLabel47))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txt_percentage)
                                .addComponent(txt_boardname)
                                .addComponent(txt_height)
                                .addComponent(txt_weight, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                                .addComponent(txt_preYear)))))
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel37, jLabel38, jLabel39, jLabel40, jLabel41, jLabel42, jLabel44, jLabel45, jLabel46, jLabel47, jLabel48});

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txt_boardname, txt_height, txt_percentage, txt_weight});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40)
                    .addComponent(txt_preYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmb_PClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(rdbtn_pass)
                    .addComponent(rdbtn_fail)
                    .addComponent(jLabel41)
                    .addComponent(txt_percentage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(txt_preSchoolname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42)
                    .addComponent(txt_boardname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jLabel43)
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44)
                    .addComponent(jLabel45)
                    .addComponent(cmb_bloodgroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_height, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46)
                    .addComponent(rdbtn_yes)
                    .addComponent(rdbtn_no)
                    .addComponent(jLabel47)
                    .addComponent(txt_weight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel48)
                    .addComponent(txt_handi_desc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(208, Short.MAX_VALUE))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel37, jLabel38, jLabel39, jLabel40, jLabel41, jLabel42, jLabel44, jLabel45, jLabel46, jLabel47, jLabel48, txt_handi_desc});

        jTabbedPane1.addTab("Previous Education / Health Details", jPanel6);

        btn_next.setText("Next");
        btn_next.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_nextActionPerformed(evt);
            }
        });

        txt_StuInfoCancel.setText("Cancel");
        txt_StuInfoCancel.setPreferredSize(new java.awt.Dimension(67, 23));
        txt_StuInfoCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_StuInfoCancelActionPerformed(evt);
            }
        });

        btn_back.setText("Back");
        btn_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_backActionPerformed(evt);
            }
        });

        lbl_registration_id.setForeground(new java.awt.Color(255, 255, 255));

        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbl_registration_id)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_back, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_next, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_StuInfoCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_back, btn_next, jButton1, txt_StuInfoCancel});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_registration_id)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(txt_StuInfoCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_next)
                        .addComponent(btn_back)))
                .addGap(16, 16, 16))
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
    
    }//GEN-LAST:event_closeDialog

    private void txt_MidileNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_MidileNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_MidileNameActionPerformed

    private void btn_nextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_nextActionPerformed
        if(evt.getSource() == btn_next) {    
        try{
        if(jTabbedPane1.getSelectedIndex() == jTabbedPane1.getTabCount()-1){
            
            jTabbedPane1.setEnabledAt(0, false);
            jTabbedPane1.setEnabledAt(1, false);
            
            Stu_Dm_Registration sr = new Stu_Dm_Registration();
            
            int classindex = cmb_Class.getSelectedIndex(); 
            String classid = null;
            Element element1 = (Element)Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno='"+classindex+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
            classid=element1.getAttribute("class_id");
//            setData();
            sr.setRegistration_id(lbl_registration_id.getText());
            sr.setForm_no(txt_Form.getText());
            sr.setDate(sdf.format(dtch_date.getDate()));
            sr.setFirstname(txt_FirstName.getText());
            sr.setMiddlename(txt_MidileName.getText());
            sr.setLastname(txt_LstName.getText());
            sr.setFathername(txt_FatherName.getText());
            sr.setMothername(txt_MotherName.getText());
            sr.setDob(sdf.format(dtch_dob.getDate()));   
            sr.setGender(cmb_Gender.getSelectedItem().toString());
            sr.setClass_id(classid);
            sr.setCategory(cmb_Category.getSelectedItem().toString());
            sr.setReligion(txt_Religion.getText());
            sr.setNationality(cmb_Nationalty.getSelectedItem().toString());
            sr.setFatherqualification(txt_FatherQuli.getText());
            sr.setFatheroccupation(txt_FatherOcc.getText());
            sr.setFatherincome(txt_FatherInco.getText());
            sr.setF_emailid(txt_FatherEmail.getText());
            sr.setMotherqualification(txt_MotherQuli.getText());
            sr.setMotheroccupation(txt_MotherOccu.getText());
            sr.setMotherincome(txt_MotherInco.getText());
            sr.setM_emailid(txt_MotherEmail.getText());
            sr.setR_address(txt_Raddress.getText());
            sr.setR_city(txt_Rcity.getText());
            sr.setR_state(txt_Rstate.getText());
            sr.setR_country(txt_Rcountry.getText());
            sr.setR_pincode(txt_Rpincode.getText());
            sr.setR_phoneno(txt_Rphoneno.getText());
            sr.setFather_mobno(txt_FatherMob.getText());
            sr.setMother_mobno(txt_MotherMob.getText());
            sr.setP_address(txt_Paddress.getText());
            sr.setP_city(txt_Pcity.getText());
            sr.setP_state(txt_Pstate.getText());
            sr.setP_country(txt_Pcountry.getText());
            sr.setP_pincode(txt_Ppincode.getText());
            sr.setP_phoneno(txt_Pphone_no.getText());
            sr.setPre_class(cmb_PClass.getSelectedItem().toString());
            sr.setPre_year(txt_preYear.getText());
            String pre_result;
            if(rdbtn_pass.isSelected()==true){
                pre_result = rdbtn_pass.getText();
            }else{
                pre_result = rdbtn_fail.getText();
            }
            sr.setPre_result(pre_result);
            sr.setPre_percentage(txt_percentage.getText());
            sr.setPre_schoolname(txt_preSchoolname.getText());
            sr.setPre_class(txt_boardname.getText());
            sr.setBloodgroup(cmb_bloodgroup.getSelectedItem().toString());
            sr.setHeight(txt_height.getText());
            sr.setWeight(txt_weight.getText());
            String handi;
            if(rdbtn_yes.isSelected()==true){
                handi = rdbtn_yes.getText();
            }else{
                handi = rdbtn_no.getText();
            }
            sr.setHandicapped_status(handi);
            sr.setHandicapped_description(txt_handi_desc.getText());
            
            if (Config.stu_cm_registration.upd_StudentRegistration(sr)) {
                JOptionPane.showMessageDialog(this, "Data Updated Successfully", "success", JOptionPane.NO_OPTION);
            }
            else{
                JOptionPane.showMessageDialog(this, "Error in Updation", "Error",JOptionPane.ERROR_MESSAGE);
            }
//            jTabbedPane1.setSelectedIndex(0);
        }
        else if(jTabbedPane1.getSelectedIndex() == jTabbedPane1.getTabCount()-2){
            if (txt_Raddress.getText().equals("") ) {
            JOptionPane.showMessageDialog(this,"Please Enter 'Address.'. ","Error",JOptionPane.ERROR_MESSAGE);
            }else if (txt_Rcity.getText().equals("") ) {
            JOptionPane.showMessageDialog(this,"Please Enter 'City.'. ","Error",JOptionPane.ERROR_MESSAGE);
            }else{
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex()+1);
            
            btn_next.setText("Update");
            jTabbedPane1.setEnabledAt(2, true);
            jTabbedPane1.setEnabledAt(0, false);
            jTabbedPane1.setEnabledAt(1, false);
            }

        }else if (txt_Form.getText().equals("") ) {
            JOptionPane.showMessageDialog(this,"Please Enter 'Form No.'. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_Form.requestFocus();
        }else if (dtch_date.getDate() == null) {
            JOptionPane.showMessageDialog(this,"Please Select 'Date'. ","Error",JOptionPane.ERROR_MESSAGE);  
            dtch_date.requestFocus();
        }else if (txt_FirstName.getText().equals("") ) {
            JOptionPane.showMessageDialog(this,"Please Enter 'First Name.'. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_FirstName.requestFocus();
        }else if (txt_LstName.getText().equals("") ) {
            JOptionPane.showMessageDialog(this,"Please Enter 'Last Name.'. ","Error",JOptionPane.ERROR_MESSAGE);
        }else if (txt_FatherName.getText().equals("") ) {
            JOptionPane.showMessageDialog(this,"Please Enter 'Father Name.'. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_FatherName.requestFocus();
        }else if (txt_MotherName.getText().equals("") ) {
            JOptionPane.showMessageDialog(this,"Please Enter 'Mother Name.'. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_MotherName.requestFocus();
        }else if (dtch_dob.getDate() == null) {
            JOptionPane.showMessageDialog(this,"Please Select 'Date of Birth'. ","Error",JOptionPane.ERROR_MESSAGE);  
            dtch_date.requestFocus();
        }else if (cmb_Class.getSelectedIndex()== 0) {
            JOptionPane.showMessageDialog(this,"Please Select 'Class'. ","Error",JOptionPane.ERROR_MESSAGE);       
        }else if ((!txt_FatherEmail.getText().equals(""))&&(!txt_FatherEmail.getText().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") )) {
            JOptionPane.showMessageDialog(this,"Please Enter Valid 'Email ID. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_FatherEmail.requestFocus();
        }else if ((!txt_FatherEmail.getText().equals(""))&&(!txt_MotherEmail.getText().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") )) {
            JOptionPane.showMessageDialog(this,"Please Enter Valid 'Email ID. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_MotherEmail.requestFocus();
        }else if (txt_FatherMob.getText().equals("") || (!txt_FatherMob.getText().matches("[0-9]*")) ) {
            JOptionPane.showMessageDialog(this,"Please Enter Valid 'Father Mobile No. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_FatherMob.requestFocus();
        }else if (!txt_MotherMob.getText().matches("[0-9]*")) {
            JOptionPane.showMessageDialog(this,"Please Enter Valid 'Mother Mobile No. ","Error",JOptionPane.ERROR_MESSAGE);
            txt_MotherMob.requestFocus();
        
        }else{           
            
            jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex()+1);
            
//            jTabbedPane1.setForegroundAt(1, Color.red);
            jTabbedPane1.setEnabledAt(1, true);
            jTabbedPane1.setEnabledAt(0, false);
            
            jTabbedPane1.setEnabledAt(2, false); 
        }
       
        
        }catch(Exception e){
            e.printStackTrace();
                }

  }

    }//GEN-LAST:event_btn_nextActionPerformed

    private void txt_MotherMobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_MotherMobActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_MotherMobActionPerformed

    private void txt_RcityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_RcityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_RcityActionPerformed

    private void rdbtn_yesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbtn_yesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbtn_yesActionPerformed

    private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_backActionPerformed
//        if(evt.getSource() == btn_back) {

     // check if we are at zero //

    if(jTabbedPane1.getSelectedIndex() ==0){
//        jTabbedPane1.setSelectedIndex( jTabbedPane1.getTabCount() - 1);
    }
    else if(jTabbedPane1.getSelectedIndex() ==1){
        jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() -1);
        jTabbedPane1.setEnabledAt(0, true);
        jTabbedPane1.setEnabledAt(1, false);
        jTabbedPane1.setEnabledAt(2, false);
    }
    else if((jTabbedPane1.getSelectedIndex() == jTabbedPane1.getTabCount()-1)){
        btn_next.setText("Next");
        jTabbedPane1.setSelectedIndex(jTabbedPane1.getSelectedIndex() -1);
        jTabbedPane1.setEnabledAt(1, true);
        jTabbedPane1.setEnabledAt(0, false);
        jTabbedPane1.setEnabledAt(2, false);
        
    }
    
    }//GEN-LAST:event_btn_backActionPerformed

    private void txt_StuInfoCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_StuInfoCancelActionPerformed
        dispose();
    }//GEN-LAST:event_txt_StuInfoCancelActionPerformed

    private void txt_FirstNameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_FirstNameCaretUpdate
                
    }//GEN-LAST:event_txt_FirstNameCaretUpdate

    private void txt_FirstNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_FirstNameFocusGained
        txt_FirstName.setText("");
        txt_FirstName.setForeground(Color.black);
    }//GEN-LAST:event_txt_FirstNameFocusGained

    private void txt_MidileNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_MidileNameFocusGained
        txt_MidileName.setText("");
        txt_MidileName.setForeground(Color.black);
    }//GEN-LAST:event_txt_MidileNameFocusGained

    private void txt_LstNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_LstNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_LstNameActionPerformed

    private void txt_LstNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_LstNameFocusGained
        txt_LstName.setText("");
        txt_LstName.setForeground(Color.black);
    }//GEN-LAST:event_txt_LstNameFocusGained

    private void chkbox_addressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkbox_addressActionPerformed
        if(chkbox_address.isSelected()==true){
           txt_Paddress.setText(txt_Raddress.getText());
           txt_Pcity.setText(txt_Rcity.getText());
           txt_Pstate.setText(txt_Rstate.getText());
           txt_Pcountry.setText(txt_Rcountry.getText());
           txt_Ppincode.setText(txt_Rpincode.getText());
           txt_Pphone_no.setText(txt_Rphoneno.getText());
        }else{           
           txt_Paddress.setText(null);
           txt_Pcity.setText(null);
           txt_Pstate.setText(null);
           txt_Pcountry.setText(null);
           txt_Ppincode.setText(null);
           txt_Pphone_no.setText(null);        }
    }//GEN-LAST:event_chkbox_addressActionPerformed

    private void txt_RaddressCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_RaddressCaretUpdate
        if(chkbox_address.isSelected()==true){
            txt_Paddress.setText(txt_Raddress.getText());
        }
            
    }//GEN-LAST:event_txt_RaddressCaretUpdate

    private void txt_RcityCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_RcityCaretUpdate
        if(chkbox_address.isSelected()==true){
            txt_Pcity.setText(txt_Rcity.getText());
        }
    }//GEN-LAST:event_txt_RcityCaretUpdate

    private void txt_RstateCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_RstateCaretUpdate
        if(chkbox_address.isSelected()==true){
            txt_Pstate.setText(txt_Rstate.getText());
        }
    }//GEN-LAST:event_txt_RstateCaretUpdate

    private void txt_RcountryCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_RcountryCaretUpdate
        if(chkbox_address.isSelected()==true){
            txt_Pcountry.setText(txt_Rcountry.getText());
        }
    }//GEN-LAST:event_txt_RcountryCaretUpdate

    private void txt_RpincodeCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_RpincodeCaretUpdate
        if(chkbox_address.isSelected()==true){
            txt_Ppincode.setText(txt_Rpincode.getText());
        }
    }//GEN-LAST:event_txt_RpincodeCaretUpdate

    private void txt_RphonenoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_RphonenoCaretUpdate
        if(chkbox_address.isSelected()==true){
            txt_Pphone_no.setText(txt_Rphoneno.getText());
        }
    }//GEN-LAST:event_txt_RphonenoCaretUpdate

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//        String str = checkValidity();
//        if (str.equals("ok")){
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure,you want to delete this record ? ","Warning",JOptionPane.YES_NO_OPTION);             
            if(dialogButton == JOptionPane.YES_OPTION) {             
                if(Config.stu_cm_registration.del_StudentRegistration(lbl_registration_id.getText())){ 
                    JOptionPane.showMessageDialog(this, "Recorded Deleted Successfully", "Success",JOptionPane.NO_OPTION);
                    dispose();            
                }else{
                    JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
                    }
            }else{                
                dispose();     
                }
//            }else{            
//                JOptionPane.showMessageDialog(this, str+"Sholud not be blank", "Error",JOptionPane.ERROR_MESSAGE );
//                }
    }//GEN-LAST:event_jButton1ActionPerformed
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(View_Student_Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(View_Student_Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(View_Student_Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(View_Student_Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
       
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back;
    private javax.swing.JButton btn_next;
    private javax.swing.JCheckBox chkbox_address;
    private javax.swing.JComboBox cmb_Category;
    private javax.swing.JComboBox cmb_Class;
    private javax.swing.JComboBox cmb_Gender;
    private javax.swing.JComboBox cmb_Nationalty;
    private javax.swing.JComboBox cmb_PClass;
    private javax.swing.JComboBox cmb_bloodgroup;
    private com.toedter.calendar.JDateChooser dtch_date;
    private com.toedter.calendar.JDateChooser dtch_dob;
    private javax.swing.ButtonGroup handicapped;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbl_registration_id;
    private javax.swing.JLabel lbl_test;
    private javax.swing.JRadioButton rdbtn_fail;
    private javax.swing.JRadioButton rdbtn_no;
    private javax.swing.JRadioButton rdbtn_pass;
    private javax.swing.JRadioButton rdbtn_yes;
    private javax.swing.ButtonGroup result;
    private javax.swing.JTextField txt_FatherEmail;
    private javax.swing.JTextField txt_FatherInco;
    private javax.swing.JTextField txt_FatherMob;
    private javax.swing.JTextField txt_FatherName;
    private javax.swing.JTextField txt_FatherOcc;
    private javax.swing.JTextField txt_FatherQuli;
    private javax.swing.JTextField txt_FirstName;
    private javax.swing.JTextField txt_Form;
    private javax.swing.JTextField txt_LstName;
    private javax.swing.JTextField txt_MidileName;
    private javax.swing.JTextField txt_MotherEmail;
    private javax.swing.JTextField txt_MotherInco;
    private javax.swing.JTextField txt_MotherMob;
    private javax.swing.JTextField txt_MotherName;
    private javax.swing.JTextField txt_MotherOccu;
    private javax.swing.JTextField txt_MotherQuli;
    private javax.swing.JTextField txt_Paddress;
    private javax.swing.JTextField txt_Pcity;
    private javax.swing.JTextField txt_Pcountry;
    private javax.swing.JTextField txt_Pphone_no;
    private javax.swing.JTextField txt_Ppincode;
    private javax.swing.JTextField txt_Pstate;
    private javax.swing.JTextField txt_Raddress;
    private javax.swing.JTextField txt_Rcity;
    private javax.swing.JTextField txt_Rcountry;
    private javax.swing.JTextField txt_Religion;
    private javax.swing.JTextField txt_Rphoneno;
    private javax.swing.JTextField txt_Rpincode;
    private javax.swing.JTextField txt_Rstate;
    private javax.swing.JButton txt_StuInfoCancel;
    private javax.swing.JTextField txt_boardname;
    private javax.swing.JTextField txt_handi_desc;
    private javax.swing.JTextField txt_height;
    private javax.swing.JTextField txt_percentage;
    private javax.swing.JTextField txt_preSchoolname;
    private javax.swing.JTextField txt_preYear;
    private javax.swing.JTextField txt_weight;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {
        
        jTabbedPane1.setEnabledAt(0, true);
        jTabbedPane1.setSelectedIndex(0);
        btn_next.setText("Next");
        jTabbedPane1.setEnabledAt(1, false);
        jTabbedPane1.setEnabledAt(2, false);
        
//        dtch_date.setDate(sdf.format(cal.getTime));
        NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
            cmb_Class.removeAllItems();   
            cmb_Class.addItem("-Select-");
            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Class.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());
                    
                }
             }
        
         
        
        txt_Form.setText("");
        dtch_date.setDate(null);
//        txt_FirstName.setText("");
//        txt_MidileName.setText("");
//        txt_LstName.setText("");
        txt_FatherName.setText("");
        txt_MotherName.setText("");
        dtch_dob.setDate(null);
        cmb_Class.setSelectedIndex(0);
        cmb_Category.setSelectedIndex(0);
        txt_Religion.setText("");
        txt_FatherQuli.setText("");
        txt_MotherQuli.setText("");
        txt_FatherOcc.setText("");
        txt_MotherOccu.setText("");
        txt_FatherInco.setText("");
        txt_MotherInco.setText("");
        txt_FatherEmail.setText("");
        txt_MotherEmail.setText("");
        txt_FatherMob.setText("");
        txt_MotherMob.setText("");
    }
    


    public void onloadReset(String regisid) {
        jTabbedPane1.setEnabledAt(0, true);
        jTabbedPane1.setSelectedIndex(0);
        btn_next.setText("Next");
        jTabbedPane1.setEnabledAt(1, false);
        jTabbedPane1.setEnabledAt(2, false);
        lbl_registration_id.setText(regisid);
        
        NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
            cmb_Class.removeAllItems();   
            cmb_Class.addItem("-Select-");
            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_Class.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());                    
                }
            }
            rdbtn_pass.setSelected(true);
            rdbtn_yes.setSelected(true);
        try {
            Config.sql = "select * from stu_registration where registration_id= '"+regisid+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            String classname = null;
            while(Config.rs.next()){
                String classid = Config.rs.getString("class_id");                    
                    Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                    classname = element1.getElementsByTagName("class_name").item(0).getTextContent();
        
                    Date date = sdf.parse(Config.rs.getString("date"));                    
                    Date dob = sdf.parse(Config.rs.getString("dob"));
                    txt_Form.setText(Config.rs.getString("form_no"));
                    dtch_date.setDate(date);
                    txt_FirstName.setText(Config.rs.getString("firstname"));
                    txt_MidileName.setText(Config.rs.getString("middlename"));
                    txt_LstName.setText(Config.rs.getString("lastname"));
                    txt_FatherName.setText(Config.rs.getString("fathername"));
                    txt_MotherName.setText(Config.rs.getString("mothername"));
                    dtch_dob.setDate(dob);
                    cmb_Gender.setSelectedItem(Config.rs.getString("gender"));
                    cmb_Class.setSelectedItem(classname);
                    cmb_Category.setSelectedItem(Config.rs.getString("category"));
                    txt_Religion.setText(Config.rs.getString("religion"));
                    cmb_Nationalty.setSelectedItem(Config.rs.getString("nationality"));
                    txt_FatherQuli.setText(Config.rs.getString("fatherqualification"));
                    txt_FatherOcc.setText(Config.rs.getString("fatheroccupation"));
                    txt_FatherInco.setText(Config.rs.getString("fatherincome"));
                    txt_FatherEmail.setText(Config.rs.getString("f_emailid"));
                    txt_MotherQuli.setText(Config.rs.getString("motherqualification"));
                    txt_MotherOccu.setText(Config.rs.getString("motheroccupation"));
                    txt_MotherInco.setText(Config.rs.getString("motherincome"));
                    txt_MotherEmail.setText(Config.rs.getString("m_emailid"));
                    txt_Raddress.setText(Config.rs.getString("r_address"));
                    txt_Rcity.setText(Config.rs.getString("r_city"));
                    txt_Rstate.setText(Config.rs.getString("r_state"));
                    txt_Rcountry.setText(Config.rs.getString("r_country"));
                    txt_Rpincode.setText(Config.rs.getString("r_pincode"));
                    txt_Rphoneno.setText(Config.rs.getString("r_phoneno"));                    
                    txt_FatherMob.setText(Config.rs.getString("father_mobno"));
                    txt_MotherMob.setText(Config.rs.getString("mother_mobno"));
                    txt_Paddress.setText(Config.rs.getString("p_address"));
                    txt_Pcity.setText(Config.rs.getString("p_city"));
                    txt_Pstate.setText(Config.rs.getString("p_state"));
                    txt_Pcountry.setText(Config.rs.getString("p_country"));
                    txt_Ppincode.setText(Config.rs.getString("p_pincode"));
                    txt_Pphone_no.setText(Config.rs.getString("p_phoneno"));
                    cmb_PClass.setSelectedItem(Config.rs.getString("pre_class"));
                    txt_preYear.setText(Config.rs.getString("pre_year"));                    
                    
                    
                    if(Config.rs.getString("pre_result").equalsIgnoreCase("Pass")){
                        System.out.println("True");
                        rdbtn_pass.setSelected(true);
                    }else{
                        rdbtn_fail.setSelected(true);
                        System.out.println("False");
                    }
                    txt_percentage.setText(Config.rs.getString("pre_percentage"));
                    txt_preSchoolname.setText(Config.rs.getString("pre_schoolname"));
                    txt_boardname.setText(Config.rs.getString("pre_boardname"));
                    cmb_bloodgroup.setSelectedItem(Config.rs.getString("bloodgroup"));
//                    if(Config.rs.getString("handicapped_status").equals("Pass")){
//                        rdbtn_yes.setSelected(true);
//                    }else{
//                        rdbtn_no.setSelected(false);
//                    }
                    txt_height.setText(Config.rs.getString("height"));
                    txt_weight.setText(Config.rs.getString("weight"));
                    txt_handi_desc.setText(Config.rs.getString("handicapped_description"));
                    
                    
                    
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
           
}
