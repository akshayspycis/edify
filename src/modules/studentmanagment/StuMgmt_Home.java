
package modules.studentmanagment;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Ashok
 */
public class StuMgmt_Home extends javax.swing.JDialog {
    DefaultTableModel tbl_StuRegistration_model;
    DefaultTableModel tbl_StuAdmission_model;
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public StuMgmt_Home() {
        initComponents();
        Dimension scrnsize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Double(scrnsize.getWidth()).intValue()-150, new Double(scrnsize.getHeight()).intValue()-100);
        setLocationRelativeTo(null);
        
        tbl_StuRegistration_model = (DefaultTableModel)tbl_StuRegistration.getModel();
        tbl_StuAdmission_model = (DefaultTableModel) tbl_StuAdmission.getModel();

        // Close the dialog when Esc is pressed
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        lbl_StuRegistrationName = new javax.swing.JLabel();
        txt_StuName = new javax.swing.JTextField();
        btn_searchRegistration = new javax.swing.JButton();
        btn_StuRegistrationEntry = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        dtch_RegisDate = new com.toedter.calendar.JDateChooser();
        lbl_StuRegistrationClass = new javax.swing.JLabel();
        cmb_StuRegistrationClass = new javax.swing.JComboBox();
        cmb_StuRegistrationDate = new javax.swing.JComboBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_StuRegistration = new javax.swing.JTable();
        lbl_StuRegistrationTotal = new javax.swing.JLabel();
        txt_StuRegistrationTotal = new javax.swing.JTextField();
        btn_StuRegistrationRefresh = new javax.swing.JButton();
        btn_StuRegistrationView = new javax.swing.JButton();
        btn_StuRegistrationDelete = new javax.swing.JButton();
        btn_StuRegistrationExport = new javax.swing.JButton();
        btn_StuRegistrationCancel = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        lbl_AdmissionName = new javax.swing.JLabel();
        txt_AdmissionName = new javax.swing.JTextField();
        btn_AdmissionSearch = new javax.swing.JButton();
        btn_AdmissionEntry = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        dtch_admissionDate = new com.toedter.calendar.JDateChooser();
        lbl_AdmissionClass = new javax.swing.JLabel();
        cmb_AdmissionClass = new javax.swing.JComboBox();
        lbl_AdmissionSection = new javax.swing.JLabel();
        cmb_Section = new javax.swing.JComboBox();
        cmb_admission = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_StuAdmission = new javax.swing.JTable();
        lbl_AdmissionTotal = new javax.swing.JLabel();
        txt_AdmissionTotal = new javax.swing.JTextField();
        btn_AdmissionRefresh = new javax.swing.JButton();
        btn_viewAdmission = new javax.swing.JButton();
        btn_AdmissionDelete = new javax.swing.JButton();
        btn_AdmissionExport = new javax.swing.JButton();
        btn_AdmissionCancel = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Student Managment");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(11, 11, 11))
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        lbl_StuRegistrationName.setText("Name :");

        txt_StuName.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txt_StuName.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_StuNameCaretUpdate(evt);
            }
        });

        btn_searchRegistration.setText("Search");
        btn_searchRegistration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchRegistrationActionPerformed(evt);
            }
        });

        btn_StuRegistrationEntry.setText("New Entry");
        btn_StuRegistrationEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationEntryActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        dtch_RegisDate.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtch_RegisDatePropertyChange(evt);
            }
        });

        lbl_StuRegistrationClass.setText("Class :");

        cmb_StuRegistrationClass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_StuRegistrationClassActionPerformed(evt);
            }
        });

        cmb_StuRegistrationDate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Registration Date", "DOB" }));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_StuRegistrationClass)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_StuRegistrationClass, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl_StuRegistrationName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_StuName, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_StuRegistrationDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dtch_RegisDate, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_searchRegistration)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_StuRegistrationEntry)
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_StuRegistrationEntry, btn_searchRegistration});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_StuRegistrationEntry)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_StuRegistrationName, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_StuName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_StuRegistrationClass)
                        .addComponent(cmb_StuRegistrationClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmb_StuRegistrationDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_searchRegistration)
                    .addComponent(dtch_RegisDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_StuRegistrationEntry, btn_searchRegistration, cmb_StuRegistrationClass, cmb_StuRegistrationDate, dtch_RegisDate, lbl_StuRegistrationClass, txt_StuName});

        tbl_StuRegistration.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Registration ID", "S.No.", "Form No.", "Student Name", "Father Name", "DOB", "Class", "Registration Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_StuRegistration.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tbl_StuRegistration.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_StuRegistration.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_StuRegistrationMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_StuRegistration);
        if (tbl_StuRegistration.getColumnModel().getColumnCount() > 0) {
            tbl_StuRegistration.getColumnModel().getColumn(0).setResizable(false);
            tbl_StuRegistration.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_StuRegistration.getColumnModel().getColumn(1).setResizable(false);
            tbl_StuRegistration.getColumnModel().getColumn(2).setResizable(false);
            tbl_StuRegistration.getColumnModel().getColumn(3).setResizable(false);
            tbl_StuRegistration.getColumnModel().getColumn(4).setResizable(false);
            tbl_StuRegistration.getColumnModel().getColumn(5).setResizable(false);
            tbl_StuRegistration.getColumnModel().getColumn(6).setResizable(false);
            tbl_StuRegistration.getColumnModel().getColumn(7).setResizable(false);
        }

        lbl_StuRegistrationTotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_StuRegistrationTotal.setText("Total Count :");

        btn_StuRegistrationRefresh.setText("Refresh");
        btn_StuRegistrationRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationRefreshActionPerformed(evt);
            }
        });

        btn_StuRegistrationView.setText("View");
        btn_StuRegistrationView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationViewActionPerformed(evt);
            }
        });

        btn_StuRegistrationDelete.setLabel("Delete");
        btn_StuRegistrationDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationDeleteActionPerformed(evt);
            }
        });

        btn_StuRegistrationExport.setLabel("Export");
        btn_StuRegistrationExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationExportActionPerformed(evt);
            }
        });

        btn_StuRegistrationCancel.setLabel("Cancel");
        btn_StuRegistrationCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lbl_StuRegistrationTotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_StuRegistrationTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_StuRegistrationRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StuRegistrationView, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StuRegistrationDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StuRegistrationExport, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StuRegistrationCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_StuRegistrationCancel, btn_StuRegistrationDelete, btn_StuRegistrationExport, btn_StuRegistrationRefresh, btn_StuRegistrationView});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_StuRegistrationTotal)
                    .addComponent(txt_StuRegistrationTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_StuRegistrationView)
                    .addComponent(btn_StuRegistrationDelete)
                    .addComponent(btn_StuRegistrationExport)
                    .addComponent(btn_StuRegistrationCancel)
                    .addComponent(btn_StuRegistrationRefresh))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_StuRegistrationCancel, btn_StuRegistrationDelete, btn_StuRegistrationExport, btn_StuRegistrationRefresh, btn_StuRegistrationView, lbl_StuRegistrationTotal, txt_StuRegistrationTotal});

        jTabbedPane1.addTab("Student Registration", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        lbl_AdmissionName.setText("Name :");

        txt_AdmissionName.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_AdmissionNameCaretUpdate(evt);
            }
        });

        btn_AdmissionSearch.setText("Search");
        btn_AdmissionSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AdmissionSearchActionPerformed(evt);
            }
        });

        btn_AdmissionEntry.setText("New Admission");
        btn_AdmissionEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AdmissionEntryActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        lbl_AdmissionClass.setText("Class :");

        cmb_AdmissionClass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_AdmissionClassActionPerformed(evt);
            }
        });

        lbl_AdmissionSection.setText("Section :");

        cmb_admission.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Admission Date", "DOB" }));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_AdmissionClass)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_AdmissionClass, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl_AdmissionSection)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_Section, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl_AdmissionName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_AdmissionName, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_admission, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dtch_admissionDate, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_AdmissionSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_AdmissionEntry)
                .addContainerGap())
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_AdmissionEntry, btn_AdmissionSearch});

        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_AdmissionEntry)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_AdmissionName, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_AdmissionName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_AdmissionClass)
                        .addComponent(cmb_AdmissionClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_AdmissionSection)
                        .addComponent(cmb_Section, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmb_admission, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_AdmissionSearch)
                    .addComponent(dtch_admissionDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_AdmissionEntry, btn_AdmissionSearch, cmb_AdmissionClass, cmb_Section, cmb_admission, dtch_admissionDate, jSeparator3, lbl_AdmissionClass, lbl_AdmissionName, lbl_AdmissionSection, txt_AdmissionName});

        tbl_StuAdmission.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Admission_Id", "SNo", "Registration No.", "Child SSMID", "Student Name", "Father Name", "DOB", "Class", "Section ", "Admission Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_StuAdmission.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tbl_StuAdmission.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_StuAdmission.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_StuAdmissionMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_StuAdmission);
        if (tbl_StuAdmission.getColumnModel().getColumnCount() > 0) {
            tbl_StuAdmission.getColumnModel().getColumn(0).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_StuAdmission.getColumnModel().getColumn(1).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(2).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(3).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(4).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(5).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(6).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(7).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(8).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(9).setResizable(false);
        }

        lbl_AdmissionTotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_AdmissionTotal.setText("Total Count :");

        btn_AdmissionRefresh.setText("Refresh");
        btn_AdmissionRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AdmissionRefreshActionPerformed(evt);
            }
        });

        btn_viewAdmission.setText("View");
        btn_viewAdmission.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewAdmissionActionPerformed(evt);
            }
        });

        btn_AdmissionDelete.setLabel("Delete");
        btn_AdmissionDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AdmissionDeleteActionPerformed(evt);
            }
        });

        btn_AdmissionExport.setLabel("Export");
        btn_AdmissionExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AdmissionExportActionPerformed(evt);
            }
        });

        btn_AdmissionCancel.setLabel("Cancel");
        btn_AdmissionCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AdmissionCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lbl_AdmissionTotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_AdmissionTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_AdmissionRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_viewAdmission, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_AdmissionDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_AdmissionExport, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_AdmissionCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_AdmissionCancel, btn_AdmissionDelete, btn_AdmissionExport, btn_AdmissionRefresh, btn_viewAdmission});

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_AdmissionTotal)
                    .addComponent(txt_AdmissionTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_viewAdmission)
                    .addComponent(btn_AdmissionDelete)
                    .addComponent(btn_AdmissionExport)
                    .addComponent(btn_AdmissionCancel)
                    .addComponent(btn_AdmissionRefresh))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_AdmissionCancel, btn_AdmissionDelete, btn_AdmissionExport, btn_AdmissionRefresh, btn_viewAdmission, lbl_AdmissionTotal, txt_AdmissionTotal});

        jTabbedPane1.addTab("Admission", jPanel4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog

    }//GEN-LAST:event_closeDialog

    private void txt_StuNameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_StuNameCaretUpdate
        btn_searchRegistrationActionPerformed(null);
      
    }//GEN-LAST:event_txt_StuNameCaretUpdate

    private void btn_searchRegistrationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchRegistrationActionPerformed
        try {  
            String classid = null;
            try {
               Element element = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno='"+cmb_StuRegistrationClass.getSelectedIndex()+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE); 
               classid = element.getAttribute("class_id"); 
            } catch (Exception e) {
            }
                        
            String text=txt_StuName.getText().trim().toUpperCase()+"%";
            String sql = "";
            
            if(cmb_StuRegistrationDate.getSelectedItem().equals("Registration Date") && (dtch_RegisDate.getDate()!= null)){
                if(cmb_StuRegistrationClass.getSelectedIndex()>0 ){ 
                    if((!txt_StuName.getText().equals(""))){
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"' and  r.firstname like '"+text+"' and r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                    else{
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"'  and r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                }else if(!txt_StuName.getText().equals("")){
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where   r.firstname like '"+text+"' and r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                }else {
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where  r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                }   
            }else if(cmb_StuRegistrationDate.getSelectedItem().equals("DOB") && (dtch_RegisDate.getDate()!= null)){
                if(cmb_StuRegistrationClass.getSelectedIndex()>0 ){ 
                    if((!txt_StuName.getText().equals(""))){
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"' and  r.firstname like '"+text+"' and r.dob = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                    else{
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"'  and r.dob = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                }else {
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where  r.dob = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                }
            
            }else if(cmb_StuRegistrationClass.getSelectedIndex()>0){
                if(!txt_StuName.getText().equals("")) {
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"' and  r.firstname like '"+text+"'";                   
                     
                }else{
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"'" ;
                    
                }
            
            
            }else if(!txt_StuName.getText().equals("")){
                sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where  r.firstname like '"+text+"'";
            
            }else{
                sql = "select registration_id, form_no, firstname, fathername, dob , class_id, date from stu_registration " ;
            }
            setRegDetails(sql);  
            int b = tbl_StuRegistration_model.getRowCount();
             txt_StuRegistrationTotal.setText(Integer.toString(b));
                
                }catch(Exception e){
            e.printStackTrace();
        }
        
        
    }//GEN-LAST:event_btn_searchRegistrationActionPerformed

    private void btn_StuRegistrationEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationEntryActionPerformed
        // TODO add your handling code here:
        Config.student_registration.onloadReset();
        Config.student_registration.setVisible(true);
        
    }//GEN-LAST:event_btn_StuRegistrationEntryActionPerformed

    private void txt_AdmissionNameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_AdmissionNameCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_AdmissionNameCaretUpdate

    private void btn_AdmissionSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AdmissionSearchActionPerformed
        try { 
            String classid = null;
            String sectionid=null;
            try {
                Element element = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno='"+cmb_AdmissionClass.getSelectedIndex()+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE); 
                classid = element.getAttribute("class_id");
//                System.out.println("ClassId="+classid);       

                Element element1 = (Element) Config.xml_config_ad_classsectionmgr.xpath.evaluate("//*[@sno='"+cmb_Section.getSelectedIndex()+"']", Config.xml_config_ad_classsectionmgr.doc, XPathConstants.NODE); 
                sectionid = element1.getAttribute("section_id");
            } catch (Exception e) {
            }
            
            String text=txt_AdmissionName.getText().trim().toUpperCase()+"%";
            String sql = "";
            
            if(cmb_admission.getSelectedItem().equals("DOB") && (dtch_admissionDate.getDate()!= null)){
                if(cmb_AdmissionClass.getSelectedIndex()>0 ){ 
                    if(cmb_Section.getSelectedIndex()>0){                        
                        if((!txt_AdmissionName.getText().equals(""))){
                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' and  r.firstname like '"+text+"' and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'";
                        }else{
                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"'  and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'" ;
                        }
                    }else if(!txt_AdmissionName.getText().equals("")){
                        sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' and  r.firstname like '"+text+"' and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"'";
                    }else{
                        sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"'  and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"'";
                    }
                  
//                }else if(cmb_Section.getSelectedIndex()>0){                       
//                        if((!txt_AdmissionName.getText().equals(""))){
//                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where   r.firstname like '"+text+"' and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'";
//                        }else{
//                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where  r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'" ;
//                        }
                }else if(!txt_AdmissionName.getText().equals("")){
                    sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.firstname like '"+text+"' and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"'";
                }else {
                    sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where  r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"' ";
                }   
            }else if(cmb_admission.getSelectedItem().equals("Admission Date") && (dtch_admissionDate.getDate()!= null)){
                if(cmb_AdmissionClass.getSelectedIndex()>0 ){ 
                    if(cmb_Section.getSelectedIndex()>0){                        
                        if((!txt_AdmissionName.getText().equals(""))){
                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' and  r.firstname like '"+text+"' and a.addmission_date = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'";
                        }else{
                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"'  and a.addmission_date = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'" ;
                        }
                    }else if(!txt_AdmissionName.getText().equals("")){
                        sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' and  r.firstname like '"+text+"' and a.addmission_date = '"+sdf.format(dtch_admissionDate.getDate())+"'";
                    }else{
                        sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"'  and a.addmission_date = '"+sdf.format(dtch_admissionDate.getDate())+"'";
                    }
                  
//                }else if(cmb_Section.getSelectedIndex()>0){                        
//                        if((!txt_AdmissionName.getText().equals(""))){
//                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' and  r.firstname like '"+text+"' and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'";
//                        }else{
//                            sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"'  and r.dob = '"+sdf.format(dtch_admissionDate.getDate())+"' and a.section_id= '"+sectionid+"'" ;
//                        }
                }else if(!txt_AdmissionName.getText().equals("")){
                    sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.firstname like '"+text+"'";
                }else {
                    sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where  a.addmission_date = '"+sdf.format(dtch_admissionDate.getDate())+"' ";
                }
            
            }else if(cmb_AdmissionClass.getSelectedIndex()>0){
                if(cmb_Section.getSelectedIndex()>0){                        
                    if((!txt_AdmissionName.getText().equals(""))){
                        sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' and  r.firstname like '"+text+"'  and a.section_id= '"+sectionid+"'";
                    }else{
                        sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"'   and a.section_id= '"+sectionid+"'" ;
                    }
                }
                else if(!txt_AdmissionName.getText().equals("")) {
                    sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' and r.firstname like '"+text+"'  " ;
                     
                }else{
                    sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where r.class_id= '"+classid+"' " ;
                    
                }
            
            
            }else if(!txt_AdmissionName.getText().equals("")){
                sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id  where  r.firstname like '"+text+"'";
            
            }else{
                sql = "select a.admission_id,a.registration_id, r.firstname,r.fathername,r.dob ,r.class_id,a.addmission_date,a.ssm_id,a.section_id from stu_admission a inner join stu_registration r on a.registration_id = r.registration_id " ;
            }           
              setAdmsionDetails(sql);
             int c = tbl_StuAdmission_model.getRowCount();
             txt_AdmissionTotal.setText(Integer.toString(c));
        }catch(Exception e){
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_AdmissionSearchActionPerformed

    private void btn_AdmissionEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AdmissionEntryActionPerformed
        // TODO add your handling code here:
        Config.stu_admissionhome.onloadReset();
        Config.stu_admissionhome.setVisible(true);
    }//GEN-LAST:event_btn_AdmissionEntryActionPerformed

    private void btn_StuRegistrationRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationRefreshActionPerformed
//        txt_SearchSupplier.setText("");
//        cmb_SearchBy.setToolTipText("");
//        onloadReset();
    }//GEN-LAST:event_btn_StuRegistrationRefreshActionPerformed

    private void btn_StuRegistrationViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationViewActionPerformed
        Config.view_stu_registration.onloadReset(tbl_StuRegistration_model.getValueAt(tbl_StuRegistration.getSelectedRow(), 0).toString());
        Config.view_stu_registration.setVisible(true);
    }//GEN-LAST:event_btn_StuRegistrationViewActionPerformed

    private void btn_StuRegistrationDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationDeleteActionPerformed
        String regisid = tbl_StuRegistration_model.getValueAt(tbl_StuRegistration.getSelectedRow(),0).toString();
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure,You Want to Delete Student Record?","Warning",JOptionPane.YES_NO_OPTION);
        if(dialogButton == JOptionPane.YES_OPTION) {
            if(Config.stu_cm_registration.del_StudentRegistration(regisid)){
                JOptionPane.showMessageDialog(this, "Student Record Deleted Successfully", "Success",JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
            }
        }
    }//GEN-LAST:event_btn_StuRegistrationDeleteActionPerformed

    private void btn_StuRegistrationCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_StuRegistrationCancelActionPerformed

    private void btn_AdmissionRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AdmissionRefreshActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_AdmissionRefreshActionPerformed

    private void btn_viewAdmissionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewAdmissionActionPerformed
        Config.view_admission.onloadreset(tbl_StuAdmission_model.getValueAt(tbl_StuAdmission.getSelectedRow(), 0).toString(),tbl_StuAdmission_model.getValueAt(tbl_StuAdmission.getSelectedRow(), 2).toString());
        Config.view_admission.setVisible(true);
    }//GEN-LAST:event_btn_viewAdmissionActionPerformed

    private void btn_AdmissionDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AdmissionDeleteActionPerformed
        String admissionid = tbl_StuAdmission_model.getValueAt(tbl_StuAdmission.getSelectedRow(),0).toString();
        int dialogButton = JOptionPane.showConfirmDialog(null, "Are You Sure,You Want to Delete Student Admission Record?","Warning",JOptionPane.YES_NO_OPTION);
        if(dialogButton == JOptionPane.YES_OPTION) {
            if(Config.stu_cm_addmission.delStu_Addmission(admissionid)){
                JOptionPane.showMessageDialog(this, "Student Record Deleted Successfully", "Success",JOptionPane.NO_OPTION);
            }else{
                JOptionPane.showMessageDialog(this, "Error in Deletion", "Error",JOptionPane.ERROR_MESSAGE );
            }
        }
    }//GEN-LAST:event_btn_AdmissionDeleteActionPerformed

    private void btn_AdmissionCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AdmissionCancelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_AdmissionCancelActionPerformed

    private void cmb_StuRegistrationClassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_StuRegistrationClassActionPerformed
        btn_searchRegistrationActionPerformed(null);
    }//GEN-LAST:event_cmb_StuRegistrationClassActionPerformed

    private void tbl_StuRegistrationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_StuRegistrationMouseClicked
        if(evt.getClickCount()==2){
            btn_StuRegistrationViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_StuRegistrationMouseClicked

    private void tbl_StuAdmissionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_StuAdmissionMouseClicked
        if(evt.getClickCount()==2){
            btn_viewAdmissionActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_StuAdmissionMouseClicked

    private void dtch_RegisDatePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtch_RegisDatePropertyChange
//        btn_searchRegistrationActionPerformed(null);
    }//GEN-LAST:event_dtch_RegisDatePropertyChange

    private void cmb_AdmissionClassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_AdmissionClassActionPerformed
        btn_AdmissionSearchActionPerformed(null);
    }//GEN-LAST:event_cmb_AdmissionClassActionPerformed

    private void btn_AdmissionExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AdmissionExportActionPerformed
        if(evt.getSource() == btn_AdmissionExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(StuMgmt_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_StuRegistration, new File(file));
				}
			}
    }//GEN-LAST:event_btn_AdmissionExportActionPerformed

    private void btn_StuRegistrationExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationExportActionPerformed
        if(evt.getSource() == btn_StuRegistrationExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(StuMgmt_Home.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_StuRegistration, new File(file));
				}
			}
    }//GEN-LAST:event_btn_StuRegistrationExportActionPerformed
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StuMgmt_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StuMgmt_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StuMgmt_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StuMgmt_Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        
        
            }
          

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_AdmissionCancel;
    private javax.swing.JButton btn_AdmissionDelete;
    private javax.swing.JButton btn_AdmissionEntry;
    private javax.swing.JButton btn_AdmissionExport;
    private javax.swing.JButton btn_AdmissionRefresh;
    private javax.swing.JButton btn_AdmissionSearch;
    private javax.swing.JButton btn_StuRegistrationCancel;
    private javax.swing.JButton btn_StuRegistrationDelete;
    private javax.swing.JButton btn_StuRegistrationEntry;
    private javax.swing.JButton btn_StuRegistrationExport;
    private javax.swing.JButton btn_StuRegistrationRefresh;
    private javax.swing.JButton btn_StuRegistrationView;
    private javax.swing.JButton btn_searchRegistration;
    private javax.swing.JButton btn_viewAdmission;
    private javax.swing.JComboBox cmb_AdmissionClass;
    private javax.swing.JComboBox cmb_Section;
    private javax.swing.JComboBox cmb_StuRegistrationClass;
    private javax.swing.JComboBox cmb_StuRegistrationDate;
    private javax.swing.JComboBox cmb_admission;
    private com.toedter.calendar.JDateChooser dtch_RegisDate;
    private com.toedter.calendar.JDateChooser dtch_admissionDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbl_AdmissionClass;
    private javax.swing.JLabel lbl_AdmissionName;
    private javax.swing.JLabel lbl_AdmissionSection;
    private javax.swing.JLabel lbl_AdmissionTotal;
    private javax.swing.JLabel lbl_StuRegistrationClass;
    private javax.swing.JLabel lbl_StuRegistrationName;
    private javax.swing.JLabel lbl_StuRegistrationTotal;
    private javax.swing.JTable tbl_StuAdmission;
    private javax.swing.JTable tbl_StuRegistration;
    private javax.swing.JTextField txt_AdmissionName;
    private javax.swing.JTextField txt_AdmissionTotal;
    private javax.swing.JTextField txt_StuName;
    private javax.swing.JTextField txt_StuRegistrationTotal;
    // End of variables declaration//GEN-END:variables
 public void onloadReset(){
        try {
            tbl_StuRegistration_model.setRowCount(0);
            Config.sql = "select registration_id,form_no, firstname,fathername,dob ,class_id,date from stu_registration";
            Config.rs = Config.stmt.executeQuery(Config.sql);
               
           
            String classname = null;
            while (Config.rs.next()) {
                try {
                    String classid = Config.rs.getString("class_id");
//                    System.out.println("classid = "+classid);
                    Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                    classname = element1.getElementsByTagName("class_name").item(0).getTextContent();                 
                    
                   
                    tbl_StuRegistration_model.addRow( new Object[]{
                    Config.rs.getString("registration_id"),                 
                    tbl_StuRegistration_model.getRowCount()+1,                 
                    Config.rs.getString("form_no"),                 
                    Config.rs.getString("firstname"),
                    Config.rs.getString("fathername"),
                    Config.rs.getString("dob"),
                    classname,
                    Config.rs.getString("date")
                    });
                    
                } catch (Exception e) {                    
                    e.printStackTrace();
                }               
            }
            
            tbl_StuAdmission_model.setRowCount(0);
            try {
                Config.sql = "select a.admission_id,r.registration_id,a.ssm_id, r.firstname,r.middlename,r.lastname,r.fathername,r.dob ,r.class_id,a.section_id,a.addmission_date from stu_admission a inner join stu_registration r inner join config_ad_class_section s where r.registration_id = a.registration_id and s.section_id=a.section_id ";
                Config.rs = Config.stmt.executeQuery(Config.sql);                

                String classnm = null;
                String section_name = null;
                while (Config.rs.next()) {
                    try {
                        String classid = Config.rs.getString("class_id");                    
                        Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                        classnm = element1.getElementsByTagName("class_name").item(0).getTextContent();                 

                        String sectionid = Config.rs.getString("section_id");                    
                        Element element2 = (Element) Config.xml_config_ad_classsectionmgr.xpath.evaluate("//*[@section_id='"+sectionid+"']", Config.xml_config_ad_classsectionmgr.doc, XPathConstants.NODE);
                        section_name = element2.getElementsByTagName("section_name").item(0).getTextContent(); 


                        tbl_StuAdmission_model.addRow( new Object[]{
                        Config.rs.getString("admission_id"),                                    
                        tbl_StuAdmission_model.getRowCount()+1,
                        Config.rs.getString("registration_id"), 
                        Config.rs.getString("ssm_id"),                 
                        Config.rs.getString("firstname"),
                        Config.rs.getString("fathername"),
                        Config.rs.getString("dob"),
                        classnm,
                        section_name,
                        Config.rs.getString("addmission_date")            
                        });

                    } catch (Exception e) {                    
                        e.printStackTrace();
                    }
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            

            
            NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
            cmb_StuRegistrationClass.removeAllItems();
            cmb_StuRegistrationClass.addItem("-Select-");
            cmb_AdmissionClass.removeAllItems();
            cmb_AdmissionClass.addItem("-Select-");
            for (int i = 0; i < nList.getLength(); i++) {
               Node nNode = nList.item(i);                
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                   Element eElement = (Element) nNode;
                   cmb_StuRegistrationClass.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());
                   cmb_AdmissionClass.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());
                }
            }
             
             cmb_Section.removeAllItems();
             cmb_Section.addItem("-Select-");
             NodeList nodeList = Config.xml_config_ad_classsectionmgr.doc.getElementsByTagName("config_ad_class_section");                         
             for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                 if (node.getNodeType() == Node.ELEMENT_NODE) {
                     Element element = (Element) node;
                     cmb_Section.addItem(element.getElementsByTagName("section_name").item(0).getTextContent());
                     
                 }
             }
            
             
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setRegDetails(String sql) {

        
        
        try {     
            tbl_StuRegistration_model.setRowCount(0);
            Config.sql = sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);          
            String classid = null;
            String classname = null;
            while (Config.rs.next()) {
                
                classid=Config.rs.getString("class_id");
                Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                classname = element1.getElementsByTagName("class_name").item(0).getTextContent(); 
                tbl_StuRegistration_model.addRow( new Object[]{
                Config.rs.getString("registration_id"),                 
                tbl_StuRegistration_model.getRowCount()+1,                 
                Config.rs.getString("form_no"),                 
                Config.rs.getString("firstname"),
                Config.rs.getString("fathername"),
                Config.rs.getString("dob"),
                classname,
                Config.rs.getString("date")
                
                });               
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAdmsionDetails(String sql) {
        try {                
            tbl_StuAdmission_model.setRowCount(0);
            Config.sql = sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);  
            String classid = null;
            String classname = null;
            String sectionid = null;
            String sectionname = null;
            while (Config.rs.next()) {
                classid=Config.rs.getString("class_id");
                Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                classname = element1.getElementsByTagName("class_name").item(0).getTextContent(); 
                
                sectionid=Config.rs.getString("section_id");
                Element element2 = (Element) Config.xml_config_ad_classsectionmgr.xpath.evaluate("//*[@section_id='"+sectionid+"']", Config.xml_config_ad_classsectionmgr.doc, XPathConstants.NODE);
                sectionname = element2.getElementsByTagName("section_name").item(0).getTextContent(); 
                
                tbl_StuAdmission_model.addRow( new Object[]{
                Config.rs.getString("admission_id"),                 
                tbl_StuAdmission_model.getRowCount()+1, 
                Config.rs.getString("registration_id"),                 
                Config.rs.getString("ssm_id"),                 
                Config.rs.getString("firstname"),
                Config.rs.getString("fathername"),
                Config.rs.getString("dob"),
                classname,
                sectionname,
                Config.rs.getString("addmission_date")
                });               
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void toExcel(JTable table, File file){
		try{
			TableModel model = table.getModel();
			FileWriter excel = new FileWriter(file);

			for(int i = 0; i < model.getColumnCount(); i++){
				excel.write(model.getColumnName(i) + "\t");
			}

			excel.write("\n");

			for(int i=0; i< model.getRowCount(); i++) {
				for(int j=0; j < model.getColumnCount(); j++) {
					excel.write(model.getValueAt(i,j).toString()+"\t");
				}
				excel.write("\n");
			}

			excel.close();
		}catch(IOException e){ 
                    e.printStackTrace();
                    
                }
	}

    

}
