package modules.studentmanagment.admission;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.xpath.XPathConstants;
import managers.datamgr.configuration.Config;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Black
 */
public class Stu_AdmissionHome extends javax.swing.JDialog {
    
    DefaultTableModel tbl_StuAdmission_model;

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    public Stu_AdmissionHome(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        setLocationRelativeTo(null);
        
        tbl_StuAdmission_model = (DefaultTableModel) tbl_StuAdmission.getModel();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        lbl_StuRegistrationName = new javax.swing.JLabel();
        txt_StuName = new javax.swing.JTextField();
        btn_StuRegistrationSearch = new javax.swing.JButton();
        dtch_RegisDate = new com.toedter.calendar.JDateChooser();
        lbl_StuRegistrationClass = new javax.swing.JLabel();
        cmb_StuAdmissionClass = new javax.swing.JComboBox();
        cmb_StuRegistrationDate = new javax.swing.JComboBox();
        btn_StuRegistrationEntry = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_StuAdmission = new javax.swing.JTable();
        lbl_StuRegistrationTotal = new javax.swing.JLabel();
        txt_StuRegistrationTotal = new javax.swing.JTextField();
        btn_StuRegistrationRefresh = new javax.swing.JButton();
        btn_StuRegistrationExport = new javax.swing.JButton();
        btn_StuRegistrationCancel = new javax.swing.JButton();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Student Admission");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(11, 11, 11))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        lbl_StuRegistrationName.setText("Name :");

        txt_StuName.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_StuNameCaretUpdate(evt);
            }
        });

        btn_StuRegistrationSearch.setText("Search");
        btn_StuRegistrationSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationSearchActionPerformed(evt);
            }
        });

        dtch_RegisDate.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtch_RegisDatePropertyChange(evt);
            }
        });

        lbl_StuRegistrationClass.setText("Class :");

        cmb_StuAdmissionClass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmb_StuAdmissionClassActionPerformed(evt);
            }
        });

        cmb_StuRegistrationDate.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Registration Date", "DOB" }));

        btn_StuRegistrationEntry.setText("Admission");
        btn_StuRegistrationEntry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationEntryActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_StuRegistrationClass)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_StuAdmissionClass, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl_StuRegistrationName, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_StuName, javax.swing.GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmb_StuRegistrationDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dtch_RegisDate, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_StuRegistrationSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_StuRegistrationEntry)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_StuRegistrationSearch)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbl_StuRegistrationName, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_StuName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbl_StuRegistrationClass)
                        .addComponent(cmb_StuAdmissionClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmb_StuRegistrationDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_StuRegistrationEntry)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dtch_RegisDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_StuRegistrationEntry, btn_StuRegistrationSearch, cmb_StuAdmissionClass, cmb_StuRegistrationDate, dtch_RegisDate, jSeparator1, lbl_StuRegistrationClass, lbl_StuRegistrationName, txt_StuName});

        tbl_StuAdmission.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Registartion ID", "S.No.", "Form No.", "Student Name", "Father Name", "DOB", "Class", "Registration Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_StuAdmission.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tbl_StuAdmission.setSelectionBackground(new java.awt.Color(0, 153, 153));
        tbl_StuAdmission.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tbl_StuAdmission);
        if (tbl_StuAdmission.getColumnModel().getColumnCount() > 0) {
            tbl_StuAdmission.getColumnModel().getColumn(0).setMinWidth(0);
            tbl_StuAdmission.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbl_StuAdmission.getColumnModel().getColumn(0).setMaxWidth(0);
            tbl_StuAdmission.getColumnModel().getColumn(1).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(2).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(3).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(4).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(5).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(5).setPreferredWidth(0);
            tbl_StuAdmission.getColumnModel().getColumn(6).setResizable(false);
            tbl_StuAdmission.getColumnModel().getColumn(7).setResizable(false);
        }

        lbl_StuRegistrationTotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbl_StuRegistrationTotal.setText("Total Count :");

        btn_StuRegistrationRefresh.setText("Refresh");
        btn_StuRegistrationRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationRefreshActionPerformed(evt);
            }
        });

        btn_StuRegistrationExport.setLabel("Export");
        btn_StuRegistrationExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationExportActionPerformed(evt);
            }
        });

        btn_StuRegistrationCancel.setLabel("Cancel");
        btn_StuRegistrationCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_StuRegistrationCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lbl_StuRegistrationTotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_StuRegistrationTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_StuRegistrationRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StuRegistrationExport, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_StuRegistrationCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_StuRegistrationExport)
                    .addComponent(btn_StuRegistrationRefresh)
                    .addComponent(lbl_StuRegistrationTotal)
                    .addComponent(txt_StuRegistrationTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_StuRegistrationCancel))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lbl_StuRegistrationTotal, txt_StuRegistrationTotal});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_StuRegistrationEntryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationEntryActionPerformed
        try{
            String result = Config.admission.onLoadreset(tbl_StuAdmission_model.getValueAt(tbl_StuAdmission.getSelectedRow(), 0).toString());
            if(result.equals("No")){
                Config.admission.onloadReset(tbl_StuAdmission_model.getValueAt(tbl_StuAdmission.getSelectedRow(), 0).toString());
                Config.admission.setVisible(true);
            }else{
                JOptionPane.showMessageDialog(this, "This student is already Admitted", "error",JOptionPane.ERROR_MESSAGE );
            }

        } catch (Exception e) {
        }

    }//GEN-LAST:event_btn_StuRegistrationEntryActionPerformed

    private void btn_StuRegistrationCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_StuRegistrationCancelActionPerformed

    private void btn_StuRegistrationRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationRefreshActionPerformed
        //        txt_SearchSupplier.setText("");
        //        cmb_SearchBy.setToolTipText("");
        //        onloadReset();
    }//GEN-LAST:event_btn_StuRegistrationRefreshActionPerformed

    private void cmb_StuAdmissionClassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmb_StuAdmissionClassActionPerformed
        btn_StuRegistrationSearchActionPerformed(null);
    }//GEN-LAST:event_cmb_StuAdmissionClassActionPerformed

    private void dtch_RegisDatePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtch_RegisDatePropertyChange
        //        btn_StuRegistrationSearchActionPerformed(null);
    }//GEN-LAST:event_dtch_RegisDatePropertyChange

    private void btn_StuRegistrationSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationSearchActionPerformed
        try {
            String classid = null;
            try {
                Element element = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@sno='"+cmb_StuAdmissionClass.getSelectedIndex()+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                classid = element.getAttribute("class_id");
            } catch (Exception e) {
            }

            String text=txt_StuName.getText().trim().toUpperCase()+"%";
            String sql = "";

            if(cmb_StuRegistrationDate.getSelectedItem().equals("Registration Date") && (dtch_RegisDate.getDate()!= null)){
                if(cmb_StuAdmissionClass.getSelectedIndex()>0 ){
                    if((!txt_StuName.getText().equals(""))){
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"' and  r.firstname like '"+text+"' and r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                    else{
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"'  and r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                }else if(!txt_StuName.getText().equals("")){
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where   r.firstname like '"+text+"' and r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                }else {
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where  r.date = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                }
            }else if(cmb_StuRegistrationDate.getSelectedItem().equals("DOB") && (dtch_RegisDate.getDate()!= null)){
                if(cmb_StuAdmissionClass.getSelectedIndex()>0 ){
                    if((!txt_StuName.getText().equals(""))){
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"' and  r.firstname like '"+text+"' and r.dob = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                    else{
                        sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"'  and r.dob = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                    }
                }else {
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where  r.dob = '"+sdf.format(dtch_RegisDate.getDate())+"'" ;
                }

            }else if(cmb_StuAdmissionClass.getSelectedIndex()>0){
                if(!txt_StuName.getText().equals("")) {
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"' and  r.firstname like '"+text+"'";

                }else{
                    sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where r.class_id='"+classid+"'" ;

                }

            }else if(!txt_StuName.getText().equals("")){
                sql = "select r.registration_id,r.form_no, r.firstname,r.fathername,r.dob ,r.class_id,r.date from stu_registration r inner join config_ad_class c on c.class_id=r.class_id where  r.firstname like '"+text+"'";

            }else{
                sql = "select registration_id, form_no, firstname, fathername, dob , class_id, date from stu_registration " ;
            }

            setRegDetails(sql);
            int d = tbl_StuAdmission_model.getRowCount();
             txt_StuRegistrationTotal.setText(Integer.toString(d));

        }catch(Exception e){
            e.printStackTrace();
        }
    }//GEN-LAST:event_btn_StuRegistrationSearchActionPerformed

    private void txt_StuNameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_StuNameCaretUpdate
        btn_StuRegistrationSearchActionPerformed(null);
    }//GEN-LAST:event_txt_StuNameCaretUpdate

    private void btn_StuRegistrationExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_StuRegistrationExportActionPerformed
        if(evt.getSource() == btn_StuRegistrationExport){
                JFileChooser fc = new JFileChooser();
                int option = fc.showSaveDialog(Stu_AdmissionHome.this);
                if(option == JFileChooser.APPROVE_OPTION){
                    String filename = fc.getSelectedFile().getName(); 
                    String path = fc.getSelectedFile().getParentFile().getPath();

					int len = filename.length();
					String ext = "";
					String file = "";

					if(len > 4){
						ext = filename.substring(len-4, len);
					}

					if(ext.equals(".xls")){
						file = path + "\\" + filename; 
					}else{
						file = path + "\\" + filename + ".xls"; 
					}
					toExcel(tbl_StuAdmission, new File(file));
				}
			}
    }//GEN-LAST:event_btn_StuRegistrationExportActionPerformed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Stu_AdmissionHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Stu_AdmissionHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Stu_AdmissionHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Stu_AdmissionHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Stu_AdmissionHome dialog = new Stu_AdmissionHome(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_StuRegistrationCancel;
    private javax.swing.JButton btn_StuRegistrationEntry;
    private javax.swing.JButton btn_StuRegistrationExport;
    private javax.swing.JButton btn_StuRegistrationRefresh;
    private javax.swing.JButton btn_StuRegistrationSearch;
    private javax.swing.JComboBox cmb_StuAdmissionClass;
    private javax.swing.JComboBox cmb_StuRegistrationDate;
    private com.toedter.calendar.JDateChooser dtch_RegisDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbl_StuRegistrationClass;
    private javax.swing.JLabel lbl_StuRegistrationName;
    private javax.swing.JLabel lbl_StuRegistrationTotal;
    private javax.swing.JTable tbl_StuAdmission;
    private javax.swing.JTextField txt_StuName;
    private javax.swing.JTextField txt_StuRegistrationTotal;
    // End of variables declaration//GEN-END:variables

    

    public void onloadReset() {
        try {
            tbl_StuAdmission_model.setRowCount(0);
            Config.sql = "select registration_id,form_no, firstname,fathername,dob ,class_id,date from stu_registration";
            Config.rs = Config.stmt.executeQuery(Config.sql);
               
           
            String classname = null;
            while (Config.rs.next()) {
                try {
                    String classid = Config.rs.getString("class_id");                    
                    Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                    classname = element1.getElementsByTagName("class_name").item(0).getTextContent();                 
                    
                   
                    tbl_StuAdmission_model.addRow( new Object[]{
                    Config.rs.getString("registration_id"),                 
                    tbl_StuAdmission_model.getRowCount()+1,                 
                    Config.rs.getString("form_no"),                 
                    Config.rs.getString("firstname"),
                    Config.rs.getString("fathername"),
                    Config.rs.getString("dob"),
                    classname,
                    Config.rs.getString("date")
                    });
                } catch (Exception e) {
                    
                    e.printStackTrace();
            }
        }           
             NodeList nList = Config.xml_config_ad_classmgr.doc.getElementsByTagName("config_ad_class");
             cmb_StuAdmissionClass.removeAllItems();
             cmb_StuAdmissionClass.addItem("-Select-");              
             for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    cmb_StuAdmissionClass.addItem(eElement.getElementsByTagName("class_name").item(0).getTextContent());
                    
                }
             }
         } catch (Exception e) {
        e.printStackTrace();
        }
    }

    private void setRegDetails(String sql) {
        try {                
            tbl_StuAdmission_model.setRowCount(0);
            Config.sql = sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);          
            String classid = null;
            String classname = null;
            while (Config.rs.next()) {
                
                classid=Config.rs.getString("class_id");
                Element element1 = (Element) Config.xml_config_ad_classmgr.xpath.evaluate("//*[@class_id='"+classid+"']", Config.xml_config_ad_classmgr.doc, XPathConstants.NODE);
                classname = element1.getElementsByTagName("class_name").item(0).getTextContent(); 
                tbl_StuAdmission_model.addRow( new Object[]{
                Config.rs.getString("registration_id"),                 
                tbl_StuAdmission_model.getRowCount()+1,                 
                Config.rs.getString("form_no"),                 
                Config.rs.getString("firstname"),
                Config.rs.getString("fathername"),
                Config.rs.getString("dob"),
                classname,
                Config.rs.getString("date")
                
                });               
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void toExcel(JTable table, File file){
		try{
			TableModel model = table.getModel();
			FileWriter excel = new FileWriter(file);

			for(int i = 0; i < model.getColumnCount(); i++){
				excel.write(model.getColumnName(i) + "\t");
			}

			excel.write("\n");

			for(int i=0; i< model.getRowCount(); i++) {
				for(int j=0; j < model.getColumnCount(); j++) {
					excel.write(model.getValueAt(i,j).toString()+"\t");
				}
				excel.write("\n");
			}

			excel.close();
		}catch(IOException e){ 
                    e.printStackTrace();
                    
                }
	}
    
    

    

    
}
