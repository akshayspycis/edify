/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.dashboard;

import javax.swing.JOptionPane;
import managers.datamgr.configuration.Config;
import managers.datamgr.dao.login.II_Dm_Login;

/**
 *
 * @author AMS
 */
public class panel_login extends javax.swing.JPanel {

    /**
     * Creates new form panel_login
     */
    public panel_login() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        txt_usrname = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_password = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(1388, 582));
        setRequestFocusEnabled(false);
        setLayout(null);

        jPanel2.setBackground(new java.awt.Color(250, 250, 250));

        txt_usrname.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_usrname.setForeground(new java.awt.Color(204, 204, 204));
        txt_usrname.setText("Enter your username here.");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 102, 102));
        jLabel6.setText("Username");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("Password");

        txt_password.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_password.setForeground(new java.awt.Color(204, 204, 204));
        txt_password.setText("password");

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(102, 102, 102));
        jButton1.setText("Login");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel8.setForeground(new java.awt.Color(58, 148, 175));
        jLabel8.setText("forgot password ?");
        jLabel8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(58, 148, 175));
        jLabel9.setText("Sign in to continue...");
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator2)
                    .addComponent(jLabel9)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                        .addGap(133, 133, 133)
                        .addComponent(jButton1))
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(txt_usrname)
                    .addComponent(txt_password))
                .addGap(30, 30, 30))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_usrname, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_password, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jLabel8))
                .addGap(45, 45, 45))
        );

        add(jPanel2);
        jPanel2.setBounds(930, 20, 390, 320);

        jLabel21.setForeground(new java.awt.Color(58, 148, 175));
        jLabel21.setText("Feedback");
        jLabel21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(jLabel21);
        jLabel21.setBounds(1200, 560, 50, 14);

        jLabel22.setForeground(new java.awt.Color(102, 102, 102));
        jLabel22.setText("|");
        add(jLabel22);
        jLabel22.setBounds(1190, 560, 10, 14);

        jLabel23.setForeground(new java.awt.Color(58, 148, 175));
        jLabel23.setText("Utilities");
        jLabel23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(jLabel23);
        jLabel23.setBounds(1150, 560, 40, 14);

        jLabel2.setForeground(new java.awt.Color(58, 148, 175));
        jLabel2.setText("Troubleshoot");
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(jLabel2);
        jLabel2.setBounds(1260, 560, 70, 14);

        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText("|");
        add(jLabel3);
        jLabel3.setBounds(1250, 560, 4, 14);

        jPanel1.setBackground(new java.awt.Color(58, 148, 175));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );

        add(jPanel1);
        jPanel1.setBounds(0, 1, 1388, 1);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(102, 102, 102));
        jLabel4.setText("Centralized server");
        add(jLabel4);
        jLabel4.setBounds(90, 90, 300, 50);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/lo.jpg"))); // NOI18N
        add(jLabel5);
        jLabel5.setBounds(40, 90, 40, 50);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel10.setText("\" New technology is common, but new thinking is rare. \"");
        add(jLabel10);
        jLabel10.setBounds(40, 20, 670, 50);

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/log.jpg"))); // NOI18N
        add(jLabel11);
        jLabel11.setBounds(40, 150, 40, 50);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(102, 102, 102));
        jLabel12.setText("Secure with customized solutions");
        add(jLabel12);
        jLabel12.setBounds(90, 150, 300, 50);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/logi.jpg"))); // NOI18N
        add(jLabel13);
        jLabel13.setBounds(40, 210, 40, 50);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(102, 102, 102));
        jLabel14.setText("Integrated internal messaging system");
        add(jLabel14);
        jLabel14.setBounds(90, 210, 300, 50);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(102, 102, 102));
        jLabel15.setText("Graphical reports");
        add(jLabel15);
        jLabel15.setBounds(490, 90, 300, 50);

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/login.jpg"))); // NOI18N
        add(jLabel16);
        jLabel16.setBounds(440, 90, 40, 50);

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/l.jpg"))); // NOI18N
        add(jLabel17);
        jLabel17.setBounds(440, 150, 40, 50);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(102, 102, 102));
        jLabel18.setText("Evaluation method support");
        add(jLabel18);
        jLabel18.setBounds(490, 150, 300, 50);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(102, 102, 102));
        jLabel19.setText("Solution for every educational organization");
        add(jLabel19);
        jLabel19.setBounds(490, 210, 350, 50);

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/loginc.jpg"))); // NOI18N
        add(jLabel20);
        jLabel20.setBounds(440, 210, 40, 50);

        jLabel24.setForeground(new java.awt.Color(102, 102, 102));
        jLabel24.setText("|");
        add(jLabel24);
        jLabel24.setBounds(1140, 560, 10, 14);

        jLabel25.setForeground(new java.awt.Color(58, 148, 175));
        jLabel25.setText("New User");
        jLabel25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        add(jLabel25);
        jLabel25.setBounds(1090, 560, 50, 14);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/loginscreen.jpg"))); // NOI18N
        add(jLabel1);
        jLabel1.setBounds(0, 241, 1388, 341);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String  user = txt_usrname.getText();
        char[] pass = txt_password.getPassword();
        
        Home.slider.previous();
//       String sql = "SELECT username,password FROM login where username= ? and password= ?"; 
//       try{
//           Config.pstmt = Config.conn.prepareStatement(sql);
//           Config.pstmt.setString(1,txt_usrname.getText());
//           Config.pstmt.setString(2,txt_password.getText());
//           Config.rs = Config.pstmt.executeQuery();
//
//           if (Config.rs.next()){
//               Home.slider.previous();
//              }
//           else{JOptionPane.showMessageDialog(null, "Please try again");}
//           }
//       catch(Exception e){
//           JOptionPane.showMessageDialog(null, e);
//                          }
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JPasswordField txt_password;
    private javax.swing.JTextField txt_usrname;
    // End of variables declaration//GEN-END:variables
}
