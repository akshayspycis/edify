package modules.dashboard;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultEditorKit;
import managers.datamgr.configuration.Config;
import modules.mis.JSLSlider;

public class Home extends javax.swing.JFrame {

    //date formates
    SimpleDateFormat sdf1;
    
    //slider variables
    static JSLSlider slider = new JSLSlider();
    
    public Home() {
        //form initilization
        initComponents();
        setLocationRelativeTo(null);
        setExtendedState(MAXIMIZED_BOTH);
        
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/icon.png")));
        
        //date formate initilization
        sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        
        //slider initilization
        panel_slider.setLayout(new BorderLayout());        
        panel_slider.add(initSlider(), BorderLayout.CENTER);        
        pack();        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu5 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        panel_slider = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        lbl_schoolname = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menu_File = new javax.swing.JMenu();
        menu_Save = new javax.swing.JMenuItem();
        menu_SaveAll = new javax.swing.JMenuItem();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        menu_PageSetup = new javax.swing.JMenuItem();
        menu_Print = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        menu_Close = new javax.swing.JMenuItem();
        menu_Edit = new javax.swing.JMenu();
        menu_Cut = new javax.swing.JMenuItem(new DefaultEditorKit.CutAction());
        menu_Copy = new javax.swing.JMenuItem(new DefaultEditorKit.CopyAction());
        menu_Paste = new javax.swing.JMenuItem(new DefaultEditorKit.PasteAction());
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        menu_Search = new javax.swing.JMenuItem();
        jSeparator12 = new javax.swing.JPopupMenu.Separator();
        menu_Delete = new javax.swing.JMenuItem();
        menu_Manage = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        jMenuItem21 = new javax.swing.JMenuItem();
        jMenuItem24 = new javax.swing.JMenuItem();
        jMenu10 = new javax.swing.JMenu();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenu11 = new javax.swing.JMenu();
        jMenuItem22 = new javax.swing.JMenuItem();
        jMenuItem23 = new javax.swing.JMenuItem();
        menu_Reports = new javax.swing.JMenu();
        menu_Utilities = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        menu_ExportFile = new javax.swing.JMenuItem();
        menu_ImportFile = new javax.swing.JMenuItem();
        menu_Cleanup = new javax.swing.JMenuItem();
        menu_GenerateLogs = new javax.swing.JCheckBoxMenuItem();
        menu_Help = new javax.swing.JMenu();
        menu_ManageAccount = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menu_HelpContent = new javax.swing.JMenuItem();
        menu_ProductLicense = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        menu_CompanyProfile = new javax.swing.JMenuItem();

        jMenu5.setText("File");
        jMenuBar2.add(jMenu5);

        jMenu6.setText("Edit");
        jMenuBar2.add(jMenu6);

        jMenuItem15.setText("jMenuItem15");

        jMenuItem16.setText("jMenuItem16");

        jMenuItem17.setText("jMenuItem17");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Edify - Administrator Home [INFOPARK INNOVATIONS]");

        panel_slider.setBackground(new java.awt.Color(255, 255, 255));
        panel_slider.setPreferredSize(new java.awt.Dimension(1388, 582));

        javax.swing.GroupLayout panel_sliderLayout = new javax.swing.GroupLayout(panel_slider);
        panel_slider.setLayout(panel_sliderLayout);
        panel_sliderLayout.setHorizontalGroup(
            panel_sliderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panel_sliderLayout.setVerticalGroup(
            panel_sliderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 584, Short.MAX_VALUE)
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("Copyright © 2013, All rights reserved.");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("|    Contact : +91 - 9039788847, +91 - 7566092796, +91 - 9685004050");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/companyname.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addGap(18, 18, 18)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 574, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(jLabel12))
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel11, jLabel12});

        jPanel1.setLayout(null);
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(10, 42, 410, 10);

        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Sukhdeep Complex, M. P. Nagar, Bhopal - 462001, Madhya Pradesh");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 50, 330, 14);

        lbl_schoolname.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbl_schoolname.setForeground(new java.awt.Color(255, 255, 255));
        lbl_schoolname.setText("Garima Higher Secondary School");
        jPanel1.add(lbl_schoolname);
        lbl_schoolname.setBounds(10, 0, 590, 40);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/finalheader.jpg"))); // NOI18N
        jLabel5.setAlignmentY(0.0F);
        jPanel1.add(jLabel5);
        jLabel5.setBounds(0, 0, 1390, 72);

        menu_File.setMnemonic('F');
        menu_File.setText("File");

        menu_Save.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        menu_Save.setMnemonic('S');
        menu_Save.setText("Save");
        menu_Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_SaveActionPerformed(evt);
            }
        });
        menu_File.add(menu_Save);

        menu_SaveAll.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menu_SaveAll.setMnemonic('A');
        menu_SaveAll.setText("Save All");
        menu_SaveAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_SaveAllActionPerformed(evt);
            }
        });
        menu_File.add(menu_SaveAll);
        menu_File.add(jSeparator11);

        menu_PageSetup.setMnemonic('g');
        menu_PageSetup.setText("Page Setup");
        menu_PageSetup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_PageSetupActionPerformed(evt);
            }
        });
        menu_File.add(menu_PageSetup);

        menu_Print.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        menu_Print.setMnemonic('P');
        menu_Print.setText("Print");
        menu_Print.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_PrintActionPerformed(evt);
            }
        });
        menu_File.add(menu_Print);
        menu_File.add(jSeparator6);

        menu_Close.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        menu_Close.setMnemonic('e');
        menu_Close.setText("Close");
        menu_Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_CloseActionPerformed(evt);
            }
        });
        menu_File.add(menu_Close);

        jMenuBar1.add(menu_File);

        menu_Edit.setMnemonic('E');
        menu_Edit.setText("Edit");

        menu_Cut.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        menu_Cut.setMnemonic('t');
        menu_Cut.setText("Cut");
        menu_Edit.add(menu_Cut);

        menu_Copy.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        menu_Copy.setMnemonic('y');
        menu_Copy.setText("Copy");
        menu_Edit.add(menu_Copy);

        menu_Paste.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        menu_Paste.setMnemonic('P');
        menu_Paste.setText("Paste");
        menu_Edit.add(menu_Paste);
        menu_Edit.add(jSeparator8);

        menu_Search.setText("Search");
        menu_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_SearchActionPerformed(evt);
            }
        });
        menu_Edit.add(menu_Search);
        menu_Edit.add(jSeparator12);

        menu_Delete.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
        menu_Delete.setMnemonic('D');
        menu_Delete.setText("Delete");
        menu_Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_DeleteActionPerformed(evt);
            }
        });
        menu_Edit.add(menu_Delete);

        jMenuBar1.add(menu_Edit);

        menu_Manage.setText("Manage");

        jMenu1.setText("Admin");

        jMenuItem9.setText("Data Repository");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem9);

        jMenuItem7.setText("demo");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);

        menu_Manage.add(jMenu1);

        jMenu2.setText("Library");
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Data Repository");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuItem3.setText("Generate Library Card");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem2.setText("Issue Book");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem4.setText("Return Book");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem5.setText("Check Book Availability");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);

        jMenuItem6.setText("jMenuItem6");
        jMenu2.add(jMenuItem6);

        menu_Manage.add(jMenu2);

        jMenu3.setText("Stores/Inventory");
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });

        jMenuItem14.setText("Data Repository");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem14);

        jMenuItem18.setText("Bill Entry");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem18);
        jMenu3.add(jSeparator2);

        jMenuItem8.setText("Check Stock");
        jMenu3.add(jMenuItem8);

        menu_Manage.add(jMenu3);

        jMenu8.setText("Admission Cell");

        jMenuItem21.setText("Student Managment");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem21);

        jMenuItem24.setText("Report");
        jMenuItem24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem24ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem24);

        menu_Manage.add(jMenu8);

        jMenu10.setText("Examination Cell");

        jMenuItem19.setText("Exam_Home");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        jMenu10.add(jMenuItem19);

        jMenuItem20.setText("Result Entry");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        jMenu10.add(jMenuItem20);

        menu_Manage.add(jMenu10);

        jMenu4.setText("Transport");

        jMenuItem10.setText("Data Repository");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem10);

        menu_Manage.add(jMenu4);

        jMenu9.setText("Human Resource");

        jMenuItem11.setText("Data Repository");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem11);

        jMenuItem12.setText("Allowance Distribution");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem12);

        jMenuItem13.setText("Employee Management");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem13);

        menu_Manage.add(jMenu9);

        jMenu11.setText("Fees Managment");

        jMenuItem22.setText("Data Repository");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem22);

        jMenuItem23.setText("Managment");
        jMenuItem23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem23ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem23);

        menu_Manage.add(jMenu11);

        jMenuBar1.add(menu_Manage);

        menu_Reports.setText("Reports");
        jMenuBar1.add(menu_Reports);

        menu_Utilities.setMnemonic('U');
        menu_Utilities.setText("Utilities");

        jMenu7.setMnemonic('u');
        jMenu7.setText("Backup");

        menu_ExportFile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        menu_ExportFile.setMnemonic('l');
        menu_ExportFile.setText("Export File");
        menu_ExportFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_ExportFileActionPerformed(evt);
            }
        });
        jMenu7.add(menu_ExportFile);

        menu_ImportFile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        menu_ImportFile.setMnemonic('I');
        menu_ImportFile.setText("Import File");
        menu_ImportFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_ImportFileActionPerformed(evt);
            }
        });
        jMenu7.add(menu_ImportFile);

        menu_Utilities.add(jMenu7);

        menu_Cleanup.setText("Cleanup");
        menu_Utilities.add(menu_Cleanup);

        menu_GenerateLogs.setSelected(true);
        menu_GenerateLogs.setText("Generate Logs");
        menu_GenerateLogs.setEnabled(false);
        menu_Utilities.add(menu_GenerateLogs);

        jMenuBar1.add(menu_Utilities);

        menu_Help.setMnemonic('H');
        menu_Help.setText("Help");

        menu_ManageAccount.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        menu_ManageAccount.setMnemonic('o');
        menu_ManageAccount.setText("Manage Account");
        menu_ManageAccount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_ManageAccountActionPerformed(evt);
            }
        });
        menu_Help.add(menu_ManageAccount);
        menu_Help.add(jSeparator3);

        menu_HelpContent.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        menu_HelpContent.setMnemonic('H');
        menu_HelpContent.setText("Help Contents");
        menu_Help.add(menu_HelpContent);

        menu_ProductLicense.setMnemonic('P');
        menu_ProductLicense.setText("Product License");
        menu_ProductLicense.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_ProductLicenseActionPerformed(evt);
            }
        });
        menu_Help.add(menu_ProductLicense);
        menu_Help.add(jSeparator9);

        menu_CompanyProfile.setMnemonic('C');
        menu_CompanyProfile.setText("Company Profile");
        menu_Help.add(menu_CompanyProfile);

        jMenuBar1.add(menu_Help);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(panel_slider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panel_slider, javax.swing.GroupLayout.PREFERRED_SIZE, 584, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, Short.MAX_VALUE)
                .addGap(5, 5, 5))
        );

        setSize(new java.awt.Dimension(1404, 742));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menu_SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_SaveActionPerformed
        menu_SaveAllActionPerformed(evt);
    }//GEN-LAST:event_menu_SaveActionPerformed

    private void menu_SaveAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_SaveAllActionPerformed
//        try {
//            Config.sql = "commit";
//            Config.pstmt = Config.conn.prepareStatement(Config.sql);
//            Config.pstmt.executeUpdate();
//        } catch (Exception ex) {
//            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_menu_SaveAllActionPerformed

    private void menu_PageSetupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_PageSetupActionPerformed
        JOptionPane.showMessageDialog(this,"This service temporary down.","Error",JOptionPane.ERROR_MESSAGE);
    }//GEN-LAST:event_menu_PageSetupActionPerformed

    private void menu_PrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_PrintActionPerformed
        JOptionPane.showMessageDialog(this,"This service temporary down.","Error",JOptionPane.ERROR_MESSAGE);
    }//GEN-LAST:event_menu_PrintActionPerformed

    private void menu_CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_CloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_menu_CloseActionPerformed

    private void menu_DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_DeleteActionPerformed
        JOptionPane.showMessageDialog(this,"This service temporary down.","Error",JOptionPane.ERROR_MESSAGE);
//        int op = JOptionPane.showConfirmDialog(this,"Are you really want to delete Customer entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
//        if (op == 0) {
//            int x = 0;
//            int y = 0;
//            int z = 0;
//            try {
//                ConfiAdminHomel = "delete from customerbill where billid ="+Home.customersid[jTable1.getSelectedRow()];
//                Config.pstmt = Config.conn.prepareStatement(Config.sql);
//                x = Config.pstmt.executeUpdate();
//                ConfigAdminHome = "delete from purchasetable where billid ="+Home.customersid[jTable1.getSelectedRow()];
//                Config.pstmt = Config.conn.prepareStatement(Config.sql);
//                y = Config.pstmt.executeUpdate();
//                ConfAdminHomeql = "delete from returntable where billid ="+Home.customersid[jTable1.getSelectedRow()];
//                Config.pstmt = Config.conn.prepareStatement(Config.sql);
//                z = Config.pstmt.executeUpdate();
//
//                if (x>=0 && y>=0 && z>=0) {
//                    Config.stockmgr.updStock();
//                    btnViewActionPerformed(null);
//                    JOptionPane.showMessageDialog(this,"Customer entry deleted successfully.","Delete Successful.",JOptionPane.NO_OPTION);
//                } else {
//                    JOptionPane.showMessageDialog(this,"Action can not be completed.","Error",JOptionPane.ERROR_MESSAGE);
//                }
//            } catch (SQLException ex) {
//                JOptionPane.showMessageDialog(this,ex.getMessage(),"Error : "+ex.getErrorCode(),JOptionPane.ERROR_MESSAGE);
//            }
//        }


//        try {
//            String localbillid = billid.get(tblBillList.getSelectedRow());
//            int op = JOptionPane.showConfirmDialog(this,"Are you really want to delete this customer entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
//            if (op == 0) {
//                if (Config.customerbillmgr.delCustomerBill(localbillid)) {
//                    btnViewActionPerformed(null);
//                    JOptionPane.showMessageDialog(this,"Customer entry deleted successfully.","Delete Successful.",JOptionPane.NO_OPTION);
//                } else {
//                    JOptionPane.showMessageDialog(this,"Action can not be completed.","Error",JOptionPane.ERROR_MESSAGE);
//                }
//            }
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(this,"No row selected.","Error",JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_menu_DeleteActionPerformed

    private void menu_ExportFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_ExportFileActionPerformed
//        JPasswordField pf = new JPasswordField();
//        int option = JOptionPane.showConfirmDialog(null,pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//        if (option == JOptionPane.OK_OPTION ) {
//            if (new String(pf.getPassword()).equals(Config.password)) {
//
//            } else {
//                JOptionPane.showMessageDialog(this,"Provided password is incorrect.","Error",JOptionPane.ERROR_MESSAGE);
//            }
//        }
    }//GEN-LAST:event_menu_ExportFileActionPerformed

    private void menu_ImportFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_ImportFileActionPerformed
//        JPasswordField pf = new JPasswordField();
//        int option = JOptionPane.showConfirmDialog(null,pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
//        if (option == JOptionPane.OK_OPTION ) {
//            if (new String(pf.getPassword()).equals(Config.configuserprofile.get(0).getPassword())) {
//
//            } else {
//                JOptionPane.showMessageDialog(this,"Provided password is incorrect.","Error",JOptionPane.ERROR_MESSAGE);
//            }
//        }
    }//GEN-LAST:event_menu_ImportFileActionPerformed

    private void menu_ManageAccountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_ManageAccountActionPerformed
//        Config.manageaccount.onloadReset();
//        Config.manageaccount.setVisible(true);
    }//GEN-LAST:event_menu_ManageAccountActionPerformed

    private void menu_ProductLicenseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_ProductLicenseActionPerformed
//        Config.productlicense.onloadReset();
//        Config.productlicense.setVisible(true);
    }//GEN-LAST:event_menu_ProductLicenseActionPerformed

    private void menu_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_SearchActionPerformed
        JOptionPane.showMessageDialog(this,"This service temporary down.","Error",JOptionPane.ERROR_MESSAGE);
    }//GEN-LAST:event_menu_SearchActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem4ActionPerformed
    
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Config.lm_repository_home.onloadReset();
        Config.lm_repository_home.setVisible(true);        
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
//       Config.
       // Config.si_repository_home.setVisible(true);
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu3ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
       
        Config.si_home.setVisible(true);
        Config.si_home.onloadReset();
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
//        Config.si_billentry.setVisible(true);       
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        Config.ad_home.onloadReset();
        Config.ad_home.setVisible(true);        
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        // TODO add your handling code here:
        Config.tr_home.onloadReset();
        Config.tr_home.setVisible(true);
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        Config.hr_home.onloadReset();
        Config.hr_home.setVisible(true);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        Config.hr_allowance_distribution.setVisible(true);
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        Config.hr_empmgmt.onloadReset();
        Config.hr_empmgmt.setVisible(true);
        
        
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        // TODO add your handling code here:
        Config.exam_home.setVisible(true);
        Config.exam_home.onloadReset();
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
        // TODO add your handling code here:
        Config.ex_resultentry.setVisible(true);
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed
        // TODO add your handling code here:
        Config.stumgmt_home.onloadReset();
        Config.stumgmt_home.setVisible(true);
        
    }//GEN-LAST:event_jMenuItem21ActionPerformed

    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
        // TODO add your handling code here:
        Config.fee_home1.onloadReset();
        Config.fee_home1.setVisible(true);
    }//GEN-LAST:event_jMenuItem22ActionPerformed

    private void jMenuItem23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem23ActionPerformed
        // TODO add your handling code here:
        Config.fee_managment.onloadReset();
        Config.fee_managment.setVisible(true);
    }//GEN-LAST:event_jMenuItem23ActionPerformed

    private void jMenuItem24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem24ActionPerformed
       
        Config.stu_Report.onloadReset();
        Config.stu_Report.setVisible(true);
    }//GEN-LAST:event_jMenuItem24ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        Config.Demo.onloadReset();
        Config.Demo.setVisible(true);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu10;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem23;
    private javax.swing.JMenuItem jMenuItem24;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JLabel lbl_schoolname;
    private javax.swing.JMenuItem menu_Cleanup;
    private javax.swing.JMenuItem menu_Close;
    private javax.swing.JMenuItem menu_CompanyProfile;
    private javax.swing.JMenuItem menu_Copy;
    private javax.swing.JMenuItem menu_Cut;
    private javax.swing.JMenuItem menu_Delete;
    private javax.swing.JMenu menu_Edit;
    private javax.swing.JMenuItem menu_ExportFile;
    private javax.swing.JMenu menu_File;
    private javax.swing.JCheckBoxMenuItem menu_GenerateLogs;
    private javax.swing.JMenu menu_Help;
    private javax.swing.JMenuItem menu_HelpContent;
    private javax.swing.JMenuItem menu_ImportFile;
    private javax.swing.JMenu menu_Manage;
    private javax.swing.JMenuItem menu_ManageAccount;
    private javax.swing.JMenuItem menu_PageSetup;
    private javax.swing.JMenuItem menu_Paste;
    private javax.swing.JMenuItem menu_Print;
    private javax.swing.JMenuItem menu_ProductLicense;
    private javax.swing.JMenu menu_Reports;
    private javax.swing.JMenuItem menu_Save;
    private javax.swing.JMenuItem menu_SaveAll;
    private javax.swing.JMenuItem menu_Search;
    private javax.swing.JMenu menu_Utilities;
    private javax.swing.JPanel panel_slider;
    // End of variables declaration//GEN-END:variables
    
    public void onloadReset() {
        try {
            
            String schoolnm = Config.endn.getData();
            lbl_schoolname.setText(schoolnm);
            

        } catch (Exception e) {
            e.printStackTrace();
        }
        
       
    }    
    
    //slider method start-------------------------------------------------------
    public static JComponent initSlider() {
        slider.addSliderComponent(Config.panel_login);
        slider.addSliderComponent(Config.panel_home);
        slider.refresh();        
        return slider;
    }
    //slider method end---------------------------------------------------------
}