package ashok;

/**
 *
 * @author Ashok
 */
 import javax.swing.*;
 import javax.swing.table.*;
 import java.awt.*;
 import java.awt.event.*;

public class JTableAction {
  JTable table;
  public static void main(String[] args) {
  new JTableAction();
  }

  public JTableAction(){
  JFrame frame = new JFrame("Setting Cell Values in JTable");
  JPanel panel = new JPanel();
  Integer data[][] = {{65,87,},
   {78,98,},
   {63,90,},
   {99,80,},
   {100,100}};
  String col[] = {"Marks1","Marks","Total"};  
  DefaultTableModel model = new DefaultTableModel(data, col);
  table = new JTable(model);

  JTableHeader header = table.getTableHeader();
  header.setBackground(Color.yellow);
  JScrollPane pane = new JScrollPane(table);
  panel.add(pane);
  frame.add(panel);
  frame.setSize(500,150);
  frame.setUndecorated(true);
  frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
  frame.setVisible(true);
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      int rows= table.getRowCount();
      for(int i=0;i<rows;i++){
      int obj1 = Integer.parseInt(GetData(table, i, 0).toString());
      int obj2 = Integer.parseInt(GetData(table, i, 1).toString());
      int obj3=obj1+obj2;
      SetData(new Integer(obj3),i,2);
      }
      System.out.println("Total Marks is added.");
  }

  public void SetData(Object obj, int row_index, int col_index){
  table.getModel().setValueAt(obj,row_index,col_index);
  }
   public Object GetData(JTable table, int row_index, int col_index){
              
       return table.getModel().getValueAt(row_index, col_index);
  }
}
