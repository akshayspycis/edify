package ashok;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Ashok
 */
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
public class TestTable 
{
    public static void main(String[] args) 
    {
        TestTable testTable = new TestTable();
    }
    public TestTable() 
    {
        EventQueue.invokeLater(new Runnable() 
        {
            @Override
            public void run() 
            {
                try
                {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                }
                JFrame frame = new JFrame("Testing");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLayout(new BorderLayout());
                frame.add(new TestTable.Bill());
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }    
    public class Bill extends JPanel implements ActionListener 
    {
        JTextField textFieldId;
        JLabel l1;
        JLabel l2;
        JButton b1,b2,b3;
        JTextField sun,sunr,sat,satr,oth,othr;
        float sum1,totall;
        ResultSet rs1 = null;
        DefaultTableModel model = new DefaultTableModel();
        JTable table = new JTable(model);
        private int rows;
        public Bill() 
        {
            setLayout(new BorderLayout());            
            JPanel fields = new JPanel();
            textFieldId = new JTextField(10);
            l1 = new JLabel("New Customer Entry :-");
            l2 = new JLabel("Customer Id");
            b1 = new JButton("OK");
            b2 = new JButton("Calculate");
            b3 = new JButton("Print");
            fields.add(l2);
            fields.add(textFieldId);
            fields.add(b1);
            fields.add(b2);
            fields.add(b3);
            add(fields, BorderLayout.NORTH);
            b1.addActionListener(this);
            b2.addActionListener(this);
            b3.addActionListener(this);
            // Don't forget to add a table.
            add(new JScrollPane(new JTable(model)));
        }
        @Override
        public void actionPerformed(ActionEvent e) 
        {
            System.out.println("You clicked the button");
            Connection con;
            if (e.getSource() == b1) 
            {
                PreparedStatement ps = null;
                Statement stmt = null;
                try 
                {
                    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                    con = DriverManager.getConnection("jdbc:odbc:devendra");
                    ps = con.prepareStatement("SELECT  * FROM Customer where Customer_Id = ?");
                    // You must bind the parameter with a value...
                    ps.setString(1, textFieldId.getText());
                    rs1 = ps.executeQuery();
                    model.addColumn("Customer_Id");
                    model.addColumn("Customer_Name");
                    model.addColumn("Contact");
                    model.addColumn("Paper_Name");
                    model.addColumn("Sunday");
                    model.addColumn("Rate");
                    model.addColumn("Satarday");
                    model.addColumn("Rate");
                    model.addColumn("Other Day");
                    model.addColumn("Rate");
                    model.addColumn("Total");
                    while (rs1.next()) 
                    {                           
                            model.addRow(new Object[]{rs1.getString(1),rs1.getString(2),rs1.getString(3),rs1.getString(4),rs1.getString(5),
                                rs1.getString(6),rs1.getString(7),rs1.getString(8),rs1.getString(9),rs1.getString(10),rs1.getString(11)});
                    }
                    Vector data = model.getDataVector();
                    JOptionPane.showMessageDialog(null, "You successfully Enter the Entry");
                } 
                catch (SQLException s) 
                {
                    System.out.println("SQL code does not execute.");
                    JOptionPane.showMessageDialog(null, "Please Enter the Detail Correctly");
                } catch (Exception exp) 
                {
                    JOptionPane.showMessageDialog(this, "Failed to perform query: " + exp.getMessage());
                } finally 
                {
                    try {
                        ps.close();
                    } 
                    catch (Exception ex)
                    {
                    }
                }
                if (e.getSource() == b2)  
                {
                //    if (e.getType() == TableModelEvent.UPDATE) 
                    int rowCount = table.getRowCount();             
                    for(int i =1; i<=rowCount;i++)
                    {
                        int valuea = (Integer) table.getModel().getValueAt(rowCount, 5);
                        int valueb = (Integer) table.getModel().getValueAt(rowCount, 6);
                        int valuec = (Integer) table.getModel().getValueAt(rowCount, 7);
                        int valued = (Integer) table.getModel().getValueAt(rowCount, 8);
                        int valuee = (Integer) table.getModel().getValueAt(rowCount, 9);
                        int valuef = (Integer) table.getModel().getValueAt(rowCount, 10);
                        Double value = new Double(( valuea * valueb)+ (valuec * valued)+(valuee*valuef));
                        model.setValueAt(value,rowCount, 11);
                        table.revalidate();
                        table.repaint();
                 }
            }
            }        
        }
    }
}