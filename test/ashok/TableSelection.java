package ashok;

/**
 *
 * @author Ashok
 */
import java.awt.BorderLayout;  
import java.awt.event.MouseAdapter;  
import java.awt.event.MouseEvent;  
   
import javax.swing.JFrame;  
import javax.swing.JScrollPane;  
import javax.swing.JTable;  
import javax.swing.JTextField;  
   
  
public class TableSelection  
{  
private JFrame frame;  
private JTextField text;  
private JTable table;  
  
private static final String[] columns = {  
"First Name", "Last Name", "Phone"  
};  
  
private static final String[][] rowData = {  
{"Nathan", "Pruett", "(555) 555-555"},  
{"rule", "S", "(555) 123-4567"}  
};  
  
public TableSelection()   
{  
frame = new JFrame("TableSelection");  
  
frame.getContentPane().setLayout(new BorderLayout());  
  
table = new JTable(rowData, columns);  
table.addMouseListener(new MouseAdapter()   
{  
public void mouseClicked(MouseEvent evt)  
{  
int col = table.getSelectedColumn();  
int row = table.getSelectedRow();  
text.setText((String)table.getModel().getValueAt(row, col));  
}  
});  
  
frame.getContentPane().add(new JScrollPane(table), BorderLayout.CENTER);  
  
text = new JTextField();  
  
frame.getContentPane().add(text, BorderLayout.SOUTH);  
  
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
frame.pack();  
frame.setLocationRelativeTo(null);  
frame.setVisible(true);  
}  
  
public static void main(String[] args)  
{  
new TableSelection();  
}  
}  