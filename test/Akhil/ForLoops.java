package Akhil;

/**
 *
 * @author Ashok
 */
public class ForLoops {
    public static void main(String[] arg){
        
        int endVal = 11;
        int addition = 0;
        for (int i = 0; i < endVal; i++) {
            addition = addition+i;
            System.out.println("sum = "+addition);
            
        }
    }
    
}
