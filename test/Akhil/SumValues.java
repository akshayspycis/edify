package Akhil;

/**
 *
 * @author Ashok
 */
import java.util.*;
public class SumValues
{
public static void main(String[] args)
{
  sum();
}

static void sum(){

  int sum=0,val,n,i;
  Scanner sc=new Scanner(System.in);
  System.out.print("Enter number of data points:");
  n=sc.nextInt();
  for(i=0;i<n;i++){
     System.out.print("Values "+i+":");
     val=sc.nextInt();
     sum+=val;
    }
  System.out.println("Total:"+sum);
}

}