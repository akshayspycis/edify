package Akhil;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

public class JTableCellSelection {
public static void showDemo(JComponent demo, String title) {
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setTitle(title);
        JPanel contentPanel = new JPanel(new BorderLayout());

        contentPanel.add(demo);

        mainFrame.add(contentPanel);
        mainFrame.pack();
        mainFrame.setVisible(true);    
        
    }
public static void main(String[] args) {
        JTable table = new JTable(5, 5);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setCellSelectionEnabled(true);
        table.addRowSelectionInterval(2, 4); // Select 2 lines
        showDemo(new JScrollPane(table), "Select a block and some rows");
    }
}
